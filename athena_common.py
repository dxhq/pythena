import humanfriendly
import datetime
from discord.ext import commands
from persona import *
import os
import asyncio
import sys
scriptdir = os.path.dirname(os.path.abspath(__file__))+"/"
timers = {}

#guild:{channel:chanid, dj:userid, nowplaying:np}
vclients = {}
def initvc(ctx, nowplaying:str="None"):
	global vclients
	purgevc(ctx)
	try:
		vclients[ctx.guild]['channel'] = ctx.channel
		vclients[ctx.guild]['dj'] = ctx.author
		vclients[ctx.guild]['nowplaying'] = nowplaying
		vclients[ctx.guild]['starttime'] = datetime.datetime.now()
		vclients[ctx.guild]['length'] = ""
		vclients[ctx.guild]['djmode'] = False
		vclients[ctx.guild]['playlist'] = []
		vclients[ctx.guild]['playlisting'] = False
		return vclients[ctx.guild]
	except:
		vclients[ctx.guild] = {}
		vclients[ctx.guild]['channel'] = ctx.channel
		vclients[ctx.guild]['dj'] = ctx.author
		vclients[ctx.guild]['nowplaying'] = nowplaying
		vclients[ctx.guild]['starttime'] = datetime.datetime.now()
		vclients[ctx.guild]['length'] = ""	
		vclients[ctx.guild]['djmode'] = False
		vclients[ctx.guild]['playlist'] = []
		vclients[ctx.guild]['playlisting'] = False	
		return vclients[ctx.guild]
		
def purgevc(ctx):
	global vclients
	try:
		del vclients[ctx.guild]
		return True
	except:
		return False

def purgevcbyguild(guild):
	global vclients
	try:
		del vclients[guild]
		return True
	except:
		return False

def vcexist(ctx):
	global vclients
	try:
		_tmp = vclients[ctx.guild]
		return True
	except:
		return False
		
def vcgexist(guild):
	global vclients
	try:
		_tmp = vclients[guild]
		return True
	except:
		return False	
			
def vcplaying(ctx, nowplaying:str, length:str=""):
	global vclients
	vclients[ctx.guild]['nowplaying'] = nowplaying
	vclients[ctx.guild]['starttime'] = datetime.datetime.now()
	vclients[ctx.guild]['length'] = length
	return vclients[ctx.guild]['nowplaying']

def vcaddplaylist(ctx, track):
	global vclients
	vclients[ctx.guild]['playlist'].append(track)
	return vclients[ctx.guild]['playlist'].index(track)

def vcrunplaylist(ctx):
	global vclients
	toptrack = vclients[ctx.guild]['playlist'][0]
	vclients[ctx.guild]['playlist'].remove(toptrack)
	return toptrack

def vctoggleplaylisting(ctx):
	global vclients
	playlisting = vclients[ctx.guild]['playlisting']
	if playlisting:
		vclients[ctx.guild]['playlisting'] = False
		return False
	else:
		vclients[ctx.guild]['playlisting'] = True
		return True
			
def vctoggledjmode(ctx):
	global vclients
	djmode = vclients[ctx.guild]['djmode']
	if djmode:
		vclients[ctx.guild]['djmode'] = False
		return False
	else:
		vclients[ctx.guild]['djmode'] = True
		return True
		
def vcget(ctx):
	global vclients
	return vclients[ctx.guild]

def vcgetbyguild(guild):
	global vclients
	return vclients[guild]

def vcsetdj(ctx, member):
	global vclients
	vclients[ctx.guild]['dj'] = member
	return vclients[ctx.guild]['dj']
							
def vcnewdj(ctx):
	global vclients
	mem = random.choice(ctx.voice_client.channel.members)
	vclients[ctx.guild]['dj'] = mem
	return vclients[ctx.guild]['dj']

def vcgnewdj(guild):
	global vclients
	realmems = []
	for mem in guild.voice_client.channel.members:
		print(mem.name)
		if not mem.bot:
			print("NOT BOT")
			realmems.append(mem)
	
	print(realmems)	
	if len(realmems) == 0:
		return None
		
	newdj = random.choice(realmems)
	vclients[guild]['dj'] = mem
	return vclients[guild]['dj']
		
def vcdj(ctx):
	global vclients
	try:
		dj = vclients[ctx.guild]['dj']
		if dj in ctx.voice_client.channel.members:
			return dj
		return None
	except:
		return None
	
#Config
def getConfig():
	with open("config.json") as f:
		return json.load(f)

def setConfig(parent:str, key:str, value, logging=False):
	if logging:
		AddLog(f"{parent} : {key} : {value}")
	data = getConfig()
	try:
		data[parent][key] = value
	except:
		data[parent] = {}
		data[parent][key] = value
	with open('config.json', "w") as s:
		json.dump(data, s, indent=4, sort_keys=True)

def dumpConfig(data):
	with open('config.json', "w") as s:
		json.dump(data, s, indent=4, sort_keys=True)
				
#Exceptions
class Bork(Exception):
	pass

class GPF(Exception):
	pass

class athenaerrs(commands.errors.CommandError):
	pass

class BadAccess(athenaerrs):
	"""Exception raised when the command being invoked is tier restricted, but user is not allowed access.
	"""
	def __init__(self):
		self.msg = f"This command required administrator permissions."
		super().__init__(self.msg)
			
class TierAccessError(athenaerrs):
	"""Exception raised when the command being invoked is tier restricted, but user is not allowed access.
	"""
	def __init__(self, user_tier, req_tier):
		self.user_tier = user_tier
		self.req_tier = req_tier
		if self.req_tier == 3:
			self.msg = f"This command is restricted to owner-whitelisted users. (AKA Tier 3 access)"
		else:
			self.msg = f"You do not have the required tier access for this command. You are {user_tier}, required is {req_tier}."
		super().__init__(self.msg)

class NoGuild(athenaerrs):
	"""Exception raised when the command being invoked is only allowed in guilds. but called from DM.
	"""
	def __init__(self):
		self.msg = f"You must be in a guild to use this command."
		super().__init__(self.msg)
		
class WrongGuild(athenaerrs):
	"""Exception raised when the command being invoked is guild restricted, but user is not in the right guild.
	"""
	def __init__(self, guild_id):
		self.guild_id = guild_id
		self.msg = f"This command is not enabled for this guild. (REF: {self.guild_id})"
		super().__init__(self.msg)	

class nsfw(athenaerrs):
	"""Exception raised when the command being invoked is nsfw restricted, but user is not in the right channel.
	"""
	def __init__(self):
		self.msg = "You must be in a NSFW channel to view this content or not have `strict NSFW rules` for this guild."
		super().__init__(self.msg)	

class NeedRole(athenaerrs):
	"""Exception raised when the command needs at least one role.
	"""
	def __init__(self):
		self.msg = "This command is only available to people who have any role in this guild."
		super().__init__(self.msg)	
						
#Classes
class ProgressBar():
	def __init__(self, width=10):
		self.pointer = 0
		self.width = width

	def __call__(self,x):
		# x in percent
		self.pointer = int(self.width*(x/100.0))
		self.s = "" #✅ ❎
		if x < 100:
			self.s = "<a:loading:480693540616273922>"
		else:
			self.s = "<:loaded:480337167080488960>"
		return "|" + "◻"*self.pointer + "◼"*(self.width-self.pointer)+\
			   "|\n "+str(x)+"% "+self.s
 
class ProgressDisp():
	def __init__(self, width=10):
		self.pointer = 0
		self.width = width

	def __call__(self,x):
		 # x in percent
		 self.pointer = int(self.width*(x/100.0))
		 return "|" + "◻"*self.pointer + "◼"*(self.width-self.pointer)+\
				"|"

#Command check decorators
def access_check():
	def predicate(ctx):
		if ctx.guild:
			if ctx.message.author.guild_permissions.administrator:
				return True
			elif ctx.message.author.id == 206903283090980864:
				return True
		else:
			if ctx.message.author.id == 206903283090980864:
				return True
				
		raise BadAccess()
	return commands.check(predicate)

def is_in_guild(guild_id:int):
	def predicate(ctx):
		if not ctx.guild:
			raise NoGuild()
		
		if ctx.guild.id != guild_id:
			raise WrongGuild(guild_id)
			
		return True
	return commands.check(predicate)

def is_in_guild_or_dm(guild_id:int):
	def predicate(ctx):
		if isinstance(ctx.message.channel, discord.channel.DMChannel):
			return True
			
		return ctx.guild and ctx.guild.id == guild_id
	return commands.check(predicate)
	
def has_role_ids(role_ids):
	def predicate(ctx):
		if ctx.guild:
			for role in ctx.message.author.roles:
				if role.id in role_ids:
					return True
		return False
	return commands.check(predicate)

def has_any_role():
	def predicate(ctx):
		if ctx.guild:
			if len(ctx.author.roles) == 1:
				raise NeedRole()
			return True
		else:
			raise NoGuild()
	return commands.check(predicate)
	
	
def client_in_voice():
	def predicate(ctx):
		if ctx.guild:
			if ctx.voice_client:
				return True
		return False
	return commands.check(predicate)

def user_in_voice():
	def predicate(ctx):
		if ctx.guild:
			if ctx.author.voice:
				return True
		return False
	return commands.check(predicate)

def match_vc():
	def predicate(ctx):
		if ctx.guild:
			if ctx.author.voice and ctx.voice_client:
				if ctx.author.voice.channel.id == ctx.voice_client.channel.id:
					return True
		return False
	return commands.check(predicate)

def is_tier(tier:int):
	def predicate(ctx):
		if ctx.guild:
			users_tier = get_tier(ctx.author)
			if tiered_guild(ctx.guild):
				if users_tier >= tier:
					return True
				else:
					raise TierAccessError(users_tier, tier)
			else:
				if tier == 3 and users_tier != 3:
					raise TierAccessError(users_tier, tier)
				else:
					return True
				
	return commands.check(predicate)

def is_nsfw():
	def predicate(ctx):
		if not ctx.channel.nsfw and strict_nsfw(ctx):
			raise nsfw()
		return True	
				
	return commands.check(predicate)
		
#Discord utilities
def tiered_guild(guild):
	data = getConfig()
	try:
		if len(data[str(guild.id)]['user_1']) > 0 or len(data[str(guild.id)]['user_2']) > 0 or len(data[str(guild.id)]['role_1']) > 0 or len(data[str(guild.id)]['role_2']) > 0:
			return True
		else:
			return False
	except KeyError:
		return False
		
def get_tier(user):
	data = getConfig()
	AddLog(f"Running {user.id} user id and {user.top_role.id} role id")
	AddLog(f"Checking in {user.guild.id}")
	try:
		AddLog(f"Checking whitelist: {data['core']['whitelist']}")
		if user.id in data['core']['whitelist']:
			return 3
		
		AddLog(f"Checking tier 2: {data[str(user.guild.id)]['user_2']}")
		AddLog(f"Checking tier 2: {data[str(user.guild.id)]['role_2']}")
		if user.id in data[str(user.guild.id)]['user_2'] or user.top_role.id in data[str(user.guild.id)]['role_2']:
			return 2

		AddLog(f"Checking tier 2: {data[str(user.guild.id)]['user_1']}")
		AddLog(f"Checking tier 2: {data[str(user.guild.id)]['role_1']}")			
		if user.id in data[str(user.guild.id)]['user_1'] or user.top_role.id in data[str(user.guild.id)]['role_1']:
			return 1
		
		AddLog("Returning null tier")
		return 0
			
	except Exception as e:
		AddLog(f"Error {e}")
		return 0
		
def get_role(ctx, role_id:int):
	for role in ctx.guild.roles:
		if role.id == role_id:
			return role

def get_member(client, member_id:int):
	for member in client.get_all_members():
		if member.id == member_id:
			return member
			
def get_member_by_name(client, member_name:str):
	for member in client.get_all_members():
		if member.name.lower() == member_name.lower() or member.display_name.lower() == member_name.lower():
			return member
	
def get_member_in_guild(guild, member_id:int):
	for member in guild.members:
		if member.id == member_id:
			return member
			
def get_member_in_guild_by_name(guild, member_name:str):
	for member in guild.members:
		if member.name.lower() == member_name.lower() or member.display_name.lower() == member_name.lower():
			return member

async def die(ctx, client):
	AddLog("==========================", colour="red")	
	AddLog(f"ALERT: Shutdown triggered by {ctx.author.name} ({ctx.author.display_name} : {ctx.author.id}) in {ctx.guild.name}.{ctx.channel.name}", colour="red")			
	AddLog("==========================", colour="red")	
	prog = ProgressBar(width=10)
	s = 0
	e = 4
	msg = await ctx.send(f"{prog(Percent(s, e, 'int'))}\n* Starting shutdown....")
	await asyncio.sleep(5)
	s += 1
	await msg.edit(content=f"{prog(Percent(s, e, 'int'))}\n* Turning off extensions...")
	astr = ""
	i = 0
	m = len(client.cogs.keys()) - 1
	for item in list(client.cogs.keys()):
		client.unload_extension(f"ext.{item.lower()}")
		i += 1
	await ctx.send(f"{i}/{m} extensions unloaded.")
	await asyncio.sleep(3)
	s += 1
	await msg.edit(content=f"{prog(Percent(s, e, 'int'))}\n* Ending event loops.")
	await asyncio.sleep(2)
	s += 1
	await msg.edit(content=f"{prog(Percent(s, e, 'int'))}\n* Turning off the lights.")
	await asyncio.sleep(3)
	s += 1
	await msg.edit(content=f"{prog(Percent(s, e, 'int'))}\n* Turning off the internet-- OH SH\n\n**BOT TERMINATED BY {ctx.author.mention}**")
	tasks = asyncio.gather(*asyncio.Task.all_tasks(), loop=client.loop)
	tasks.cancel()
	await client.logout()
	quit()

													
def advArg(find:str, arg:str, extend:int=1):
	a = arg.split(" ")
	s = ""
	i = 0
	if find not in a:
		return ""

	print(a)
	while i < extend:
		for item in a:
		#	print("("+str(i)+") Testing if "+find+" is "+item)
			if item == find:
				i += 1
				#print("Found "+a[a.index(item) + i])
				s += f"{a[a.index(item) + i]} "
				
				
	#print("Returning "+s)
	return s[:-1]

def strict_nsfw(ctx):
	try:
		are_we_strict = bool(getConfig()[str(ctx.guild.id)]['strict_nsfw'])
	except KeyError:
		AddLog("Creating config variable for strict_nsfw.")
		setConfig(parent=str(ctx.guild.id), key="strict_nsfw", value=False)
		are_we_strict = False
	
	return are_we_strict

def set_strict_nsfw(ctx, strict_value):
	setConfig(parent=str(ctx.guild.id), key="strict_nsfw", value=strict_value)
	
#Utilities
def correct_time(hour):
	hour -= 2
	if hour == -1:
		hour = 23
	if hour == -2:
		hour = 22
	return hour
	
def ifill(i:int):
	if i < 10:
		return "0"+str(i)
	else:
		return str(i)

def format_clock(seconds:int):
	minutes = 0
	while seconds >= 60:
		seconds -= 60
		minutes += 1
	minutes = ifill(minutes)
	seconds = ifill(seconds)
	return f"{minutes}:{seconds}"
		
def SpeechTTS(txt, *, voice:str="2"):
	txt = txt.replace('"', "")
	txt = txt.replace("'", "")

	if "*0" in txt:
		voice = "0"
			
	if "*1" in txt:
		voice = "1"

	if "*2" in txt:
		voice = "2"
	
	if "*3" in txt:
		voice = "3"
	
	if "*4" in txt:
		voice = "4"

	if "*5" in txt:
		voice = "5"
	
	if "*6" in txt:
		voice = "6"

	if "*7" in txt:
		voice = "7"
	
	if "*8" in txt:
		voice = "8"
	
	if "*9" in txt:
		voice = "9"

	if "*10" in txt:
		voice = "10"	

	if "*11" in txt:
		voice = "11"

	if "*12" in txt:
		voice = "12"	
	
	if voice == "0":
		voice = ""							
	elif voice == "1":
		voice = "file://cmu_indic_sxs_hi.flitevox"
	elif voice == "2":
		voice = "file://cmu_us_clb.flitevox"
	elif voice == "3":
		voice = "file://cmu_us_ljm.flitevox"
	elif voice == "4":
		voice = "file://cmu_indic_slp_mr.flitevox"
	elif voice == "5":
		voice = "file://cmu_us_aew.flitevox"
	elif voice == "6":
		voice = "file://cmu_us_awb.flitevox"
	elif voice == "7":
		voice = "file://cmu_us_eey.flitevox"
	elif voice == "8":
		voice = "file://cmu_us_fem.flitevox"
	elif voice == "9":
		voice = "file://cmu_us_jmk.flitevox"
	elif voice == "10":
		voice = "file://cmu_us_ksp.flitevox"
	elif voice == "11":
		voice = "file://cmu_us_rms.flitevox"
	elif voice == "12":
		voice = "slt"
	else:
		raise Bork("Invalid voice choice.")

	txt = txt.replace("*0", "")
	txt = txt.replace("*1", "")
	txt = txt.replace("*2", "")	
	txt = txt.replace("*3", "")
	txt = txt.replace("*4", "")
	txt = txt.replace("*5", "")
	txt = txt.replace("*7", "")
	txt = txt.replace("*8", "")	
	txt = txt.replace("*9", "")
	txt = txt.replace("*10", "")
	txt = txt.replace("*6", "")
	txt = txt.replace("*11", "")
	txt = txt.replace("*12", "")	
	synth = ""
	if "/kaiser/" in scriptdir:
		synth = "/home/kaiser/flite-2.0.0-release/bin/flite"
	else:
		synth = "flite"
		
	AddLog(f"Using voice {voice} and synth path: {synth}")
	
	if voice == "":
		os.system(f'{synth} -o athtts.wav -t "{txt}"')
	else:
		os.system(f'{synth} -voice {voice} -o athtts.wav -t "{txt}"')
	#subprocess.run(["flite", "-voice slt", '-o '+scriptdir+'athtts.wav', '-t "'+txt+'""'])
	#subprocess.run(["espeak", "-ven-n+f4", "-s160", "-w "+scriptdir+"athtts.wav", txt])
	
def trim(msg:str, limit=50, cut:str="..."):
	if len(msg) > limit:
		msg = msg[0:limit-len(cut)]+cut
	
	return msg
				
def Percent(part, whole, returntype="string"):
	if returntype == "string": # "50"
		return str(100 * float(part)/float(whole)).split(".")[0]
		
	if returntype == "string_raw": # "50.123"
		return str(100 * float(part)/float(whole))
		
	if returntype == "int": # 50
		return int(str(100 * float(part)/float(whole)).split(".")[0])
		
	if returntype == "float": # 50.123
		return 100 * float(part)/float(whole)
	
def boolinate(string:str):
	truth = ['true', '1', 'yes', 'on']
	if string.lower() in truth:
		return True
	else:
		return False
		
def HumanizeList(ls):
	if len(ls) > 0:
		return humanfriendly.text.concatenate(ls)
	else:
		return "None."
	
def SetTimer(name, returnmicro=False, forcestart=False, forceend=False, log=True):
	finish = name in timers
	
	if forcestart is True:
		if log:
			AddLog("Opened timer: "+name)
		starttime = datetime.datetime.now()
		timers[name] = starttime
		return 0
		
	if forceend is True:
		if log:
			AddLog("Closed timer: "+name)
		f = datetime.datetime.now() - timers[name]
		ret = f.seconds
		del timers[name]
		if returnmicro is True:
			return f.microseconds
		return humanfriendly.format_timespan(ret, True)
		
	if finish is False:
		if log:
			AddLog("Opened timer: "+name)
		starttime = datetime.datetime.now()
		timers[name] = starttime
		return 0
	else:
		if log:
			AddLog("Closed timer: "+name)
		f = datetime.datetime.now() - timers[name]
		ret = f.seconds
		del timers[name]
		if returnmicro is True:
			return f.microseconds
		return humanfriendly.format_timespan(ret, True)
	
def GetTimer(name, humanize=False):
	exists = name in timers
	
	if exists is False:
		return None
	else:
		f = datetime.datetime.now() - timers[name]
		if humanize is False:
			return f
		else:
			return humanfriendly.format_timespan(f.seconds, True)

def AddLog(logmsg:str, *, error=False, colour=""):
	c = humanfriendly.terminal.ansi_style(color=colour)
	e = "\x1b[0m"#humanfriendly.terminal.ansi_style(color="white")
	b = humanfriendly.terminal.ansi_style(color="white")
	r = humanfriendly.terminal.ansi_style(color="white", faint=True)
	
	try:
		with open(scriptdir+"sys.log", "r+") as f:
			now = datetime.datetime.now()
			mt = f.read()
			#f.writelines(str(now.day)+"/"+str(now.month)+"/"+str(now.year)+": "+logmsg+"\n")
			f.writelines(now.strftime("%Y-%m-%d %H:%M:%S")+": "+logmsg+"\n")
			f.close()
			if error == False:
				if ":" in logmsg:
					logmsg = logmsg.replace(":", ":"+b)
				if "#" in logmsg:
					logmsg = logmsg.replace("#", r+"#"+e+c)					
				if "(" in logmsg:
					logmsg = logmsg.replace("(", r+"(")
					logmsg = logmsg.replace(")", ")"+e+c)
				print(c+now.strftime("%Y-%m-%d %H:%M:%S")+": "+logmsg+e)
			else:
				humanfriendly.terminal.warning("[ERROR] "+now.strftime("%Y-%m-%d %H:%M:%S")+": "+logmsg)
	except FileNotFoundError:
		open(scriptdir+'sys.log', 'a')
		humanfriendly.terminal.warning("FAILED TO LOG - Log file not found. Creating sys.log.")
		
def AddChatLog(guild, logmsg):
	w = "\x1b[0m"
	b = humanfriendly.terminal.ansi_style(color="blue")

	try:
		with open(scriptdir+str(guild.id)+'_chat.log', "r+") as f:
			now = datetime.datetime.now()
			mt = f.read()
			#f.writelines(str(now.day)+"/"+str(now.month)+"/"+str(now.year)+": "+logmsg+"\n")
			f.writelines(now.strftime("%Y-%m-%d %H:%M:%S")+": "+logmsg+"\n")
			f.close()
			r = humanfriendly.terminal.ansi_style(color="white", faint=True)
			c = w+b
			if "#" in logmsg:
					logmsg = logmsg.replace("#", r+"#"+c)
					
			if "(" in logmsg:
					logmsg = logmsg.replace("(", r+"(")
					logmsg = logmsg.replace(")", ")"+c)
			humanfriendly.terminal.message(b+"[CHAT] "+now.strftime("%Y-%m-%d %H:%M:%S")+": "+logmsg+w)
			#humanfriendly.terminal.ansi_style()
			#print("[CHAT] "+now.strftime("%Y-%m-%d %H:%M:%S")+": "+logmsg)
	except:
		open(scriptdir+str(guild.id)+'_chat.log', 'w').close()
			
async def exception(client, message=None):
	AddLog(str(sys.exc_info()[0]))
	#if type(sys.exc_info()[0]) is websockets.exceptions.ConnectionClosed:
	if "websockets.exceptions.ConnectionClosed" in repr(sys.exc_info()[0]):
		await AthNotif(client, "Reconnected to API network.")
		AddLog("Caught exception: WebProxy restart.", error=True)
		return
	
	RaisePersona('anger', 3)
	LowerPersona('mood', 4)
	if message.channel.name != "botmasters-testzone":
		source = "Unknown"
		if message:
			source = f"Guild = {message.guild.name}\nChannel = {message.channel.name}\nAuthor = {message.author.name} ({message.author.display_name})\n"
			if message.content != "":
				source += f"```{message.content}```"
		await AthNotif(client, f"### Unhandled error log ###\nSource:\n{source} \n```"+repr(sys.exc_info()[0])+"\n"+ str(sys.exc_info()[1])+"```")
	
	if message is not None:	
		times = datetime.datetime.utcnow() - message.created_at
		AddLog(f"Checking message age: {times.seconds}")
		if times.seconds < 120:
			em = discord.Embed(colour=aecol)
			#em.set_image(url="http://deusex.ucoz.net/files/Bork_Alert.png")
			em.add_field(name="Exception", value='<:borksmall:439476312000692224> | **Exception Unhandled**\nSorry about that, something broke that I didn\'t account for.\nHeres some stuff that might help, or might not, idk. /shrug\n'+discord.utils.get(message.guild.members, name='Kaiser').mention+' ('+str(discord.utils.get(message.guild.members, name='Kaiser').status)+') has been notified.\n```'+repr(sys.exc_info()[0])+'\n'+str(sys.exc_info()[1])+'```')
			await message.channel.send(embed=em)
			AddLog("Error message printed in "+message.channel.name, error=True)
			AddLog("EXCEPTION UNHANDLED: "+str(sys.exc_info()[0]), error=True)
			AddLog("EXCEPTION UNHANDLED: "+str(sys.exc_info()[1]), error=True)
			#print("TEST: "+str(traceback.extract_stack().format())+" :END_TEST")
			#print("TEST: "+traceback.extract_stack(sys.exc_info()[2]).text)
			aNotify("Athena", "There was an error in the code...\n"+repr(sys.exc_info()[0])+"\n"+ str(sys.exc_info()[1]))

async def AthNotif(client, message, *, embed=None, channel=421285829818974209):
	c = client.get_channel(channel)
	AddLog("Sending notification to "+c.name)
	AddLog(message, error=True)
	await c.send(message, embed=embed)
