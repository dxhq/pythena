import requests
import json

__version__ = "0.0324"
class uServer:
	def __init__(self, hostname, numplayers, maxplayers, mapname, maptitle, gametype, ip, hostport, country, description, diff, uid, added):
		self.hostname = hostname
		self.numplayers = int(numplayers)
		self.maxplayers = int(maxplayers)
		self.mapname = mapname
		self.maptitle = maptitle
		self.gametype = gametype
		self.ip = ip
		self.hostport = hostport
		self.country = country
		self.description = description
		self.diff = diff
		self.id = uid
		self.added = added
		
	def __str__(self):
		return self.hostname

class uServerF:
	def __init__(self, ip:str, port:str):
		self.data = json.loads(requests.get(f"http://333networks.com/json/333networks/{ip}:{port}").text)
		print(self.data)
		self.hostname = data['hostname']
		self.numplayers = int(data['numplayers'])
		self.maxplayers = int(data['maxplayers'])
		self.mapname = data['mapname']
		self.maptitle = data['maptitle']
		self.gametype = data['gametype']
		self.ip = ip
		self.hostport = port
		self.country = data['country']
		self.adminname = data['adminname']
		self.adminemail = data['adminemail']
		self.mutators = data['mutators']
		self.added = data['added']
		
	def __str__(self):
		return self.hostname
			
class uMS:
	def __init__(self, game, limit=10, sort="numplayers", order="d"):
		self.servers = []
		self.game = game
		self.unr = f"http://333networks.com/json/{game}?s={sort}&o={order}&r={limit}"
		print(f"uMS created: game={game}, lim={limit}, sort={sort}, order={order}")
		self.refresh()
		
	def refresh(self):
		self.servers.clear()
		print(f"Sending API request for: {self.game}")
		self.data = json.loads(requests.get(self.unr).text)
		#print(self.data)
		self.srv = 0
		self.players = 0
		while self.srv < len(self.data[0]):
			self.servers.append(uServer(
			self._removeNonAscii(self.data[0][self.srv]['hostname']), 
			self.data[0][self.srv]['numplayers'],
			self.data[0][self.srv]['maxplayers'],
			self.data[0][self.srv]['mapname'],
			self.data[0][self.srv]['maptitle'],
			self.data[0][self.srv]['gametype'],
			self.data[0][self.srv]['ip'],
			self.data[0][self.srv]['hostport'],
			self.data[0][self.srv]['country'],
			self.data[0][self.srv]['description'],
			self.data[0][self.srv]['diff'],
			self.data[0][self.srv]['id'],
			self.data[0][self.srv]['added']))
			self.srv += 1	
					
	def _removeNonAscii(self, s): 
		s = s.replace("", "")
		s = s.replace("", "")
		return "".join(i for i in s if ord(i)<128)
