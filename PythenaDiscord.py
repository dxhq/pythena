#!/usr/bin/env python
import discord
from discord.ext import commands
from discord.ext.commands.cooldowns import BucketType
import asyncio
import sys
import subprocess
import os
import time
import random
import socket
import zipfile
import datetime
import configparser
#import notify2 ~~ REMOVED
import logging
import traceback
import humanfriendly
import secrets
import ast
import num2words
import fnmatch
import json
import requests
import faulthandler
import websockets
from pytz import timezone
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
from word2number import w2n
from dice_notation.parser import DiceParser
from math import *
from bs4 import BeautifulSoup
import importlib

#Custom Athena modules
import minerva
from persona import *
import tttn
from athena_common import *

initial_extensions = ['ext.admin', 'ext.autohelp', 'ext.fun', 'ext.trivia', 'ext.gaming', 'ext.info', 'ext.roles', 'ext.utility', 'ext.api', 'ext.voice']

#<:loadfail:480337167038676992>
#<:loaded:480337167080488960> 
#<a:loading:480693540616273922> 
#<:loadother:480422495585959939>

client = commands.Bot(command_prefix=commands.when_mentioned_or('ath!', 'a!', '!'), 
						description=f"Placeholder.", 
						owner_id=206903283090980864)
	
#Class instants
#client = discord.Client()
ChatRoom = None
lastchannel = None
lastmessage = None
selfmem = None

jobs = {}
uptimemins = 0
uptimehours = 0
tasks_wait = 0

stats = {}

changes = '''
!weather, !walerts, !forecast
!sr
'''

ignores = {}

aecol = 0x92005D #0xDEADBF #92005D

#Variables
liststr = ""
botid = ""
botuserid = ""
tostr = ""

aiConvID = 0
aiConvID_owner = 0

bAlerting = False #Are we running the Alert task, should simplify this later.
bFoundPlayers = False #Have we found players? Related to MSQ command.
bFoundPlayersQ = False #As above.
bOnline = False #Flag set once the on_ready function is finished, to track when the bot is "active"
devmode = False #Flag for developer mode, disables notifications and commands, to stop spam while we're in the "constant restart cycle" of development.

#CFG vars
update_interval = 30
	
def aNotify(title, text):
	AddLog(f"NOTIFICATION: [{title}] {text}")
	return
	global devmode
	if devmode is False:
		n = notify2.Notification(title,text,"notification-message-im")
		n.show()

def ReadJobs():
	global scriptdir, jobs
	data = {}
	with open('jobs.json') as f:
		data = json.load(f)
	jobs = data

def WriteJobs():
	global scriptdir, jobs
	with open('jobs.json', "w") as s:
		json.dump(jobs, s, indent=4, sort_keys=True)	
			
def FileCheck():
	if os.path.isfile(scriptdir+'sys.log'):
		AddLog("System log file exists.", colour="green")
	else:
		open(scriptdir+'sys.log', 'a')
		AddLog("Creating log file.")
			
def ReadIgnores():
	global ignores
	data = {}
	with open('ignores.json') as f:
		data = json.load(f)
	ignores = data
	
	return
	ignores.clear()
	roletxt = open(scriptdir+'ignores.txt', 'r') 
	templist = roletxt.readlines()
	roletxt.close()
	for i in range(len(templist)):
		ignores.append(templist[i].strip('\n'))

def WriteIgnores():
	global ignores
	with open('ignores.json', "w") as s:
		json.dump(ignores, s, indent=4, sort_keys=True)	
	
	return
	open(scriptdir+'ignores.txt', 'w').close()
	roletxt = open(scriptdir+'ignores.txt', 'w') 
	for item in ignores:
		roletxt.write("%s\n" % item)
	roletxt.close()

def AddJob(job_name, job_type, job_target, job_datetime, job_created_at, job_creator, job_created_in):
	global jobs
	ReadJobs()
	jobs[job_name] = {"job_type":job_type, "job_target":job_target, "job_time":job_datetime, "created":job_created_at, "creator":job_creator, "created_in":job_created_in}
	WriteJobs()

def CheckTime(*, job_name="", check_time="", format_return=False):
	global jobs
	
	if job_name != "":
		ReadJobs()
		_time = jobs[job_name]['job_time']
	else:
		if check_time != "":
			_time = check_time
		else:
			raise Bork("CheckTime requires a check_time argument if job_name is omitted.")
			return
			
	dt = _time.split(" ")
	j_month = int(dt[0].split("/")[1])
	j_day = int(dt[0].split("/")[0])
	j_year = int(dt[0].split("/")[2])         
	j_min = int(dt[1].split(":")[1])
	j_hour = int(dt[1].split(":")[0])
	j_sec = 0
	try:
		j_sec = int(dt[1].split(":")[2])
	except IndexError:
		pass
	
	then = datetime.datetime(j_year, j_month, j_day, j_hour, j_min, j_sec)
	now = datetime.datetime.now()
	nowf = datetime.datetime(now.year, now.month, now.day, now.hour, now.minute, now.second)
	s = (then - nowf).total_seconds()
#	print(f"{j_day} / {j_month} / {j_year} - {j_hour}:{j_min}:{j_sec}\n{s}")
#	if s < 0:
	#	print("PASSED")
	if format_return:
		return humanfriendly.format_timespan(int(s)) 
	else:
		return s   
	       	
#======================
#Events
#======================
@client.event
async def on_ready():
	global scriptdir, bOnline, selfmem
	AddLog('---------------------------', colour="green")
	if boolinate(getConfig()['core']['devmode']):
		devmode = True
		AddLog('===== STARTING IN DEVELOPER MODE =====', colour="yellow")
	
	#faulthandler.enable()
	AddLog(f'Logged in as {client.user.name} ({client.user.id})', colour="green")

	#if True is False: ##FEATURE DISABLED if devmode is False:
	#notify2.init('Athena')
	#AddLog("Notification daemon started...", colour="green")
	
	ReadIgnores()
	logging.basicConfig(level=logging.INFO)
	logger = logging.getLogger('discord')
	logger.setLevel(logging.INFO)
	handler = logging.FileHandler(filename=scriptdir+'discord.log', encoding='utf-8', mode='w')
	handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
	logger.addHandler(handler)
	
	#aNotify("Athena", "The Athena client is ready.")
	bOnline=True
	AddLog(f"Online state: {bOnline}", colour="green")
	ReadJobs()
	if GetTimer('init') is not None:
		s = GetTimer('init').microseconds
		sec = SetTimer('init', forceend=True, log=False)
		AddLog(f"Startup completed in {sec}. ({s}mcs)", colour="green")
	await start_status()

async def start_status():
	if not boolinate(getConfig()['core']['devmode']):
		AddLog("Running task: status", colour="green")
		await client.loop.create_task(status_task())
	else:
		await client.change_presence(activity=discord.Game(name="DEV MODE"))

async def status_task():
	await asyncio.sleep(10)
	cycles = 0
	while True:
		if boolinate(getConfig()['core']['devmode']):
			break
		
		if cycles == 0:	
			now = datetime.datetime.now()
			hour = correct_time(int(now.strftime("%H")))
			game = discord.Game(" with time: "+str(hour)+":"+now.strftime("%M")+" GMT")
			#print("Debug: Updating time: "+str(hour)+":"+now.strftime("%M")+" GMT")
			await client.change_presence(activity=game)
		
		if cycles == 1:
			await client.change_presence(activity=discord.Game(f" with {len(client.voice_clients)} voices."))
		
		if cycles == 2:
			await client.change_presence(activity=discord.Game(f" with {len(client.voice_clients)} voices."))
			
		await asyncio.sleep(75) 
		#cycles += 1
		
async def old_status_task_with_fuckin_taskshit():
	global uptimemins, uptimehours, tasks_wait, jobs, update_interval
	await asyncio.sleep(10)
	while True:
		
		now = datetime.datetime.now()
		hour = int(now.strftime("%H")) - 2
		#game = discord.Game(" with time: "+str(hour)+":"+now.strftime("%M")+" GMT")
		#print("Debug: Updating time: "+str(hour)+":"+now.strftime("%M")+" GMT")
		#await client.change_presence(activity=game)
		await asyncio.sleep(75) # task runs every 60 seconds

		tasks_wait += 1
		if tasks_wait >= update_interval:
			try:
				tasks_wait = 0
				#print("Checking tasks...")
				if len(jobs.keys()) == 0:
					return
					
				for item in jobs.keys():
					print(f"Checking {item}...")
					s = CheckTime(job_name=item)
					if s < 0:
						print(f"{item} expired.")
						await AthNotif(client, f"A `{jobs[item]['job_type']}` task `{item}` has expired.\nTarget: {jobs[item]['job_target']}\nCreated: {jobs[item]['created']} by {jobs[item]['creator']}")
						if jobs[item]['job_type'] == "test":
							print("I guess test task succeeded.")
							del jobs[item]
							
						elif jobs[item]['job_type'] == "del":
							
							channel = client.get_channel(int(jobs[item]['job_target'].split(".")[0]))
							async for message in channel.history():
								
								if message.id == int(jobs[item]['job_target'].split(".")[1]):
									await AthNotif(client, f"The following message was scheduled for deletion, the author has been notified;\nAuthor: {message.author.name} ({message.author.display_name})\n```{message.content}```")
									await client.get_channel(jobs[item]['created_in']).send(f"The task for deleting {message.author.display_name}'s message has expired and processed.")
									await message.author.send(f"Greetings!\nYour message in {channel.name} has been deleted due to a timed task that was created at {jobs[item]['created']} by {jobs[item]['creator']}. Just letting you know!\n\nContent:\n```{message.content}```")
									
									await message.delete()
									print("Message deleted.")
							del jobs[item]
						WriteJobs()
						
					else:
						print(f"{CheckTime(job_name=item, format_return=True)} to go.")
			except Exception as e:
				AddLog(e, True)
				AddLog(str(sys.exc_info()[1]), True)
		
@client.event
async def on_message_delete(message):
	if message.author.bot is False:
		AddLog(f"[{message.author.name} ({message.author.display_name}) in #{message.channel.name}:{message.guild.name}] deleted message: {message.content}")

@client.event
async def on_guild_available(guild):
	global bOnline
	AddLog(guild.name+" active.", colour="green")
	
@client.event
async def on_guild_unavailable(guild):
	AddLog(guild.name+" unavailable.", colour="red")
			
@client.event
async def on_message_edit(before, after):
	if before.author.bot is False and before.content != after.content:
		AddLog(f"[{before.author.name} ({before.author.display_name}) in #{before.channel.name}:{before.guild.name}] edited their message...")
		AddLog(f'Previous: {before.content}', colour="yellow")
		AddLog(f'Changed: {after.content}', colour="green")
		
		if "||" not in after.content: #further dirty dirty hack
			await client.process_commands(after)

@client.event
async def on_member_update(before, after):
	#global defaultchannel
	if before.name != after.name:
		AddLog(f"[{before.guild.name} event] {before.name} changed username to [{after.name}]")
	if before.display_name != after.display_name:
		AddLog(f"{before.display_name} now known as [{after.display_name}] in [{before.guild.name}]")
		
"""@client.event
async def on_error(event, *args, **kwargs):
	AddLog("Error found: "+str(sys.exc_info()), True)
	#aNotify("Error", str(sys.exc_info()))
	await exception(lastmessage)
	
@client.event
async def on_command_completion(ctx):
	await ctx.message.add_reaction(Emote())
"""
@client.event
async def on_member_join(member):
	RaisePersona('mood', 10)
	LowerPersona('anger', 10)
	AddLog(f"{member.name} joined {member.guild.name}", colour="green")
	
@client.event
async def on_member_remove(member):
	RaisePersona('anger', 10)
	LowerPersona('mood', 10)
	AddLog(f"{member.name} ({member.display_name}) left {member.guild.name}")

@client.event	
async def on_resume():
	await AthNotif(client, "Client resumed.")
			
@client.event
async def on_message(message):
	global lastchannel
	global lastmessage
	global selfmem
	global ignores
	
	
	#Creating variables
	settings = getConfig()
	lastchannel = message.channel
	lastmessage = message
	devmode = boolinate(settings['core']['devmode'])
	try:
		airoom = int(settings[str(message.guild.id)]['airoom'])
	except KeyError:
		AddLog("Creating config variable for ai room.")
		setConfig(parent=str(message.guild.id), key="airoom", value=0)
		airoom = 0
	except AttributeError:
		pass
		
	#Private Messaging
	if isinstance(message.channel, discord.channel.DMChannel):
		if message.author != client.user:
			AddLog(f"PM from {message.author.name}: {message.content}")
			
			if str(message.author.id) in ignores['members'] or message.author.name.lower() in ignores['members'] or message.author.display_name.lower() in ignores['members']:
				return
		
			if message.content.startswith('!') or message.content.startswith('a!') or message.content.startswith('ath!'):
				pass
			else:
				await queryAI(message, 0)	
					
	if isinstance(message.channel, discord.channel.TextChannel):
		#Sanity checks
		if message.author == client.user or message.author.bot is True:
			return
		
		#Logging
		logname = message.author.name
		if message.author.name != message.author.display_name:
			logname += f" ({message.author.display_name})"	
			
		if message.content != "":
			AddChatLog(message.guild, f"[{logname} in #{message.channel.name}:{message.guild.name}] {message.content}")	
 
		if len(message.attachments) > 0:
			for item in message.attachments:
				AddLog(f"[{logname} in #{message.channel.name}:{message.guild.name}] uploaded {item.filename} ({humanfriendly.format_size(item.size)}) : {item.url}")
		
		#Ignores		
		try:
			if str(message.channel.id) in ignores['channels']:
				return
		except KeyError:
			pass
			
		try:
			if str(message.author.id) in ignores['members']:
				return
		except KeyError:
			pass
			
		#Commands ignore check
		try:
			raw_cmd = message.content.split(" ")[0].lower()
			raw_cmd = raw_cmd.replace("!", "")
			raw_cmd = raw_cmd.replace("a!", "")
			raw_cmd = raw_cmd.replace("ath!", "")
			raw_cmd = raw_cmd.replace(message.guild.me.mention+" ", "")
			com = ignores[raw_cmd]
			if str(message.channel.id) in com or str(message.author.id) in com or str(message.guild.id) in com:
				return
		except KeyError:
			pass
		
		try:
			if str(message.guild.id) in ignores["*"]:
				raw_cmd = message.content.split(" ")[0].lower()
				raw_cmd = raw_cmd.replace("!", "")
				raw_cmd = raw_cmd.replace("a!", "")
				raw_cmd = raw_cmd.replace("ath!", "")
				raw_cmd = raw_cmd.replace(message.guild.me.mention+" ", "")
				if raw_cmd != "ignores":
					return
				 
			for item in ignores.keys():
				raw_cmd = message.content.split(" ")[0].lower()
				raw_cmd = raw_cmd.replace("!", "")
				raw_cmd = raw_cmd.replace("a!", "")
				raw_cmd = raw_cmd.replace("ath!", "")
				raw_cmd = raw_cmd.replace(message.guild.me.mention+" ", "")
				if raw_cmd == item:
					if str(message.guild.id) in ignores[item]:
						return
						
		except KeyError:
			pass
			
		#Disable commands in dev mode
		if devmode and message.author.id != client.owner_id:
			if message.content.startswith("!") or message.content.startswith("a!") or message.content.startswith(message.guild.me.mention):
				await message.channel.send("Commands are currently disabled due to development mode. Service will be resumed shortly.")
			return
			
		#Trigger for the AI.
		if message.content.lower().startswith(f'{message.guild.me.name.lower()}, ') and str(message.channel.id) not in ignores['ai'] and str(message.author.id) not in ignores['ai']:
			#co = client.get_command("send_to_ai")
			#await client.invoke(co, a_msg=message, a_type=1)
			#await queryAI(message, 1)
			ctx = await client.get_context(message)
			ai_cmd = client.get_command('send_to_ai')
			if not ai_cmd.enabled:
				await ctx.send("AI system currently not available.")
				return
				
			if ai_cmd.is_on_cooldown(ctx):
				await ctx.send("You're on cooldown!")
				return
				
			await ctx.invoke(ai_cmd)
	
				
			return
		
		if "how" in message.content.lower() and "are" in message.content.lower() and "you" in message.content.lower() and "athena" in message.content.lower() and message.content.endswith("?"):
			await message.channel.send(f"{message.author.mention} I'm feeling {EvalMood()} today. {Emote()}")
					
		if message.channel.name == airoom:
			await queryAI(message, 0)		
			return
		
		if message.content == "!help me":
			await ctx.send("You are beyond helping.")
			return
			
		if message.content == "!help" or message.content == message.guild.me.mention+" help":
			em = discord.Embed(description=f"**{client.description}**")
			for item in sorted(client.cogs.keys()):
				em.add_field(name=item.capitalize(), value=client.get_cog(item).__doc__)
			
			em.set_footer(text="Type !help <category> to show the command list for that section.\nType !changes to keep track of any updates to the bot.")	
			await message.channel.send(embed=em)	
			return
	
		if message.content.startswith(message.guild.me.mention+" "):
			if len(message.content.split(" ")) > 1:
				if not is_command(message.content.split(" ")[1]):
					await queryAI(message, 1)
					return	
			
	if "||" not in message.content: #further dirty dirty hack
		await client.process_commands(message)

def is_command(string):	
	for item in list(client.commands):
		if string == item.name:
			print(item.aliases)
			return True
			
		if string in item.aliases:
			return True
	
	return False
			
def owner_check():
    def predicate(ctx):
        return ctx.message.author.id == client.owner_id
    return commands.check(predicate)

def admin_check():
    def predicate_admin(ctx):
        return ctx.message.author.permissions_in(ctx.channel).administrator
    return commands.check(predicate_admin)

#===========================
# Commands
#===========================		
@client.command(hidden=True)
async def rt(ctx):
	ReadJobs()
	await ctx.send("Refreshing tasks.")

@client.command(hidden=True)
async def fix_status(ctx):
	await ctx.send("Fixing status task...")
	await status_task()

@access_check()	
@client.command(hidden=True)
async def devmode(ctx, onornot:bool):
	if onornot:
		await ctx.send("Enabling Dev Mode...")
		setConfig(parent="core", key="devmode", value="true")
		await client.change_presence(activity=discord.Game(name="DEV MODE"))
	else:
		await ctx.send("Disabling Dev Mode...")
		setConfig(parent="core", key="devmode", value="false")
		await status_task()

@client.command(hidden=True)
@access_check()
async def addtask(ctx, *, arg:str = ""): #name, _type, target, *, when):
	"""Adds a new timed task.
	
	Args:
	-w day/month/year hour:minute
	-n Name of the task
	-t Type of task (del or test are the only ones supported at the moment)
	-f The Target, format of "channelid messageid" (only required if using the del type for now)"""
	errs = ""
	name = advArg("-n", arg) #The name
	_type = advArg("-t", arg) #The type, `del`, `test`
	target = ""
	when = ""
	try:
		when = advArg("-w", arg, 2) #The datetime, day/mon/year followed by hour:minute and optional seconds
	except IndexError:
		errs += "❎ Invalid argument length from datetime arg.\n"
	try:
		target = advArg("-f", arg, 2) #The target, channel.id followed by message.id
	except IndexError:
		errs += "❎ Invalid argument length from target arg.\n"
	print(f"Finished args: n {name} t {_type} ta {target} w {when}")
	
	validtypes = ['del', 'test']
	if name == "":
		errs += "❎ A name is required. (-n name)\n"
	
	if _type == "":
		errs += "❎ A type is required. (-t type)\n"
	elif _type not in validtypes:
		errs += f"❎ Type is invalid. ({HumanizeList(validtypes)})\n"
	
	if when == "":
		errs += "❎ A datetime is required. (-w d/m/yyyy hh:mm)\n"
	else:
		try:
			date = when.split(" ")[0]
			time = when.split(" ")[1]
			day = date.split("/")[0]
			month = date.split("/")[1]
			year = date.split("/")[2]
			hour = time.split(":")[0]
			minute = time.split(":")[1]
			
			if day.isdigit() is False:
				errs += "❎ Day is not a valid number.\n"
			if month.isdigit() is False:
				errs += "❎ Month is not a valid number.\n"
			if year.isdigit() is False:
				errs += "❎ Year is not a valid number.\n"
			if hour.isdigit() is False:
				errs += "❎ Hour is not a valid number.\n"
			if minute.isdigit() is False:
				errs += "❎ Minute is not a valid number.\n"
					
		except Exception as e:
			errs += "❎ "+e
	message = None
	channel = None
	if target == "" and _type == "del":
		errs += "❎ If type is del, a target is required. (-f target)\n"
	elif target != "":
		chanid = target.split(" ")[0]
		mesid = target.split(" ")[1]
		channel = client.get_channel(int(chanid))
		if channel is None:
			errs += "❎ Channel ID is not valid.\n"
		else:
			async for msg in channel.history():
				if msg.id == int(mesid):
					message = msg
			if message is None:
				errs += "❎ Message ID is not valid.\n"
		target = target.replace(" ", ".")
		target = target.replace("/", ".")
	
	if errs != "":
		await ctx.send(errs)	
		return
		
	now = datetime.datetime.now()
	now_date = f"{now.day}/{now.month}/{now.year}"
	now_time = f"{now.hour}:{ifill(now.minute)}:{ifill(now.second)}"
	
	AddJob(name, _type, target, when, now_date+" "+now_time, f"{ctx.author.name} ({ctx.author.display_name}) [{ctx.author.id}]", ctx.channel.id)
	em = discord.Embed()
	em.add_field(name="Adding task", value=f"Name: {name}\nType: {_type}\nTarget: {target}\nWhen: {when} ({CheckTime(job_name=name, format_return=True)})\n\nCreator Info: {ctx.author.name} ({ctx.author.display_name}) [{ctx.author.id}] in {ctx.channel.id}")
	if _type == "del":
		em.add_field(name=message.author.name+" in #"+channel.name, value=message.content)
	await ctx.send(embed=em)
	
@access_check()
@client.command(enabled=False, hidden=True)
async def jobtest(ctx):
	now = datetime.datetime.now()
	now_date = f"{now.day}/{now.month}/{now.year}"
	now_time = f"{now.hour}:{now.minute}:{now.second}"
	
	AddJob("tonight", "test", "478902193752375307", "14/08/2018 21:21:02", now_date+" "+now_time, f"{ctx.author.name} ({ctx.author.display_name}) [{ctx.author.id}]", ctx.channel.id)
	AddJob("yesterday", "test", "478902193752375307", "13/08/2018 21:21:06", now_date+" "+now_time, f"{ctx.author.name} ({ctx.author.display_name}) [{ctx.author.id}]", ctx.channel.id)

	AddJob("now", "test", "478902193752375307", now_date+" "+now_time, now_date+" "+now_time, f"{ctx.author.name} ({ctx.author.display_name}) [{ctx.author.id}]", ctx.channel.id)

@access_check()
@client.command(hidden=True)
async def deltask(ctx, task_name):
	global jobs
	ReadJobs()
	try:
		del jobs[task_name]
		await ctx.send("Deleted task "+task_name)
	except KeyError:
		await ctx.send("Task doesn't exist.")
		
@client.command(hidden=True)
async def checktask(ctx, task_name=""):
	global jobs
	ReadJobs()
	if task_name == "":
		tasks = []
		for item in jobs.keys():
			#if item != "update_interval":
			tasks.append(item)
		if len(tasks) > 0:
			await ctx.send(embed=discord.Embed(description=HumanizeList(tasks)))
		else:
			await ctx.send(embed=discord.Embed(description="No tasks scheduled."))
		
		return
		
	try:
		cur = jobs[task_name]
	except KeyError:
		await ctx.send("That task does not exist.")
		return
		
	time_until = CheckTime(job_name=task_name, format_return=True)+" remaining."
	c = client.get_channel(cur['created_in']).mention
	
	v = f"The `{cur['job_type']}` task called `{task_name}` has {time_until}.\n"
	v += f"This task was created by {cur['creator']} at {cur['created']} in {c}."
	em = discord.Embed()
	em.add_field(name=task_name, value=v)
	await ctx.send(embed=em)

@access_check()
@client.group(name="ignores", invoke_without_command=True, hidden=True)
async def ig(ctx):
	"""Channel/User Blacklisting System
	Allows admins to disable specific commands for specific users or channels, or alternatively globally ignoring users or channels."""
	ReadIgnores()
	s = ""
	for item in ignores.keys():
		if item in ['members', 'ai', 'channels']:
			s += f"🔒 **{item}**: {HumanizeList(ignores[item])}\n"
		else:
			s += f"**{item}**: {HumanizeList(ignores[item])}\n"
	
	em = discord.Embed(colour=aecol)
	em.add_field(name="Ignores", value=s)
	await ctx.send(embed=em)	

@access_check()
@ig.command(name="add")
async def _ig_add(ctx, key:str, ignore_id:str):
	"""Add to an Ignore List
	Input <ignore_id> as either a user or channel's name or id.
	The system will try to match your input and find the most relevant channel or user automatically and add it to the list specified as <key>.
	
	Examples:
	!ignores add channels 480335262879318019
	!ignores add voice 480335262879318019
	!ignores add * guild
	!ignores add voice guild
	"""
	em = discord.Embed(colour=aecol)
	ignored_chan = None
	ignored_user = None
	ignored_guild = None
	value = ""
	if ignore_id.isdigit() is False:
		#If the value is a string, then try to convert to ID
		if ignore_id == "guild":
			if key != "*":
				if key != "ignores":
					fkey = ""
					for item in client.walk_commands():
						if key in item.name and not item.parent:
							fkey = item.name
					if fkey == "":
						await ctx.send(f"If `guild` is the target, `key` must refer to a legit command, but no command was found. Command will process but may not work if `{key}` is not a real command.")
					fkey = key
				else:
					await ctx.send("Best not to ignore the ignores command.")
					return
					
				key = fkey
			
			ignored_guild = ctx.guild
			value = str(ignored_guild.id)
		
		if value == "":	
			for channel in ctx.guild.channels:
				if ignore_id.lower() in channel.name.lower():
					value = str(channel.id)
					ignored_chan = channel
			
			for member in ctx.guild.members:
				if ignore_id.lower() in member.name.lower():
					value = str(member.id)
					ignored_user = member
	else:
		for channel in ctx.guild.channels:
			if ignore_id in str(channel.id):
				value = str(channel.id)
				ignored_chan = channel
		
		for member in ctx.guild.members:
			if ignore_id in str(member.id):
				value = str(member.id)
				ignored_user = member
		
	if value == "":	
		em.colour = 0xFF0000
		em.add_field(name="Error", value="Input converting failed. (No channel or member found with that name or ID.)")
		await ctx.send(embed=em)
		return
	
	#Custom sanity checks
	if key == "members" and ignored_chan is not None:
		em.colour = 0xFF0000
		em.add_field(name="Error", value="Sanity Check: Channels in the `members` key will do nothing, aborting.")
		await ctx.send(embed=em)
		return
		
	if key == "channels" and ignored_user is not None:
		em.colour = 0xFF0000
		em.add_field(name="Error", value="Sanity Check: Users in the `channels` key will do nothing, aborting.")
		await ctx.send(embed=em)
		return			
					
	key = key.lower()
	value = value.lower()
	
	ReadIgnores()
	try:
		cur = ignores[key]
	except KeyError:
		ignores[key] = []
		cur = ignores[key]
		em.add_field(name="New Key", value=f"The list for {key} has been created.")
		
	if value in cur:
		em.colour = 0xFF0000
		em.add_field(name="Error", value=f"That value is already in the **{key}** ignore list.")
	else:
		cur.append(value)
		WriteIgnores()
		if ignored_user:
			em.add_field(name="Success", value=f"User {ignored_user.mention} ({ignored_user.name}) **ID**: `{ignored_user.id}` was added to ignore key **{key}**")
		elif ignored_chan:
			em.add_field(name="Success", value=f"Channel {ignored_chan.mention}  **ID**: `{ignored_chan.id}` was added to ignore key **{key}**")
		elif ignored_guild:
			em.add_field(name="Success", value=f"Command **{key}** has been disabled in {ignored_guild.name}  **ID**: `{ignored_guild.id}`.")
		else:
			em.add_field(name="Success", value=f"ID **{value}** was added to ignore key **{key}**")
		
	em.add_field(name=f"Ignore List for **{key}** key", value=HumanizeList(ignores[key]))
	
	await ctx.send(embed=em)

@access_check()	
@ig.command(name="rem")
async def _ig_remove(ctx, key:str, ignore_id:str):
	"""Removes from an Ignore List
	Input <ignore_id> as either a user or channel's name or id.
	The system will try to match your input and find the most relevant channel or user automatically and remove it from the list specified as <key>.
	
	!ignores rem voice guild
	!ignores rem voice general"""
	em = discord.Embed(colour=aecol)
	ignored_chan = None
	ignored_user = None
	ignores_guild = None
	value = ""
	if ignore_id.isdigit() is False:
		if ignore_id == "guild":
			ignored_guild = ctx.guild
			value = str(ctx.guild.id)
			
		#If the value is a string, then try to convert to ID
		for channel in ctx.guild.channels:
			if ignore_id.lower() in channel.name.lower():
				value = str(channel.id)
				ignored_chan = channel
		
		for member in ctx.guild.members:
			if ignore_id.lower() in member.name.lower():
				value = str(member.id)
				ignored_user = member
	else:
		for channel in ctx.guild.channels:
			if ignore_id in str(channel.id):
				value = str(channel.id)
				ignored_chan = channel
		
		for member in ctx.guild.members:
			if ignore_id in str(member.id):
				value = str(member.id)
				ignored_user = member
		
	if value == "":	
		await ctx.send("Input converting failed. (No channel or member found with that name or ID.)")
		return
		
	key = key.lower()
	value = value.lower()
	ReadIgnores()
	try:
		cur = ignores[key]
	except KeyError:
		em.colour = 0xFF0000
		em.add_field(name="Error", value=f"The **{key}** ignore list does not exist, so a value can not be removed from it.")
		await ctx.send(embed=em)
		return
		
	if value not in cur:
		em.colour = 0xFF0000
		em.add_field(name="Error", value=f"That value is not in the **{key}** ignore list.")
	else:
		cur.remove(value)
		WriteIgnores()
		if ignored_user:
			em.add_field(name="Success", value=f"User {ignored_user.mention} ({ignored_user.name}) **ID**: `{ignored_user.id}` was removed from ignore key **{key}**")
		elif ignored_chan:
			em.add_field(name="Success", value=f"Channel {ignored_chan.mention}  **ID**: `{ignored_chan.id}` was removed from ignore key **{key}**")
		else:
			em.add_field(name="Success", value=f"ID **{value}** was removed from ignore key **{key}**")

	em.add_field(name=f"Ignore List for **{key}** key", value=HumanizeList(ignores[key]))
	
	await ctx.send(embed=em)
	
@access_check()
@ig.command(name="purge")
async def _ig_purge(ctx, key:str):
	"""Clears an Ignore List
	Will remove all entries from the list specified as <key>.
	"""
	key = key.lower()
	ReadIgnores()
	em = discord.Embed(colour=aecol)
	try:
		em.add_field(name=f"Purging {key} key", value=HumanizeList(ignores[key]))
	except KeyError:
		em.colour = 0xFF0000
		em.add_field(name="Error", value="No key found for that name.")
		await ctx.send(embed=em)
		return
		
	ignores[key].clear()
	WriteIgnores()
	em.add_field(name=f"Purged {key} key", value=HumanizeList(ignores[key]))
	await ctx.send(embed=em)

@access_check()
@ig.command(name="nuke")
async def _ig_nuke(ctx, key:str):
	"""Deletes an Ignore List
	Will remove all entries from the list specified as <key> and also erase that list entirely from the memory.
	"""
	key = key.lower()
	ReadIgnores()
	em = discord.Embed(colour=aecol)
	if key in ['members', 'ai', 'channels']:
		em.colour = 0xFF0000
		em.add_field(name="Error", value="Internal lists can not be deleted.")
		await ctx.send(embed=em)
		return
	try:
		em.add_field(name=f"Nuking {key} key", value=HumanizeList(ignores[key]))
	except KeyError:
		em.colour = 0xFF0000
		em.add_field(name="Error", value="No key found for that name.")
		await ctx.send(embed=em)
		return
		
	del ignores[key]
	WriteIgnores()
	em.add_field(name=f"Purged {key} key", value="Remaining: "+HumanizeList(ignores.keys()))
	await ctx.send(embed=em)
				
@access_check()	
@ig.command(name="view")
async def _ig_view(ctx, key:str):
	"""Shows an Ignore List's entries
	Shows all ignores for the list specified as <key>.
	"""
	key = key.lower()
	ReadIgnores()
	em = discord.Embed(colour=aecol)
	try:
		em.add_field(name=f"Ignore List for {key} key", value=HumanizeList(ignores[key]))
	except KeyError:
		em.colour = 0xFF0000
		em.add_field(name="Error", value="No key found for that name.")
		await ctx.send(embed=em)
		return	
		
	await ctx.send(embed=em)
	
@commands.cooldown(1, 15, BucketType.user)		
@client.command(hidden=True, aliases=['ai'])
async def send_to_ai(ctx):
	if len(ctx.author.roles) == 1:
		await ctx.send("This function is only allowed for users with a role in the guild.")
		return
		
	await queryAI(ctx.message, 1)
			
@client.command(hidden=True)
@owner_check()
async def test(ctx):
	await ctx.send(f'{ctx.message.author.permissions_in(ctx.channel).administrator}')

@client.command(hidden=True)
async def latency(ctx):
	await ctx.send(f"Delay in Discord protocol is {client.latency}")
	
@client.command(hidden=True)
async def emotes(ctx):
	await ctx.send(f"{client.emojis}")
						
@client.command(name='load', hidden=True)
@owner_check()
async def cog_load(ctx, *, cog: str):
	#await ctx.channel.trigger_typing()
	if cog == "all":
		astr = ""
		i = 0
		for extension in initial_extensions:
			try:
				client.load_extension(extension)
				AddLog(f"{extension} loaded.", colour="green")
			except Exception as e:
				astr += f'❌ **`{extension} ERROR:`** {type(e).__name__} - {e}\n'
			else:
				astr += f'▶ **`{extension} module loaded. [{i}/{len(initial_extensions) - 1}]`**\n'
			i += 1
		await ctx.send(astr)
		return
		
	if "ext." not in cog:
		cog = f"ext.{cog}"
	try:
		client.load_extension(cog)
	except Exception as e:
		await ctx.send(f'❌ **`{cog} ERROR:`** {type(e).__name__} - {e}')
	else:
		await ctx.send(f'▶ **`{cog} module loaded.`**')

@client.command(name='reloadall', hidden=True)
@owner_check()
async def cog_reloadall(ctx):
	#await ctx.channel.trigger_typing()
	fm = ""
	i = 0
	for item in initial_extensions:
		try:
			AddLog("Unloading "+item, colour="red")
			client.unload_extension(item)
			client.load_extension(item)
			AddLog(item+" loaded.", colour="green")
		except Exception as e:
			fm += f'❌ **`{item} ERROR:`** {type(e).__name__} - {e}\n'
		else:
			fm += f'🔄 **`{item} module reloaded. [{i}/{len(initial_extensions) - 1}]`**\n'
		
		i += 1
	await ctx.send(fm)
	
#'ext.deusex', 'ext.info', 'ext.roles', 'ext.utility', 'ext.api'
@client.command(name='extlist', hidden=True)
@owner_check()
async def cog_list(ctx):
	#await ctx.channel.trigger_typing()
	exts = list(client.cogs.keys())
	if len(exts) > 0:
		await ctx.send(f"Active Modules: {HumanizeList(exts)}")
	else:
		await ctx.send(f"Active Modules: _None_.")

@client.command(name='unload', hidden=True)
@owner_check()
async def cog_unload(ctx, *, cog):
	#await ctx.channel.trigger_typing()
	if cog == "all":
		astr = ""
		i = 0
		m = len(client.cogs.keys()) - 1
		for item in list(client.cogs.keys()):
			client.unload_extension(f"ext.{item.lower()}")
			astr += f'⏸ **`ext.{item.lower()} module unloaded. [{i}/{m}]`**\n'
			i += 1
		await ctx.send(astr)
		return
		
	if "ext." not in cog:
		cog = f"ext.{cog}"
	try:
		client.unload_extension(cog)
	except Exception as e:
		await ctx.send(f'❌ **`{cog} ERROR:`** {type(e).__name__} - {e}')
	else:
		await ctx.send(f'⏸ **`{cog} module unloaded.`**')

@client.command(name='reload', hidden=True)
@owner_check()
async def cog_reload(ctx, *, cog:str):
	if cog == "all":
		astr = ""
		i = 0
		for item in list(client.cogs.keys()):
			try:
				AddLog(f"Unloading ext.{item.lower()}", colour="red")
				client.unload_extension(f"ext.{item.lower()}")
				client.load_extension(f"ext.{item.lower()}")
				AddLog(f"ext.{item.lower()} loaded.", colour="green")
			except Exception as e:
				astr += f'❌ **`{item} ERROR:`** {type(e).__name__} - {e}\n'
			else:
				astr += f'🔄 **`{item} module reloaded. [{i}/{len(client.cogs.keys()) - 1}]`**\n'
			i += 1
		await ctx.send(astr)
		return
		
	if "ext." not in cog:
		cog = f"ext.{cog}"
	try:
		client.unload_extension(cog)
		client.load_extension(cog)
	except Exception as e:
		await ctx.send(f'❌ **`{cog} ERROR:`** {type(e).__name__} - {e}')
	else:
		await ctx.send(f'🔄 **`{cog} module reloaded.`**\n')
			
#======================
#Async Functions
#======================
async def ServerBackup():
	chans = 0
	servs = 0
	msgs = 0
	errorchans = ""

	for server in client.guilds:
		servs += 1
		print("Entering "+guild.name)
		for channel in guild.channels:
			chans += 1
			if str(channel.type) == "text":
				print("Entering "+server.name+" / "+channel.name)
			
				try:
					os.makedirs(scriptdir+server.id)
					print("Creating directory for "+server.name)
				except:
					pass
				open(scriptdir+server.id+"/"+channel.name+".log", 'w').close()
				with open(scriptdir+server.id+"/"+channel.name+".log", "r+") as f:
					try:
						async for message in client.logs_from(channel):
							msgs += 1
							f.writelines(message.author.name+": "+message.content+"\n")
					except discord.errors.Forbidden:
						errorchans += server.name+"/"+channel.name+", "
						print("Access denied to channel.")
					f.close()
					
	em = discord.Embed(colour=aecol)
	
	em.add_field(name="**System Log Manager**", value="Log action complete.\n"+str(msgs)+" messages from "+str(chans)+" channels across "+str(servs)+" servers printed to "+scriptdir)
	if errorchans is not "":
		errorchans = "The following channels could not be logged: \n"+errorchans[:-2]
		em.add_field(name="Errors", value=errorchans)
	await client.send_message(lastchannel, embed=em)
					
async def queryAI(message, amode):
	global aiConvID, aiConvID_owner, tts
	settings = getConfig()
	enc = minerva.Pantheon()
	aidisable = settings['core']['aidisable']
	password = enc.decode(settings['core']['password'])
	botuserid = settings['core']['botuserid']
	botid = settings['core']['botid']
	tts = boolinate(settings['core']['tts'])
	airesponse = "def"
	
	if message.guild:
		data = {}
		with open('stats.json') as f:
			data = json.load(f)
			
		try:
			data['commands']['ai'] += 1
		except:
			data['commands']['ai'] = 1
		
		try:
			data['channels'][str(message.channel.id)] += 1
		except:
			data['channels'][str(message.channel.id)] = 1
			
		try:
			data['users'][str(message.author.id)] += 1
		except:
			data['users'][str(message.author.id)] = 1
			
		try:
			data['guilds'][str(message.guild.id)] += 1
		except:
			data['guilds'][str(message.guild.id)] = 1
				
		with open('stats.json', "w") as s:
			json.dump(data, s, indent=4, sort_keys=True)
				
	owner_mode = False
	if message.author.id == client.owner_id:
		owner_mode = True
		
	await message.channel.trigger_typing()	
	if aidisable != "":
		if owner_mode is True:
			await message.channel.send(f"Bypassing disable. ({aidisable})")
		else:
			RaisePersona('anger', 2)
			em = discord.Embed(colour=0xFF0000)
			em.add_field(name="**System Message**", value='⚠ | Chat is currently disabled.\nReason: '+aidisable)
			await message.channel.send(embed=em)
			await message.add_reaction('⚠')
			await message.add_reaction(Emote())
			return
		
	if amode == 0: #Send full raw message
		bmsg = message.content
	elif amode == 1:
		bmsg = ' '.join(message.content.split(" ")[1:])
		
		#https://www.botlibre.com/rest/api/form-chat?instance=19852766&message=hello&application=6164811714561807251
	AddLog(f"Original: {bmsg}")
	bmsg = bmsg.replace(" ", "%20")
	AddLog(f"URL Version: {bmsg}")
	
	cburl = ""
	if owner_mode is True:
		owner_pass = minerva.Pantheon().decode("w4nDk8OXw43DjcOaw6Y=")
		cburl = f'https://www.botlibre.com/rest/api/form-chat?user=AthenaUser&password={owner_pass}&instance={botid}&message={bmsg}&application={botuserid}'
		if aiConvID_owner != 0:
			cburl = cburl+'&conversation='+str(aiConvID_owner)
	else:
		cburl = f'https://www.botlibre.com/rest/api/form-chat?user=DiscordUser&password={password}&instance={botid}&message={bmsg}&application={botuserid}'
		if aiConvID != 0:
			cburl = cburl+'&conversation='+str(aiConvID)
			
	if "?" in message.content:
		cburl = cburl+'&includeQuestion=true'
		
	print("QUERY URL: "+cburl)

	try:
		result =  requests.get(cburl)
	except UnicodeEncodeError:
		RaisePersona('anger', 3)
		LowerPersona('mood', 4)
		await message.channel.send('⚠ | '+message.author.mention+' That request returned an encoding error. Please use plaintext only while communicating.')
		AddLog(str(sys.exc_info()), True)
		await message.add_reaction('⚠')
		await message.add_reaction(Emote())
		return
	except urllib.error.HTTPError:
		RaisePersona('anger', 3)
		LowerPersona('mood', 4)
		await message.channel.send('⚠ | '+message.author.mention+' That request returned an error. Please use plaintext only while communicating or rephrase the query.')
		AddLog(str(sys.exc_info()), True)
		await message.add_reaction('⚠')
		await message.add_reaction(Emote())
		return
	except: 
		await exception(client, message)
		await message.add_reaction('⚠')
		await message.add_reaction(Emote())
		return

	soup = BeautifulSoup(result.text, 'html.parser')
	try:
		soupf = soup.message.string.replace("<br/>", "\n")
	except AttributeError:
		AddLog(soup.text)
		RaisePersona('anger', 3)
		LowerPersona('mood', 4)
		if soup.text == "Profanity, offensive or sexual language is not permitted.":
			await message.channel.send('⚠ | '+message.author.mention+' Profanity, offensive or sexual language is not permitted. Try different wording.')
		elif soup.text == "Daily maximum API calls reached, please upgrade your account.":
			await message.channel.send('⚠ | '+message.author.mention+' The AI server could not be reached at this time. Try again later.')
		else:
			await message.channel.send('⚠ | '+message.author.mention+' Unknown error from the AI server.')
		AddLog(str(sys.exc_info()), error=True)
		await message.add_reaction('⚠')
		await message.add_reaction(Emote())
		return
		
	AddLog(soup.response['conversation']+" ["+soup.response['emote']+"]-: "+soupf, colour="blue")
	if owner_mode:
		if aiConvID_owner == 0:		
			aiConvID_owner = soup.response['conversation']
			AddLog("Set new owner conversation ID: "+str(aiConvID_owner), colour="green")
	else:
		if aiConvID == 0:		
			aiConvID = soup.response['conversation']
			AddLog("Set new conversation ID: "+str(aiConvID), colour="green")
			
	if tts:
		SpeechTTS(soupf)
		await message.channel.send(file=discord.File(scriptdir+"athtts.wav"))
	
	if airesponse == "datalink":
		await message.channel.send('<:info:410758617067814912> | '+message.author.mention+' '+soupf)
	elif airesponse == "embed":
		em = discord.Embed(colour=aecol, description=soupf)
		#em.add_field(name="Responding to "+message.author.display_name, value=soupf)
		em.set_thumbnail(url='https://cdn.discordapp.com/emojis/410758617067814912.png')
		em.set_author(name="Responding to "+message.author.display_name, icon_url=message.author.avatar_url)
		await message.channel.send(embed=em)
	else:
		NormalizeMood(2)
		LowerPersona('anger', 1)
		if owner_mode:
			await message.add_reaction(client.get_emoji(410758616950505473))
			await message.channel.send(f"{message.author.mention} **{soupf}** {Emote()}")
		else:
			await message.add_reaction(client.get_emoji(410758616761630721))
			await message.channel.send(f"{message.author.mention} {soupf} {Emote()}")
			
	#await message.add_reaction('\u2705')
	await message.add_reaction(Emote())
		
async def list_files(message, subdir):
	
	listr = await client.send_message(message.channel, 'Checking...')
	dlpath = '/home/kaiz0r/.wine/drive_c/Deus Ex GOTY/'+subdir+'/'
	filelist = ""
	ilines = 0
	
	for file in os.listdir(dlpath):
		filename = os.fsdecode(file)
		if filename.endswith(".dx") or filename.endswith(".umx") or filename.endswith(".u") or filename.endswith(".ini") or filename.endswith(".int") or filename.endswith(".utx") or filename.endswith(".uax") or filename.endswith(".uc"): 
			ilines += 1
			if ilines == 50:
				ilines = 0
				filesize = byteconv(os.stat(dlpath+'/'+filename).st_size)
				filelist = filelist+"\n"+filename+" ("+filesize+")"
				await client.send_message(message.channel, filelist)
				filelist = ""
			else:
				filesize = byteconv(os.stat(dlpath+'/'+filename).st_size)
				filelist = filelist+"\n"+filename+" ("+filesize+")"

				
	try:
		await client.send_message(message.channel, filelist)
		await client.add_reaction(message, "🔍")
	except discord.errors.HTTPException:
		await client.send_message(message.channel, 'No files were found.')
		await client.add_reaction(message, "⛔")
	except:
		await exception(client, message)
				#await client.add_reaction(message, "‼")
		
#======================
#Utility Functions
#======================						
def equal(a, b):
    try:
        return a.lower() == b.lower()
    except AttributeError:
        return a == b
        			
def byteconv(number_of_bytes):
    if number_of_bytes < 0:
        raise ValueError("!!! numberOfBytes can't be smaller than 0 !!!")

    step_to_greater_unit = 1024.

    number_of_bytes = float(number_of_bytes)
    unit = 'bytes'

    if (number_of_bytes / step_to_greater_unit) >= 1:
        number_of_bytes /= step_to_greater_unit
        unit = 'KB'

    if (number_of_bytes / step_to_greater_unit) >= 1:
        number_of_bytes /= step_to_greater_unit
        unit = 'MB'

    if (number_of_bytes / step_to_greater_unit) >= 1:
        number_of_bytes /= step_to_greater_unit
        unit = 'GB'

    if (number_of_bytes / step_to_greater_unit) >= 1:
        number_of_bytes /= step_to_greater_unit
        unit = 'TB'

    precision = 1
    number_of_bytes = round(number_of_bytes, precision)

    return str(number_of_bytes) + ' ' + unit

def isover9mb(number_of_bytes):
	if number_of_bytes < 0:
		raise ValueError("!!! numberOfBytes can't be smaller than 0 !!!")

	step_to_greater_unit = 1024.

	number_of_bytes = float(number_of_bytes)
	unit = 'bytes'

	if (number_of_bytes / step_to_greater_unit) >= 1:
		number_of_bytes /= step_to_greater_unit
		unit = 'KB'

	if (number_of_bytes / step_to_greater_unit) >= 1:
		number_of_bytes /= step_to_greater_unit
		unit = 'MB'

	if (number_of_bytes / step_to_greater_unit) >= 1:
		number_of_bytes /= step_to_greater_unit
		unit = 'GB'

	if (number_of_bytes / step_to_greater_unit) >= 1:
		number_of_bytes /= step_to_greater_unit
		unit = 'TB'

	precision = 1
	number_of_bytes = round(number_of_bytes, precision)

	if 'GB' in unit:
		return True
	elif 'MB' in unit:
		if number_of_bytes > 9:
			return True
	else:
		return False

def Webhook(text="", embed=None):
	#Testzone: 461851689922854922 / EHjh3I2uAZXpt1MPf0ehzH8yzwfvYLz5YiU9zALRN2YBEj0ebafmAmR7w2YPDHzTDG_o
	#Matchmaking: 461858508237701120 / jUIj3q0tb42XSDVEJnG2_7umXfRGJZZiablLN0oHcVkzfVj-ZJ4n201P-JGrgFCLb97c
	#Announce: 461868227664805908 8VHKMw0PRZc_5Rmng3NYgOiYjvCR2eV3rVNQsv7G2mxUgy2u4Vw7AaR8-7DJf1GYGrvo
	
	#https://discordapp.com/api/webhooks/461851689922854922/EHjh3I2uAZXpt1MPf0ehzH8yzwfvYLz5YiU9zALRN2YBEj0ebafmAmR7w2YPDHzTDG_o
	
	webhook_id = "461851689922854922"
	webhook_token = "EHjh3I2uAZXpt1MPf0ehzH8yzwfvYLz5YiU9zALRN2YBEj0ebafmAmR7w2YPDHzTDG_o"
	webhook = discord.Webhook.partial(webhook_id, webhook_token, adapter=discord.RequestsWebhookAdapter())
	webhook.send(text, embed=embed)

#======================
#Background Tasks
#======================

async def wait_task(channel, senderuser, timewait, message):
	global bTimerRunning
	
	while not client.is_closed and bTimerRunning is False:
		sw = timewait*60
		print("Waiting for "+timewait+" minutes, or "+sw+" seconds.")
		await client.send_message(channel, "Timer set for "+timewait+" minutes.")
		await asyncio.sleep(int(sw))
		await client.send_message(channel, senderuser.mention+message)
		bTimerRunning=True
	
async def timer_task(channel, senderuser, timehour, timemins, message, bAll):
	#  await client.wait_until_ready()
	global bAlerting
	
	while not client.is_closed and bAlerting is False:
		await asyncio.sleep(60) # task runs every 60 seconds
		now = datetime.datetime.now()
		print("Checking "+now.strftime("%H")+":"+now.strftime("%M")+" against "+timehour+":"+timemins+" for "+message)
		if now.strftime("%M") == timemins and now.strftime("%H") == timehour:
			bAlerting = True
			await client.send_message(channel, senderuser.mention+' '+message)
			
async def status_task_redacted():
	global statusvar, uptimemins, uptimehours

	while not client.is_closed():
		now = datetime.datetime.now()
		game = discord.Game(Emote()+" Time: "+now.strftime("%H")+":"+now.strftime("%M")+" GMT")
		await client.change_presence(activity=game)
		#statusvar = -1
			
		await asyncio.sleep(60) # task runs every 60 seconds
		
		uptimemins += 1
		if uptimemins == 60:
			uptimemins = 0
			uptimehours += 1
			
	 	
#======================
#Script initiation
#======================
SetTimer('init', log=False)
if __name__ == '__main__':
    for extension in initial_extensions:
        try:
            client.load_extension(extension)
            AddLog(f"Loaded {extension}")
        except Exception as e:
            print(f'Failed to load extension {extension}.', file=sys.stderr)
            traceback.print_exc()
            
def Athinit():
	FileCheck()
	if os.path.isfile(os.path.dirname(os.path.realpath(__file__))+"/config.json"):
		AddLog("Config file exists.", colour="green")
		AddLog("Running from "+scriptdir, colour="green")
		try:
			enc = minerva.Pantheon()
			AddLog("Minerva is in the house.", colour="magenta")
		except:
			AddLog("Minerva isn't around today.")
		token_raw = getConfig()['core']['token']
		token = enc.decode(token_raw)
		client.run(token)
	else:
		AddLog("ERROR: Config file not found. Create a file in the script directory named 'config.json'.", True)

Athinit()
