import discord
from discord.ext import commands
import datetime
import humanfriendly
import socket
import sys
import random
from persona import *
from athena_common import *

aecol = 0x92005D

class roles:
	"""The Role Manager commands, for assigning self-roles."""
	def __init__(self, bot):
		self.bot = bot
	
	@commands.guild_only()	
	@commands.has_permissions(manage_roles=True)
	@commands.command(aliases=['aj'], help="Adds a role to be joinable", brief="Let's assist in some roleplay.", hidden=True)
	async def addjoinable(self, ctx, rolename: str):
		await ctx.channel.trigger_typing()
		_role = None
		for role in ctx.guild.roles:
			if rolename.lower() == role.name.lower():
				_role = role
				
		if not _role:
			await ctx.send("Role doesn't exist. ")
			return
			
		joinableroles = ReadRoles(ctx.guild)
		joinableroles.append(_role.id)
		WriteRoles(ctx.guild, joinableroles)
		r = []
		for role in ctx.guild.roles:
			if "everyone" not in role.name and role.managed is False:
				if role.id in joinableroles:
					r.append(role.name)
							
		await ctx.send(f"Added role to joinable list.\nCurrent joinable roles: {HumanizeList(r)}")
		PersonaCommand()
		
	@commands.guild_only()
	@commands.has_permissions(manage_roles=True)
	@commands.command(aliases=['rj'], help="Removes a role from being joinable", brief="Let's stop that roleplay.", hidden=True)
	async def removejoinable(self, ctx, rolename: str):
		await ctx.channel.trigger_typing()
		try:
			f = getRoleId(rolename)
			joinableroles = ReadRoles(ctx.guild)
			joinableroles.remove(f)
			WriteRoles(ctx.guild, joinableroles)
			r = []
			for role in ctx.guild.roles:
				if "everyone" not in role.name and role.managed is False:
					if role.name.lower() in joinableroles:
						r.append(role.name)

			await ctx.send(f"Removed role from joinable list.\nCurrent joinable roles: {HumanizeList(r)}")
		except:
			await ctx.send(f"Some error there. /shrug")
		PersonaCommand()
		
	@commands.guild_only()	
	@commands.command(aliases=['rls'], help="Shows your list of roles, and help about the roles system.", brief="How do I roleplay?")
	async def roles(self, ctx):
		await ctx.channel.trigger_typing()
		role_names = [role.name for role in ctx.author.roles]
		i = 0
		
		while i < (len(role_names) - 1):
			if "everyone" in role_names[i]:
				role_names[i] = "everyone"
			i += 1
		
		joinableroles = ReadRoles(ctx.guild)
		rolelistf = ""
		roles_admin = []
		roles_mods = []
		roles_join = []
		roles_other = []
		for role in ctx.guild.roles: # ⚜	
			if "everyone" not in role.name and role.managed is False:
				if role.permissions.administrator is True:
					roles_admin.append("🔱 __**"+role.mention+"**__")
				elif "moderator" in role.name.lower():
					roles_mods.append("🛡 **"+role.mention+"**")
				elif role.id in joinableroles:
					roles_join.append("⚜ **"+role.mention+"**")
				else:
					roles_other.append("👤 "+role.mention+"")
		
		rolelistf += '\n'.join(roles_admin)	
		rolelistf += '\n'
		rolelistf += '\n'.join(roles_mods)	
		rolelistf += '\n'
		rolelistf += '\n'.join(roles_other)	
		rolelistf += '\n'
		rolelistf += '\n'.join(roles_join)
		
							
		rolesvalue = f'**Current roles**: {HumanizeList(role_names)}\n\nTo join one of the joinable roles, use command `!join <role name>`.\nTo leave a role, use the command `!leave <role name>`\nOnce in that role, you will get notified if someone mentions that role name.\nFor a list of all players in a role, see command `!roleinfo`'
		
		em = discord.Embed(colour=aecol, description=rolesvalue)
		#em.add_field(name="**Role Manager**", value=rolesvalue)
		em.set_author(name=ctx.author.display_name, icon_url=ctx.author.avatar_url)
		em.add_field(name="Role List", value=rolelistf)
		em.add_field(name="Legend", value="_⚜ joinable using `!join`\n🔱 administrators\n🛡 moderators\n👤 restricted-access_")
		await ctx.send(embed=em)		
		PersonaCommand()
		
	@commands.guild_only()
	@commands.command(aliases=['rc'])
	async def rolecompare(self, ctx, rolename : commands.RoleConverter, second_rolename : commands.RoleConverter):
		"""Compares two roles.
		At the moment, this only shows the member list, comparing who is in both roles.
		"""
		em = discord.Embed(colour=aecol)
		mems = []
		for member in rolename.members:
			if member in second_rolename.members:
				if member.display_name is not member.name:
					mems.append(member.display_name+" ("+member.name+")")
				else:
					mems.append(member.display_name)
		mems_f = '\n'.join(mems)			
		em.add_field(name=f"Members in {rolename.name} and {second_rolename.name}", value=f"{mems_f}\n**{len(mems)} in total.**")
		await ctx.send(embed=em)
		PersonaCommand()
			
	@commands.guild_only()				
	@commands.command(aliases=['ri'], help="If `rolename` is left out, shows the servers role list.\nElse, shows information about `rolename`", brief="How do people roleplay?")
	async def roleinfo(self, ctx, rolename=""):
		await ctx.channel.trigger_typing()
		rolelistf = ""
		rolecolour = aecol
		if rolename == "":
			joinableroles = ReadRoles(ctx.guild)
			rolelistf = ""
			roles_admin = []
			roles_mods = []
			roles_join = []
			roles_other = []
			for role in ctx.guild.roles: # ⚜	
				if "everyone" not in role.name and role.managed is False:
					if role.permissions.administrator is True:
						roles_admin.append("🔱 __**"+role.mention+"**__")
					elif "moderator" in role.name.lower():
						roles_mods.append("🛡 **"+role.mention+"**")
					elif role.id in joinableroles:
						roles_join.append("⚜ "+role.mention+"")
					else:
						roles_other.append("👤 "+role.mention+"")
			
			rolelistf += '\n'.join(roles_admin)	
			rolelistf += '\n'
			rolelistf += '\n'.join(roles_mods)	
			rolelistf += '\n'
			rolelistf += '\n'.join(roles_other)	
			rolelistf += '\n'
			rolelistf += '\n'.join(roles_join)
						
			em = discord.Embed(colour=rolecolour, description="To see information about a specific role, add the role name.\n**Format**: `!roleinfo <rolename>`\n\n**Valid rolename(s)**:"+rolelistf)
			em.set_author(name="Responding to "+ctx.author.display_name, icon_url=ctx.author.avatar_url)
			em.add_field(name="Legend", value="_⚜ joinable using `!join`\n🔱 administrators\n🛡 moderators\n👤 restricted-access_")
			await ctx.send(embed=em)
			return;
		
		bFoundRole = False
		admin = False
		managed = False
		created = ""
		for role in ctx.guild.roles:
			if rolename in role.name.lower():
				bFoundRole = True
				admin = role.permissions.administrator
				managed = role.managed
				created = role.created_at
				rid = role.id
				
		if bFoundRole is False:
			await ctx.send(ctx.author.mention+" "+msg_generator(types="notfound_role", context=rolename))
			return
				
		rolestr = ""
		rolint = 0
		membstotal = 0
		membs = []
		for member in ctx.guild.members:
			membstotal += 1
			for role in member.roles:
				if rolename in role.name.lower():
					rolecolour = role.colour
					membs.append(member.display_name)
					rolint += 1
			
		mperc = Percent(rolint, membstotal)
		#rolestr = rolestr[2:]	
		rolestr = HumanizeList(membs)	
		if admin is True:
			rolestr = rolestr+"\n\n**Role is administrator.**"
		
		rolestr = rolestr+"\n**"+str(rolint)+" members. ("+mperc+"% of server total)**"
		rolestr = rolestr+"\n**Created at**: "+str(created.day)+"/"+str(created.month)+"/"+str(created.year)+" - "+str(created.hour)+":"+str(created.minute)
		
		if managed is True:
			rolestr = "This role is managed by a third-party client and is not an official server role."
			
		em = discord.Embed(colour=rolecolour)
		em.add_field(name="**Role information ** ("+rolename+" - "+str(rid)+")", value=rolestr+"\n")
		await ctx.send(embed=em)
		PersonaCommand()
		
	@is_in_guild(396716931962503169) #agents 396825212362162186 mod 473271802035175424
	@has_role_ids([413143675640479755, 396825212362162186, 473271802035175424])
	@commands.command(enabled=False, hidden=True)
	async def addhx(self, ctx, membername):
		"""Adds a member to the HX role."""
		for member in ctx.guild.members:
			if membername.lower() in member.name.lower() or membername.lower() in member.display_name.lower():					
				hx = get_role(ctx, 413143675640479755)
				if hx in member.roles:
					await ctx.send(f"{ctx.author.mention} That person is already in the hx group.")
					return
				await member.add_roles(hx, reason="By command, requested by "+ctx.author.display_name)
				await ctx.send(f"{member.mention} was added to <#413145099787304960> by {ctx.author.mention}")
	
	@is_in_guild(396716931962503169)	
	@has_role_ids([396825212362162186, 473271802035175424])
	@commands.command(enabled=False, hidden=True)
	async def kickhx(self, ctx, membername):
		"""Removes a member from HX."""
		for member in ctx.guild.members:
			if membername.lower() in member.name.lower() or membername.lower() in member.display_name.lower():
				hx = get_role(ctx, 413143675640479755)
				if hx not in member.roles:
					await ctx.send(f"{ctx.author.mention} That person isn't in the hx group.")
					return
					
				await member.remove_roles(hx, reason="By command, requested by "+ctx.author.display_name)
				await ctx.send(f"{member.mention} was kicked from <#413145099787304960> by {ctx.author.mention}")
					
	@commands.guild_only()	
	@commands.command(aliases=['j', 'addrole'], help="Adds a role to your user.", brief="Start roleplaying.")
	async def join(self, ctx, rolename:str):
		await ctx.channel.trigger_typing()
		rolecolour=aecol

		_role = None
		for role in ctx.guild.roles:
			if rolename.lower() == role.name.lower():
				_role = role
			
		
		if not _role:
			PersonaError()
			await ctx.send(ctx.author.mention+" "+msg_generator(types="notfound_role", context=rolename))
			return
				
		rolecolour = _role.colour
		
		joinableroles = ReadRoles(ctx.guild)	
		if _role.id in joinableroles:
			if _role in ctx.author.roles:
				PersonaError()
				await ctx.send(ctx.author.mention+" "+msg_generator(types="already_in_role"))
				return

			await ctx.author.add_roles(_role, reason="By command, requested by "+ctx.author.display_name)
			em = discord.Embed(colour=rolecolour, description="Added "+ctx.author.mention+" to role **"+_role.name+"**")
			em.set_author(name="Responding to "+ctx.author.display_name, icon_url=ctx.author.avatar_url)
			await ctx.send(embed=em)
			PersonaCommand()
		else:
			await ctx.send(ctx.author.mention+" That role is not joinable on this server.")

	@commands.guild_only()
	@commands.command(aliases=['lr', 'removerole'], help="Removes a role from your user.", brief="Stop roleplaying.")
	async def leave(self, ctx, rolename):
		await ctx.channel.trigger_typing()
			
		_role = None
		for role in ctx.guild.roles:
			if rolename.lower() == role.name.lower():
				_role = role
			
		
		if not _role:
			PersonaError()
			await ctx.send(ctx.author.mention+" "+msg_generator(types="notfound_role", context=rolename))
			return
			
		rolecolour = _role.colour
		
		
		joinableroles = ReadRoles(ctx.guild)	
		if _role.id in joinableroles:
			if _role not in ctx.author.roles:
				await ctx.send(ctx.author.mention+" "+msg_generator(types="not_in_role"))
				return
				
			await ctx.author.remove_roles(_role, reason="By command, requested by "+ctx.author.display_name)
			em = discord.Embed(colour=rolecolour, description="Removed "+ctx.author.mention+" from role **"+_role.name+"**")
			em.set_author(name="Responding to "+ctx.author.display_name, icon_url=ctx.author.avatar_url)
			await ctx.send(embed=em)
			PersonaCommand()
		else: 
			await ctx.send(ctx.author.mention+" Role is not joinable, and so is not leavable.")

def getRoleName(guild, roleid):
	for item in guild.roles:
		if item.id == roleid:
			return item

def getRoleId(guild, rolename):
	for item in guild.roles:
		if item.name == rolename:
			return item
					
def ReadRoles(guild):
	_r = getConfig()
	try:
		return _r[str(guild.id)]['joinable_roles']
	except:
		return []
		
def WriteRoles(guild, new_list):
	setConfig(str(guild.id), 'joinable_roles', new_list)
							
def setup(bot):
	global client
	bot.add_cog(roles(bot))
	client = bot
