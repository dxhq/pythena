import discord
from discord.ext import commands
from discord.ext.commands.cooldowns import BucketType
import datetime
import humanfriendly
import requests
import configparser
import json
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
import random
import asyncio
from persona import *
from athena_common import *
from operator import itemgetter
from collections import OrderedDict
import aiohttp

current_trivia_question = None

trivia_fails = 0
trivia_question_num = 0
trivia_questions_max = 0
question_answers = []
trivia_allow_fuzzy = "true"
trivia_sens = 80
trivia_tries = 3

class TriviaQuestion:
	def __init__(self, channel, category):
		global trivia_question_num, question_answers
		self.answered = []
		try:
			trivia_question_num += 1
			question_answers.clear()
			self.cat = category
			self.formattedquestion = ""
			AddLog("Generating Trivia question "+str(trivia_question_num))
			self.channel = channel
			if self.cat == -1:
				self.f = requests.get("https://opentdb.com/api.php?amount=1")
			else:
				self.f = requests.get(f"https://opentdb.com/api.php?amount=1&category={self.cat}")
			print(self.f)
			self.data = json.loads(self.f.content)
			print(self.data)
		except json.decoder.JSONDecodeError:
			RaisePersona('anger', 1)
			LowerPersona('mood', 2)
			AddLog("Caught decoder error!")
			return
			
		self.category = self.data.get('results')[0].get('category')
		self.ttype = self.data.get('results')[0].get('type')
		self.difficulty = self.data.get('results')[0].get('difficulty').capitalize()
		
		self.question = self.data.get('results')[0].get('question')
		self.question = self.fixString(self.question)
		
		self.correct = self.data.get('results')[0].get('correct_answer')
		self.correct = self.fixString(self.correct)
		
		self.incorrect = self.data.get('results')[0].get('incorrect_answers')
		self.incorrect_h = HumanizeList(self.incorrect)
		self.incorrect_h = self.fixString(self.incorrect_h)
		
		print(self.incorrect_h)
		print("TYPE: "+self.ttype+"   CATEGORY: "+self.category+"   DIF: "+self.difficulty)
		print("Q: "+self.question)
		print("A: "+self.correct)
		
		self.m = ""
		if self.ttype == "boolean":
			self.m = "True or False:"
		elif self.ttype == "multiple":
			self.m = "Question:"
			
		SetTimer("trivia", forcestart=True)
		self.formattedquestion = "Category: "+self.category+" |  Difficulty: "+self.difficulty+"\n"+self.m+" "+self.question
	
	def fixString(self, string):
		self.f = string
		self.f = self.f.replace("&#039;", "'")
		self.f = self.f.replace("&quot;", '"')
		self.f = self.f.replace("&amp;", "&")
		return self.f
		
	def getChoices(self):
		global question_answers
		
		full = []
		full.append(self.fixString(self.correct))
		for item in self.incorrect:
			full.append(self.fixString(item))
		
		random.shuffle(full)
		i = 0
		s = ""
		
		for item in full:
			question_answers.append(item)
			i += 1
			s += str(i)+": "+item+"\n"
			
		return "```"+s+"```"

class trivia:
	"""
	A set of commands for a Discord trivia game.
	Uses Open Trivia Database for questions.
	Scores are saved offline.
	"""
	global client
	def __init__(self, bot):
		self.bot = bot
		client = bot
		
		#@bot.listen('on_ready')
		#async def trivia_sync():
		#	await client.loop.create_task(trivia_task())
			
		@bot.listen('on_message')
		async def my_message(message):
			global current_trivia_question
			global trivia_fails
			global trivia_questions_max
			global trivia_question_num
			global question_answers
			if message.author.bot is True:
				return
				
			#TriviaQuestion
			if current_trivia_question is not None:
				if GetTimer('trivia').seconds < 1:
					AddLog("Caught in delay... "+message.content)
					return
					
				if message.channel.name == current_trivia_question.channel.name:
					if message.content == ">choices":
						await message.channel.send(current_trivia_question.getChoices())
					if message.content == ">skip":
						RaisePersona('anger', 1)
						SetTimer("trivia", forceend=True)
						if trivia_question_num < trivia_questions_max:
								await message.channel.trigger_typing()
								trivia_fails = 0
								last_cat = current_trivia_question.cat
								current_trivia_question = TriviaQuestion(message.channel, last_cat)
								await message.channel.send(f"<:datacube:411016380322676736> | Question: {trivia_question_num}/{trivia_questions_max}\n```\n{current_trivia_question.formattedquestion}```")	
						else:
							await message.channel.trigger_typing()
							await message.channel.send("❎ | Game over.")	
							current_trivia_question = None
						return
					elif message.content == ">reroll":
						RaisePersona('anger', 2)
						SetTimer("trivia", forceend=True)
						trivia_fails = 0
						await message.channel.trigger_typing()
						last_cat = current_trivia_question.cat
						current_trivia_question = TriviaQuestion(message.channel, last_cat)
						trivia_question_num -= 1
						await message.channel.send(f"<:datacube:411016380322676736> | Question: {trivia_question_num}/{trivia_questions_max}\n```\n{current_trivia_question.formattedquestion}```")
						return
					elif message.content.startswith(">"):
						return
					
					if message.author in current_trivia_question.answered:
						await message.channel.send(f"{message.author.mention} You have already answered this round.")
						return
					
					if len(question_answers) > 0:
						if message.content == 'a' or message.content == '1':
							message.content = question_answers[0]
							await message.channel.send(message.author.mention+" is answering "+message.content)
						if message.content == 'b' or message.content == '2':
							message.content = question_answers[1]
							await message.channel.send(message.author.mention+" is answering "+message.content)
						if message.content == 'c' or message.content == '3':
							try:
								message.content = question_answers[2]
								await message.channel.send(message.author.mention+" is answering "+message.content)
							except:
								await message.channel.send("No third answer in this round.")
						if message.content == 'd' or message.content == '4':
							try:
								message.content = question_answers[3]
								await message.channel.send(message.author.mention+" is answering "+message.content)
							except:
								await message.channel.send("No fourth answer in this round.")
								
					accuracy = fuzz.ratio(current_trivia_question.correct.lower(), message.content.lower())
					if accuracy < 20:
						current_trivia_question.answered.append(message.author)
						trivia_fails += 1
						await message.channel.trigger_typing()
						await message.channel.send(f"{message.author.mention} Not even close. **({accuracy}%)**\n{(trivia_tries-trivia_fails)} attempts remaining.")
					elif accuracy >= 20 and accuracy < 50:
						current_trivia_question.answered.append(message.author)
						trivia_fails += 1
						await message.channel.trigger_typing()
						await message.channel.send(f"{message.author.mention} Kinda close, maybe? **({accuracy}%)**\n{(trivia_tries-trivia_fails)} attempts remaining.")
					elif accuracy >= 50 and accuracy < trivia_sens:
						current_trivia_question.answered.append(message.author)
						trivia_fails += 1
						await message.channel.trigger_typing()
						await message.channel.send(f"{message.author.mention} Almost! **({accuracy}%)**\n{(trivia_tries-trivia_fails)} attempts remaining.")
					
					if (trivia_tries-trivia_fails) == 0 and current_trivia_question:
						LowerPersona('mood', 5)
						SetTimer("trivia", forceend=True)
						await message.channel.trigger_typing()
						await message.channel.send(f"Too many fails! The correct answer was: {current_trivia_question.correct}\nSome incorrect answers include; {current_trivia_question.incorrect_h}")	
						current_trivia_question = None
						if trivia_question_num < trivia_questions_max:
							await message.channel.trigger_typing()
							trivia_fails = 0
							last_cat = current_trivia_question.cat
							current_trivia_question = TriviaQuestion(message.channel, last_cat)
							await message.channel.send(f"<:datacube:411016380322676736> | Question: {trivia_question_num}/{trivia_questions_max}\n```\n{current_trivia_question.formattedquestion}```")	
						else:
							await message.channel.send("❎ | Game over.")
						return
						
					if accuracy >= trivia_sens:
						SetTimer("trivia", forceend=True)
						RaisePersona('mood', 7)
						LowerPersona('anger', 15)
						try:
							await message.channel.trigger_typing()
							self.wins = getConfig()['trivia'][str(message.author.id)]
							setConfig(parent="trivia", key=str(message.author.id), value=self.wins+(trivia_tries-trivia_fails))
							
							await message.channel.send(f"🏆 | {message.author.mention} wins!\n{message.author.display_name}'s score: {self.wins+(trivia_tries-trivia_fails)} **(+{trivia_tries-trivia_fails})**\nCorrect Answer was: {current_trivia_question.correct}!\nGood thing you didn't say anything like {current_trivia_question.incorrect_h}, 'cause that would be silly.")
						except:
							await message.channel.trigger_typing()
							setConfig(parent="trivia", key=str(message.author.id), value=trivia_tries-trivia_fails)
							self.wins = trivia_tries-trivia_fails
							await message.channel.send(f"🏆 | {message.author.mention} wins!\n{message.author.display_name}'s score: {self.wins} **(+{trivia_tries-trivia_fails})** 🆕\nCorrect Answer was: {current_trivia_question.correct}!\nGood thing you didn't say anything like {current_trivia_question.incorrect_h}, 'cause that would be silly.")
						current_trivia_question = None
						if trivia_question_num < trivia_questions_max:
							await message.channel.trigger_typing()
							trivia_fails = 0
							last_cat = current_trivia_question.cat
							current_trivia_question = TriviaQuestion(message.channel, last_cat)
							await message.channel.send(f"<:datacube:411016380322676736> | Question: {trivia_question_num}/{trivia_questions_max}\n```\n{current_trivia_question.formattedquestion}```")		
						else:
							await message.channel.send("❎ | Game over.")
						
	#================================================
	@commands.command(aliases=['ts'], help="Shows the current trivia scoreboard.", brief="Who knows the most useless information.")
	async def triviascores(self, ctx):	
		await ctx.channel.trigger_typing()
		scoreboard = getConfig()['trivia']
		
		c = ""
		i = 0
		for key in dict(OrderedDict(sorted(scoreboard.items(), key=itemgetter(1), reverse=True))):
			i += 1
			c += f"[{i}] {client.get_user(int(key)).mention}: {scoreboard[key]}\n"
			if i == 15:
				break
				
		await ctx.channel.send(embed=discord.Embed(description=c))
	
	@commands.command()
	async def triviacat(self, ctx, name=""):
		r = requests.get("https://opentdb.com/api_category.php")
		c = r.content
		data = json.loads(c)
		if name != "":
			for item in data['trivia_categories']:
				if name.lower() in item['name'].lower():
					await ctx.send(f"ID is item {item['id']}")
		else:
			s = []
			for item in data['trivia_categories']:
				s.append(item['name'])
			await ctx.send(HumanizeList(s))
						
	@commands.command(aliases=['triv', 't'], help="Play a trivia game against the bot. Questions are supplied by the Open Trivia Database.", brief="You don't know jack.")
	async def trivia(self, ctx, RoundLimit=1, *, category=""):
		global current_trivia_question
		global trivia_fails
		global trivia_questions_max
		global trivia_question_num
		cat = -1
		cat_name = ""
		if category != "":
			r = requests.get("https://opentdb.com/api_category.php")
			c = r.content
			data = json.loads(c)
			for cats in data['trivia_categories']:
				if category.lower() in cats['name'].lower():
					cat = cats['id']
					cat_name = cats['name']
					
		if cat_name == "":
			cat_name = "Random (ALL)"
			
		await ctx.send(f"Generating new game with settings: Rounds: {RoundLimit}  Category: {cat_name}")	
			
		if current_trivia_question is None:
			await ctx.channel.trigger_typing()
			trivia_question_num = 0
			trivia_fails = 0
			trivia_questions_max = RoundLimit
			current_trivia_question = TriviaQuestion(ctx.channel, cat)
			
			if current_trivia_question.formattedquestion != "":
				await ctx.channel.send(f"✅ | Game starting!\n<:datacube:411016380322676736> | {trivia_question_num}/{trivia_questions_max}\n```\n{current_trivia_question.formattedquestion}```\nShow multiple choice answers with `>choices`\nGet a different question for this round with `>reroll`")
				await client.loop.create_task(trivia_task())
			else:
				await ctx.channel.send("There was a problem with the database server. Try again later.")
									
async def trivia_task():
	global current_trivia_question, trivia_question_num, trivia_questions_max

	#while 1 == 1:
	while current_trivia_question is not None:
		s = GetTimer("trivia").seconds
		channel = current_trivia_question.channel
		AddLog(f"Timer: Trivia check. ({60-s})")
		if s >= 60:
			LowerPersona('mood', 10)
			SetTimer("trivia", forceend=True)
			await channel.trigger_typing()
			await channel.send("Noone got it! The correct answer was: "+current_trivia_question.correct+"\nSome incorrect answers include; "+current_trivia_question.incorrect_h)	
			if trivia_question_num < trivia_questions_max:
					trivia_fails = 0
					
					last_cat = current_trivia_question.cat
					current_trivia_question = TriviaQuestion(message.channel, last_cat)
					await channel.send("Question: "+str(trivia_question_num)+"/"+str(trivia_questions_max)+"\n```\n"+current_trivia_question.formattedquestion+"```")	
			else:
				await channel.send("❎ | Game over.")	
				current_trivia_question = None
					
		await asyncio.sleep(30) 
					
def setup(bot):
	global client
	bot.add_cog(trivia(bot))
	client = bot
