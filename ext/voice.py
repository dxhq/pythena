import discord
from discord.ext import commands
from discord.ext.commands.cooldowns import BucketType
from athena_common import *
import os
import asyncio
import youtube_dl
import minerva
import requests
from functools import partial
from humanfriendly import format_timespan

# Suppress noise about console usage from errors
youtube_dl.utils.bug_reports_message = lambda: ''

ytdl_format_options = {
	'format': 'bestaudio/best',
	'outtmpl': '%(extractor)s-%(id)s-%(title)s.%(ext)s',
	'restrictfilenames': True,
	'noplaylist': True,
	'nocheckcertificate': True,
	'ignoreerrors': False,
	'logtostderr': False,
	'quiet': True,
	'no_warnings': True,
	'default_search': 'auto',
	'source_address': '0.0.0.0' # bind to ipv4 since ipv6 addresses cause issues sometimes
}

ffmpeg_options = {
	'options': '-vn'
}

ytdl = youtube_dl.YoutubeDL(ytdl_format_options)
	
class YTDLSource(discord.PCMVolumeTransformer):
	def __init__(self, source, *, data, volume=0.5):
		super().__init__(source, volume)
		self.data = data
		self.title = data.get('title')
		self.url = data.get('url')
		self.duration = format_timespan(data.get('duration'))
		self.duration_raw = data.get('duration')
		self.duration_format = format_clock(data.get('duration'))

	@classmethod
	async def from_url(cls, url, *, loop=None, stream=False):
		loop = loop or asyncio.get_event_loop()
		data = await loop.run_in_executor(None, lambda: ytdl.extract_info(url, download=not stream))

		if 'entries' in data:
			# take first item from a playlist
			data = data['entries'][0]

		filename = data['url'] if stream else ytdl.prepare_filename(data)
		return cls(discord.FFmpegPCMAudio(filename, **ffmpeg_options), data=data)

class voice:
	"""For interfacing with voice channels"""
	global client
	def __init__(self, bot):
		self.bot = bot
		client = bot

		@bot.listen('on_voice_state_update')
		async def vcnotifs(member, before, after):
			AddLog(f"{member.name} voice state updated.")
			if member.bot:
				return
			if vcgexist(member.guild):
				vc = vcgetbyguild(member.guild)
				if before.channel != after.channel:
					for item in client.voice_clients:
						if item.channel == after.channel:
							if item.is_playing():
								await vc['channel'].send(embed=discord.Embed(colour=0x00FF00, description=f"🔊 {member.display_name} joined {item.channel.name}."))
							else:
								await asyncio.sleep(2)
								SpeechTTS(f"{member.display_name} just joined the {item.channel.name} party.")
								source = discord.PCMVolumeTransformer(discord.FFmpegPCMAudio("athtts.wav"))
								item.play(source, after=lambda e: print('Player error: %s' % e) if e else None)
						if before.channel == item.channel:
							if not item.is_playing():
								SpeechTTS(f"{member.display_name} just left the {item.channel.name} party.")
								source = discord.PCMVolumeTransformer(discord.FFmpegPCMAudio("athtts.wav"))
								item.play(source, after=lambda e: print('Player error: %s' % e) if e else None)
							await vc['channel'].send(embed=discord.Embed(colour=0xFF0000, description=f"🔇 {member.display_name} left {item.channel.name}."))


		@bot.listen('on_ready')
		async def run_check():
			await client.loop.create_task(voice_check())
			AddLog("Running check task.")

	def _end_play(self, ctx, error):
		#AddLog("END_PLAY test")
		#await ctx.send(error)
		try:
			if vcget(ctx)['playlisting']:
				if len(vcget(ctx)['playlist']) > 0:
					self.parse_playlist(ctx, vcrunplaylist(ctx))
				else:
					client.loop.create_task(self.perform_message(ctx, "No more tracks in the playlist."))
		except Exception as e:
			AddLog(f"_end_play error, ignored. ({e})", error=True)
			
	async def perform_message(self, ctx, message):
		await ctx.send(embed=discord.Embed(description=message))

	def parse_playlist(self, ctx, playlist):
		parts = playlist.split(":")
		_func = parts[0]
		_query = parts[1]
		#loop = asyncio.get_event_loop()
		client.loop.create_task(self.perform_message(ctx, "Moving to next track in playlist..."))
		if _func == "yt":
			client.loop.create_task(self._run_yt(ctx, _query))
		if _func == "stream":
			client.loop.create_task(self._run_yt(ctx, _query, True))
		if _func == "search":
			client.loop.create_task(self._run_ytsearch(ctx, _query))
		if _func == "sound":
			client.loop.create_task(self._run_sound(ctx, _query))
		if _func == "tts":
			client.loop.create_task(self._run_tts(ctx, _query))
	
	def format_playlist(self, ctx):
		s = ""
		for item in vcget(ctx)['playlist']:
			items = item.split(":")
			s += f"{items[1]} ~{items[2]}\n"
		return s
		
	async def _play_check(self, ctx):
		if not ctx.author.voice:
			await ctx.send(embed=discord.Embed(colour=0xFF0000, description=f"🔇 You're not in a voice channel!"))
			return False
		if not ctx.voice_client:
			await ctx.author.voice.channel.connect()
			#await ctx.guild.me.edit(nick="[:musical_note:] Athena")
			initvc(ctx)
			await ctx.send(embed=discord.Embed(colour=0x1E90FF, description=f"🔊 Joined voice channel {ctx.author.voice.channel.mention} bound to {ctx.channel.mention} by {ctx.author.mention}."))
			return True
		else:
			chan = vcget(ctx)['channel']
			if chan.name != ctx.channel.name:
				await ctx.send(embed=discord.Embed(colour=0xFF0000, description=f"🔇 I can only be controlled in {chan.mention}."))
				return False

			if vcget(ctx)['djmode'] and ctx.author is not vcget(ctx)['dj']:
				await ctx.send(embed=discord.Embed(colour=0xFF0000, description=f"🔇 DJ mode is enabled. Commands are only accepted from {vcget(ctx)['dj'].mention}."))
				return False

		return True

	def playlisting(self, ctx, sector, track):
		if vcget(ctx)['playlisting']:
			vcaddplaylist(ctx, sector+":"+track+":"+ctx.author.mention)
			loop = asyncio.get_event_loop()
			loop.create_task(self.perform_message(ctx, f"**{track}** added to playlist."))
			return True
		else:
			ctx.voice_client.stop()
			return False

	@commands.guild_only()
	@commands.group(invoke_without_command=True, aliases=['voip', 'v', 'vc'])
	async def voice(self, ctx):
		"""Group for all voice commands."""
		s = ""
		em = discord.Embed(colour=0x1E90FF)

		if ctx.voice_client is None:
			s += "Voice client not active."
		else:
			s += f"🔊 Voice client active in {ctx.voice_client.channel.name}.\n"

		if vcexist(ctx):
			try:
				mlist = []
				for mem in ctx.voice_client.channel.members:
					mlist.append(mem.display_name)
				dj = vcdj(ctx).mention
				chan = vcget(ctx)['channel'].mention
				np = vcget(ctx)['nowplaying']
				
				timedata = ""
				length = vcget(ctx)['length']
				if length != "":
					sec = datetime.datetime.now() - vcget(ctx)['starttime']
					st = str(datetime.timedelta(seconds=sec.seconds))
					timedata = f"({st.split(':')[1]}:{st.split(':')[2]}/{length})"
					
				if vcget(ctx)['playlisting']:
					pl = "PLAYLISTING"
				else:
					pl = "SINGLE TRACK"
				if vcget(ctx)['djmode']:
					djmode = "🔒"
				else:
					djmode = "<:website:480693541186568202>"

				s += f"**{pl}**\n<:alex:396832378313768961> {dj} {djmode}\n<:discord:480693539974545408> {chan}\n🎶 {np} {timedata}"
				s += f"\n👥 {HumanizeList(mlist)}"
				if vcget(ctx)['playlisting'] and len(vcget(ctx)['playlist']) != 0:
					#plays = '\n'.join(vcget(ctx)['playlist'])
					em.add_field(name="Playlist", value=self.format_playlist(ctx))
			except Exception as e:
				s += f"Error parsing voice client data; {e}"

		em.add_field(name="Voice Client", value=s)

		if ctx.author.voice is None:
			em.description = "You are not in a voice channel."
		else:
			em.description = f"🔊 You are in {ctx.author.voice.channel.name}"

		await ctx.send(embed=em)

	@voice.command()
	async def dj(self, ctx):
		"""Toggles between OPEN and DJ mode."""
		if await self._play_check(ctx):
			djm = vctoggledjmode(ctx)
			await ctx.send(embed=discord.Embed(colour=0x1E90FF, description=f"<:alex:396832378313768961> DJ mode is now {djm}."))

	@voice.command(aliases=['playlisting', 'toggleplaylist'])
	async def pl(self, ctx):
		"""Toggles between SINGLE TRACK and PLAYLISTING mode."""
		if await self._play_check(ctx):
			djm = vctoggleplaylisting(ctx)
			await ctx.send(embed=discord.Embed(colour=0x1E90FF, description=f"<:alex:396832378313768961> Playlisting is now {djm}."))

	@voice.command(hidden=True)
	async def playlist(self, ctx):
		pl = '\n'.join(vcget(ctx)['playlist'])
		await ctx.send(embed=discord.Embed(colour=0x1E90FF, description=f"<:alex:396832378313768961> "+pl))

	@voice.command(aliases=['passdj'])
	async def givedj(self, ctx, name):
		"""Passes DJ access to another user."""
		if await self._play_check(ctx):
			for mem in ctx.voice_client.channel.members:
				if name.lower() in mem.display_name.lower() or name.lower() in mem.name.lower():
					if mem.bot:
						await ctx.send(embed=discord.Embed(colour=0xFF0000, description=f"<:alex:396832378313768961> {mem.name} is a bot and can not be made the DJ."))
						return

					if mem is vcdj(ctx):
						await ctx.send(embed=discord.Embed(colour=0xFF0000, description=f"<:alex:396832378313768961> {mem.name} is already the DJ."))
						return

					newdj = vcsetdj(ctx, mem)
					await ctx.send(embed=discord.Embed(colour=0x1E90FF, description=f"<:alex:396832378313768961> New DJ set! {newdj.mention}"))
					return
			await ctx.send(embed=discord.Embed(colour=0xFF0000, description=f"<:alex:396832378313768961> Nobody in the voice channel by that name found."))

	@commands.cooldown(1, 30, BucketType.user)
	@voice.command(aliases=['sc', 'cloud']) #ONCE ACTIVE: Be sure to add playlisting support, add the keys to the playlisting
	async def soundcloud(self, ctx, *, query):
		"""Searches soundcloud for tracks."""

		await ctx.send("Soundcloud is currently unavailable due to the service currently not supplying API access. This command will be updated when the service is active again.\n\n```Due to the high amount of requests recently received, we will no longer be processing API application requests at this time. We are working to re-evaluate our process to make it more efficient.```")
		return
		if await self._play_check(ctx):
			if ctx.voice_client.is_playing() and self.playlisting(ctx, "sc", query):
				return

			await self._run_sc(ctx, query)

	@commands.cooldown(1, 30, BucketType.user)
	@voice.command(aliases=['durl', 'download'])
	async def download_url(self, ctx, *, url):
		"""Plays from a url (almost anything youtube_dl supports)"""
		if await self._play_check(ctx):
			if ctx.voice_client.is_playing() and self.playlisting(ctx, "yt", url):
				return

			await self._run_yt(ctx, url)

	@commands.cooldown(1, 30, BucketType.user)
	@voice.command(aliases=['surl', 'stream'])
	async def stream_url(self, ctx, *, url):
		"""Streams from a url (same as download, but doesn't predownload)"""
		if await self._play_check(ctx):
			if ctx.voice_client.is_playing() and self.playlisting(ctx, "stream", url):
				return

			await self._run_yt(ctx, url, True)

	async def _run_yt(self, ctx, url, streaming=False):
		async with ctx.typing():
			player = await YTDLSource.from_url(url, loop=self.bot.loop, stream=streaming)
			if player.duration_raw > 600:
				await ctx.send(embed=discord.Embed(colour=0xFF0000, description=f'🔇 **{player.title}** duration is *{player.duration}*, longer than the *10 minute limit*, and will not be played.'))
				self._end_play(ctx, None)
				return

			self.ep = partial(self._end_play, ctx)
			ctx.voice_client.play(player, after=self.ep)

		vcplaying(ctx, player.title+" ~"+ctx.author.mention, player.duration_format)
		await ctx.send(embed=discord.Embed(colour=0x1E90FF, description=f'🎶 [{player.duration_format}] {player.title}'))

	@voice.command(aliases=['join', 'j', 'c'])
	async def connect(self, ctx):
		"""Joins the bot to the voice channel you're in."""
		if not ctx.author.voice:
			await ctx.send(embed=discord.Embed(colour=0xFF0000, description=f"🔇 You're not in a voice channel!"))
			return

		if ctx.voice_client:
			chan = vcget(ctx)['channel']
			if chan.name != ctx.channel.name:
				await ctx.send(embed=discord.Embed(colour=0xFF0000, description=f"🔇 I can only be controlled in {chan.mention}."))
				return

			if ctx.voice_client.channel != ctx.author.voice.channel:
				initvc(ctx)
				await ctx.voice_client.disconnect()
				await ctx.author.voice.channel.connect()
				ctx.voice_client.stop()
				await ctx.send(embed=discord.Embed(colour=0x1E90FF, description=f"🔊 Switched to voice channel {ctx.author.voice.channel.mention} bound to {ctx.channel.mention} by {ctx.author.mention}."))
		else:
			initvc(ctx)
			await ctx.author.voice.channel.connect()
			#await ctx.guild.me.edit(nick="[🎵] Athena")
			ctx.voice_client.stop()
			await ctx.send(embed=discord.Embed(colour=0x1E90FF, description=f"🔊 Joined voice channel {ctx.author.voice.channel.mention} bound to {ctx.channel.mention} by {ctx.author.mention}."))

	@voice.command(aliases=['quit', 'dc', 'q'])
	async def disconnect(self, ctx):
		"""Makes the bot leave the voice channel."""
		if not ctx.voice_client or not ctx.author.voice or ctx.author.voice.channel is not ctx.voice_client.channel:
			return

		if await self._play_check(ctx):
			await ctx.send(embed=discord.Embed(description=f'🔊 Disconnected from {ctx.voice_client.channel.mention}.'))
			#await ctx.guild.me.edit(nick="Athena")
			purgevc(ctx)
			await ctx.voice_client.disconnect()

	@commands.cooldown(1, 10, BucketType.user)
	@voice.command()
	async def tts(self, ctx, *, query):
		"""Streams text-to-speech in to the voice channel."""
		if await self._play_check(ctx):
			if ctx.voice_client.is_playing() and self.playlisting(ctx, "tts", query):
				return

			await self._run_tts(ctx, query)

	async def _run_tts(self, ctx, query):
		SpeechTTS(query)
		vcplaying(ctx, query+" ~"+ctx.author.mention)
		source = discord.PCMVolumeTransformer(discord.FFmpegPCMAudio("athtts.wav"))
		self.ep = partial(self._end_play, ctx)
		ctx.voice_client.play(source, after=self.ep)
		await ctx.send(embed=discord.Embed(colour=0x1E90FF, description=f'🎙 {query}'))

	@voice.command(name="fave")
	async def _add_fave(self, ctx, *, fave):
		faves = []
		try:
			faves = getConfig()[str(ctx.author.id)]['voice_faves']
		except:
			await ctx.send("Creating new list...")
		faves.append(fave)
		setConfig(parent=str(ctx.author.id), key="voice_faves", value=faves)
		await ctx.send(f"Added {fave} to your faves list. You have {len(faves)} items.")

	@voice.command(name="find")
	async def _search_fave(self, ctx, *, fave=""):
		faves = []
		try:
			faves = getConfig()[str(ctx.author.id)]['voice_faves']
			if fave == "":
				await ctx.send(HumanizeList(faves))
			else:
				for item in faves:
					if fave in item:
						await ctx.send(item)
		except:
			await ctx.send("You have no faves saved.")
			return
	
	@voice.command(name="del")
	async def _del_fave(self, ctx, *, fave=""):
		faves = []
		try:
			faves = getConfig()[str(ctx.author.id)]['voice_faves']
			if fave == "":
				faves.clear()
				setConfig(parent=str(ctx.author.id), key="voice_faves", value=faves)
			else:
				faves.remove(fave)
				setConfig(parent=str(ctx.author.id), key="voice_faves", value=faves)
		except Exception as e:
			await ctx.send(f"Error: {e}")
			return
						
	@commands.cooldown(1, 30, BucketType.user)
	@voice.command()
	async def search(self, ctx, *, search_term):
		"""Searches youtube for a video and streams it in to the voice channel."""
		if await self._play_check(ctx):
			if ctx.voice_client.is_playing() and self.playlisting(ctx, "search", search_term):
				return
			
			try:
				await self._run_ytsearch(ctx, search_term)
			except:
				await ctx.send("That request hit an error from the download server, maybe try again.")

	async def _run_ytsearch(self, ctx, search_term):
		decoder = minerva.Pantheon()
		encoded_key = "wqnCrsOqw4nCtMOewrTDisOfw5LCm8KXwqbDjcOTw5HDjcOBw4fDjMOdwp3DosKaw5nDi8OSw4zDm8OHwp_CksK9w4vDmcK-w4bDjsOg"
		decoded_key = decoder.decode(encoded_key)
		base_url = "https://www.googleapis.com/youtube/v3/search?"
		query = f"q={search_term.replace(' ', '+')}&"
		maxresults = "maxResults=1&"
		part = "part=snippet&"
		key = f"key={decoded_key}"
		r = requests.get(base_url+query+maxresults+part+key).text
		data = json.loads(r)
		AddLog(str(data))
		url = ""
		try:
			title = data['items'][0]['snippet']['channelTitle']
			video = data['items'][0]['id']['videoId']
			url = f"https://www.youtube.com/watch?v={video}"
		except KeyError:
			await ctx.send(embed=discord.Embed(colour=0xFF0000, description=f"🔇 Search error, try a different query."))
			return

		async with ctx.typing():
			player = await YTDLSource.from_url(url, loop=self.bot.loop, stream=True)
			if player.duration_raw > 600:
				await ctx.send(embed=discord.Embed(colour=0xFF0000, description=f'🔇 **{player.title}** duration is *{player.duration}*, longer than the *10 minute* limit, and will not be played.'))
				self._end_play(ctx, None)
				return

			self.ep = partial(self._end_play, ctx)
			ctx.voice_client.play(player, after=self.ep)
		vcplaying(ctx, player.title+" ~"+ctx.author.mention, player.duration_format)
		await ctx.send(embed=discord.Embed(colour=0x1E90FF, description=f'🎶 [{player.duration_format}] {player.title}'))

	@commands.cooldown(1, 5, BucketType.user)
	@voice.command()
	async def sound(self, ctx, query):
		"""Accesses the soundboard and streams it in to the voice channel.
		Uses same sounds as the !playsound command.
		See !playsound for the sound list."""
		if await self._play_check(ctx):
			if ctx.voice_client.is_playing() and self.playlisting(ctx, "sound", query):
				return

			await self._run_sound(ctx, query)

	async def _run_sound(self, ctx, query):
		found = False
		soundspath = scriptdir+"Sounds/"
		for file in os.listdir(soundspath):
			filename = os.fsdecode(file)
			if query.lower() in filename.lower():
				source = discord.PCMVolumeTransformer(discord.FFmpegPCMAudio(soundspath+filename))
				self.ep = partial(self._end_play, ctx)
				ctx.voice_client.play(source, after=self.ep)
				await ctx.send(embed=discord.Embed(colour=0x1E90FF, description=f'🔊 {query}'))
				vcplaying(ctx, filename+" ~"+ctx.author.mention)
				found=True

		if not found:
			await ctx.send(embed=discord.Embed(colour=0xFF0000, description=f'Sound file not found.'))

	@voice.command()
	async def pause(self, ctx):
		"""Toggles pause of current stream."""
		if not ctx.voice_client or not ctx.author.voice or ctx.author.voice.channel is not ctx.voice_client.channel:
			return

		if await self._play_check(ctx):

			if ctx.voice_client.is_paused():
				ctx.voice_client.resume()
				await ctx.send(embed=discord.Embed(description=f"▶ Resumed."))
			else:
				ctx.voice_client.pause()
				await ctx.send(embed=discord.Embed(description=f"⏸ Paused."))

	@voice.command()
	async def volume(self, ctx, volume: int):
		"""Changes the player's volume."""
		if not ctx.voice_client or not ctx.author.voice or ctx.author.voice.channel is not ctx.voice_client.channel:
			return
		
		if await self._play_check(ctx):	
			if volume > 100:
				volume = 100
			elif volume < 0:
				volume = 0	
			ctx.voice_client.source.volume = (volume/100)
			await ctx.send(f"🔉 Changed volume to {volume}%")

	@voice.command()
	async def stop(self, ctx):
		"""Ends the current stream."""
		if not ctx.voice_client or not ctx.author.voice or ctx.author.voice.channel is not ctx.voice_client.channel:
			return
		if await self._play_check(ctx):
			vcplaying(ctx, "None")
			ctx.voice_client.stop()

async def voice_check():
	while True:
		await asyncio.sleep(30)
		for vc in client.voice_clients:
			if len(vc.channel.members) < 2:
				AddLog("VC: Timing out.")
				#await vc.guild.me.edit(nick="Athena")
				c = vcgetbyguild(vc.guild)['channel']
				AddLog(c.name)
				await c.send(embed=discord.Embed(colour=0xFF0000, description=f"🔇 Automatically disconnected from {vc.channel.name}.\n**Reason**: Empty channel."))
				purgevcbyguild(vc.guild)
				await vc.disconnect()
			else:
				if vcgetbyguild(vc.guild)['dj'] not in vc.channel.members:
					rndj = vcgnewdj(vc.guild)
					print(rndj)
					if not rndj:
						await vcgetbyguild(vc.guild)['channel'].send(embed=discord.Embed(colour=0xFF0000, description=f"🔇 Automatically disconnected from {vc.channel.name}.\n**Reason**: DJ left the channel and no humans were available to take their place."))
						purgevcbyguild(vc.guild)
						await vc.disconnect()
						return


					await vcgetbyguild(vc.guild)['channel'].send(embed=discord.Embed(colour=0xFF0000, description=f"The DJ left the room... choosing new one."))
					#newdj = vcgnewdj(vc.guild)
					await vcgetbyguild(vc.guild)['channel'].send(rndj.mention+" has become the new channel DJ.")

async def get_current_vc(ctx):
	for vc in client.voice_clients:
		if vc.channel.id == ctx.author.voice.channel.id:
			return vc

def setup(bot):
	global client
	bot.add_cog(voice(bot))
	discord.opus.load_opus('opus/libopus.so')
	client = bot
