import discord
from discord.ext import commands
from discord.ext.commands.cooldowns import BucketType
import humanfriendly
import socket
import subprocess
import requests
import re
import sys
import os
import random
import io
import textwrap
import json
import traceback
import datetime
import fnmatch
import asyncio
import contextlib
from contextlib import redirect_stdout
import minerva
from persona import *
from athena_common import *
from typing import Union
import emoji
import inspect
import copy
from textblob import TextBlob, Word
from textblob.exceptions import NotTranslated, TranslatorError
from PIL import Image, ImageFilter

def to_emoji(c):
	base = 0x1f1e6
	return chr(base + c)
	
class Poll:
	def __init__(self, question:str, options:list):
		self.question = question
		self.options = options
		self.votes = {}
		self.populate_votes()
	
	def populate_votes(self):
		i = 0
		for item in self.options:
			self.votes[i] = {'string':item, 'voters':[]}
			i += 1
			
	def vote(self, voter, voted_for:int):
		self.votes[voted_for].append(voter)
		return self.votes[voted_for]
	
	def calculate_win(self):
		win = {"winner":-1, "total": 0}
		for item in self.votes.keys():
			if len(self.votes[item]) > win['total']:
				win['winner'] = item
				win['total'] = len(self.votes[item])
		
		if win['total'] > 0:
			return win
		else:
			return None

aecol = 0x7F7F7F	
	   
class utility:
	"""Specific utility commands."""
	def __init__(self, bot):
		self.bot = bot
		self._last_result = None
		self.sessions = set()
		
	def cleanup_code(self, content):
		if content.startswith('```') and content.endswith('```'):
			return '\n'.join(content.split('\n')[1:-1])
		return content.strip('` \n')

	def get_syntax_error(self, e):
		if e.text is None:
			return f'```py\n{e.__class__.__name__}: {e}\n```'
		return f'```py\n{e.text}{"^":>{e.offset}}\n{e.__class__.__name__}: {e}```'

	@commands.command()
	@commands.guild_only()
	@is_tier(1)	
	async def poll(self, ctx, *, question):
		"""Interactively creates a poll with the following question.
		To vote, use reactions!
		"""

		# a list of messages to delete when we're all done
		messages = [ctx.message]
		answers = []

		def check(m):
			return m.author == ctx.author and m.channel == ctx.channel and len(m.content) <= 100

		for i in range(20):
			messages.append(await ctx.send(f'Say poll option or cancel to publish poll.'))

			try:
				entry = await self.bot.wait_for('message', check=check, timeout=60.0)
			except asyncio.TimeoutError:
				break

			messages.append(entry)

			if entry.clean_content.startswith(f'cancel'):
				break

			answers.append((to_emoji(i), entry.clean_content))

		try:
			await ctx.channel.delete_messages(messages)
		except:
			pass # oh well

		answer = '\n'.join(f'{keycap}: {content}' for keycap, content in answers)
		actual_poll = await ctx.send(f'{ctx.author.mention} asks: {question}\n`Press reaction letters to vote.`\n`Use !checkvotes %i to check results.`\n\n{answer}')
		new = actual_poll.content.replace("%i", str(actual_poll.id))
		await actual_poll.edit(content=new)
		for emoji, _ in answers:
			await actual_poll.add_reaction(emoji)

	@poll.error
	async def poll_error(self, ctx, error):
		if isinstance(error, commands.MissingRequiredArgument):
			return await ctx.send('Missing the question.')
	
	
	@commands.command()
	@commands.guild_only()
	@is_tier(1)
	async def quickpoll(self, ctx, *questions_and_choices: str):
		"""Makes a poll quickly.
		The first argument is the question and the rest are the choices.
		Each option is seperated by spaces, so if you need spaces in a single option, enclose the option in quote marks.
		Examples:
		!quickpoll Borks? Yes No Maybe
		!quickpoll "Do you like bots?" "They're Okay" No Maybe
		"""

		if len(questions_and_choices) < 3:
			return await ctx.send('Need at least 1 question with 2 choices.')
		elif len(questions_and_choices) > 21:
			return await ctx.send('You can only have up to 20 choices.')

		perms = ctx.channel.permissions_for(ctx.me)
		if not (perms.read_message_history or perms.add_reactions):
			return await ctx.send('Need Read Message History and Add Reactions permissions.')

		question = questions_and_choices[0]
		choices = [(to_emoji(e), v) for e, v in enumerate(questions_and_choices[1:])]

		body = "\n".join(f"{key}: {c}" for key, c in choices)
		qpoll = await ctx.send(f'{ctx.author.mention} asks: {question}\n`Press reaction letters to vote.`\n`Use !checkvotes %i to check results.`\n\n{body}')
		new = qpoll.content.replace("%i", str(qpoll.id))
		await qpoll.edit(content=new)
		for emoji, _ in choices:
			await qpoll.add_reaction(emoji)
		
		try:
			await ctx.message.delete()
		except:
			pass
	
	@commands.command()
	async def checkvotes(self, ctx, message_id):
		"""Simple method to check a poll messages reactions."""
		try:
			msg = await ctx.channel.get_message(message_id)
			lines = msg.content.split("\n")
			del lines[1]
			del lines[1]
			
			lines.append("\n")
			
			i = 2
			for item in msg.reactions:
				lines[i] = lines[i]+" x"+str(item.count - 1)
				i += 1
			em = discord.Embed(description=f"*[Jump to post]({msg.jump_url})*")
			await ctx.send('\n'.join(lines), embed=em)	
		except:
			await ctx.send("Probably not a valid poll message.")

		
	@commands.command()
	async def analyze(self, ctx, *, text: str):
		"""Text analysis (WIP)"""
		with contextlib.suppress(RuntimeError):
			await ctx.channel.trigger_typing()
			blob = TextBlob(text)
			tags = blob.tags
			ph = blob.noun_phrases
			se = blob.sentiment.subjectivity
			po = blob.sentiment.polarity
			await ctx.send(f"Tags of the text: {tags}\nNoun-Phrases: {ph}\nSubjectivity: {se}\nPolarity: {po}")
	
	@commands.command()
	async def singular(self, ctx, word):
		"""Gets the singular version of a word."""
		with contextlib.suppress(RuntimeError):
			await ctx.channel.trigger_typing()
			f = Word(word).singularize()
			await ctx.send(f)

	@commands.command()
	async def lemmatize(self, ctx, word):
		"""Lemmatizes a word."""
		await ctx.channel.trigger_typing()
		with contextlib.suppress(RuntimeError):
			f = Word(word).lemmatize()
			await ctx.send(f)
		
	@commands.command()
	async def plural(self, ctx, word):
		"""Gets the plural version of a word."""
		with contextlib.suppress(RuntimeError):
			await ctx.channel.trigger_typing()
			f = Word(word).pluralize()
			await ctx.send(f)
				
	@commands.command()
	async def spell(self, ctx, word:str):
		"""Will try to spell `word` correctly."""
		with contextlib.suppress(RuntimeError):
			await ctx.channel.trigger_typing()
			w = Word(word)
			sc = w.spellcheck()
			await ctx.send(f"`{sc[0][0]}`")

	@commands.command()
	async def define2(self, ctx, word:str):
		"""Tries to get a definition of `word`."""
		with contextlib.suppress(RuntimeError):
			await ctx.channel.trigger_typing()
			blob = Word(word)
			defs = blob.definitions
			s = ""
			if len(defs) > 0:
				for item in defs:
					s += item+"\n"
				await ctx.send(s)	
			else:
				await ctx.send("None found.")
				
	@commands.command()
	async def correct(self, ctx, *, text:str):
		"""Tries to spellcheck and correct `text`"""
		with contextlib.suppress(RuntimeError):
			await ctx.channel.trigger_typing()
			blob = TextBlob(text)
			await ctx.send(blob.correct())	
		
	@commands.command()
	async def lang(self, ctx, *, text:str):
		"""Tries to detect what language your speaking.\nWill return the country code."""
		with contextlib.suppress(RuntimeError):
			await ctx.channel.trigger_typing()
			blob = TextBlob(text)
			await ctx.send(f"I think that language is {blob.detect_language()}.")	
						
	@commands.command()
	async def tr(self, ctx, lang:str, *, text:str):
		"""A basic translator.
		Uses country codes for lang.
		Example: tr en madre de dios, tr es mother of god"""
		with contextlib.suppress(RuntimeError):
			await ctx.channel.trigger_typing()
			blob = TextBlob(text)
			try:
				await ctx.send(blob.translate(to=lang))	
			except NotTranslated:
				await ctx.send("The text was not translated. Are you trying to translate to and from the same language, or the word couldn't be translated?")
			except TranslatorError:
				await ctx.send("There was an error on the translator side. Maybe try again later?")
			
	@commands.is_owner()				
	@commands.command(hidden=True, enabled=False)
	async def fileinfo(self, ctx, path:str):
		if "~" in path:
			path = path.replace("~", "/home/kaiz0r/")	
		if "#" in path:
			path = path.replace("#", "/home/kaiz0r/PythenaRewrite/")
		
		if "/" not in path:
			path = "/home/kaiz0r/PythenaRewrite/" + path
			
		ff = os.stat(path)
		s = "File Info for " + path.split("/")[-1]
		
		s += "\nSize: " + humanfriendly.format_size(ff.st_size)
		
		a_time_raw = str(datetime.datetime.fromtimestamp(ff.st_atime))
		m_time_raw = str(datetime.datetime.fromtimestamp(ff.st_mtime))
		
		a_date = a_time_raw.split(" ")[0]
		_time = a_time_raw.split(" ")[1]
		a_time = _time.split(":")[0]+":"+_time.split(":")[1]

		m_date = m_time_raw.split(" ")[0]
		_timem = m_time_raw.split(" ")[1]
		m_time = _timem.split(":")[0]+":"+_timem.split(":")[1]
			
		s += f"\nLast Accessed on {a_date} at {a_time}" 
		
		s += f"\nLast Modified on {m_date} at {m_time}"
		i = 0
		try:
			with open(path) as f:
				l = f.readlines()
				for item in l:
					i += 1
		except:
			pass
		
		if i > 0:
			s += f"\nContains {i} lines."
			
		await ctx.send(s)
		PersonaCommand()
		
	@access_check()
	@commands.command(hidden=True)
	async def msg_del(self, ctx, message_id, when_date="", when_time=""):
		async for message in ctx.channel.history():
			if message.id == message_id:
				await message.delete()
			
	@commands.command()
	async def rand(self, ctx, limit=10):
		"""Rolls a random number.
		If no limit is set, 10 is the default."""
		await ctx.channel.trigger_typing()
		await ctx.send(f"You rolled a {random.randrange(0, limit)} out of {limit}.")
		PersonaCommand()

	@commands.command()
	async def linksplit(self, ctx, *msg):
		'''Splits provided links into chunks of 5, so that they can all embed at once.

		Note that this splits on *all* links, regardless of embeddability.
		'''
		await ctx.message.delete()
		# This regex needs to never exist again.
		r = re.compile(r"https?://(([\w{0}])+\.)+([/{0}])*\.?\w*((#|\?)([\w{0}=])+)?(&[\w{0}=]+)*".format("$\-+!*'(),"))
		temp = [link for link in msg if r.match(link)]
		for msg in [temp[i:i + 5] for i in range(0, len(temp), 5)]:
			await ctx.send('\n'.join(msg))
					
	@commands.command()
	async def emote(self, ctx, emote):
		"""I can emote!
		Posts a custom server emote based on the name. 
		Note: Discord Bots have Nitro access, meaning I can post emotes from other servers, and also animated emotes."""
		await ctx.channel.trigger_typing()
		found = False
		for emot in client.emojis:
			if emote.lower() in emot.name.lower():
				found = True
				if emot.animated:
					await ctx.send(f"<a:{emot.name}:{emot.id}>")
				else:
					await ctx.send(f"<:{emot.name}:{emot.id}>")
		
		if not found:
			emojis = emoji.unicode_codes.EMOJI_UNICODE
			for item in emojis.keys():
				if emote.lower() in item.lower():
					await ctx.send(emoji.emojize(item))
					found = True
					return
		
		if ctx.guild.me.permissions_in(ctx.channel).administrator or ctx.guild.me.permissions_in(ctx.channel).manage_messages:
			await ctx.message.delete()
			
			
		if not found:
			await ctx.send("No emotes found.")
		PersonaCommand()
		
	@commands.command()
	async def avatar(self, ctx, membername=""):
		"""
		Displays a members avatar.
		
		<membername> should be a part of a members name.
		"""
		await ctx.channel.trigger_typing()
		if membername == "":
			await ctx.send(ctx.author.avatar_url_as(format='jpeg'))
		else:
			for member in list(client.get_all_members()):
				if membername.lower() == member.name.lower() or membername.lower() == member.display_name.lower():
					await ctx.send(member.name+" ("+member.display_name+") "+member.avatar_url_as(format='jpeg'))
					return
					
			await ctx.send("No users found.")
		PersonaCommand()			

	@commands.command()
	async def avatar2(self, ctx, *, membermentions=""):
		"""
		Displays a members avatar.
		
		Mention people to show their avatar.
		"""
		await ctx.channel.trigger_typing()
		lim = 2
		access_level = get_tier(ctx.author)
		if access_level == 1:
			lim = 3
		elif access_level == 2:
			lim = 4
		elif access_level == 3:
			lim = 5
		if len(ctx.message.mentions) > lim:
			await ctx.send("Too many mentions, don't be a dick.")
			return
			
		if membermentions == "":
			await ctx.send(ctx.author.avatar_url_as(format='jpeg'))
		else:
			for member in ctx.message.mentions:
				await ctx.send(member.name+" ("+member.display_name+") "+member.avatar_url_as(format='jpeg'))
		PersonaCommand()			
	
	@commands.command(name='inviteme')
	async def _invie(self, ctx):
		"""Joins a server."""
		await ctx.channel.trigger_typing()
		perms = discord.Permissions.none()
		perms.read_messages = True
		perms.external_emojis = True
		perms.send_messages = True
		perms.manage_roles = True
		perms.manage_channels = True
		perms.ban_members = True
		perms.kick_members = True
		perms.manage_messages = True
		perms.embed_links = True
		perms.read_message_history = True
		perms.attach_files = True
		perms.add_reactions = True
		await ctx.send(f'<{discord.utils.oauth_url(384007758527332363, perms)}>')
		PersonaCommand()
					
	@commands.cooldown(1, 60, BucketType.user)		
	@commands.command(hidden=True)
	async def getemojiurl(self, ctx, num_of_emoji_urls_to_get : int = 1, channel_id : int = None):
		"""gets emoji urls from messages containing emojis"""
		await ctx.channel.trigger_typing()
		if num_of_emoji_urls_to_get > 10:
			await ctx.send("Choose a number lower than 10, otherwise this can get very spammy.")
			return
			
		list_of_ids = []
		f = ""
		num_of_emoji_urls = 0
		emoji_re = re.compile(r"<(a)?:.+:\d{18}>")
		id_re = re.compile(r"\d{18}")
		channel = channel_id or ctx.channel.id
		channel = self.bot.get_channel(channel)
		async for message in channel.history(limit = 5000):
			if num_of_emoji_urls == num_of_emoji_urls_to_get:
				break
			emoji = emoji_re.search(message.content)
			if emoji is not None:
				num_of_emoji_urls += 1
				id = id_re.search(emoji.group())
				list_of_ids.append(id.group())
		for emoji_id in list_of_ids:
			await ctx.send(f"https://cdn.discordapp.com/emojis/{emoji_id}.png?v=1\n")
		PersonaCommand()
	
	@is_tier(1)	
	@commands.cooldown(1, 20, BucketType.user)	
	@commands.command()
	async def pins(self, ctx, show_num=-1):
		await ctx.channel.trigger_typing()
		pins_list = await ctx.channel.pins()
		if len(pins_list) == 0:
			await ctx.send("No pins in this channel.")
			return
			
		em = discord.Embed()
			
		if show_num > -1: # Show a specific pin
			
			em_title = f"📌 #{show_num}/{len(pins_list)-1} [By {pins_list[show_num].author.name} on {pins_list[show_num].created_at.day}/{pins_list[show_num].created_at.month}/{pins_list[show_num].created_at.year} {pins_list[show_num].created_at.hour}:{pins_list[show_num].created_at.minute}]"
			
			em_content = pins_list[show_num].content
				
			em_url = pins_list[show_num].jump_url
			try:	
				em.add_field(name=em_title, value=f"{em_content}\n*[Jump to post]({em_url})*")
				await ctx.send(embed=em)
			except:
				await ctx.send(em_content)

		elif show_num < 0:
			intie = 0
			f = ""
			for item in pins_list:
				if len(item.content) < 1024:
					intie += 1	
					em.add_field(name=f"📌 #{intie}/{len(pins_list)-1} [By {item.author.name} on {item.created_at.day}/{item.created_at.month}/{item.created_at.year} {item.created_at.hour}:{item.created_at.minute}]", value=f"{item.content}")
				else:
					fl = item.content.replace("@everyone", "everyone")
					await ctx.send(fl)
			if intie > 0:	
				await ctx.send(embed=em)
		else:
			pinz = ""
			intie = 0
			for item in pins_list:
				pinz += f"📌 #{intie}/{len(pins_list)-1} [By {item.author.name} on {item.created_at.day}/{item.created_at.month}/{item.created_at.year} {item.created_at.hour}:{item.created_at.minute}]\n"
				intie += 1
			await ctx.send(pinz)	
		PersonaCommand()
		
	@commands.command(name="terminal", hidden=True)
	@commands.is_owner()
	async def _terminal(self, ctx, *command):
		await ctx.channel.trigger_typing()
		test = subprocess.run(command, stdout=subprocess.PIPE)
		output = str(test.stdout,"latin-1")
		await ctx.send(f"{output}")
		
	@commands.command(hidden=True)
	async def regexp(self, ctx, expression, applied_to):
		await ctx.channel.trigger_typing()
		p = re.compile(expression)
		await ctx.send(p.match(applied_to))
		
	@commands.cooldown(1, 10, BucketType.user)
	@commands.command(aliases=['talk'], help="Parses text through espeak TTS engine and sends the dumped wav file.", brief="Listen to my sexy speech.")
	async def tts(self, ctx, *, text):
		await ctx.channel.trigger_typing()
		SpeechTTS(text)
		await ctx.channel.send(file=discord.File(scriptdir+"athtts.wav"))
		PersonaCommand()	
			
	@commands.cooldown(1, 5, BucketType.user)
	@commands.group(aliases=['sound', 'snd', 'soundboard', 'sb'], help="Sends a meme sound file.\nSending no sound name will display ALL available sounds. To search for specific sounds, see the -s sub command.\nYou can request multiple sounds at once by inputting the different names with a space between, example; `!sound diehard dixie disco` will pull diehard, dixie, and disco sounds.", brief="Jesus, how many meme sounds do we need?", invoke_without_command=True)
	async def playsound(self, ctx, *, sounds=""):
		await ctx.channel.trigger_typing()
		sound = sounds.split(" ")
		soundspath = scriptdir+"Sounds/"
		if sound[0] == "":
			em = discord.Embed()
			size = 0
			count = 0
			soundsdict = {'#':[], 'a':[],'b':[],'c':[],'d':[],'e':[],'f':[],'g':[],'h':[],'i':[],'j':[],'k':[],'l':[],'m':[],'n':[],'o':[],'p':[],'q':[],'r':[],'s':[],'t':[],'u':[],'v':[],'w':[],'x':[],'y':[],'z':[]}
			for file in os.listdir(soundspath):
				count += 1
				filename = os.fsdecode(file)
				size += os.stat(soundspath+filename).st_size
				if filename[0].isdigit():
					soundsdict['#'].append(filename[:-4])
				else:
					soundsdict[filename[0].lower()].append(filename[:-4])
			
			for soundletter in list(soundsdict):
				if len(soundsdict[soundletter]) > 0:
					soundsdict[soundletter].sort()
					em.add_field(name=soundletter, value=' '.join(soundsdict[soundletter]))
					
			await ctx.send(f"<:loaded:480337167080488960> Contents of `~/Sounds/`..\n**{count}** files, totalling **{humanfriendly.format_size(size)}**", embed=em) 
			PersonaCommand()
			#+HumanizeList(soundslist))
		else:
			lim = 5
			access_level = get_tier(ctx.author)
			if access_level == 1:
				lim = 6
			elif access_level == 2:
				lim = 7
			elif access_level == 3:
				lim = 10
			if len(sound) > lim:
				await ctx.send(f"<:loadother:480426177748533249> You can only request {lim} sounds at a time. The last {len(sound) - lim} sounds were ignored.")
				sound = sound[:lim]
				
			for item in sound:
				try:
					ext = ""
					for file in os.listdir(soundspath):
						filename = os.fsdecode(file)
						if sound[sound.index(item)].lower() in filename.lower():

							if filename.endswith(".wav"):
								ext = "wav"
							elif filename.endswith(".mp3"):
								ext = "mp3"
							
					await ctx.send(f"<:loaded:480337167080488960>", file=discord.File(soundspath+sound[sound.index(item)]+"."+ext))
					PersonaCommand()
				except Exception as e:
					await ctx.send(f"<:loadfail:480337167038676992> That sound, was not found.")
					PersonaError()
		
	@commands.cooldown(1, 5, BucketType.user)
	@playsound.command(name="-search", help="Searches a meme sound file.", brief="Jesus, how many meme sounds do we need?", invoke_without_command=True)
	async def _sound_search(self, ctx, sound):
		await ctx.channel.trigger_typing()
		lmsg = await ctx.send("<a:loading:480693540616273922> Searching...")
		soundspath = scriptdir+"Sounds/"
		em = discord.Embed()
		c = 0
		soundsdict = {'#':[], 'a':[],'b':[],'c':[],'d':[],'e':[],'f':[],'g':[],'h':[],'i':[],'j':[],'k':[],'l':[],'m':[],'n':[],'o':[],'p':[],'q':[],'r':[],'s':[],'t':[],'u':[],'v':[],'w':[],'x':[],'y':[],'z':[]}
		for file in os.listdir(soundspath):
			filename = os.fsdecode(file)
			
			if sound in filename:
				c += 1
				if filename[0].isdigit():
					soundsdict['#'].append(filename[:-4])
				else:
					soundsdict[filename[0].lower()].append(filename[:-4])
		
		if c == 0:
			await lmsg.edit(content="<:loadother:480422495585959939> No sounds found.")
			PersonaCommand()
		else:
			for soundletter in list(soundsdict):
				if len(soundsdict[soundletter]) > 0:
					soundsdict[soundletter].sort()
					em.add_field(name=soundletter, value=HumanizeList(soundsdict[soundletter]))
					
			await lmsg.edit(content=f"<:loaded:480337167080488960> Contents of `~/Sounds/` with filter `{sound}` [{c} results]...", embed=em) #+HumanizeList(soundslist))
			PersonaCommand()	
			
	@playsound.command(name="-i", enabled=False, hidden=True)
	async def _sound_install(self, ctx):
		lmsg = await ctx.send("<a:loading:480693540616273922> Starting sound installer...")
		await asyncio.sleep(1)
		files_list = []
		dlpath = '/home/kaiz0r/Downloads/'
		topath = '/home/kaiz0r/PythenaRewrite/Sounds/'
		for file in os.listdir(dlpath):
			filename = os.fsdecode(file)
			if filename.endswith(".wav") or filename.endswith(".mp3"):
				files_list.append(filename)
				print(f"[{len(files_list)}] Moving {dlpath+filename} -> {topath}/{filename}")
				os.rename(dlpath+filename, topath+"/"+filename)	
		
		if len(files_list) > 0:
			await lmsg.edit(content=f"<:loaded:480337167080488960> {len(files_list)} sound files installed.", embed=discord.Embed(description=f"{HumanizeList(files_list)}"))
			PersonaCommand()
		else:
			await lmsg.edit(content="<:loadfail:480337167038676992> No new files installed.")

	@commands.is_owner()
	@playsound.command(name="-rename")
	async def _sound_rename(self, ctx, original, new_name):
		soundpath = scriptdir+"Sounds/"
		for file in os.listdir(soundpath):
			filename = os.fsdecode(file)
			if original in filename:
				if filename.endswith(".wav"):
					os.rename(soundpath+filename, soundpath+new_name+".wav")	
					await ctx.send(f"Rename Process: {soundpath+filename} > {soundpath}{new_name}.wav")
				elif filename.endswith(".mp3"):
					os.rename(soundpath+filename, soundpath+new_name+".mp3")
					await ctx.send(f"Rename Process: {soundpath+filename} > {soundpath}{new_name}.mp3")
					
	@commands.is_owner()
	@playsound.command(name="-del")
	async def _sound_delete(self, ctx, sound_name):
		soundpath = scriptdir+"Sounds/"
		for file in os.listdir(soundpath):
			filename = os.fsdecode(file)
			if sound_name == filename:
				os.remove(soundpath+filename)	
				await ctx.send(soundpath+filename+" deleted.")

	@playsound.command(name="-rand")
	async def _sound_rand(self, ctx):
		await ctx.channel.trigger_typing()
		soundpath = scriptdir+"Sounds/"
		sounds = []
		for file in os.listdir(soundpath):
			filename = os.fsdecode(file)
			sounds.append(soundpath+filename)
		
		await ctx.send(f"<:loaded:480337167080488960> Here's a random sound.", file=discord.File(random.choice(sounds)))
		PersonaCommand()
												
	@commands.command(help="Shows the raw text of your input.", brief="Keepin' it real.", hidden=True)
	async def raw(self, ctx, *, text):
		await ctx.channel.trigger_typing()
		await ctx.send(f"```{text}```")
		PersonaCommand()
		
	@commands.command(help="Calculates the value.", brief="Goin' back to my roots of bein' a calculator.")
	async def calc(self, ctx, *, text):
		await ctx.channel.trigger_typing()
		text = text.replace("x", "*")
		await ctx.send(f"```{eval(text)}```")
		PersonaCommand()
					
	@commands.group(aliases=['invites', 'inv'], help="Manages the current guild invites.\nIf extracommand is create, creates a temporary invite.", brief="Get over here.", invoke_without_command=True)
	async def invite(self, ctx):
		await ctx.channel.trigger_typing()
		ls = await ctx.guild.invites()
		lstr = ""
		
		num = 0
		skipped = 0
		for invite in ls:
			num += 1
			if num > 9:
				AddLog("Skipped one.")
				skipped += 1
			else:
				iu = invite.url
				inv = invite.inviter.name
				ichan = invite.channel.name
				iuses = invite.uses
				imaxuses = invite.max_uses
				if imaxuses == 0:
					imaxusesstr = "∞"
				else:
					imaxusesstr = str(imaxuses)
				iage = invite.max_age
				
				iagestr = ""
				if iage != 0:
					iagestr = "[Expires in "+humanfriendly.format_timespan(iage, True)+"]"
					
				lstr += "🔸 Created by "+inv+" for #"+ichan+" ("+str(iuses)+" / "+imaxusesstr+") "+iagestr+"\n"+iu+" \n"
				
			
		em = discord.Embed(colour=aecol)
		em.add_field(name="**Invite List for "+ctx.guild.name+"**", value=f"{lstr}\n**+ {skipped} more invites that couldn't fit.**")
		await ctx.send(embed=em)
		PersonaCommand()
		
	@commands.cooldown(1, 600, BucketType.guild)	
	@invite.command(name="create")
	async def _create_invite(self, ctx):
		await ctx.channel.trigger_typing()
		newinv = await ctx.channel.create_invite(reason="By command, requested by "+ctx.author.display_name, max_age=5600, max_uses=5)
		await ctx.send("Invite created.\n"+newinv.url)
		PersonaCommand()
		
	@commands.is_owner()
	@commands.command(hidden=True)
	async def announce(self, ctx, *, announcement:str):
		await ctx.send(f"Dispatching to webhook:\n{announcement}")
		await AthNotif(client, announcement, channel=396716932436328468)
		
	#@commands.is_owner() #has_permissions(administrator=True)
	@is_tier(3)
	@commands.command(help="Evaluates a string.", brief="I'll give you a good evaluation.", pass_context=True, hidden=True, name='eval', aliases=['script', 'py', 'python', 'exec'])
	async def _eval(self, ctx, *, body: str):
		await ctx.channel.trigger_typing()
		restrictions = ['import os', "input("]
		
		if ctx.author.id != 206903283090980864:
			for item in restrictions:
				if item in body:
					await ctx.send(f"{msg_generator('denied_sec')}\n**Restriction**: {item}")
					return
				
		env = {
			'bot': self.bot,
			'ctx': ctx,
			'channel': ctx.channel,
			'author': ctx.author,
			'guild': ctx.guild,
			'message': ctx.message,
			'_': self._last_result,
			
			"discord": discord,
			"commands": commands,

			# utilities
			"_get": discord.utils.get,
			"_find": discord.utils.find,
		}

		env.update(globals())

		body = self.cleanup_code(body)
		stdout = io.StringIO()

		to_compile = f'async def func():\n{textwrap.indent(body, "  ")}'

		try:
			exec(to_compile, env)
		except Exception as e:
			return await ctx.send(f'```py\n{e.__class__.__name__}: {e}\n```')
			await ctx.message.add_reaction('\u2716')
			
		func = env['func']
		try:
			with redirect_stdout(stdout):
				ret = await func()
		except Exception as e:
			value = stdout.getvalue()
			await ctx.send(f'```py\n{value}{traceback.format_exc()}\n```')
			await ctx.message.add_reaction('\u2716')
		else:
			value = stdout.getvalue()

			if ret is None:
				if value:
					await ctx.send(f'```py\n{value}\n```')
			else:
				self._last_result = ret
				await ctx.send(f'```py\n{value}{ret}\n```')


	@commands.command(name="sh", aliases=["bash"])
	@commands.is_owner()
	async def shell(self, ctx, *, cmd):
		"""Run a subprocess using shell."""
		result = ""
		async with ctx.typing():
			result = await run_subprocess(cmd)
		if len(result) < 1800:
			await ctx.send(f"```{result}```")
		else:
			x = 1800
			s = [result[i: i + x] for i in range(0, len(result), x)]
			for item in s:
				await ctx.send(f"```{item}```")
				
	@commands.command()
	@commands.is_owner()
	async def download(self, ctx, filepath):
		"""Attaches a stored file"""
		if "/" in filepath:
			file = scriptdir+filepath
			filename = filepath.split("/")[-1]
		else:
			file = filepath
			filename = filepath
		with open(file, "rb") as f:
			try:
				await ctx.send(file=discord.File(f, filename))
			except FileNotFoundError:
				await ctx.send(f"no such file: {file}")

	@commands.command()
	@commands.is_owner()
	async def upload(self, ctx, other_dir=""):
		"""Upload a file"""
		attachments = ctx.message.attachments

		if not attachments:
			await ctx.send("No attachment found! Please upload it in your next message.")

			def check(msg_: discord.Message) -> bool:
				return msg_.channel.id == ctx.channel.id and msg_.author.id == ctx.author.id and msg_.attachments

			try:
				msg = await self.bot.wait_for("message", check=check, timeout=60 * 10)
			except asyncio.TimeoutError:
				return await ctx.send("Stopped waiting for file upload, 10 minutes have passed.")
	
			attachments = msg.attachments

		for attachment in attachments:
			with open(attachment.filename, "wb") as f:
				if other_dir:
					await attachment.save(f"{scriptdir}{other_dir}/{attachment.filename}")
					await ctx.send(f"saved as {scriptdir}{other_dir}/{attachment.filename}")
				else:
					await attachment.save(f)
					await ctx.send(f"saved as {attachment.filename}")

	@commands.command()
	async def convert2(self, ctx, to_filetype):
		"""Upload a file"""
		attachments = ctx.message.attachments
		if to_filetype == "jpeg" or to_filetype == "jpg":
			to_filetype = "JPEG"
		if not attachments:
			await ctx.send("No attachment found! Please upload it in your next message.")

			def check(msg_: discord.Message) -> bool:
				return msg_.channel.id == ctx.channel.id and msg_.author.id == ctx.author.id and msg_.attachments

			try:
				msg = await self.bot.wait_for("message", check=check, timeout=60 * 10)
			except asyncio.TimeoutError:
				return await ctx.send("Stopped waiting for file upload, 10 minutes have passed.")
	
			attachments = msg.attachments

		for attachment in attachments:
			with open(attachment.filename, "wb") as f:
				await attachment.save(f"{scriptdir}factory/{attachment.filename}")
				await ctx.send(f"Saving file...")
				img = Image.open(f"{scriptdir}factory/{attachment.filename}")
				#new_img = img.resize( (256, 256) )
				try:
					img.save(f"{scriptdir}factory/{attachment.filename.split('.')[0]}.{to_filetype}", to_filetype)
					imgfile = discord.File(f"{scriptdir}factory/{attachment.filename.split('.')[0]}.{to_filetype}")
					await ctx.channel.send(file=imgfile)
				except:
					await ctx.send("Filetype not recognized.")

	@commands.command()
	async def convert(self, ctx, to_filetype):
		"""Upload a file"""
		attachments = ctx.message.attachments

		if not attachments:
			await ctx.send("No attachment found! Please upload it in your next message.")

			def check(msg_: discord.Message) -> bool:
				return msg_.channel.id == ctx.channel.id and msg_.author.id == ctx.author.id and msg_.attachments

			try:
				msg = await self.bot.wait_for("message", check=check, timeout=60 * 10)
			except asyncio.TimeoutError:
				return await ctx.send("Stopped waiting for file upload, 10 minutes have passed.")
	
			attachments = msg.attachments

		for attachment in attachments:
			with open(attachment.filename, "wb") as f:
				await attachment.save(f"{scriptdir}factory/{attachment.filename}")
				await ctx.send(f"Saving file...")
				img = Image.open(f"{scriptdir}factory/{attachment.filename}")
				try:
					img.save(f"{scriptdir}factory/{attachment.filename.split('.')[0]}.{to_filetype}")
					imgfile = discord.File(f"{scriptdir}factory/{attachment.filename.split('.')[0]}.{to_filetype}")
					await ctx.channel.send(file=imgfile)
				except:
					await ctx.send("Filetype not recognized.")

	@commands.command()
	async def addfilter(self, ctx, img_filter):
		"""Upload a file"""
		attachments = ctx.message.attachments

		if not attachments:
			await ctx.send("No attachment found! Please upload it in your next message.")

			def check(msg_: discord.Message) -> bool:
				return msg_.channel.id == ctx.channel.id and msg_.author.id == ctx.author.id and msg_.attachments

			try:
				msg = await self.bot.wait_for("message", check=check, timeout=60 * 10)
			except asyncio.TimeoutError:
				return await ctx.send("Stopped waiting for file upload, 10 minutes have passed.")
	
			attachments = msg.attachments

		for attachment in attachments:
			with open(attachment.filename, "wb") as f:
				await attachment.save(f"{scriptdir}factory/{attachment.filename}")
				await ctx.send(f"Saving file...")
				img = Image.open(f"{scriptdir}factory/{attachment.filename}")
				if img_filter == "blur":
					img2 = img.filter(ImageFilter.BLUR)
				elif img_filter == "contour":
					img2 = img.filter(ImageFilter.CONTOUR)
				elif img_filter == "detail":
					img2 = img.filter(ImageFilter.DETAIL)
				elif img_filter == "edge_enhance":
					img2 = img.filter(ImageFilter.EDGE_ENHANCE)
				elif img_filter == "edge_enhance2":
					img2 = img.filter(ImageFilter.EDGE_ENHANCE_MORE)
				elif img_filter == "emboss":
					img2 = img.filter(ImageFilter.EMBOSS)
				elif img_filter == "find_edges":
					img2 = img.filter(ImageFilter.FIND_EDGES)
				elif img_filter == "sharpen":
					img2 = img.filter(ImageFilter.SHARPEN)
				elif img_filter == "smooth":
					img2 = img.filter(ImageFilter.SMOOTH)
				elif img_filter == "smooth2":
					img2 = img.filter(ImageFilter.SMOOTH_MORE)
				else:
					await ctx.send("Bad image filter...")
					return
					
				img2.save(f"{scriptdir}factory/{attachment.filename}")
				imgfile = discord.File(f"{scriptdir}factory/{attachment.filename}")
				await ctx.channel.send(file=imgfile)
					
	@is_tier(3)
	@commands.command(hidden=True)
	async def sudo(self, ctx, who: Union[discord.Member, discord.User], *, command: str):
		"""Run a command as another user."""
		if who ==  ctx.guild.me:
			await ctx.send("Can't sudo as self.")
			return
		
		if command == "help":
			await ctx.send("Help as sudo is disabled.")
			return
		
		await ctx.send(f"Executing {command} as {who.display_name}...")	
		msg = copy.copy(ctx.message)
		msg.author = who
		msg.content = ctx.prefix + command
		new_ctx = await self.bot.get_context(msg)
		await self.bot.invoke(new_ctx)
	
	@is_tier(3)
	@commands.command(pass_context=True, hidden=True)
	async def repl(self, ctx):
		"""Launches an interactive REPL session.
		
		A Read–Eval–Print Loop (REPL), also known as an interactive toplevel or language shell, is a simple, interactive computer programming environment that takes single user inputs (i.e. single expressions), evaluates them, and returns the result to the user; a program written in a REPL environment is executed piecewise.
		"""
		variables = {
			'ctx': ctx,
			'bot': self.bot,
			'client':self.bot,
			'message': ctx.message,
			'guild': ctx.guild,
			'channel': ctx.channel,
			'author': ctx.author,
			'_': None,
			"discord": discord,
			"commands": commands,

			# utilities
			"_get": discord.utils.get,
			"_find": discord.utils.find,
		}
		version_python = str(sys.version_info[0])+"."+str(sys.version_info[1])+"."+str(sys.version_info[2])
		if ctx.channel.id in self.sessions:
			await ctx.send('Already running a REPL session in this channel. Exit it with `quit`.')
			return

		self.sessions.add(ctx.channel.id)
		await ctx.send(f'REPL (Read–Eval–Print Loop) (Python {version_python}) session activated.\nEnter code to execute or evaluate. `exit()` or `quit` to exit.')

		def check(m):
			return m.author.id == ctx.author.id and \
				   m.channel.id == ctx.channel.id and \
				   m.content.startswith('`')

		while True:
			try:
				response = await self.bot.wait_for('message', check=check, timeout=10.0 * 60.0)
			except asyncio.TimeoutError:
				await ctx.send('Exiting REPL session.')
				self.sessions.remove(ctx.channel.id)
				break

			cleaned = self.cleanup_code(response.content)

			if cleaned in ('quit', 'exit', 'exit()'):
				await ctx.send('Exiting.')
				self.sessions.remove(ctx.channel.id)
				return

			executor = exec
			if cleaned.count('\n') == 0:
				# single statement, potentially 'eval'
				try:
					code = compile(cleaned, '<repl session>', 'eval')
				except SyntaxError:
					pass
				else:
					executor = eval

			if executor is exec:
				try:
					code = compile(cleaned, '<repl session>', 'exec')
				except SyntaxError as e:
					await ctx.send(self.get_syntax_error(e))
					continue

			variables['message'] = response

			fmt = None
			stdout = io.StringIO()

			try:
				with redirect_stdout(stdout):
					result = executor(code, variables)
					if inspect.isawaitable(result):
						result = await result
			except Exception as e:
				value = stdout.getvalue()
				fmt = f'```py\n{value}{traceback.format_exc()}\n```'
			else:
				value = stdout.getvalue()
				if result is not None:
					fmt = f'```py\n{value}{result}\n```'
					variables['_'] = result
				elif value:
					fmt = f'```py\n{value}\n```'

			try:
				if fmt is not None:
					if len(fmt) > 2000:
						await ctx.send('Content too big to be printed.')
					else:
						await ctx.send(fmt)
			except discord.Forbidden:
				pass
			except discord.HTTPException as e:
				await ctx.send(f'Unexpected error: `{e}`')
					  
async def run_subprocess(cmd: str) -> str:
	"""Runs a subprocess and returns the output."""
	process = await asyncio.create_subprocess_shell(cmd, stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE)
	results = await process.communicate()
	return "".join(x.decode("utf-8") for x in results)
  
def Webhook(text="", embed=None):
	#Testzone: 461851689922854922 / EHjh3I2uAZXpt1MPf0ehzH8yzwfvYLz5YiU9zALRN2YBEj0ebafmAmR7w2YPDHzTDG_o
	#Matchmaking: 461858508237701120 / jUIj3q0tb42XSDVEJnG2_7umXfRGJZZiablLN0oHcVkzfVj-ZJ4n201P-JGrgFCLb97c
	#Announce: 461868227664805908 8VHKMw0PRZc_5Rmng3NYgOiYjvCR2eV3rVNQsv7G2mxUgy2u4Vw7AaR8-7DJf1GYGrvo
	webhook_id = "461868227664805908"
	webhook_token = "8VHKMw0PRZc_5Rmng3NYgOiYjvCR2eV3rVNQsv7G2mxUgy2u4Vw7AaR8-7DJf1GYGrvo"
	webhook = discord.Webhook.partial(webhook_id, webhook_token, adapter=discord.RequestsWebhookAdapter())
	webhook.send(text, embed=embed)

def setup(bot):
	global client
	bot.add_cog(utility(bot))
	client = bot
