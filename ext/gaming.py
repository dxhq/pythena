import discord
from discord.ext import commands
from discord.ext.commands.cooldowns import BucketType
from bs4 import BeautifulSoup
import datetime
import humanfriendly
import socket
import sys
import random
import requests
import json
import tttn
from persona import *
from athena_common import *
import valve.source.a2s

servercounttotal = 0
servercount = 0
servercounttimedout = 0
totalplayercount = 0
storedservers = []
bFoundPlayers = False #Have we found players? Related to MSQ command.
bFoundPlayersQ = False #As above.
aecol = 0x030399
hxcol = 0x005AB1
masterserverdomain = "master.333networks.com"
masterserverport = "28900"

countryflags = {
	'EU':'🇪🇺',
	'DE':'🇩🇪',
	'US':'🇺🇸',
	'CA':'🇨🇦',
	'UA':'🇺🇦',
	'NL':'🇳🇱',
	'SK':'🇸🇰',
	'SI':'🇸🇮',
	'RU':'🇷🇺',
	'GB':'🇬🇧',
	'None':'🕹'
}

serverowners = {
'46.163.66.75':'][FGS][Nobody',
'74.91.113.107':'Sev',
'84.255.248.189':'DejaVu',
'217.246':'Zora',
'74.91.112.139':'Defaultplayer001',
'81.99.252.228':'Kaiser',
'84.52.189.92':'Bamboo',
'168.235.108.14':'Jhonny',
'72.14.191.134':'Cirno',
'97.98.66.185':'Cirno'
}

gameplayers = []
hxgameplayers = []
team_1 = []
team_2 = []
lobby_name = ""
lobby_debug = False
saved_servers = []

bannedservers = {}
#{'73.82.161.83':'Connection to this server causes script errors.'} #Dae's
tms = None

def ReadLobby():
	global lobby_name
	global team_1, team_2, gameplayers, hxgameplayers
	global lobby_debug
	global saved_servers
	data = {}
	with open('lobby.json') as f:
		data = json.load(f)
		lobby_name = data['lobby_name']
		gameplayers = data['gameplayers']
		hxgameplayers = data['hxgameplayers']
		team_1 = data['team_1']
		team_2 = data['team_2']
		saved_servers = data['saved_servers']
		lobby_debug = boolinate(data['lobby_debug'])

def GetLobby():
	data = {}
	with open('lobby.json') as f:
		return json.load(f)
		
def WriteLobby():
	global lobby_name
	global team_1, team_2, gameplayers, hxgameplayers
	global lobby_debug
	global saved_servers
	data = {}
	data['lobby_name'] = lobby_name
	data['gameplayers'] = gameplayers
	data['hxgameplayers'] = hxgameplayers
	data['team_1'] = team_1
	data['team_2'] = team_2
	data['lobby_debug'] = str(lobby_debug)
	data['saved_servers'] = saved_servers
	with open('lobby.json', "w") as s:
		json.dump(data, s, indent=4, sort_keys=True)	
		
class gaming:
	"""Any commands related to Gaming."""
	global servercounttotal
	global servercount
	global servercounttimedout
	global totalplayercount
	global client
	global lobby_name
	global team_1, team_2, gameplayers, hxgameplayers
	global lobby_debug
	global saved_servers
	def __init__(self, bot):
		self.bot = bot
		client = bot
		
#	async def __local_check(self, ctx):
#		allowed = [396716931962503169]
#		return ctx.guild.id in allowed

	@commands.command()
	@access_check()
	async def add_saved(self, ctx, address):
		ReadLobby()
		saved_servers.append(address)
		await ctx.send(f"Adding {address} to saved servers.")
		WriteLobby()
		
	@commands.command(help="Shows information about the Playground GMOD server.\nData provided by Valve and Gametracker", brief="GMOD stuff.", aliases=['gm'])
	@is_in_guild(426371662359953411)
	@commands.cooldown(1, 15, BucketType.user)
	async def gmod(self, ctx):
		"""Shows information for The Playground GMOD server.
		Data provided by Valve and GameTracker."""
		await ctx.channel.trigger_typing()
		_v = getConfig()
		s_players = _v['gmod']['playermethod_1']
		blockgt = _v['gmod']['playermethod_1']
		starttime = datetime.datetime.now()
		lmsg = await ctx.send("<a:loading:480693540616273922> Connecting to the server...")
		
		se = ("54.36.230.51", 27015)
		
		try:
			with valve.source.a2s.ServerQuerier(se) as server:
				gm_name = server.info()['server_name']
				gm_map = server.info()['map']
				gm_players = server.info()['player_count']
				gm_players_max = server.info()['max_players']
				gm_platform = server.info()['platform']
			#	gm_vac = boolinate(server.info()['vac_enabled'])
				gm_version = server.info()['version']
				gm_ping = server.ping()
				
				
				gm_players_list = ""
				if s_players is True:
					for player in server.players()["players"]:
						p = f"{player['name']} [{player['score']}] / Connected for {humanfriendly.format_timespan(player['duration'])}\n"
						gm_players_list += p

			try:
				if s_players is False and gm_players > 0 and blockgt is False:
					#HAAAAAAAAAAAAAX
					result = requests.get(f'https://www.gametracker.com/server_info/54.36.230.51:27015/')
					outp = BeautifulSoup(result.text, 'html5lib')

					s = str(outp).split("TOP 10 PLAYERS")[0]
					s = s.split("ONLINE PLAYERS")[1]
					s = s.replace("<td>", "")
					s = s.replace("</td>", "")
					s = s.replace("<div>", "")
					s = s.replace("</div>", "")
					s = s.replace("<tr>", "")
					s = s.replace("</tr>", "")
					s = s.replace("	", "")
					s = s.replace("\n", " ")
					s = s.strip()
					#print(s)
					outp = BeautifulSoup(s, 'html5lib')
					f = []
					flist = outp.find_all('a')
					for link in flist:
						f.append(f"{link.string.strip()}")
						
					gm_players_list = ' | '.join(f)
			except Exception as e:
				AddLog(str(e), error=True)	
					
			em = discord.Embed(colour=0x3EC1FA ,description=f"**Game**: Garry's Mod\n**Map**: {gm_map}\n**Platform**: {gm_platform}\n**Version**: {gm_version}\n**Ping**: {str(gm_ping).split('.')[0]}\n\n**Click to Join**: steam://connect/54.36.230.51:27015")
			#em = discord.Embed(colour=0x3EC1FA ,description=f"**Game**: Garry's Mod\n**Map**: {gm_map}\n**Platform**: {gm_platform}\n**Version**: {gm_version}\n\n**Click to Join**: steam://connect/54.36.230.51:27015")
			em.set_author(name=f"{gm_name} ({gm_players}/{gm_players_max})", icon_url="https://cdn.discordapp.com/emojis/480692378819100693.png")
			#em.add_field(name=f"{gm_name} ({gm_players}/{gm_players_max})", value=f"**Game**: Garry's Mod\n**Map**: {gm_map}\n**Platform**: {gm_platform}\n**Version**: {gm_version}\n**Ping**: {str(gm_ping).split('.')[0]}\n\n**Click to Join**: steam://connect/54.36.230.51:27015")
			if gm_players_list != "":
				em.add_field(name="Players", value=gm_players_list)
				em.set_footer(text="Player names pulled from GameTracker, the name list updates slower than the other data does, so may sometimes seem out of sync.", icon_url="https://cdn.discordapp.com/emojis/482503517819371530.png")
				
			if lmsg is not None:
				f = datetime.datetime.now() - starttime
				await lmsg.edit(content=f"<:loaded:480337167080488960> Connected to GMOD//Playground in {f.seconds} seconds.", embed=em)
			else:
				await ctx.send(embed=em)
			AddLog("Connection to GMOD API completed.", colour="green")
			PersonaCommand()
				
		except valve.source.a2s.NoResponseError:
			#em = discord.Embed()
			#em.add_field(name=f"NO RESPONSE (N/A)", value=f"**Game**: Garry's Mod\n**Map**: N/A\n**Platform**: N/A\n**Version**: N/A\n**Ping**: N/A\n\n**Click to Join**: steam://connect/54.36.230.51:27015")
			#em.add_field(name="⚠ Error", value="Server could not be reached. (No Response from a2s)")
			#await ctx.send(embed=em)
			
			AddLog("Server timed out.", colour="red")
			RaisePersona('anger', 8)
			LowerPersona('mood', 10)
			if lmsg is not None:
				f = datetime.datetime.now() - starttime
				await lmsg.edit(content="<:loadfail:480337167038676992> Connection failed.", embed=discord.Embed(colour=0xFF0000, description=f"Server timed out after {f.seconds} seconds..."))
			else:
				await ctx.send("<:loadfail:480337167038676992> Server is currently unavailable and could not be reached.")
			PersonaError()
			AddLog("No response from a2s!", colour="red")
				
		except Exception as e:
			await ctx.send(f"<:loadfail:480337167038676992> Unhandled Exception: {e}")
			source = f"Guild = {ctx.message.guild.name}\nChannel = {ctx.message.channel.name}\nAuthor = {ctx.message.author.name} ({ctx.message.author.display_name})```{ctx.message.content.replace('```', '')}```"
			await AthNotif(client, "### Unhandled error log ###\n"+source+"```"+repr(sys.exc_info()[0])+"\n"+ str(sys.exc_info()[1])+"```")
			if lmsg is not None:
				await lmsg.delete()
			
	@is_in_guild(426371662359953411)
	@commands.command(hidden=True, enabled=False)
	async def tf(self, ctx):
		"""Command has been disabled due to server closure."""
		await ctx.channel.trigger_typing()
		await ctx.send("This command will be removed soon.\nSee <#470313993194438657> for details.")
		s_players = False
		
	
		se = ("54.36.224.23", 27015)
		try:
			with valve.source.a2s.ServerQuerier(se) as server:
				gm_name = server.info()['server_name']
				gm_map = server.info()['map']
				gm_players = server.info()['player_count']
				gm_players_max = server.info()['max_players']
				gm_platform = server.info()['platform']
				gm_vac = boolinate(server.info()['vac_enabled'])
				gm_version = server.info()['version']
				gm_ping = server.ping()
				
				
				gm_players_list = ""
				if s_players is True:
					for player in server.players()["players"]:
						p = f"{player['name']} [{player['score']}] / Connected for {humanfriendly.format_timespan(player['duration'])}\n"
						gm_players_list += p

			if s_players is False and gm_players > 0:
				result = requests.get(f'https://www.gametracker.com/server_info/54.36.224.23:27015/')
				outp = BeautifulSoup(result.text, 'html5lib')

				s = str(outp).split("TOP 10 PLAYERS")[0]
				s = s.split("ONLINE PLAYERS")[1]
				s = s.replace("<td>", "")
				s = s.replace("</td>", "")
				s = s.replace("<div>", "")
				s = s.replace("</div>", "")
				s = s.replace("<tr>", "")
				s = s.replace("</tr>", "")
				s = s.replace("	", "")
				s = s.replace("\n", " ")
				s = s.strip()
				outp = BeautifulSoup(s, 'html5lib')
				f = []
				flist = outp.find_all('a')
				for link in flist:
					f.append(f"{link.string.strip()}")
					
				gm_players_list = ' | '.join(f)
					
			em = discord.Embed()
			em.add_field(name=f"{gm_name} ({gm_players}/{gm_players_max})", value=f"**Game**: Team Fortress 2\n**Map**: {gm_map}\n**Platform**: {gm_platform}\n**Version**: {gm_version}\n**Ping**: {str(gm_ping).split('.')[0]}\n\n**Click to Join**: steam://connect/54.36.224.23:27015")
			if gm_players_list != "":
				em.add_field(name="Players", value=gm_players_list)
			await ctx.send(embed=em)
		except valve.source.a2s.NoResponseError:
			await ctx.send("⚠\nServer is currently unavailable and could not be reached.")
			AddLog("Server timed out.", colour="red")
		except Exception as e:
			await ctx.send(f"⚠ \nUnhandled Exception: {e}")
			AddLog(f"UNHANDLED: {e}", colour="red")
			RaisePersona('anger', 3)
			LowerPersona('mood', 4)
			source = f"Guild = {ctx.message.guild.name}\nChannel = {ctx.message.channel.name}\nAuthor = {ctx.message.author.name} ({ctx.message.author.display_name})```{ctx.message.content.replace('```', '')}```"
			AthNotif("### Unhandled error log ###\n"+source+"```"+repr(sys.exc_info()[0])+"\n"+ str(sys.exc_info()[1])+"```")
							
	@is_in_guild(396716931962503169)
	@commands.group(invoke_without_command=True, name="lobbyhx", aliases=['hxl', 'hxlobby'])
	async def hxlobby(self, ctx):
		"""A simple HX lobby.
		Entering this command on its own will add/remove you from the lobby."""
		em = discord.Embed(color=hxcol)
		ReadLobby()
			
		if ctx.author.id in hxgameplayers:
			hxgameplayers.remove(ctx.author.id)
			gameplayers_names = []
			for member in hxgameplayers:
				gameplayers_names.append(ctx.guild.get_member(member).display_name)
			em.add_field(name=f"Lobby for HX", value=f"Removed {ctx.author.display_name} from the game lobby.\nCurrent Players: {HumanizeList(gameplayers_names)}")
			WriteLobby()	
			await ctx.send(embed=em)
		else:
			hxgameplayers.append(ctx.author.id)
			gameplayers_names = []
			for member in hxgameplayers:
				gameplayers_names.append(ctx.guild.get_member(member).display_name)
			em.add_field(name=f"Lobby for HX", value=f"Added {ctx.author.display_name} to the game lobby.\nCurrent Players: {HumanizeList(gameplayers_names)}")
			WriteLobby()	
			await ctx.send(embed=em)
		PersonaCommand()
		
	@is_in_guild(396716931962503169)
	@hxlobby.command(name="start")
	async def _hxlobby_start(self, ctx, *, details=""):
		"""Begins the event.
		Will DM players signed up in any list details."""
		if len(hxgameplayers) == 0:
			await ctx.send("Game can't start, no members.")
			return

		if details == "":
			details = "Server is most likely DP's; 74.91.112.139:7790"
		await ctx.send(f"DM'ing registered players for HX.\nSending details:\n{details}\n")
		
		for member in hxgameplayers:
			em = discord.Embed(color=hxcol)	
			if len(hxgameplayers) > 0:
				gameplayers_names = []
				for member in hxgameplayers:
					gameplayers_names.append(ctx.guild.get_member(member).display_name)
				
				em.add_field(name=f"Lobby for HX", value=HumanizeList(gameplayers_names))
			
			await ctx.guild.get_member(member).send(f"HX is starting! From; {ctx.author.display_name}\n{details}", embed=em)	
		PersonaCommand()
		
	@is_in_guild(396716931962503169)				
	@hxlobby.command(name="mod")
	async def _hxlobby_add(self, ctx, othername):
		"""Enter someone else.
		Will add another member to the lobby, so they don't have to."""
		ReadLobby()
		othermem = None
		for member in ctx.guild.members:
			if othername.lower() in member.name.lower() or othername.lower() in member.display_name.lower():
				othermem = member
		
		if othermem is None:
			await ctx.send("Member not found.")	
			return
				
		em = discord.Embed(color=aecol)
					
		if othermem.id in hxgameplayers:
			hxgameplayers.remove(othermem.id)
			gameplayers_names = []
			for member in hxgameplayers:
				gameplayers_names.append(ctx.guild.get_member(member).display_name)
			em.add_field(name=f"Lobby for HX", value=f"Removed {othermem.display_name} from the game lobby by {ctx.author.display_name}.\nCurrent Players: {HumanizeList(gameplayers_names)}")
		else:
			hxgameplayers.append(othermem.id)
			gameplayers_names = []
			for member in hxgameplayers:
				gameplayers_names.append(ctx.guild.get_member(member).display_name)
			em.add_field(name=f"Lobby for HX", value=f"Added {othermem.display_name} to the game lobby by {ctx.author.display_name}.\nCurrent Players: {HumanizeList(gameplayers_names)}")
		WriteLobby()	
		await ctx.send(embed=em)
		PersonaCommand()
		
	@is_in_guild(396716931962503169)
	@hxlobby.command(name="list")
	async def _hxlobby_list(self, ctx):
		"""Show the current lobby.
		Will also show teams if sorted."""
		
		if len(hxgameplayers) == 0:
			await ctx.send("The lobby is currently empty.")
			return
			
		em = discord.Embed(color=hxcol)	
		if len(hxgameplayers) > 0:
			gameplayers_names = []
			for member in hxgameplayers:
				gameplayers_names.append(ctx.guild.get_member(member).display_name)
			
			em.add_field(name=f"Lobby for HX", value=HumanizeList(gameplayers_names))
		
		await ctx.send(embed=em)
		PersonaCommand()
		
	@is_in_guild(396716931962503169)		
	@hxlobby.command(name="clear")
	@access_check()
	async def _hxlobby_clear(self, ctx):
		"""Resets the lobby entirely."""
		ReadLobby()
		hxgameplayers.clear()
		await ctx.send("All lists cleared.")
		WriteLobby()
		PersonaCommand()
		
	@hxlobby.command(name="debug", hidden=True, enabled=False)
	async def _hxlobby_debug_fill(self, ctx):
		f = "[lobby_debug MODE ENABLED] Your randomly chosen names for debugging purposes is:\n"
		ReadLobby()
		while len(hxgameplayers) < 8:
			m = random.choice(ctx.guild.members)
			hxgameplayers.append(m.id)
			f += f"{m.display_name}\n"
		WriteLobby()
		await ctx.send(f)
		PersonaCommand()
		
	@is_in_guild(396716931962503169)
	@commands.group(invoke_without_command=True)
	async def lobby(self, ctx):
		"""A simple matchmaking lobby.
		Entering this command on its own will add/remove you from the lobby."""
		em = discord.Embed(color=aecol)
		ReadLobby()
		if ctx.author.id in team_1:
			team_1.remove(ctx.author.id)
			gameplayers_names = []
			for member in team_1:
				gameplayers_names.append(ctx.guild.get_member(member).display_name)
			em.add_field(name=f"Team UNATCO for {lobby_name}", value=f"Removed {ctx.author.display_name} from team UNATCO.\nCurrent Players: {HumanizeList(gameplayers_names)}")
			WriteLobby()	
			await ctx.send(embed=em)
			return

		if ctx.author.id in team_2:
			team_2.remove(ctx.author.id)
			gameplayers_names = []
			for member in team_2:
				gameplayers_names.append(ctx.guild.get_member(member).display_name)
			em.add_field(name=f"Team NSF for {lobby_name}", value=f"Removed {ctx.author.display_name} from team NSF.\nCurrent Players: {HumanizeList(gameplayers_names)}")
			WriteLobby()	
			await ctx.send(embed=em)
			return
			
		if ctx.author.id in gameplayers:
			gameplayers.remove(ctx.author.id)
			gameplayers_names = []
			for member in gameplayers:
				gameplayers_names.append(ctx.guild.get_member(member).display_name)
			em.add_field(name=f"Lobby for {lobby_name}", value=f"Removed {ctx.author.display_name} from the game lobby.\nCurrent Players: {HumanizeList(gameplayers_names)}")
			WriteLobby()	
			await ctx.send(embed=em)
		else:
			gameplayers.append(ctx.author.id)
			gameplayers_names = []
			for member in gameplayers:
				gameplayers_names.append(ctx.guild.get_member(member).display_name)
			em.add_field(name=f"Lobby for {lobby_name}", value=f"Added {ctx.author.display_name} to the game lobby.\nCurrent Players: {HumanizeList(gameplayers_names)}")
			WriteLobby()	
			await ctx.send(embed=em)
		PersonaCommand()
		
	@is_in_guild(396716931962503169)
	@lobby.command(name="start")
	async def _lobby_start(self, ctx, *, details=""):
		"""Begins the event.
		Will DM players signed up in any list details."""
		if len(team_1) == 0 and len(team_2) == 0:
			await ctx.send("Game can't start, no teams decided.")
			return

		await ctx.send(f"DM'ing registered players for {lobby_name}.\nSending details:\n{details}\n")
		
		if lobby_debug is True:
			t1 = []
			for team in team_1:
				t1.append(ctx.guild.get_member(team).display_name)
				
			t2 = []
			for tean in team_2:
				t2.append(ctx.guild.get_member(tean).display_name)
			
			gameplayers_names = []
			for member in gameplayers:
				gameplayers_names.append(ctx.guild.get_member(member).display_name)
				
			await ctx.send(f"DEBUG\nWill DM the following;\n\nUNATCO: {HumanizeList(t1)}\nNSF: {HumanizeList(t2)}\nNO_TEAM: {HumanizeList(gameplayers_names)}")
			return
			
		for member in team_1:
			t1 = []
			for team in team_1:
				t1.append(ctx.guild.get_member(team).display_name)
				
			t2 = []
			for team in team_2:
				t2.append(ctx.guild.get_member(team).display_name)
				
			em = discord.Embed()
			em.add_field(name="UNATCO", value=HumanizeList(t1))	
			em.add_field(name="NSF", value=HumanizeList(t2))	
			await ctx.guild.get_member(member).send(f"{lobby_name} is starting! \n{details}\nFrom; {ctx.author.display_name}\nYou are on team UNATCO, the lineup looks like this:", embed=em)
		
		for member in team_2:
			t1 = []
			for team in team_1:
				t1.append(ctx.guild.get_member(team).display_name)
				
			t2 = []
			for team in team_2:
				t2.append(ctx.guild.get_member(team).display_name)
			
			em = discord.Embed()
			em.add_field(name="UNATCO", value=HumanizeList(t1))	
			em.add_field(name="NSF", value=HumanizeList(t2))	
				
			await ctx.guild.get_member(member).send(f"{lobby_name} is starting! From; {ctx.author.display_name}\nYou are on team NSF, the lineup looks like this:", embed=em)	
		
		for member in gameplayers:
			await ctx.guild.get_member(member).send(f"{lobby_name} is starting! From; {ctx.author.display_name}\nYou aren't on any team yet, so join someones!")		
		PersonaCommand()
		
	@is_in_guild(396716931962503169)
	@lobby.command(name="join")
	async def _lobby_join(self, ctx, location:int=0):
		"""Joins a specific list.
		0 - or any other value = lobby
		1 = team 1
		2 = team 2"""
		
		changes = ""
		ReadLobby()
		if ctx.author.id in gameplayers:
			if location is not 1 and location is not 2:
				await ctx.send("Trying to join lobby while already there?")
				return
				
			gameplayers.remove(ctx.author.id)
			changes += "Removed from main lobby...\n"
		
		if ctx.author.id in team_1:
			if location is not 0 and location is not 2:
				await ctx.send("Trying to join team UNATCO while already there?")
				return
			team_1.remove(ctx.author.id)
			changes += "Removed from team UNATCO...\n"
		
		if ctx.author.id in team_2:
			if location is not 0 and location is not 1:
				await ctx.send("Trying to join team NSF while already there?")
				return
			team_2.remove(ctx.author.id)
			changes += "Removed from team NSF...\n"	
		
		if location is 0:
			gameplayers.append(ctx.author.id)
			changes += "Added to Lobby"
		elif location is 1:
			team_1.append(ctx.author.id)
			changes += "Added to team UNATCO"
		elif location is 2:
			team_2.append(ctx.author.id)
			changes += "Added to team NSF"
		
		WriteLobby()
		await ctx.send(changes)	
		PersonaCommand()
		
	@is_in_guild(396716931962503169)			
	@lobby.command(name="mod")
	async def _lobby_add(self, ctx, othername):
		"""Enter someone else.
		Will add another member to the lobby, so they don't have to."""
		ReadLobby()
		othermem = None
		for member in ctx.guild.members:
			if othername.lower() in member.name.lower() or othername.lower() in member.display_name.lower():
				othermem = member
		
		if othermem is None:
			await ctx.send("Member not found.")	
			return
				
		em = discord.Embed(color=aecol)

		if othermem.id in team_1:
			team_1.remove(othermem.id)
			gameplayers_names = []
			for member in team_1:
				gameplayers_names.append(ctx.guild.get_member(member).display_name)
			em.add_field(name=f"Team UNATCO for {lobby_name}", value=f"Removed {othermem.display_name} from team UNATCO.\nCurrent Players: {HumanizeList(gameplayers_names)}")
			WriteLobby()	
			await ctx.send(embed=em)
			return

		if othermem.id in team_2:
			team_2.remove(othermem.id)
			gameplayers_names = []
			for member in team_2:
				gameplayers_names.append(ctx.guild.get_member(member).display_name)
			em.add_field(name=f"Team NSF for {lobby_name}", value=f"Removed {othermem.display_name} from team NSF.\nCurrent Players: {HumanizeList(gameplayers_names)}")
			WriteLobby()	
			await ctx.send(embed=em)
			return
					
		if othermem.id in gameplayers:
			gameplayers.remove(othermem.id)
			gameplayers_names = []
			for member in gameplayers:
				gameplayers_names.append(ctx.guild.get_member(member).display_name)
			em.add_field(name=f"Lobby for {lobby_name}", value=f"Removed {othermem.display_name} from the game lobby by {ctx.author.display_name}.\nCurrent Players: {HumanizeList(gameplayers_names)}")
		else:
			gameplayers.append(othermem.id)
			gameplayers_names = []
			for member in gameplayers:
				gameplayers_names.append(ctx.guild.get_member(member).display_name)
			em.add_field(name=f"Lobby for {lobby_name}", value=f"Added {othermem.display_name} to the game lobby by {ctx.author.display_name}.\nCurrent Players: {HumanizeList(gameplayers_names)}")
		WriteLobby()	
		await ctx.send(embed=em)
		PersonaCommand()
		
	@is_in_guild(396716931962503169)
	@lobby.command(name="list")
	async def _lobby_list(self, ctx):
		"""Show the current lobby.
		Will also show teams if sorted."""
		
		if len(team_1) == 0 and len(team_2) == 0 and len(gameplayers) == 0:
			await ctx.send("The lobby is currently empty.")
			return
			
		em = discord.Embed(color=aecol)	
		if len(gameplayers) > 0:
			gameplayers_names = []
			for member in gameplayers:
				gameplayers_names.append(ctx.guild.get_member(member).display_name)
			
			em.add_field(name=f"Lobby for {lobby_name}", value=HumanizeList(gameplayers_names))
		
		if len(team_1) > 0:
			t1 = []
			for member in team_1:
				t1.append(ctx.guild.get_member(member).display_name)
			em.add_field(name="UNATCO", value=HumanizeList(t1))
		
		if len(team_2) > 0:
			t2 = []
			for member in team_2:
				t2.append(ctx.guild.get_member(member).display_name)
			em.add_field(name="NSF", value=HumanizeList(t2))
		
		await ctx.send(embed=em)
		PersonaCommand()
		
	@is_in_guild(396716931962503169)
	@lobby.command(name="disband")
	async def _lobby_disband(self, ctx):
		"""Sends the teams back to lobby.
		Puts everyone back in the lobby."""
		ReadLobby()
		for item in team_1:
			gameplayers.append(item)

		for item in team_2:
			gameplayers.append(item)
		
		team_1.clear()
		team_2.clear()
		WriteLobby()
		await ctx.send("All teams have been disbanded back in to the lobby.")
		PersonaCommand()
		
	@is_in_guild(396716931962503169)
	@lobby.command(name="shuffle")	
	async def _lobby_shuffle(self, ctx, shuffle_all=""):
		"""Shuffles everyone in to teams.
		Will try to keep teams balanced while also randomly selecting everyones teams."""
		ReadLobby()	
		print(shuffle_all)
		if boolinate(shuffle_all) is True:
			for item in team_1:
				gameplayers.append(item)

			for item in team_2:
				gameplayers.append(item)
			print("Shuffling all back")
			
		team_1.clear()
		team_2.clear()
		if len(gameplayers) == 0:
			await ctx.send("No players to shuffle.")
			return
	
		teams = [1, 2]
		p = ""
		while len(gameplayers) > 0:
			override = False
			random.shuffle(gameplayers)
			f = random.choice(teams)
			if f == 1:
				if len(team_1) > len(team_2): #team_1 has 2 members, team_2 has 3
					m = random.choice(gameplayers)
					p += f"**{ctx.guild.get_member(m).display_name}** has been assigned to team NSF.\n"
					gameplayers.remove(m)
					team_2.append(m)
				else:
					p += ""
					m = random.choice(gameplayers)
					p += f"**{ctx.guild.get_member(m).display_name}** has been assigned to team UNATCO.\n"
					gameplayers.remove(m)
					team_1.append(m)
			else:
				if len(team_2) > len(team_1): #team_1 has 2 members, team_2 has 3
					m = random.choice(gameplayers)
					p += f"**{ctx.guild.get_member(m).display_name}** has been assigned to team UNATCO.\n"
					gameplayers.remove(m)
					team_1.append(m)
				else:
					p += ""
					m = random.choice(gameplayers)
					p += f"**{ctx.guild.get_member(m).display_name}** has been assigned to team NSF.\n"
					gameplayers.remove(m)
					team_2.append(m)
		em = discord.Embed(colour=aecol)
		em.add_field(name=f"Team Assignments for {lobby_name}", value=p)
		WriteLobby()	
		await ctx.send(embed=em)
		PersonaCommand()
		
	@lobby.command(name="name", hidden=True)
	async def _lobby_name(self, ctx, *, lobbyname:str):
		ReadLobby()
		lobby_name = lobbyname
		await ctx.send(f"The lobby name is set to {lobby_name}")
		WriteLobby()
	
	@is_in_guild(396716931962503169)	
	@lobby.command(name="clear")
	@access_check()
	async def _lobby_clear(self, ctx, mode:int=0):
		"""Resets the lobby entirely."""
		ReadLobby()
		if mode == 0:
			team_1.clear()
			team_2.clear()
			gameplayers.clear()
			await ctx.send("All lists cleared.")
		elif mode == 1:
			team_1.clear()
			await ctx.send("Team 1 cleared.")
		elif mode == 2:
			team_2.clear()
			await ctx.send("Team 2 cleared.")
		elif mode == 3:
			gameplayers.clear()
			await ctx.send("Main lobby cleared.")
			
		WriteLobby()
		PersonaCommand()
		
	@lobby.command(name="dbgmode", hidden=True, enabled=False)
	async def _lobby_dbg(self, ctx):
		ReadLobby()
		if lobby_debug is True:
			lobby_debug = False
			await ctx.send("Debug mode disabled.")
		else:
			lobby_debug = True
			await ctx.send("Debug mode enabled.")
		WriteLobby()
			
	@lobby.command(name="debug", hidden=True, enabled=False)
	async def _lobby_debug_fill(self, ctx):
		f = "[lobby_debug MODE ENABLED] Your randomly chosen names for debugging purposes is:\n"
		ReadLobby()
		lobby_debug = True
		while len(gameplayers) < 8:
			m = random.choice(ctx.guild.members)
			gameplayers.append(m.id)
			f += f"{m.display_name}\n"
		WriteLobby()
		await ctx.send(f)
	
	@commands.group(hidden=True)
	async def tn(self, ctx):
		return
	
	@tn.command(hidden=True)
	async def check(self, ctx):
		global tms
		if not tms:
			await ctx.send("MS not connected.")
		else:
			await ctx.send("MS enabled.")

	@tn.command(hidden=True)
	async def state(self, ctx, game):
		global tms
		if not tms:
			tms = tttn.uMS(game)
			await ctx.send("MS connected.")
		else:
			await ctx.send("MS closed.")
			tms = None
			
	@tn.command(hidden=True)
	async def refresh(self, ctx):
		global tms
		if not tms:
			await ctx.send("MS can't refresh.")
		else:
			await ctx.send("MS refreshing.")
			tms.refresh()
			
	@tn.command(hidden=True)
	async def server(self, ctx, index):
		global tms
		if not tms:
			await ctx.send("MS can't query.")
		else:
			self.tempsrv = tms.servers[int(index)]
			await ctx.send(f"{self.tempsrv}\n{self.tempsrv.ip}:{self.tempsrv.port}\n{self.tempsrv.gametype}")
	
	@is_in_guild(396716931962503169)		
	@commands.command(help="Shows information about a Deus Ex game server.\nIf `lookup_value` is an integer, it will run it against the server list, as shown by `list` command. (See `help list`)\nIf `lookup_value` is detected as an IP:Port format, it will query that particular server.", brief="I'll magically join the server to see what's goin' in.")
	@commands.cooldown(1, 5, BucketType.user)
	async def dx(self, ctx, lookup_value=""):
		await ctx.channel.trigger_typing()
		if lookup_value == "":
			PersonaError()
			await ctx.send(msg_generator(types="missing_arg")+"\nRequires either List Index number, or IP:Port format")
			return
		lmsg = await ctx.send("<a:loading:480693540616273922> Connecting...")	
		ext = lookup_value
		if ext != "" and len(ext) < 3:
			if len(storedservers) == 0:
				await lmsg.edit(content="<:loadother:480426177748533249> The servers list is empty, try running `!list` first.", delete_after=10)
				return
			try:
				if isinstance(ctx.channel, discord.channel.TextChannel):
					selfmem = ctx.guild.me
					if str(selfmem.status) == 'dnd':
						await lmsg.edit(content="<:loadfail:480337167038676992>  "+msg_generator(types="busy"), delete_after=5)
						return
				extsrv = storedservers[int(ext)]
				arg = extsrv.split(":")
				em = await getStatus(arg[0], int(arg[1]) + 1, ctx.message)
				print(type(em))
				if type(em) == str:	
					await lmsg.edit(content=f"<:loadfail:480337167038676992> Encountered an error connecting to {arg[0]}:{int(arg[1])}", embed=discord.Embed(colour=0xFF0000, description=em))
					AddLog("DX Query time-out!", colour="red")
				else:
					await lmsg.edit(content=f"<:loaded:480337167080488960> Connected to {arg[0]}:{int(arg[1])}", embed=em)
					AddLog("DX Query command index passed!", colour="green")
				PersonaCommand()
				return
			except Exception as e:
				AddLog("DX Query command error!", colour="red")
				AddLog(str(type(e)), colour="red")
				AddLog(str(e), colour="red")
				await lmsg.edit(content=f"<:loadother:480426177748533249>Please use a number between 0 and {len(storedservers) - 1}.", delete_after=10)
				PersonaError()
				return
		else:
			try:
				arg = ext.split(":")
				print(arg[0])
				print(arg[1])
				ip = arg[0].split(".")
				ip_test = ip[0]+"."+ip[1]+"."+ip[2]+"."+ip[3]
			except Exception as e:
				AddLog("DX Query command error!", colour="red")
				AddLog(str(type(e)), colour="red")
				AddLog(str(e), colour="red")
				await lmsg.edit(content="<:loadother:480426177748533249> Incorrect format, needs IP and Port. (Example: 123.456.78.91:7777)")
				PersonaError()
				return
				
		em = await getStatus(arg[0], int(arg[1]) + 1, ctx.message)	
		#print(type(em))
		if type(em) == str:	
			await lmsg.edit(content=f"<:loadfail:480337167038676992> Encountered an error connecting to {arg[0]}:{int(arg[1])}", embed=discord.Embed(colour=0xFF0000, description=em))
			AddLog("DX query operation encountered an error.", colour="red")
		else:
			await lmsg.edit(content=f"<:loaded:480337167080488960> Connected to {arg[0]}:{int(arg[1])}", embed=em)
			AddLog("DX query operation completed.", colour="green")
		PersonaCommand()
	
	@is_in_guild(396716931962503169)
	@commands.cooldown(1, 5, BucketType.user)	
	@commands.command(help="Shows information about the HX COOP server.", brief="Even HX can't hide from me.")
	async def hx(self, ctx, index=0):
		if isinstance(ctx.channel, discord.channel.TextChannel):
			selfmem = ctx.guild.me
			if str(selfmem.status) == 'dnd':
				await ctx.channel.trigger_typing()
				await ctx.send("⚠ | "+msg_generator(types="busy"), delete_after=5)
				return
		servers = GetLobby()['saved_servers']
		addr = ""
		try:
			addr = servers[index].split(":")
		except IndexError:
			await ctx.send("Invalid index number. No server in that slot. Defaulting to index 0")
			addr = servers[0].split(":")
		ipstr = str(addr[0])
		port = int(addr[1])
		lmsg = await ctx.send("<a:loading:480693540616273922> Connecting...")	
		em = await getStatus(ipstr, port+1, ctx.message)
		if type(em) == str:	
			await lmsg.edit(content=f"<:loadfail:480337167038676992> Encountered an error connecting to {ipstr}:{port}", embed=discord.Embed(colour=0xFF0000, description=em))
			AddLog("HX encountered an error...", colour="red")
		else:	
			await lmsg.edit(content=f"<:loaded:480337167080488960> Connected to {ipstr}:{port}", embed=em)
			AddLog("HX operation completed.", colour="green")
		PersonaCommand()
			
	@commands.command(help="Checks servers for players, if any are found, we send a message to the chat.", brief="Let's play Ping Pong.", aliases=['p'])
	@commands.cooldown(1, 30)
	async def ping(self, ctx, game=""):
		if game == "" and ctx.guild.id == 396716931962503169:
			game = "deusex"
			
		if game == "" or game == "?":
			await ctx.send("Supported game list is found here: http://333networks.com/g/all\n\nHow to use:\nFind the game you want, look at its URL, use the part after the /s/ as the game name here.")
			return
		lmsg = await ctx.send("<a:loading:480693540616273922> Connecting...")	
		self.pingms = tttn.uMS(game=game.lower())
		self.fstr = ""
		servers = 0
		players = 0
		for item in self.pingms.servers:
			if item.numplayers > 0:
				if item.numplayers == 1:
					players += item.numplayers
					servers += 1
					self.fstr += f"There is someone on {item.hostname}\n"
				else:
					self.fstr += f"There are {item.numplayers} players on {item.hostname}\n"
		
		if self.fstr != "":
			AddLog(f"Ping Result: {players} players on {servers} servers", colour="green")
			await lmsg.edit(content=f"<:loaded:480337167080488960> I found people!\n```{self.fstr}```")
			await ctx.message.add_reaction(client.get_emoji(480337167080488960))
		else:
			AddLog("Ping Result: None", colour="red")
			await lmsg.edit(content="<:loadother:480426177748533249> No players found.")
			await ctx.message.add_reaction(client.get_emoji(480337167038676992))
		PersonaCommand()
		#await ctx.message.add_reaction('\u2705')
				
	@commands.command(aliases=['l'], help="Shows a serverlist.", brief="I have full access to Deus Ex systems.")
	@commands.cooldown(1, 60)
	async def list(self, ctx, game = ""):
		auth = ctx.message.author
		if game == "" and ctx.guild.id == 396716931962503169:
			game = "deusex"
			
		if game == "" or game == "?":
			await ctx.send("Supported game list is found here: http://333networks.com/g/all\n\nHow to use:\nFind the game you want, look at its URL, use the part after the /s/ as the game name here.")
			return
		lmsg = await ctx.send("<a:loading:480693540616273922> Connecting...")
		em = await msunreal(ctx.channel, game.lower())	
		await lmsg.edit(content=f"<:loaded:480337167080488960> Connected to `{game}://333networks.com`...", embed=em)
		AddLog("List operation complete.", colour="green")
		try:
			await ctx.message.add_reaction('\u2705')
			PersonaCommand()
		except discord.errors.NotFound:
			await ctx.send(f"Welp, looks like {auth.mention} deleted the message that triggered the list command, which made the script bork a tad. NAUGHTY!")
			PersonaError()
	
	@commands.command()
	async def hxlist(self, ctx):
		lmsg = await ctx.send("<a:loading:480693540616273922> Connecting to server list...")
		em = await hxlist()
		await lmsg.edit(content="<:loaded:480337167080488960> Connected to server list...", embed=em)
		AddLog("List operation complete.", colour="green")
				
def Webhook(text="", embed=None, hx=False):
	#Testzone: 461851689922854922
	#Matchmaking: 461858508237701120 / jUIj3q0tb42XSDVEJnG2_7umXfRGJZZiablLN0oHcVkzfVj-ZJ4n201P-JGrgFCLb97c
	RaisePersona('anger', 1)
	if hx == False:
		webhook_id = "461858508237701120"
		webhook_token = "jUIj3q0tb42XSDVEJnG2_7umXfRGJZZiablLN0oHcVkzfVj-ZJ4n201P-JGrgFCLb97c"
	else:
		webhook_id = "461905462355558430"
		webhook_token = "l6Eyp_7C7bQ5-ZijHlOEDibNJo9-2jLgGN20SxwOR1Qu9umRreKjHINRtaekD4JHv7eM"#
		
	webhook = discord.Webhook.partial(webhook_id, webhook_token, adapter=discord.RequestsWebhookAdapter())
	webhook.send(text, embed=embed)

def ErrorNotif(text="", embed=None):
	#Testzone: 461851689922854922 / EHjh3I2uAZXpt1MPf0ehzH8yzwfvYLz5YiU9zALRN2YBEj0ebafmAmR7w2YPDHzTDG_o
	#Matchmaking: 461858508237701120 / jUIj3q0tb42XSDVEJnG2_7umXfRGJZZiablLN0oHcVkzfVj-ZJ4n201P-JGrgFCLb97c
	#Announce: 461868227664805908 8VHKMw0PRZc_5Rmng3NYgOiYjvCR2eV3rVNQsv7G2mxUgy2u4Vw7AaR8-7DJf1GYGrvo

	webhook_id = "461851689922854922"
	webhook_token = "EHjh3I2uAZXpt1MPf0ehzH8yzwfvYLz5YiU9zALRN2YBEj0ebafmAmR7w2YPDHzTDG_o"
	webhook = discord.Webhook.partial(webhook_id, webhook_token, adapter=discord.RequestsWebhookAdapter())
	webhook.send(text, embed=embed)
					
#Deprecated function
async def queryNormalDeusExServer(ip, port, channel):
	global bFoundPlayersQ, servercount
	servercount += 1
	banlist = bannedservers.get(ip, "")
	if banlist is not "":
		return
		
	try:
		particularQuery = "\\info\\"
		sockUDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		sockUDP.settimeout(1.0)
		sockUDP.sendto(bytes(particularQuery,"utf-8"), (ip, port))
		#sockUDP.sendto(b'particularQuery', (ip, port))
		data = sockUDP.recvfrom(2048) # buffer size is 2048 bytes
		shostname = data[0].split(b'\\hostname\\')[1].split(b'\\hostport\\')[0]
		playernum = data[0].split(b'\\maxplayers\\')[0].split(b'\\numplayers\\')[1]
		#AddLog("Received message ::: "+str(data[0],"latin-1"))
		AddLog("Host name :::"+str(shostname,"latin-1"))
		AddLog("Players::: "+str(playernum,"latin-1"))
		shostx = str(shostname,"latin-1")
		ipstr = " (IP: "+str(ip)+":"+str(port)+")"
		r = random.randrange(0,4)
		if(int(playernum) != 0):
			playerstr = "player"
			if(int(playernum) > 1):
				playerstr + "s"
			playerint = str(playernum,"latin-1")
			hostnamestr = str(shostname,"latin-1") + ipstr
			if r == 0:
				await channel.send("🕹 | Yay! There is "+playerint+" "+playerstr+" online on "+hostnamestr )
			elif r == 1:
				await channel.send("🕹 | Look what I found! There is "+playerint+" "+playerstr+" online on "+hostnamestr )
			elif r == 2:
				await channel.send("🕹 | Party time! There is "+playerint+" "+playerstr+" online on "+hostnamestr )
			elif r == 3:
				await channel.send("🕹 | Woo! There is "+playerint+" "+playerstr+" online on "+hostnamestr )

			bFoundPlayersQ=True
		sockUDP.close()

	except:
		AddLog("This server did not respond our query ::: "+ip+":"+str(port)+" Reason: "+str(sys.exc_info()[1]), True)

	return
		
async def getStatus(ip, port, message):
	SetTimer(ip)
	channel = message.channel
	#await channel.trigger_typing()
	_tmp = 0
	pstr = ""
	#	
	#dxdesc = ""
	
	error = ""
	sgametype = ""
	dxmutators = ""
	dxvict = ""
	dxskills=""
	dxskillskill=""
	dxaugs=""
	dxaugkill=""
	owner=""
	dxadmin=""
	dxadmincontact=""
	
	banlist = bannedservers.get(ip, "")
	if banlist is not "":
		em = discord.Embed(colour=aecol)
		em.add_field(name="Blocked", value="This server is blocked by the botmaster.")
		em.add_field(name="Error Note", value=banlist)
		#await channel.send(embed=em)
		await message.channel.send(embed=em)
		return
	try:
		particularQuery = "\\basic\\\\info\\\\rules\\"
		
		sockUDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		sockUDP.settimeout(1.0)
		sockUDP.sendto(bytes(particularQuery,"utf-8"), (ip, port))
		data = sockUDP.recvfrom(2048) # buffer size is 2048 bytes
		datastring = str(data[0], "latin-1")
		
		print(datastring)
		print(len(datastring))
		if len(datastring) <= 100:
			print("RETRYING")
			particularQuery = "\\info\\\\rules\\"
		
			sockUDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
			sockUDP.settimeout(1.0)
			sockUDP.sendto(bytes(particularQuery,"utf-8"), (ip, port))
			data = sockUDP.recvfrom(2048) # buffer size is 2048 bytes
			datastring = str(data[0], "latin-1")
			print(datastring)
			
		#print(datastring)
		#owner = serverowners.get(ip, "")
		for dxserver, ownername in serverowners.items():
			if dxserver in ip:
				owner = ownername
		dxhost = datastring.split('\\hostname\\')[1].split('\\')[0]
		dxplayers = datastring.split('\\numplayers\\')[1].split('\\')[0]
		dxplayersmax = datastring.split('\\maxplayers\\')[1].split('\\')[0]
		dxmap = datastring.split('\\mapname\\')[1].split('\\')[0]
		dxmaptitle = datastring.split('\\maptitle\\')[1].split('\\')[0]
		
		try:
			dxvict = "**Time Limit**: "+datastring.split('\\TimeToWin\\')[1].split('\\')[0]+"\n"
		except:
			try:
				dxvict = "**Frag Limit**: "+datastring.split('\\KillsToWin\\')[1].split('\\')[0]+"\n"
			except:
				dxvict = ""
			
		if dxmaptitle != "Untitled":
			dxmaptitle = "("+dxmaptitle+")"
		else:
			dxmaptitle = ""
		try:
			dxgame = datastring.split('\\gametype\\')[1].split('\\')[0]
		except:
			dxgame = ""
		# password\False\SkillsAvail\0\SkillsPerKill\0\InitialAugs\0\AugsPerKill\0\TimeToWin\
		
		try:
			dxadmin = "**Server Admin**: "+datastring.split('\\AdminName\\')[1].split('\\')[0]+"\n"
		except:
			dxadmin = ""

		try:
			dxadmincontact = "("+datastring.split('\\AdminEMail\\')[1].split('\\')[0]+")"
		except:
			dxadmincontact = ""
								
		try:
			dxskills = "**Starting Skills**: "+datastring.split('\\SkillsAvail\\')[1].split('\\')[0]+"\n"
		except:
			dxskills = ""
			
		try:
			dxskillskill = "**Skills Per Kill**: "+datastring.split('\\SkillsPerKill\\')[1].split('\\')[0]+"\n"
		except:
			dxskillskill = ""	

		try:
			dxaugs = "**Starting Augs**: "+datastring.split('\\InitialAugs\\')[1].split('\\')[0]+"\n"
		except:
			dxaugs = ""
			
		try:
			dxaugkill = "**Augs Per Kill**: "+datastring.split('\\AugsPerKill\\')[1].split('\\')[0]+"\n"
		except:
			dxaugkill = ""	
		
		try:		
			if int(datastring.split('\\AugsPerKill\\')[1].split('\\')[0]) == 2 and int(datastring.split('\\InitialAugs\\')[1].split('\\')[0]) == 2:
				sgametype = "ATDM Server: "
		except:
			pass
		
		try:
			if int(datastring.split('\\AugsPerKill\\')[1].split('\\')[0]) == 0 and int(datastring.split('\\InitialAugs\\')[1].split('\\')[0]) == 0:
				sgametype = "0Augs Server: "
		except:
			pass
				
		try:
			if int(datastring.split('\\InitialAugs\\')[1].split('\\')[0]) == 8:
				sgametype = "BTDM Server: "
		except:
			pass
										
		try:
			dxmutators = "**Mutators**: "+datastring.split('\\mutators\\')[1].split('\\')[0]+"\n"
		except:
			dxmutators = ""
			
		AddLog(datastring)
		if int(dxplayers) > 0:
			sockUDP2 = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
			sockUDP2.settimeout(1.0)
			sockUDP2.sendto(bytes("\\players\\","utf-8"), (ip, port))
			data = sockUDP2.recvfrom(2048) # buffer size is 2048 bytes
			playerdata = str(data[0], "latin-1")
			print(playerdata)
			while _tmp < int(dxplayers):
				AddLog("Trying "+str(_tmp))
				try:
					pname = playerdata.split('\\player_'+str(_tmp)+'\\')[1].split('\\')[0]
					pfrag = playerdata.split('\\frags_'+str(_tmp)+'\\')[1].split('\\')[0]
					pping = playerdata.split('\\ping_'+str(_tmp)+'\\')[1].split('\\')[0]
					pmesh = playerdata.split('\\mesh_'+str(_tmp)+'\\')[1].split('\\')[0]
					pstr += "**"+pname+"** \nScore: "+pfrag+"\nPing: "+pping+"\nClass: "+pmesh+"\n"
				except:
					pstr += "(Player Data Unavailable)\n"
					AddLog("Error check: "+str(sys.exc_info()), error=True)
				_tmp += 1
		
	except socket.timeout:
		error = "Connection to this address could not be completed. (raised socket.timeout)"
		dxhost = "UNREACHABLE SERVER"
		dxplayers = "N"
		dxplayersmax = "A"
		dxmap = "N/A"
		dxmaptitle = ""
		dxgame = "N/A"
	except IndexError:
		error = "Server returned a bad status which could not be parsed in to the required information."
		dxhost = "DEUS EX SERVER"
		dxplayers = "N"
		dxplayersmax = "A"
		dxmap = "N/A"
		dxmaptitle = ""
		dxgame = "N/A"
	except Exception as e:
		print(e)
		error = e
		#await exception(channel)
		#return
	
	if error is not "":
		#em = discord.Embed(colour=discord.colour.red(), description=error)
		SetTimer(ip)
		return error

		
	#print("INFO="+datastring)
	# str(SetTimer(ip, returnmicro=True)/10000).split(".")[0]
	em = discord.Embed(colour=aecol, description="**Map**: "+dxmap+" "+dxmaptitle+"\n**Game**: "+dxgame+"\n**Players**: "+dxplayers+"/"+dxplayersmax+"\n"+dxmutators+dxvict+dxskills+dxskillskill+dxaugs+dxaugkill+dxadmin+dxadmincontact+"\n**IP**: "+ip+":"+str(port-1)+"\n**Estimated Ping**: "+str(SetTimer(ip, returnmicro=True)/10000).split(".")[0]+"ms")
	em.set_author(name=sgametype+dxhost, icon_url="https://cdn.discordapp.com/emojis/420490333164666890.png")

	if pstr is not "":
		em.add_field(inline=True, name="Player Info", value=pstr)
	if owner is not "":
		em.set_footer(text="Server owned by "+owner)
	#await channel.send(embed=em)
	
	return em

async def hxlist():
	hxl = GetLobby()['saved_servers']
	em = discord.Embed(colour=hxcol)
	alive = 0
	for item in hxl:
		ip = item.split(":")[0]
		port = int(item.split(":")[1])
		error = ""
		sgametype = ""
		owner=""
		dxadmin=""
		dxadmincontact=""
		dxgame = ""
		dxhost = ""
		dxplayers = ""
		dxplayersmax = ""
		dxmap = ""
		dxmaptitle = ""
		try:
			particularQuery = "\\basic\\\\info\\\\rules\\"
			
			sockUDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
			sockUDP.settimeout(1.0)
			sockUDP.sendto(bytes(particularQuery,"utf-8"), (ip, port+1))
			data = sockUDP.recvfrom(2048) # buffer size is 2048 bytes
			datastring = str(data[0], "latin-1")
			
			print(datastring)
			print(len(datastring))
			if len(datastring) <= 100:
				print("RETRYING")
				particularQuery = "\\info\\\\rules\\"
			
				sockUDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
				sockUDP.settimeout(1.0)
				sockUDP.sendto(bytes(particularQuery,"utf-8"), (ip, port+1))
				data = sockUDP.recvfrom(2048) # buffer size is 2048 bytes
				datastring = str(data[0], "latin-1")
				print(datastring)
				
			for dxserver, ownername in serverowners.items():
				if dxserver in ip:
					owner = ownername
			dxhost = datastring.split('\\hostname\\')[1].split('\\')[0]
			dxplayers = datastring.split('\\numplayers\\')[1].split('\\')[0]
			dxplayersmax = datastring.split('\\maxplayers\\')[1].split('\\')[0]
			dxmap = datastring.split('\\mapname\\')[1].split('\\')[0]
			dxmaptitle = datastring.split('\\maptitle\\')[1].split('\\')[0]
				
			if dxmaptitle != "Untitled":
				dxmaptitle = "("+dxmaptitle+")"
			else:
				dxmaptitle = ""
			try:
				dxgame = datastring.split('\\gametype\\')[1].split('\\')[0]
			except:
				dxgame = ""
			# password\False\SkillsAvail\0\SkillsPerKill\0\InitialAugs\0\AugsPerKill\0\TimeToWin\
			
			try:
				dxadmin = "**Server Admin**: "+datastring.split('\\AdminName\\')[1].split('\\')[0]
			except:
				dxadmin = ""

			try:
				dxadmincontact = "("+datastring.split('\\AdminEMail\\')[1].split('\\')[0]+")\n"
			except:
				dxadmincontact = ""
			
			em.add_field(name=dxhost, value="**Map**: "+dxmap+" "+dxmaptitle+"\n**Game**: "+dxgame+"\n**Players**: "+dxplayers+"/"+dxplayersmax+"\n"+dxadmin+dxadmincontact+"**IP**: "+ip+":"+str(port)+"\n**Owner**: "+owner)
			alive += 1
		except Exception as e:
			print(e)
			
	if alive == 0:
		em.colour = 0xFF0000
		em.add_field(name="No servers", value="No servers were active in the list.")
		
	return em
						
async def queryListDeusExServer(ip, port, qmessage, msname):
	global servercount, totalplayercount, servercounttimedout, tostr, starttime, liststr, storedservers
	starttime = datetime.datetime.now()
	
	tostr = ""
	
	banlist = bannedservers.get(ip, "")
	if banlist is not "":
		AddLog("This server was blocked by rule: banlist ["+banlist+"]... "+ip+":"+str(port), True)
		#liststr += "\n\n [ Athena refused connection to this server (Server IP is on ban list). Reason: "+banlist+" :: "+ip+":"+str(port)+" ]"
		#if servercounttimedout is not 0:
			#tostr = "(+"+str(servercounttimedout)+" timed-out)"
		#await client.edit_message(qmessage, "**Server List for Deus Ex** ("+msname+")```css\n\n"+liststr+"\n\nServers: "+str(servercount)+" "+tostr+" Total Players: "+str(totalplayercount)+"```")
		return
	servercount += 1
	try:
		particularQuery = "\\info\\"
		sockUDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		sockUDP.settimeout(10.0)
		sockUDP.sendto(bytes(particularQuery,"utf-8"), (ip, port))
		#sockUDP.sendto(b'particularQuery', (ip, port))
		data = sockUDP.recvfrom(9999) # buffer size is 2048 bytes
		
	#	if "\\final\\" not in str(data[0], "latin-1"):
			#print("NO FINAL")
			#data2 = sockUDP.recvfrom(2048)
		shostname = data[0].split(b'\\hostname\\')[1].split(b'\\hostport\\')[0]
		playernum = data[0].split(b'\\maxplayers\\')[0].split(b'\\numplayers\\')[1]
		maxplayers = data[0].split(b'\\gamemode\\')[0].split(b'\\maxplayers\\')[1]
		gametype = data[0].split(b'\\numplayers\\')[0].split(b'\\gametype\\')[1]
		mapname = data[0].split(b'\\gametype\\')[0].split(b'\\mapname\\')[1]
		port = port - 1
		#print(str(data[0]))
		#try:
		#	print("STATUS2="+str(data2))
		#except:
		#	pass
		#await getStatus(ip, port, qmessage)
		if "password\True" in str(data[0], "latin-1"):
			lstr = "🔒"
		else:
			lstr = ""
		ipstr = "{IP: "+str(ip)+":"+str(port)+"}"
		
		endtime = datetime.datetime.now()
		responsetime = starttime - endtime
		# str(ceil(responsetime.microseconds / 10000))
		liststr += "\n\n"+lstr+" ("+str(servercount - 1)+") Host: "+str(shostname,"latin-1")+" ("+str(playernum,"latin-1")+"/"+str(maxplayers,"latin-1")+")  Map: "+str(mapname,"latin-1")+"  Game: "+str(gametype,"latin-1") +" "+ipstr
		tempstr = str(playernum,"latin-1")
		totalplayercount += int(tempstr)
		if servercounttimedout is not 0:
			tostr = "(+"+str(servercounttimedout)+" timed-out)"
		if Percent(servercount+servercounttimedout, servercounttotal, returntype="int") == 100:
			await qmessage.edit(content="✅ | **Server List for Deus Ex** ("+msname+")```css\n\n"+liststr+"\n\nServers: "+str(servercount)+" "+tostr+" Total Players: "+str(totalplayercount)+"```")
		else:
			await qmessage.edit(content="[Loading "+Percent(servercount+servercounttimedout, servercounttotal)+"%] **Server List for Deus Ex** ("+msname+")```css\n\n"+liststr+"\n\nServers: "+str(servercount)+" "+tostr+" Total Players: "+str(totalplayercount)+"```")
		storedservers.append(str(ip)+":"+str(port))
		sockUDP.close()
			
	except socket.timeout:
		AddLog("This server did not respond our query... "+ip+":"+str(port)+" Reason: "+str(sys.exc_info()[0])+" :: "+str(sys.exc_info()[1]))
		servercount -= 1
		servercounttimedout += 1
		if servercounttimedout is not 0:
			tostr = "(+"+str(servercounttimedout)+" timed-out)"
		#liststr += "\n\n [ TIMED OUT :: "+ip+":"+str(port)+" ]"
		await qmessage.edit(content="[Loading "+Percent(servercount+servercounttimedout, servercounttotal)+"%] **Server List for Deus Ex** ("+msname+")```css\n\n"+liststr+"\n\nServers: "+str(servercount)+" "+tostr+" Total Players: "+str(totalplayercount)+" ```")
	
	except IndexError:
		AddLog("This server did not respond our query... "+ip+":"+str(port)+" Reason: "+str(sys.exc_info()[0])+" :: "+str(sys.exc_info()[1]), True)
	
	except:
		AddLog("This server did not respond our query... "+ip+":"+str(port)+" Reason: "+str(sys.exc_info()[0])+" :: "+str(sys.exc_info()[1]), True)
		if servercounttimedout is not 0:
			tostr = "(+"+str(servercounttimedout)+" timed-out)"
		liststr += "\n\n [ "+str(sys.exc_info()[1])+" :: "+ip+":"+str(port)+" ]"
		await qmessage.edit(content="[Loading "+Percent(servercount+servercounttimedout, servercounttotal)+"%] **Server List for Deus Ex** ("+msname+")```css\n\n"+liststr+"\n\nServers: "+str(servercount)+" "+tostr+" Total Players: "+str(totalplayercount)+"```")
		
	return

def _removeNonAscii(s): 
	s = s.replace("", "")
	s = s.replace("", "")
	return "".join(i for i in s if ord(i)<128)

async def msunreal(channel, game):
	global storedservers
	limit = 10
	if game == "unreal":
		limit = 10
		em = discord.Embed(colour=0xFFA500)
	elif game == "deusex":
		limit = 100
		em = discord.Embed(colour=0x000060)
		storedservers.clear()
	else:
		em = discord.Embed(colour=0xBFBFBF)
		
		
	#unr = "http://333networks.com/json/"+game+"?s=numplayers&o=d&r="+limit
	#resp = requests.get(unr).text
	#data = json.loads(resp)

	uMS = tttn.uMS(game, limit)
	
	players = 0
	for item in uMS.servers:
		if game == "deusex":
			storedservers.append(f"{item.ip}:{item.hostport}")
		players += item.numplayers
		country = item.country
		if countryflags.get(country, "") != "":
			country = countryflags.get(country)
		else:
			AddLog(f"========== NO COUNTRY CODE ========= [{item.country}]")
			country = f"🕹"
		em.add_field(name=f"{country} [{uMS.servers.index(item)}] {item.hostname} ({item.numplayers}/{item.maxplayers})", value=f"**Map**: {item.mapname} | **Gametype**: {item.gametype} | **IP**: {item.ip}:{item.hostport}")
	
	results = ""
	if game == "unreal":
		results += f"**Game**: Unreal Gold, {len(uMS.servers)} servers, {players} players. **(master.333networks.com)**\nDue to the amount of servers on Unreal Gold, this is a filtered list.\n**Filters**: `sort=numplayers`, `order=descending`, `limit=10`"
	elif game == "deusex":
		results += f"**Game**: Deus Ex, {len(uMS.servers)} servers, {players} players. **(master.333networks.com)**\nUse `!dx <server index number>` to query a server. (Index: 0-{len(storedservers) - 1})\nUse `!dx <server IP>:<server port>` to query that specific server."
	else:
		results += f"**Game**: {game}, {len(uMS.servers)} servers, {players} players. **(master.333networks.com)**\nThis game has no specific settings designed by Athena."
	em.add_field(name="✅ Notes", value=results)
	#await channel.send(embed=em)
	return em
		
def setup(bot):
	global client
	bot.add_cog(gaming(bot))
	ReadLobby()
	client = bot
