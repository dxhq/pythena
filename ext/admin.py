import discord
from discord.ext import commands
from persona import *
import datetime
import humanfriendly
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
import minerva
import requests
import asyncio
from .utils import checks, time
from collections import Counter
import sys
import psutil
import tttn
from athena_common import *
from operator import itemgetter
from collections import OrderedDict
import hashlib
import copy
import contextlib
	
lastmessage = None
welcomes = {}
suggestions = []

version = "18.10.07.5"
version_discord = discord.__version__
version_python = str(sys.version_info[0])+"."+str(sys.version_info[1])+"."+str(sys.version_info[2])
version_minerva = minerva.__version__
version_persona = PersonaVersion()
version_ttt = tttn.__version__

_changelog = """
Trivia leveled up.
Now with categories, numbered answering, and anti-spam abuse
Cooldown tier whitelisting.
Cooldown access being rewritten, more benefits for each tier. (In-progress)
* Increased limits on !sound and !avatar2 mentions.
Added pwned command, connects to HaveIBeenPwned.com API.
Rewrote global API access code for optimizations.
Redesign of help main menu.

[Previous]
Initial test of "command chain" method, sllowing multiple commands to be executed at once.
In your message, split the commands with `||`, example;
!art||!tts bork||!snd 9000 -> Will trigger !art, then tts, then snd, with a 3 second delay between each.
Lenny has leveled up.
Emote has leveled up.
!kym added, pulls data from KnowYourMeme.
!admins added.
!define2 added as an alternative definition service.
!tr is a basic translator.
Trivia scores have been reset.
Trivia scores are now ordered.
Added some reddit commands, plus other fixes.
Improved reddit NSFW detection.
Improved help messages for "fun" module.
"""

def ReadSuggestions():
	global suggestions
	data = {}
	with open('suggestions.json') as f:
		data = json.load(f)
	suggestions = data


def WriteSuggestions():
	global suggestions
	#AddLog("Writing welcomes")
	with open('suggestions.json', "w") as s:
		json.dump(suggestions, s, indent=4, sort_keys=True)
			
def ReadWelcome():
	global welcomes
	data = {}
	with open('welcome.json') as f:
		data = json.load(f)
	welcomes = data

def WriteWelcome():
	global welcomes
	with open('welcome.json', "w") as s:
		json.dump(welcomes, s, indent=4, sort_keys=True)	

def repls(msg, member, guild):
	msg = msg.replace("(wname)", member.name)
	msg = msg.replace("(wment)", member.mention)
	msg = msg.replace("(guild)", guild.name)
	return msg
	
async def send_goodbye(member):
	ReadWelcome()
	try:
		for gid in welcomes.keys():
			if str(member.guild.id) == gid:
				msg = welcomes[str(gid)]['goodbye']
				target = welcomes[str(gid)]['goodbye_channel']
				if target == "user":
					chan = member
					msg = repls(msg, chan, chan.guild)
				else:
					chan = client.get_channel(int(target))
					msg = repls(msg, member, chan.guild)
				
				await chan.send(msg)
	except KeyError:
		pass
			
async def send_hello(member):
	ReadWelcome()
	try:
		for gid in welcomes.keys():
			#AddLog(gid)
			if str(member.guild.id) == gid:
				msg = welcomes[str(gid)]['welcome']
				target = welcomes[str(gid)]['welcome_channel']
				if target == "user":
					chan = member
					msg = repls(msg, chan, chan.guild)
				else:
					chan = client.get_channel(int(target))
					msg = repls(msg, member, chan.guild)
				await chan.send(msg)
	except:
		pass
		
async def delay_command(message, wait_for=3):
	await asyncio.sleep(3)
	if message.content.startswith(" "):
		message.content = message.content[1:]
	await client.process_commands(message)	
						  
class admin:
	"""Commands and systems that keep the bot running.
	Many of the systems in this extension are access-restricted.
	Other commands are publically available, but are still related to administration."""
	def __init__(self, bot):
		self.bot = bot	
		#bot.remove_command('help')	
		bot.description = f"Athena Version: {version}\nDiscord API Version: {version_discord} REWRITE\nPython Version: {version_python}\n\nCustom Modules: MINERVA security system ({version_minerva}), PERSONA adaptive messages and mood ({version_persona}), 333networks Wrapper ({version_ttt})."
		
		AddLog(f'Script version: {version}', colour="green")
		AddLog(f'Discord.py version: {version_discord}', colour="green")
		AddLog(f'Python version: {version_python}', colour="green")
		
		@bot.listen('on_guild_join')
		async def bot_join(guild):
			ow = client.get_user(client.owner_id)
			await ow.send(f"**Connected to guild: {guild.name} ({guild.id})**")
			
		@bot.listen('on_guild_remove')
		async def bot_rem(guild):
			ow = client.get_user(client.owner_id)
			await ow.send(f"**Disconnected from guild: {guild.name} ({guild.id})**")
		
		@bot.listen('on_message')
		async def auth_msg(message):
			if "i agree" in message.content.lower():
				data = getConfig()[str(message.guild.id)]
				if data['auth']:
					_new = None
					for item in message.guild.roles:
						if item.id == data['authrole']:
							_new = item
							
					if _new not in message.author.roles:
						await message.channel.send(f"Thanks, authenticated. Welcome to {message.guild.name}.")
						await message.author.add_roles(_new, reason="Authenticated.")
					
		@bot.listen('on_message')
		async def message_proc(message):
			global lastmessage
			lastmessage = message
			if "||" in message.content and not message.author.bot:
				main_body = message.content
				command_list = main_body.split("||")
				lim = 2
				access_level = get_tier(message.author)
				if access_level == 1:
					lim = 3
				elif access_level == 2:
					lim = 4
				elif access_level == 3:
					lim = 5
				if len(command_list) > lim:
					await message.channel.send(f"Command chain for your access level can not exceed {lim}. Only the first {lim} will be processed.")
					
				i = 0
				for item in command_list:
					chainmsg = copy.copy(message)
					chainmsg.content = main_body.split("||")[i]
					i += 1
					if i > lim:
						break
					await client.loop.create_task(delay_command(chainmsg))
		
		@bot.listen('on_member_join')
		async def bot_welcome(member):
			await send_hello(member)
			
		@bot.listen('on_member_remove')	
		async def bot_goodbye(member):
			await send_goodbye(member)
				
		@bot.listen('on_command_completion')
		async def command_completion_handler(ctx):
			with contextlib.suppress(discord.errors.NotFound):
				await ctx.message.add_reaction(Emote())
				if ctx.guild is None:
					return
				data = {}
				with open('stats.json') as f:
					data = json.load(f)
					
				try:
					com = ""
					try:
						com = ctx.command.parent.name
					except:
						com = ctx.command.name
					data['commands'][com] += 1
				except:
					com = ""
					try:
						com = ctx.command.parent.name
					except:
						com = ctx.command.name
					data['commands'][com] = 1
				
				try:
					data['channels'][str(ctx.channel.id)] += 1
				except:
					data['channels'][str(ctx.channel.id)] = 1
					
				try:
					data['users'][str(ctx.author.id)] += 1
				except:
					data['users'][str(ctx.author.id)] = 1
					
				try:
					data['guilds'][str(ctx.guild.id)] += 1
				except:
					data['guilds'][str(ctx.guild.id)] = 1
						
				with open('stats.json', "w") as s:
					json.dump(data, s, indent=4, sort_keys=True)	
				
		@bot.listen('on_error')
		async def generic_error_handler(event, *args, **kwargs):
			global lastmessage
			AddLog("Error found: "+str(sys.exc_info()), error=True)
			AddLog(f"Source Error: {lastmessage.author}\nSource Event: {event}")
			await exception(bot, lastmessage)
		
		@bot.listen('on_command_error')
		async def command_error_handler(ctx, error):
			PersonaError()
			AddLog(str(type(error)))
			AddLog(str(error), error=True)

			#if bOnline is True and lastmessage is not None:
			if isinstance(error, commands.errors.CommandOnCooldown):
				AddLog(str(error.retry_after))
				cdmod = 100
				try:
					if ctx.author.id in getConfig()['core']['whitelist']: #ctx.author.id == client.owner_id or 
						AddLog("Whitelist - Bypassing cooldown.")
						await ctx.reinvoke()
						ctx.command.reset_cooldown(ctx)
						return
				except KeyError:
					setConfig(parent="core", key="whitelist", value=[])
					AddLog(f"Created whitelisted users cfg key.")
				except Exception as e:
					await ctx.send(f"`{e}`")
					
				try:
					if cdmod == 100 and ctx.author.top_role.id in getConfig()[str(ctx.guild.id)]['role_1']: #top tier
						cdmod = 1.4
						AddLog(f"{error.retry_after-error.cooldown.per/cdmod}")
						if (error.retry_after-error.cooldown.per/cdmod) < 0:
							await ctx.send("[TIER_1 ROLE DEBUG] Bypassing cooldown.")
							await ctx.reinvoke()
							ctx.command.reset_cooldown(ctx)
							return
				except KeyError:
					setConfig(parent=str(ctx.guild.id), key='role_1', value=[])
					AddLog(f"Created key for tier_1 in guild {ctx.guild.name}")
				except Exception as e:
					await ctx.send(f"`{e}`")
					
				try:	
					if cdmod == 100 and ctx.author.top_role.id in getConfig()[str(ctx.guild.id)]['role_2']: #second tier
						cdmod = 2
						AddLog(f"{error.retry_after-error.cooldown.per/cdmod}")
						if (error.retry_after-error.cooldown.per/cdmod) < 0:
							await ctx.send("[TIER_2 ROLE DEBUG] Bypassing cooldown.")
							await ctx.reinvoke()
							ctx.command.reset_cooldown(ctx)
							return
				except KeyError:
					setConfig(parent=str(ctx.guild.id), key='role_2', value=[])
					AddLog(f"Created role key for tier_2 in guild {ctx.guild.name}")
				except Exception as e:
					await ctx.send(f"`{e}`")
						
				try:
					if cdmod == 100 and ctx.author.id in getConfig()[str(ctx.guild.id)]['user_1']: #top tier
						cdmod = 1.4
						AddLog(f"{error.retry_after-error.cooldown.per/cdmod}")
						if (error.retry_after-error.cooldown.per/cdmod) < 0:
							await ctx.send("[TIER_1 USER DEBUG] Bypassing cooldown.")
							await ctx.reinvoke()
							ctx.command.reset_cooldown(ctx)
							return
				except KeyError:
					setConfig(parent=str(ctx.guild.id), key='user_1', value=[])
					AddLog(f"Created user key for tier_1 in guild {ctx.guild.name}")
				except Exception as e:
					await ctx.send(f"`{e}`")
					
				try:	
					if cdmod == 100 and ctx.author.id in getConfig()[str(ctx.guild.id)]['user_2']: #second tier
						cdmod = 2
						AddLog(f"{error.retry_after-error.cooldown.per/cdmod}")
						if (error.retry_after-error.cooldown.per/cdmod) < 0:
							await ctx.send("[TIER_2 USER DEBUG] Bypassing cooldown.")
							await ctx.reinvoke()
							ctx.command.reset_cooldown(ctx)
							return
				except KeyError:
					setConfig(parent=str(ctx.guild.id), key='user_2', value=[])
					AddLog(f"Created user key for tier_2 in guild {ctx.guild.name}")				
				except Exception as e:
					await ctx.send(f"`{e}`")
					
				AddLog(f"{error.retry_after-error.cooldown.per/cdmod}")
				
				#await ctx.message.add_reaction('⌛')
				await ctx.message.add_reaction(client.get_emoji(480337167038676992))
				await ctx.message.add_reaction(Emote())
				cd = humanfriendly.format_timespan(int(str(error.retry_after-error.cooldown.per/cdmod).split(".")[0]), True)
				# msg_generator(types='cooldown, context=ctx.invokedwith)
				await ctx.send(embed=discord.Embed(description=f"{ctx.author.mention} {msg_generator(types='cooldown', context=ctx.prefix+ctx.invoked_with)} (Try again in **{cd}**).\n**This message will be deleted when the cooldown expires.**", colour=0xFF0000), delete_after=error.retry_after-error.cooldown.per/cdmod)
				return
			
			elif isinstance(error, athenaerrs):
				await ctx.message.add_reaction("❎")
				await ctx.send(embed=discord.Embed(description=error.msg, colour=0xFF0000))
				return
				
			elif isinstance(error, commands.errors.NoPrivateMessage):
				await ctx.message.add_reaction("❎")
				await ctx.send(embed=discord.Embed(description=f"{ctx.author.mention} The command `{ctx.prefix}{ctx.invoked_with}` is only available in guilds, can not be used in PM's.", colour=0xFF0000))
				return		
						
			elif isinstance(error, commands.errors.DisabledCommand):
				await ctx.message.add_reaction(client.get_emoji(480337167038676992))
				await ctx.send(embed=discord.Embed(description=f"{ctx.author.mention} The command `{ctx.prefix}{ctx.invoked_with}` is currently unavailable.", colour=0xFF0000))
				return
				
			elif isinstance(error, commands.MissingPermissions):
				await ctx.message.add_reaction(client.get_emoji(480337167038676992))
				await ctx.message.add_reaction(Emote())
				await ctx.send(embed=discord.Embed(description=f"{ctx.author.mention} {msg_generator(types='denied')}\n**Source**: `{ctx.prefix}{ctx.invoked_with}`.\n**Missing Permissions**: {HumanizeList(error.missing_perms)}.", colour=0xFF0000))
				return
				
			elif isinstance(error, commands.CheckFailure):
				AddLog("Check fail: "+ctx.invoked_with)
				await ctx.message.add_reaction(client.get_emoji(480337167038676992))
				await ctx.message.add_reaction(Emote())
				await ctx.send(embed=discord.Embed(description=f"{ctx.author.mention} {msg_generator(types='denied')}\n**Source**: `{ctx.prefix}{ctx.invoked_with}`.", colour=0xFF0000))
				return
				
			elif isinstance(error, discord.ext.commands.errors.CommandNotFound):
				#if ctx.prefix != "!":
				matches = FindMatchCommands(ctx.invoked_with)
				if matches != "":
					await ctx.send(f"{error}\n{matches}")
					await ctx.message.add_reaction(client.get_emoji(480337167038676992))
					await ctx.message.add_reaction(Emote())
				return
					
			elif isinstance(error, discord.ext.commands.errors.MissingRequiredArgument):
				#if ctx.invoked_subcommand is None:
				#	help_cmd = self.bot.get_command('help')
				#	await ctx.invoke(help_cmd, ctx.invoked_with)
				farg = str(error.param)
				if ":" in farg:
					farg = farg.split(":")[0]
				com = ""
				if ctx.invoked_subcommand:
					com += str(ctx.invoked_subcommand)
				else:
					com = str(ctx.invoked_with)
				await ctx.send(embed=discord.Embed(description=f"{msg_generator(types='missing_arg', context=ctx.invoked_with)}\n**Suggested Command Format**: {ctx.prefix}{com} <{farg}>", colour=0xFF0000))
				await ctx.message.add_reaction(client.get_emoji(480337167038676992))
				await ctx.message.add_reaction(Emote())
				return
				
			else:
				await ctx.message.add_reaction(client.get_emoji(480337167038676992))
				await ctx.message.add_reaction(Emote())
				await ctx.send(f"`{str(error)}`")
			
			if ctx.channel.name != "botmasters-testzone":
				source = f"Guild = {ctx.message.guild.name}\nChannel = {ctx.message.channel.name}\nAuthor = {ctx.message.author.name} ({ctx.message.author.display_name})```{ctx.message.content.replace('```', '')}```"
				await AthNotif(client, f"### Unhandled command error log ###\n**Source**: \n{source}\n**Error**:```{type(error)}\n{error}```")

	@commands.command()
	@commands.is_owner()
	async def shutdown(self, ctx):
		await die(ctx, client)
		
	@commands.command(hidden=True)
	@is_tier(1)
	async def tiertest1(self, ctx):
		await ctx.send("Tier 1 pass.")

	@commands.command(hidden=True)
	@is_tier(2)
	async def tiertest2(self, ctx):
		await ctx.send("Tier 2 pass.")
		
	@commands.command(hidden=True)
	@is_tier(3)
	async def tiertest3(self, ctx):
		await ctx.send("Tier 3 pass.")	
	
	@commands.guild_only()
	@commands.group(invoke_without_command=True)
	async def guild_auth(self, ctx):	
		"""
		Manages the Guild Authentication protocols.
		Format:
		* If all channels except a welcome channel for example, but you supply an "authenticated role" e.g. "Users" which has access to all the channels, having the new member say "i agree" will automatically give the authenticated role to the member and open the server to them.
		"""
		s = ""
		data = None
		_auth = False
		_auth_role = None
		
		try:
			data = getConfig()[str(ctx.guild.id)]
		except:
			await ctx.send("Guild settings not available. (UNKNOWN ERR)")
			return
		
		try:
			_auth = data['auth']
		except:
			setConfig(parent=str(ctx.guild.id), key="auth", value=False)
			_auth = False
			
		if _auth:
			s += "Authentication enabled.\n"	
		else:
			s += "Authentication disabled.\n"	
	
		try:
			_auth_role = data['auth_role']
		except:
			setConfig(parent=str(ctx.guild.id), key="authrole", value=0)
			_auth_role = 0
			
		if _auth_role != 0:
			get_role(ctx, _auth_role)
			s += f"Authentication Role is {_auth_role.mention}.\n"	
		else:
			s += "Authentication Role not found.\n"	
		
		await ctx.send(embed=discord.Embed(description=s))	

	@guild_auth.command(name="disable")
	@access_check()
	@commands.guild_only()
	async def _ga_off(self, ctx):
		"""Turns off authentication. (Note; does not un-hide any channels automatically, only turns off the authentication code)"""
		s = ""
		data = None
		_auth = False
		
		try:
			data = getConfig()[str(ctx.guild.id)]
		except:
			await ctx.send("Guild settings not available. (UNKNOWN ERR)")
			return

		try:
			_auth = data['auth']
		except:
			setConfig(parent=str(ctx.guild.id), key='auth', value=False)
			_auth = False
			
		if _auth:
			s += "Authentication has been disabled.\n"	
			await ctx.send(embed=discord.Embed(description=s))	
			setConfig(parent=str(ctx.guild.id), key='auth', value=False)	
			return				
		else:
			s += "Authentication isn't enabled.\n"	
			await ctx.send(embed=discord.Embed(description=s))	
						
	@guild_auth.command(name="enable")
	@access_check()
	@commands.guild_only()
	async def _ga_on(self, ctx):
		"""Turns on authentication if all parameters check okay."""
		s = ""
		data = None
		_auth = False
		_auth_role = None
		
		try:
			data = getConfig()[str(ctx.guild.id)]
		except:
			await ctx.send("Guild settings not available. (UNKNOWN ERR)")
			return
		
		try:
			_auth = data['auth']
		except:
			setConfig(parent=str(ctx.guild.id), key="auth", value=False)
			_auth = False
			
		if _auth:
			s += "Authentication already enabled.\n"	
			await ctx.send(embed=discord.Embed(description=s))		
			return
			
		try:
			_auth_role = data['authrole']
		except:
			setConfig(parent=str(ctx.guild.id), key="authrole", value=0)
			_auth_role = 0
			
		if _auth_role != 0:
			_auth_role = get_role(ctx, _auth_role)
			s += f"Authentication Role is {_auth_role.mention}.\n"	
		else:
			s += "Authentication Role not found.\n"	
			await ctx.send(embed=discord.Embed(description=s))	
			return
		
		setConfig(parent=str(ctx.guild.id), key='auth', value=True)
		s += "Authentication has been enabled."
		await ctx.send(embed=discord.Embed(description=s))	

	@guild_auth.command(name="role")
	@access_check()
	@commands.guild_only()
	async def _ga_role_in(self, ctx, role_id:int):
		"""If a none-zero integer is given, adds the role as the authenticated role based on the ID."""
		if role_id == 0:
			setConfig(parent=str(ctx.guild.id), key='authrole', value=0)
			await ctx.send("Authenticated role has been disabled.")
			return
			
		_role = get_role(ctx, role_id)
		if _role:
			setConfig(parent=str(ctx.guild.id), key='authrole', value=_role.id)
			await ctx.send(f"Authenticated role is set as {_role.name}")
		else:
			await ctx.send("Authenticated role not set. (ERROR: Role not found)")
			
	@commands.group(name="tiers", invoke_without_command=True)
	async def tiers_config(self, ctx):
		"""User/role tier whitelisting manager.
		Run with no sub-command to show current guild tiers.
		
		Note: The advantages of each tier will change over time.
		Tier 1 has 25% cooldown time.
		Tier 2 has 50% cooldown time.
		Users whitelisted by bot creator have no cooldowns."""
		em = discord.Embed()
		keys = ['user_1', 'user_2', 'role_1', 'role_2']
		data = getConfig()[str(ctx.guild.id)]
		failed = False
		for item in keys:
			try:
				_tmp = data[item]
			except:
				failed = True
				setConfig(parent=str(ctx.guild.id), key=item, value=[])
				print(item+" failed")
				
		if failed:
			data = getConfig()[str(ctx.guild.id)]
			
		f = False
		if len(data['user_1']) > 0:
			f = True
			s = []
			for item in data['user_1']:
				s.append(get_member_in_guild(ctx.guild, item).mention)
			em.add_field(name="Tier 1 Users (Highest Access)", value=HumanizeList(s))

		if len(data['user_2']) > 0:
			f = True
			s = []
			for item in data['user_2']:
				s.append(get_member_in_guild(ctx.guild, item).mention)
			em.add_field(name="Tier 2 Users (Better Access)", value=HumanizeList(s))

		if len(data['role_1']) > 0:
			f = True
			s = []
			for item in data['role_1']:
				s.append(get_role(ctx, item).mention)
			em.add_field(name="Tier 1 Roles (Highest Access)", value=HumanizeList(s))

		if len(data['role_2']) > 0:
			f = True
			s = []
			for item in data['role_2']:
				s.append(get_role(ctx, item).mention)
			em.add_field(name="Tier 2 Roles (Better Access)", value=HumanizeList(s))
		
		if f:
			await ctx.send(embed=em)
		else:
			await ctx.send(embed=discord.Embed(description="No access granted in this guild."))
	
	@commands.is_owner()				
	@commands.command(name="whitelist")
	async def _users_add_whitelist(self, ctx, user):
		f = None
		if user.isdigit():
			f = get_member_in_guild(ctx.guild, int(user))
		else:
			f = get_member_in_guild_by_name(ctx.guild, user)
			
		if f:
			m_list = []
			try:
				m_list = getConfig()['core']['whitelist']
			except:
				setConfig(parent="core", key="whitelist", value=[])
				await ctx.send(f"Whitelist key created.")
				m_list = getConfig()['core']['whitelist']
			
			if f.id not in m_list:
				m_list.append(f.id)
				master = getConfig()
				master['core']['whitelist'] = m_list
				dumpConfig(master)
				await ctx.send(f"User {f.name} added to global whitelist access.")
			else:
				await ctx.send(f"User {f.name} is already in the list.")
		else:
			await ctx.send(f"User {user} not found.")
	
	@commands.is_owner()							
	@commands.command(name="dewhitelist")
	async def _users_rem_whitelist(self, ctx, user):
		f = None
		if user.isdigit():
			f = get_member_in_guild(ctx.guild, int(user))
		else:
			f = get_member_in_guild_by_name(ctx.guild, user)
			
		if f:
			m_list = []
			try:
				m_list = getConfig()['core']['whitelist']
			except:
				setConfig(parent="core", key="whitelist", value=[])
				await ctx.send(f"Whitelist key created.")
				m_list = getConfig()['core']['whitelist']
			
			if f.id in m_list:
				m_list.remove(f.id)
				master = getConfig()
				master['core']['whitelist'] = m_list
				dumpConfig(master)
				
				await ctx.send(f"User {f.name} removed from global whitelist access.")
			else:
				await ctx.send(f"User {f.name} is not in the list.")
		else:
			await ctx.send(f"User {user} not found.")
	
	@commands.command(name="whitelisted")
	async def _show_whitelist(self, ctx):
		em = discord.Embed()
		
		data = getConfig()['core']
		if len(data['whitelist']) > 0:
			s = []
			for item in data['whitelist']:
				s.append(get_member(client, item).mention)
			em.add_field(name="Whitelisted Users", value=HumanizeList(s))

			await ctx.send(embed=em)
		else:
			await ctx.send(embed=discord.Embed(description="No access granted in this guild."))
			
	@access_check()	
	@tiers_config.command(name="adduser")
	async def _users_add(self, ctx, tier:int, user):
		if tier != 1 and tier != 2:
			await ctx.send("Tier must be either 1 or 2.")
			return
			
		f = None
		if user.isdigit():
			f = get_member_in_guild(ctx.guild, int(user))
		else:
			f = get_member_in_guild_by_name(ctx.guild, user)
			
		if f:
			m_list = []
			try:
				m_list = getConfig()[str(ctx.guild.id)][f'user_{tier}']
			except:
				setConfig(parent=str(ctx.guild.id), key=f"user_{tier}", value=[])
				await ctx.send(f"Tier {tier} key created in this guild.")
				m_list = getConfig()[str(ctx.guild.id)][f'user_{tier}']
			
			if f.id not in m_list:
				m_list.append(f.id)
				master = getConfig()
				master[str(ctx.guild.id)][f'user_{tier}'] = m_list
				dumpConfig(master)
				await ctx.send(f"User {f.name} added to tier {tier} access.")
			else:
				await ctx.send(f"User {f.name} is already in the list.")
		else:
			await ctx.send(f"User {user} not found.")
	
	@access_check()				
	@tiers_config.command(name="remuser")
	async def _users_rem(self, ctx, tier:int, user):
		if tier != 1 and tier != 2:
			await ctx.send("Tier must be either 1 or 2.")
			return
			
		f = None
		if user.isdigit():
			f = get_member_in_guild(ctx.guild, int(user))
		else:
			f = get_member_in_guild_by_name(ctx.guild, user)
			
		if f:
			m_list = []
			try:
				m_list = getConfig()[str(ctx.guild.id)][f'user_{tier}']
			except:
				setConfig(parent=str(ctx.guild.id), key=f"user_{tier}", value=[])
				await ctx.send(f"Tier {tier} key created in this guild.")
				m_list = getConfig()[str(ctx.guild.id)][f'user_{tier}']
			
			if f.id in m_list:
				m_list.remove(f.id)
				master = getConfig()
				master[str(ctx.guild.id)][f'user_{tier}'] = m_list
				dumpConfig(master)
				await ctx.send(f"User {f.name} removed from tier {tier} access.")
			else:
				await ctx.send(f"User {f.name} is not in the list.")
		else:
			await ctx.send(f"User {user} not found.")
		
	@access_check()				
	@tiers_config.command(name="addrole")
	async def _roles_add(self, ctx, tier:int, role):
		if tier != 1 and tier != 2:
			await ctx.send("Tier must be either 1 or 2.")
			return
			
		f = None
		if role.isdigit():
			for newrole in ctx.guild.roles:
				if newrole.id == int(role):
					f = newrole
		else:
			for newrole in ctx.guild.roles:
				if newrole.name.lower() == role.lower():
					f = newrole
			
		if f:
			m_list = []
			try:
				m_list = getConfig()[str(ctx.guild.id)][f'role_{tier}']
			except:
				setConfig(parent=str(ctx.guild.id), key=f"role_{tier}", value=[])
				await ctx.send(f"Tier {tier} role key created in this guild.")
				m_list = getConfig()[str(ctx.guild.id)][f'role_{tier}']
			
			if f.id not in m_list:
				m_list.append(f.id)
				master = getConfig()
				master[str(ctx.guild.id)][f'role_{tier}'] = m_list
				dumpConfig(master)
				await ctx.send(f"Role {f.name} added to tier {tier} access.")
			else:
				await ctx.send(f"Role {f.name} is already in the list.")
		else:
			await ctx.send(f"Role {role} not found.")	

	@access_check()	
	@tiers_config.command(name="remrole")
	async def _roles_rem(self, ctx, tier:int, role):
		if tier != 1 and tier != 2:
			await ctx.send("Tier must be either 1 or 2.")
			return
			
		f = None
		if role.isdigit():
			for newrole in ctx.guild.roles:
				if newrole.id == int(role):
					f = newrole
		else:
			for newrole in ctx.guild.roles:
				if newrole.name.lower() == role.lower():
					f = newrole
			
		if f:
			m_list = []
			try:
				m_list = getConfig()[str(ctx.guild.id)][f'role_{tier}']
			except:
				setConfig(parent=str(ctx.guild.id), key=f"role_{tier}", value=[])
				await ctx.send(f"Tier {tier} role key created in this guild.")
				m_list = getConfig()[str(ctx.guild.id)][f'role_{tier}']
			if f.id in m_list:
				m_list.remove(f.id)
				master = getConfig()
				master[str(ctx.guild.id)][f'role_{tier}'] = m_list
				dumpConfig(master)
				await ctx.send(f"Role {f.name} removed from tier {tier} access.")
			else:
				await ctx.send(f"Role {f.name} is not in the list.")
		else:
			await ctx.send(f"Role {role} not found.")	
	
	@commands.command()
	async def tt(self, ctx, num:int):
		f = get_member_in_guild(ctx.guild, num)
		ti = get_tier(f)
		await ctx.send(f"Tier {ti}")		
						
		
	@commands.command(name='help2')
	async def _help(self, ctx):
		"""Shows help about a command or the bot"""
		em = discord.Embed(description=f"{client.description}**Type !help command for more info on a command.You can also type !help category for more info on a category.**")
		for item in sorted(client.cogs.keys()):
			em.add_field(name=item, value=client.get_cog(item).__doc__)
			
		await ctx.send(embed=em)
		
	@commands.command()
	@commands.is_owner()
	async def loop(self, ctx, times:int, *, command):
		"""Loop a command a x times."""
		try:
			msg = copy.copy(ctx.message)
			msg.content = command
			for i in range(times):
				await self.bot.process_commands(msg)
		except Exception as e:
			await ctx.send(e)
			
	@commands.group(name='hash', invoke_without_command=True)
	async def hash_cmd(self, *, txt:str):
		"""MD5 Encrypt Text"""
		md5 = hashlib.md5(txt.encode()).hexdigest()
		await ctx.send('**MD5**\n'+md5)

	@hash_cmd.command(name='sha1')
	async def hash_sha1(self, *, txt:str):
		"""SHA1 Encrypt Text"""
		sha = hashlib.sha1(txt.encode()).hexdigest()
		await ctx.send('**SHA1**\n'+sha)

	@hash_cmd.command(name='sha256')
	async def hash_sha256(self, *, txt:str):
		"""SHA256 Encrypt Text"""
		sha256 = hashlib.sha256(txt.encode()).hexdigest()
		await ctx.send('**SHA256**\n'+sha256)

	@hash_cmd.command(name='sha512')
	async def hash_sha512(self, *, txt:str):
		"""SHA512 Encrypt Text"""
		sha512 = hashlib.sha512(txt.encode()).hexdigest()
		await ctx.send('**SHA512**\n'+sha512)
		
	@commands.command(aliases=['staff', 'onlineadmins', 'modsonline', 'mods', 'onlinemods'])
	async def admins(self, ctx, level:int=1):
		"""Show current online admins and mods"""
		await ctx.channel.trigger_typing()
		# roles_ = ['admin', 'mod', 'moderator', 'administrator', 'owner', 'underadministrator', 'moderators', 'founder']
		mod_perms = ['manage_messages', 'ban_members', 'kick_members']
		admin = []
		mod = []
		admin_busy = []
		mod_busy = []
		admin_off = []
		mod_off = []							
		for member in ctx.message.guild.members:
			if not member.bot:
				if member.permissions_in(ctx.channel).administrator or member.permissions_in(ctx.channel).manage_guild:
					if str(member.status) == "online":
						admin.append(member.mention)
					elif str(member.status) == "offline" or str(member.status) == "invisible":
						admin_off.append(member.mention)
					elif str(member.status) == "dnd" or str(member.status) == "idle":
						admin_busy.append(member.mention)
		
		for member in ctx.message.guild.members:	
			if not member.bot:
				if member.permissions_in(ctx.channel).manage_messages or member.permissions_in(ctx.channel).ban_members or member.permissions_in(ctx.channel).kick_members:
					if member.mention not in admin and member.mention not in admin_busy and member.mention not in admin_off:
						if str(member.status) == "online":
							mod.append(member.mention)
						elif str(member.status) == "offline" or str(member.status) == "invisible":
							mod_off.append(member.mention)
						elif str(member.status) == "dnd" or str(member.status) == "idle":
							mod_busy.append(member.mention)
			
		em = discord.Embed()
		
		if level < 1 or level > 3:
			level = 1
			
		if level == 1:	
			if len(admin) == 0 and len(mod) == 0:
				em.add_field(name="Error", value="No staff were found that are online!")
				await ctx.send(embed=em)
				return
			else:
				if len(admin) > 0:
					f = ""
					for item in admin:
						f += "<:loaded:480337167080488960> "+item+"\n"
					em.add_field(name="Admins", value=f)
				if len(mod) > 0:
					f = ""
					for item in mod:
						f += "<:loaded:480337167080488960> "+item+"\n"
					em.add_field(name="Moderators", value=f)

		if level == 2:	
			if len(admin) == 0 and len(mod) == 0 and len(mod_busy) == 0 and len(admin_busy) == 0:
				em.add_field(name="Error", value="No staff were found that are available!")
				await ctx.send(embed=em)
				return
			else:
				if len(admin) > 0 or len(admin_busy) > 0:
					f = ""
					for item in admin:
						f += "<:loaded:480337167080488960> "+item+"\n"
					for item in admin_busy:
						f += "<:loaded:480337167080488960> "+item+"\n"
					em.add_field(name="Admins", value=f)
				
				if len(mod) > 0 or len(mod_busy) > 0:
					f = ""
					for item in mod:
						f += "<:loaded:480337167080488960> "+item+"\n"
					for item in mod_busy:
						f += "<:loadother:480426177748533249> "+item+"\n"
					em.add_field(name="Moderators", value=f)	

		if level == 3:	
			if len(admin) == 0 and len(mod) == 0 and len(mod_busy) == 0 and len(admin_busy) == 0 and len(mod_off) == 0 and len(admin_off) == 0:
				em.add_field(name="Error", value="No staff were found... at all... wtf!")
				await ctx.send(embed=em)
				return
			else:
				if len(admin) > 0 or len(admin_busy) > 0 or len(admin_off) > 0:
					f = ""
					for item in admin:
						f += "<:loaded:480337167080488960> "+item+"\n"
					for item in admin_busy:
						f += "<:loaded:480337167080488960> "+item+"\n"
					for item in admin_off:
						f += "<:loadfail:480337167038676992> "+item+"\n"
					em.add_field(name="Admins", value=f)
				if len(mod) > 0 or len(mod_busy) > 0 or len(mod_off) > 0:
					f = ""
					for item in mod:
						f += "<:loaded:480337167080488960> "+item+"\n"
					for item in mod_busy:
						f += "<:loadother:480426177748533249> "+item+"\n"
					for item in mod_off:
						f += "<:loadfail:480337167038676992> "+item+"\n"
					em.add_field(name="Moderators", value=f)	
									
		await ctx.send(embed=em)
			
	@access_check()
	@commands.command()
	async def strict_nsfw(self, ctx, strictness="false"):
		set_strict_nsfw(ctx, boolinate(strictness))
		await ctx.send(f"NSFW Strictness level for your guild is now {boolinate(strictness)}")
			
	@commands.is_owner()
	@commands.group(name="debug", hidden=True)
	async def debug_command(self, ctx):
		pass
	
	@debug_command.command(name="set")
	async def _set(self, ctx, parent, key, name):
		await ctx.send(f"Trying to set {parent}:{key}:{name}")
		setConfig(parent=parent, key=key, value=name)
		await ctx.send(getConfig()[parent][key])
	
	@debug_command.command(name="get")
	async def _get(self, ctx, parent, key):
		await ctx.send(getConfig()[parent][key])
	
	@commands.is_owner()
	@commands.command()
	async def cfg(self, ctx, parent:str="", key:str="", *, value=""):
		"""Accesses config file.
		
		Formatting:
		* if args are null; shows all keys
		* if parent entered, key is null; shows all keys in the parent
		* if parent and key entered, value is null; shows all keys in parent.key
		* if all 3 args entered, sets value for parent.key and saves.
		
		Shortcuts:
		* if parent is guild; accesses current guild settings
		* if parent is me;  accesses your user settings
		
		* when setting values
		* * if value is ERASE or DEL; deletes the current key
		* * if value is none or null; sets current key as blank
		* * if value is [] or new_list; sets current key as a list type
		* * if word "int" in value and the rest of the key is a valid integer; sets current key as int type instead of default string type
		"""
		await ctx.channel.trigger_typing()
		if parent == "":
			f = getConfig()
			s = ""
			for item in f.keys():
				if item.isdigit():
					fnd = get_member(client, int(item)) or client.get_guild(int(item))
					if fnd:
						s += f"**{fnd.name}** ({item})\n"
					else:
						s += f"**{item}**\n"
				else:
					s += f"**{item}**\n"
			await ctx.send(embed=discord.Embed(description=s))
			return
		
		if parent == "guild":
			parent = str(ctx.guild.id)
		
		if parent == "me":
			parent = str(ctx.author.id)
			
		if key == "":
			try:
				f = getConfig()[parent]
			except:
				await ctx.send(embed=discord.Embed(description="Invalid cfg key."))
				return
				
			s = ""
			for item in f.keys():
				if item == "token" or item == "password":
					s += f"**{item}** = ~~redacted~~\n"
				elif item == "":
					s += f"**{item}** = None\n"
				else:
					s += f"**{item}** = {f[item]} *{type(f[item])}*\n"
			
			await ctx.send(embed=discord.Embed(description=s))
			return
		
		if value == "":
			try:
				f = getConfig()[parent][key]
			except:
				await ctx.send(embed=discord.Embed(description="Invalid cfg key."))
				return
				
			if key == "token" or key == "password":
				s = "Viewing this var is not permitted in chat."
			else:
				s = f"**{parent}.{key}** = {f} *{type(f)}*\n"
			
			await ctx.send(embed=discord.Embed(description=s))		
			return
		
		if value == "ERASE"	or value == "DEL":
			try:
				data = getConfig()
				if data['core']['cfg_safe']:
					await ctx.send(embed=discord.Embed(colour=0xFF0000, description="Settings are locked, values can not be erased.\n`cfg core cfg_safe False` to disable."))
					return
				_old = data[parent][key]	
				del data[parent][key]
				dumpConfig(data)
				await ctx.send(embed=discord.Embed(description=f"**Value erased**: {parent}.{key}\n**Value**: {_old}"))
				return
			except:
				await ctx.send(embed=discord.Embed(description=f"**Value doesn't exist.**"))
				return
			
		if value == "none" or value == "null":
			value = ""
		
		if value == "new_list" or value == "[]":
			value = []
			
		if "int" in value:
			_value = value.replace("int", "")
			if _value.isdigit():
				value = int(_value)
				
		if value == "True" or value == "False":
			value = boolinate(value)	
		
		try:		
			_old = getConfig()[parent][key]
			if getConfig()['core']['cfg_safe']:
				if type(_old) != type(value):
					await ctx.send(embed=discord.Embed(colour=0xFF0000, description="Settings are locked, value type mismatch.\n`cfg core cfg_safe False` to disable."))
					return
			setConfig(parent=parent, key=key, value=value)
			await ctx.send(embed=discord.Embed(description=f"**Key**: {parent}.{key}\n**Previous value**: {_old} *{type(_old)}*\n**Value set**: {value} *{type(value)}*"))
		except:
			if getConfig()['core']['cfg_safe']:
				await ctx.send(embed=discord.Embed(colour=0xFF0000, description="Settings are locked, values can not be added.\n`cfg core cfg_safe False` to disable."))
				return
			setConfig(parent=parent, key=key, value=value)
			await ctx.send(embed=discord.Embed(description=f"🆕 **Value set**: {parent}.{key} = {value} *{type(value)}*"))
			
	@commands.command()
	async def changes(self, ctx):
		await ctx.channel.trigger_typing()
		await ctx.send(f"```ini\n[{version}]{_changelog}\n```")
		
	@commands.command(name="feedback", aliases=['fb'])
	async def _feedback(self, ctx, *, feedback_message):
		"""Sends a message directly to the bot's owner."""
		ow = client.get_user(client.owner_id)
		await ctx.send(f"Thanks, your feedback has been sent to {ow.name}. :)")
		await ow.send(f"**Feedback from {ctx.author.name} ({ctx.author.id}).**", embed=discord.Embed(description=feedback_message))
	
	@commands.is_owner()
	@commands.command(name="dm")
	async def _direct_msg(self, ctx, user_id:int, *, message):
		ow = client.get_user(user_id)
		if ow:
			await ow.send(f"**Message from {ctx.author.name} ({ctx.author.id}).**", embed=discord.Embed(description=message))
		
	@commands.command(aliases=['rs', 'remote'])
	@commands.is_owner()
	async def remotesay(self, ctx, target_id:int, *, message):
		chan = client.get_channel(target_id) or client.get_user(target_id)
		if chan:
			await chan.send(message)
		else:
			await ctx.send("No target found.")

	@commands.group(invoke_without_command=True)
	@commands.is_owner()
	async def sdel(self, ctx, sug_id:str):
		global suggestions
		ReadSuggestions()
		try:
			await ctx.send(f"Deleted suggestion {sug_id}\n```{suggestions['suggestions'][sug_id]}```")
			del suggestions['suggestions'][sug_id]
		except KeyError:
			await ctx.send("No suggestion found for that key.")
		WriteSuggestions()

	@sdel.command(name="admin", aliases=['a'])
	@commands.is_owner()
	async def _del_admin(self, ctx, sug_id:str):
		global suggestions
		ReadSuggestions()
		try:
			await ctx.send(f"Deleted suggestion {sug_id}\n```{suggestions['suggestions_admin'][sug_id]}```")
			del suggestions['suggestions_admin'][sug_id]
		except Exception as e:
			await ctx.send("No suggestion found for that key.\n"+e)
		WriteSuggestions()

	@sdel.command(name="urgent", aliases=['u'])
	@commands.is_owner()
	async def _del_urgent(self, ctx, sug_id:str):
		global suggestions
		ReadSuggestions()
		try:
			await ctx.send(f"Deleted suggestion {sug_id}\n```{suggestions['suggestions_urgent'][sug_id]}```")
			del suggestions['suggestions_urgent'][sug_id]
		except KeyError:
			await ctx.send("No suggestion found for that key.")
		WriteSuggestions()
		
	@sdel.command(name="low", aliases=['l'])
	@commands.is_owner()
	async def _del_low(self, ctx, sug_id:str):
		global suggestions
		ReadSuggestions()
		try:
			await ctx.send(f"Deleted suggestion {sug_id}\n```{suggestions['suggestions_low'][sug_id]}```")
			del suggestions['suggestions_low'][sug_id]
		except KeyError:
			await ctx.send("No suggestion found for that key.")
		WriteSuggestions()	
					
	@commands.group(invoke_without_command=True, aliases=['suggestions'])
	@commands.is_owner()
	async def suggests(self, ctx):
		global suggestions
		ReadSuggestions()
		
		len_s = len(suggestions['suggestions'].keys())
		len_su = len(suggestions['suggestions_urgent'].keys())
		len_sa = len(suggestions['suggestions_admin'].keys())
		len_sl = len(suggestions['suggestions_low'].keys())
		
		s = f"Suggestions (**{len_s}**)\nLow-Priority (**{len_sl}**)\nSuggestions by Admin (**{len_sa}**)\nUrgent Suggestions (**{len_su}**)\n\nTotal: **{len_s+len_sa+len_sl+len_su}**"
		
		await ctx.send(embed=discord.Embed(description=s))
	
	@suggests.command(name="all", aliases=['full', 'f'])
	async def _suggestions_all(self, ctx, limit=10):
		global suggestions
		ReadSuggestions()
		s = ""
		cur = 0
		if len(suggestions['suggestions_urgent'].keys()) > 0:
			for item in suggestions['suggestions_urgent'].keys():
				cur += 1
				if cur > limit:
					break	
				s += f"**[URGENT:{item}]** {suggestions['suggestions_urgent'][item]}\n\n"

		if len(suggestions['suggestions'].keys()) > 0:
			for item in suggestions['suggestions'].keys():
				cur += 1
				if cur > limit:
					break	
				s += f"**[MAIN:{item}]** {suggestions['suggestions'][item]}\n\n"

		if len(suggestions['suggestions_admin'].keys()) > 0:
			for item in suggestions['suggestions_admin'].keys():
				cur += 1
				if cur > limit:
					break	
				s += f"**[ADMIN:{item}]** {suggestions['suggestions_admin'][item]}\n\n"

		if len(suggestions['suggestions_low'].keys()) > 0:
			for item in suggestions['suggestions_low'].keys():
				cur += 1
				if cur > limit:
					break	
				s += f"**[LOW:{item}]** {suggestions['suggestions_low'][item]}\n\n"	
		await ctx.send(embed=discord.Embed(description=s))
		
	@suggests.command(name="main", aliases=['m'])
	async def _suggestions_main(self, ctx):
		global suggestions
		ReadSuggestions()
		em = discord.Embed()
		if len(suggestions['suggestions'].keys()) == 0:
			await ctx.send("No suggestions currently.")
			return
		em = discord.Embed()
		for item in suggestions['suggestions'].keys():
			em.add_field(name=item, value=suggestions['suggestions'][item])
		await ctx.send(embed=em)
		
	@suggests.command(name="admin", aliases=['a'])
	async def _suggestions_admin(self, ctx):
		global suggestions
		ReadSuggestions()
		em = discord.Embed()
		if len(suggestions['suggestions_admin'].keys()) == 0:
			await ctx.send("No admin suggestions currently.")
			return
		em = discord.Embed()
		for item in suggestions['suggestions_admin'].keys():
			em.add_field(name=item, value=suggestions['suggestions_admin'][item])
		await ctx.send(embed=em)
	
	@suggests.command(name="low", aliases=['l'])
	async def _suggestions_low(self, ctx):
		global suggestions
		ReadSuggestions()
		em = discord.Embed()
		if len(suggestions['suggestions_low'].keys()) == 0:
			await ctx.send("No low-priority suggestions currently.")
			return
		em = discord.Embed()
		for item in suggestions['suggestions_low'].keys():
			em.add_field(name=item, value=suggestions['suggestions_low'][item])
		await ctx.send(embed=em)
	
	@suggests.command(name="urgent", aliases=['y'])
	async def _suggestions_urgent(self, ctx):
		global suggestions
		ReadSuggestions()
		em = discord.Embed()
		if len(suggestions['suggestions_urgent'].keys()) == 0:
			await ctx.send("No urgent suggestions currently.")
			return
		em = discord.Embed()
		for item in suggestions['suggestions_urgent'].keys():
			em.add_field(name=item, value=suggestions['suggestions_urgent'][item])
		await ctx.send(embed=em)
								
	@commands.group(invoke_without_command=True)
	async def suggest(self, ctx, *, suggestion):
		"""Leave a suggestion for Kaiser regarding the bot.
		Using one of the sub commands let you sort the suggestion in to a specific category.
		
		Examples:
		suggest Bork tings! > leaves a standard suggestion
		suggest urgent I DIED > leaves an urgent suggestion"""
		global suggestions
		ReadSuggestions()
		sug_id = random.randrange(10000, 99999)
		#print(suggestions['suggestions'].keys())
		while sug_id in suggestions['suggestions'].keys():
			sug_id = random.randrange(10000, 99999)

		suggestion_string = f"{ctx.author.display_name} ({ctx.author.name}) [Chan:{ctx.channel.id}  Guild: {ctx.guild.id}] {suggestion}"
		
		try:
			suggestions['suggestions'][str(sug_id)] = suggestion_string
		except:
			suggestions = {'suggestions':{}}
			#suggestions['suggestions'] = {}
			suggestions['suggestions'][str(sug_id)] = suggestion_string
			
		WriteSuggestions()
		await ctx.send(f"{ctx.author.mention} Thanks, your suggestion has been sent and will be reviewed by Kaiser shortly.")

	@access_check()
	@suggest.command(name="admin", aliases=['a'])
	async def _suggest_admin(self, ctx, *, suggestion):
		global suggestions
		ReadSuggestions()
		sug_id = random.randrange(10000, 99999)
		#print(suggestions['suggestions'].keys())
		while sug_id in suggestions['suggestions_admin'].keys():
			sug_id = random.randrange(10000, 99999)

		suggestion_string = f"{ctx.author.display_name} ({ctx.author.name}) [Chan:{ctx.channel.id}  Guild: {ctx.guild.id}] {suggestion}"
		
		suggestions['suggestions_admin'][str(sug_id)] = suggestion_string
	
		WriteSuggestions()
		await ctx.send(f"{ctx.author.mention} Thanks, your admin suggestion has been sent and will be reviewed by Kaiser shortly.")

	@suggest.command(name="urgent", aliases=['u'])
	async def _suggest_high(self, ctx, *, suggestion):
		global suggestions
		ReadSuggestions()
		sug_id = random.randrange(10000, 99999)
		#print(suggestions['suggestions'].keys())
		while sug_id in suggestions['suggestions_urgent'].keys():
			sug_id = random.randrange(10000, 99999)

		suggestion_string = f"{ctx.author.display_name} ({ctx.author.name}) [Chan:{ctx.channel.id}  Guild: {ctx.guild.id}] {suggestion}"
		
		suggestions['suggestions_urgent'][str(sug_id)] = suggestion_string
	
		WriteSuggestions()
		await ctx.send(f"{ctx.author.mention} Thanks, your suggestion has been sent as urgent and will be reviewed by Kaiser.")
		
	@suggest.command(name="low", aliases=['l'])
	async def _suggest_low(self, ctx, *, suggestion):
		global suggestions
		ReadSuggestions()
		sug_id = random.randrange(10000, 99999)
		#print(suggestions['suggestions'].keys())
		while sug_id in suggestions['suggestions_low'].keys():
			sug_id = random.randrange(10000, 99999)

		suggestion_string = f"{ctx.author.display_name} ({ctx.author.name}) [Chan:{ctx.channel.id}  Guild: {ctx.guild.id}] {suggestion}"
		
		suggestions['suggestions_low'][str(sug_id)] = suggestion_string
	
		WriteSuggestions()
		await ctx.send(f"{ctx.author.mention} Thanks, your suggestion has been sent as low-priority and will be reviewed by Kaiser.")
						
	@access_check()
	@commands.group(invoke_without_command=True)
	async def greeter(self, ctx):
		"""Sets a welcome/goodbye message for members.
		
		Examples:
		!greeter <welcome or leave> <channel name or ID> <message>
		!greeter welcome general Hello (wment) and welcome to (guild)
		!greeter leave general Welp, looks like (wname) left. Their loss.
		!greeter welcome user Hello (wment) and welcome to (guild)
		
		Having user as the channel target will send the message to the user.
		
		You can use the following tags to dynamically add certain values:
		(wname) becomes the persons name,
		(wment) mentions the person,
		(guild) becomes the guilds name."""
		if ctx.invoked_subcommand is None:
			help_cmd = self.bot.get_command('help')
			await ctx.invoke(help_cmd, 'greeter')
	
	@greeter.command(name="show")
	async def _show_greets(self, ctx):
		"""Shows the current message parameters."""
		msg = ""
		for gid in welcomes.keys():
			if str(ctx.guild.id) == gid:
				try:
					target = welcomes[str(gid)]['goodbye_channel']
					if target == "user":
						chan = "user"
					else:
						chan = client.get_channel(int(target)).mention
					msg += "Leaving message are sent to "+chan+"\n---\n"+welcomes[str(gid)]['goodbye']+"\n----\n"
				except:
					pass
				
				try:
					target = welcomes[str(gid)]['welcome_channel']
					if target == "user":
						chan = "user"
					else:
						chan = client.get_channel(int(target)).mention
					msg += "Welcome message are sent to "+chan+"\n---\n"+welcomes[str(gid)]['welcome']+"\n----"
				except:
					pass
		
		if msg == "":
			await ctx.send("No messages are set up for this server.")
			return	
					
		msg = repls(msg, ctx.author, ctx.guild)
		await ctx.send(msg)		
		
	@greeter.command(name="test_welcome")
	async def _test_welc(self, ctx):
		"""Triggers a test welcome message."""
		await send_hello(ctx.author)
		
	@greeter.command(name="test_leave")
	async def _test_leave(self, ctx):
		"""Triggers a test leaving message."""
		await send_goodbye(ctx.author)
	
	@greeter.command(name="clear")
	async def _clear(self, ctx, value):
		"""Clears the current greeter messages.
		Value should be either: welcome, goodbye, all"""
		if value == "welcome":
			ReadWelcome()
			try:
				del welcomes[str(ctx.guild.id)]['welcome']
				del welcomes[str(ctx.guild.id)]['welcome_channel']
				await ctx.send("Welcome messages for this guild have been cleared.")
			except:
				pass
			WriteWelcome()
		if value == "goodbye":
			ReadWelcome()
			try:
				del welcomes[str(ctx.guild.id)]['goodbye']
				del welcomes[str(ctx.guild.id)]['goodbye_channel']
				await ctx.send("Leaver messages for this guild have been cleared.")
			except:
				pass
			WriteWelcome()
		if value == "all":
			ReadWelcome()
			try:
				del welcomes[str(ctx.guild.id)]['welcome']
				del welcomes[str(ctx.guild.id)]['welcome_channel']
				await ctx.send("Welcome messages for this guild have been cleared.")
			except:
				pass
			try:
				del welcomes[str(ctx.guild.id)]['goodbye']
				del welcomes[str(ctx.guild.id)]['goodbye_channel']
				await ctx.send("Leaver messages for this guild have been cleared.")
			except:
				pass
			WriteWelcome()
			
	@greeter.command(name="welcome")
	async def _set_welc(self, ctx, channel, *, message):
		"""Sets a new greeter message."""
		global welcomes
		chan = None
		if channel.isdigit():
			for item in ctx.guild.channels:
				if item.id == int(channel):
					chan = item.id
		else:
			if channel.lower() == "user":
				chan = "user"
			else:
				for item in ctx.guild.channels:
					if channel in item.name:
						chan = item.id
				
		if chan is None:
			await ctx.send("Channel not found.")
			return
			
		ReadWelcome()
		try:
			welcomes[str(ctx.guild.id)]['welcome'] = message
			welcomes[str(ctx.guild.id)]['welcome_channel'] = chan
		except:
			welcomes[str(ctx.guild.id)] = {}
			welcomes[str(ctx.guild.id)]['welcome'] = message
			welcomes[str(ctx.guild.id)]['welcome_channel'] = chan
			
		WriteWelcome()
		await ctx.send("New welcome message set, a test message will be triggered with you as the target.")
		await send_hello(ctx.author)

	@greeter.command(name="leave")
	async def _set_bye(self, ctx, channel, *, message):
		"""Sets a new leaving message."""
		global welcomes
		chan = None
		if channel.isdigit():
			for item in ctx.guild.channels:
				if item.id == int(channel):
					chan = item
		else:
			if channel.lower() == "user":
				chan = "user"
			else:
				for item in ctx.guild.channels:
					if channel in item.name:
						chan = item.id
				
		if channel is None:
			await ctx.send("Channel not found.")
			return
		
		ReadWelcome()
		try:
			welcomes[str(ctx.guild.id)]['goodbye'] = message
			welcomes[str(ctx.guild.id)]['goodbye_channel'] = chan
		except:
			welcomes[str(ctx.guild.id)] = {}
			welcomes[str(ctx.guild.id)]['goodbye'] = message
			welcomes[str(ctx.guild.id)]['goodbye_channel'] = chan
		WriteWelcome()
		await ctx.send("New goodbye message set, a test message will be triggered with you as the target.")
		await send_goodbye(ctx.author)
			
	async def _basic_cleanup_strategy(self, ctx, search):
		count = 0
		async for msg in ctx.history(limit=search, before=ctx.message):
			if msg.author == ctx.me:
				await msg.delete()
				count += 1
		return { 'Bot': count }

	async def _complex_cleanup_strategy(self, ctx, search):
		prefixes = tuple(['!', 'a!', 'ath!', ctx.guild.me.mention]) # thanks startswith

		def check(m):
			return m.author == ctx.me or m.content.startswith(prefixes)

		deleted = await ctx.channel.purge(limit=search, check=check, before=ctx.message)
		return Counter(m.author.display_name for m in deleted)

	@commands.command(hidden=True)
	async def about(self, ctx):
		em = discord.Embed(colour=0x92005D)
		owner = client.get_user(client.owner_id)
		em.add_field(name="Athena", value=f"Athena is a multi-purpose (currently) private Discord bot created by {owner.mention}.\n\n{client.description}")
		await ctx.send(embed=em)
		PersonaCommand()
	
	@commands.command(hidden=True)
	@access_check()
	async def hiden(self, ctx, commandname):
		for com in client.commands:
			if com.name.lower() == commandname:
				if com.hidden:
					com.hidden = False
					await ctx.send(f"{com.name} shown.")
				else:
					com.hidden = True
					await ctx.send(f"{com.name} hidden.")
						
	@commands.command(hidden=True)
	@access_check()
	async def ca(self, ctx, commandname):
		for com in client.commands:
			if com.name.lower() == commandname:
				if com.enabled:
					com.enabled = False
					await ctx.send(f"{com.name} disabled.")
				else:
					com.enabled = True
					await ctx.send(f"{com.name} enabled.")
		PersonaCommand()
						
	@commands.command()
	@checks.has_permissions(manage_messages=True)
	async def cleanup(self, ctx, search=100):
		"""Cleans up the bot's messages from the channel.
		If a search number is specified, it searches that many messages to delete.
		If the bot has Manage Messages permissions then it will try to delete
		messages that look like they invoked the bot as well.
		After the cleanup is completed, the bot will send you a message with
		which people got their messages deleted and their count. This is useful
		to see which users are spammers.
		You must have Manage Messages permission to use this.
		"""
		PersonaCommand()
		strategy = self._basic_cleanup_strategy
		if ctx.me.permissions_in(ctx.channel).manage_messages:
			strategy = self._complex_cleanup_strategy

		spammers = await strategy(ctx, search)
		deleted = sum(spammers.values())
		messages = [f'{deleted} message{" was" if deleted == 1 else "s were"} removed.']
		if deleted:
			messages.append('')
			spammers = sorted(spammers.items(), key=lambda t: t[1], reverse=True)
			messages.extend(f'- **{author}**: {count}' for author, count in spammers)

		await ctx.send('\n'.join(messages), delete_after=10)

	@commands.group(aliases=['purge'])
	@commands.guild_only()
	@checks.has_permissions(manage_messages=True)
	async def remove(self, ctx):
		"""Removes messages that meet a criteria.
		In order to use this command, you must have Manage Messages permissions.
		Note that the bot needs Manage Messages as well. These commands cannot
		be used in a private message.
		When the command is done doing its work, you will get a message
		detailing which users got removed and how many messages got removed.
		"""
		PersonaCommand()
		if ctx.invoked_subcommand is None:
			help_cmd = self.bot.get_command('help')
			await ctx.invoke(help_cmd, 'remove')

	async def do_removal(self, ctx, limit, predicate, *, before=None, after=None):
		if limit > 2000:
			return await ctx.send(f'Too many messages to search given ({limit}/2000)')

		if before is None:
			before = ctx.message
		else:
			before = discord.Object(id=before)

		if after is not None:
			after = discord.Object(id=after)

		try:
			deleted = await ctx.channel.purge(limit=limit, before=before, after=after, check=predicate)
		except discord.Forbidden as e:
			return await ctx.send('I do not have permissions to delete messages.')
		except discord.HTTPException as e:
			return await ctx.send(f'Error: {e} (try a smaller search?)')

		spammers = Counter(m.author.display_name for m in deleted)
		deleted = len(deleted)
		messages = [f'{deleted} message{" was" if deleted == 1 else "s were"} removed.']
		if deleted:
			messages.append('')
			spammers = sorted(spammers.items(), key=lambda t: t[1], reverse=True)
			messages.extend(f'**{name}**: {count}' for name, count in spammers)

		to_send = '\n'.join(messages)

		if len(to_send) > 2000:
			await ctx.send(f'Successfully removed {deleted} messages.', delete_after=10)
		else:
			await ctx.send(to_send, delete_after=10)

	@remove.command()
	async def embeds(self, ctx, search=100):
		"""Removes messages that have embeds in them."""
		await self.do_removal(ctx, search, lambda e: len(e.embeds))

	@remove.command()
	async def files(self, ctx, search=100):
		"""Removes messages that have attachments in them."""
		await self.do_removal(ctx, search, lambda e: len(e.attachments))

	@remove.command()
	async def images(self, ctx, search=100):
		"""Removes messages that have embeds or attachments."""
		await self.do_removal(ctx, search, lambda e: len(e.embeds) or len(e.attachments))

	@remove.command(name='all')
	async def _remove_all(self, ctx, search=100):
		"""Removes all messages."""
		await self.do_removal(ctx, search, lambda e: True)

	@remove.command()
	async def user(self, ctx, member: discord.Member, search=100):
		"""Removes all messages by the member."""
		await self.do_removal(ctx, search, lambda e: e.author == member)

	@remove.command()
	async def contains(self, ctx, *, substr: str):
		"""Removes all messages containing a substring.
		The substring must be at least 3 characters long.
		"""
		if len(substr) < 3:
			await ctx.send('The substring length must be at least 3 characters.')
		else:
			await self.do_removal(ctx, 100, lambda e: substr in e.content)

	@remove.command(name='bot')
	async def _bot(self, ctx, search=100):
		"""Removes a bot user's messages and messages with their optional prefix."""

		def predicate(m):
			return (m.webhook_id is None and m.author.bot)

		await self.do_removal(ctx, search, predicate)

	@remove.command(name='emoji')
	async def _emoji(self, ctx, search=100):
		"""Removes all messages containing custom emoji."""
		custom_emoji = re.compile(r'<:(\w+):(\d+)>')
		def predicate(m):
			return custom_emoji.search(m.content)

		await self.do_removal(ctx, search, predicate)

	@remove.command(name='reactions')
	async def _reactions(self, ctx, search=100):
		"""Removes all reactions from messages that have them."""

		if search > 2000:
			return await ctx.send(f'Too many messages to search for ({search}/2000)')

		total_reactions = 0
		async for message in ctx.history(limit=search, before=ctx.message):
			if len(message.reactions):
				total_reactions += sum(r.count for r in message.reactions)
				await message.clear_reactions()

		await ctx.send(f'Successfully removed {total_reactions} reactions.')
					
	@commands.command(aliases=['newmembers'])
	@commands.guild_only()
	async def newusers(self, ctx, *, count=5):
		"""Tells you the newest members of the server.
		This is useful to check if any suspicious members have
		joined.
		The count parameter can only be up to 25.
		"""
		await ctx.channel.trigger_typing()
		count = max(min(count, 25), 5)
		PersonaCommand()
		if not ctx.guild.chunked:
			await self.bot.request_offline_members(ctx.guild)

		members = sorted(ctx.guild.members, key=lambda m: m.joined_at, reverse=True)[:count]

		e = discord.Embed(title='New Members', colour=discord.Colour.green())

		for member in members:
			body = f'**Joined**: {time.human_timedelta(member.joined_at)}\n**Created**: {time.human_timedelta(member.created_at)}'
			e.add_field(name=f'{member} (ID: {member.id})', value=body, inline=False)

		await ctx.send(embed=e)
					
	async def say_permissions(self, ctx, member, channel):
		permissions = channel.permissions_for(member)
		e = discord.Embed(colour=member.colour, description=f"Permissions for {member.mention} in {channel.mention}")
		allowed, denied = [], []
		for name, value in permissions:
			name = name.replace('_', ' ').replace('guild', 'server').title()
			if value:
				allowed.append(name)
			else:
				denied.append(name)
		PersonaCommand()
		e.add_field(name='Allowed', value='\n'.join(allowed))
		e.add_field(name='Denied', value='\n'.join(denied))
		await ctx.send(embed=e)
	
	def find_chan(self, ctx, inp):
		for channel in ctx.guild.channels:
			if channel.name.lower() == inp.lower():
				return channel
		return None
				
	def find_mem(self, ctx, inp):
		for member in ctx.guild.members:
			if member.name.lower() == inp.lower() or member.display_name.lower() == inp.lower():
				return member
		return None
			
	@commands.command()
	@commands.guild_only()
	async def permissions(self, ctx, member_name = "", channel_name = ""):
		"""Shows a member's permissions in a specific channel.
		If no channel is given then it uses the current one.
		You cannot use this in private messages. If no member is given then
		the info returned will be yours.
		"""
		channel = self.find_chan(ctx, channel_name) or ctx.channel
		member = self.find_mem(ctx, member_name) or ctx.author
		PersonaCommand()
		await self.say_permissions(ctx, member, channel)
		
	@commands.command(name="except", hidden=True)
	async def _except(self, ctx):
		raise GPF("This is a test. Do not be alarmed.")    
	
	@commands.group(invoke_without_command=True)
	async def stats(self, ctx):
		await ctx.channel.trigger_typing()
		servers = len(client.guilds)
		members = len(list(client.get_all_members()))
		version_discord = discord.__version__
		version_python = str(sys.version_info[0])+"."+str(sys.version_info[1])+"."+str(sys.version_info[2])
		p = psutil.Process()
		time = datetime.datetime.fromtimestamp(p.create_time()).strftime("%Y-%m-%d %H:%M:%S")
		mem = p.memory_percent()
		mem = str(mem).split(".")[0]
		d = LoadPersona()
		s = datetime.datetime.utcnow() - ctx.message.created_at
		delay = ""
		if s.seconds == 0:
			delay = "< 0 seconds"
		else:
			delay = humanfriendly.format_timespan(s.seconds)
		em = discord.Embed(colour=ctx.guild.me.colour, description=f"Running Python {version_python}\nDiscord API version: {version_discord} REWRITE\nProcess running since {time}\nUsing {mem}% memory.\nClient Latency: {client.latency} / {delay}")
		guilds = ""
		for item in client.guilds:
			guilds += f"{item.name} ({item.id}) - {len(item.members)} members\n"
		em.add_field(name="Guild Data", value=f"{servers} servers containing {members} members.\n{guilds}")
		em.add_field(name=f"Mood Module {PersonaVersion()}", value=f"Mood: {d['mood']}\nAnger: {d['anger']}\nEvaluation: {EvalMood()} ({EvalMoodVague()})")
		print(len(client.voice_clients))
		if len(client.voice_clients) > 0:
			vcs = ""
			for item in client.voice_clients:
				channel = vcgetbyguild(item.guild)['channel']
				np = vcgetbyguild(item.guild)['nowplaying']
				vcs += f"[{channel.guild.name} - #{channel.name}] - {np}"
			em.add_field(name="Voice Clients", value=vcs)
		await ctx.send(embed=em)

	@stats.command(name="commands", aliases=['com', 'c'])
	async def _commands(self, ctx, sub_group=""):
		await ctx.channel.trigger_typing()
		em = discord.Embed()
		with open('stats.json') as f:
			data = json.load(f)
			c = ""
			i = 0
			for key in dict(OrderedDict(sorted(data['commands'].items(), key=itemgetter(1), reverse=True))):
				i += 1
				c += f"{key}: {data['commands'][key]}\n"
				if i == 5:
					break
					
			em.add_field(name="Commands", value=c)
			
			i = 0
			c = ""
			for key in OrderedDict(sorted(data['users'].items(), key=itemgetter(1), reverse=True)):
				i += 1
				c += f"{get_member(client, int(key)).mention}: {data['users'][key]}\n"
				if i == 5:
					break
					
			em.add_field(name="Users", value=c)
			
			i = 0
			c = ""
			for key in OrderedDict(sorted(data['guilds'].items(), key=itemgetter(1), reverse=True)):
				i += 1
				c += f"{self.bot.get_guild(int(key)).name}: {data['guilds'][key]}\n"
				if i == 5:
					break
					
			em.add_field(name="Guilds", value=c)
			
			i = 0
			c = ""
			for key in OrderedDict(sorted(data['channels'].items(), key=itemgetter(1), reverse=True)):
				i += 1
				c += f"{self.bot.get_channel(int(key)).mention}: {data['channels'][key]}\n"
				if i == 5:
					break
					
			em.add_field(name="Channels", value=c)
			
		await ctx.send(embed=em)
				
	@commands.command(name="persona", hidden=True)
	async def _persona_check(self, ctx):
		d = LoadPersona()
		await ctx.send(f"Mood: {d['mood']}\nAnger: {d['anger']}\nEvaluation: {EvalMood()}")

	@commands.command(hidden=True)
	@commands.is_owner()
	async def history(self, ctx, name=""):
		target = ctx.author
		if name != "":
			for member in ctx.guild.members:
				if name.lower() in member.display_name.lower() or name.lower() in member.name.lower():
					target = member
					
		messages = await ctx.channel.history(limit=5).flatten()
		for item in messages:
			if item.author is target:
				await ctx.send(item.content)
	
	@access_check()
	@commands.group(name="backup", hidden=True, invoke_without_command=True) # TO FINISH
	async def backup(self, ctx):
		prog = ProgressBar(width=10)
		mems = "NAME 	|	DISPLAY_NAME 	|	ID\n"
		i = 0
		c = 0
		msg = await ctx.send("⏩ Starting... Saving Member List...")
		for member in ctx.guild.members:
			i += 1
			c += 1
			mems += f"{member.name} 	|	({member.display_name}) 	|	[{member.id}]\n"
			if c == 10:
				await msg.edit(content=f"<a:loading:480693540616273922> {i} members saved.\n{prog(Percent(i, len(ctx.guild.members), 'int'))}")
				c = 0
		
		await msg.edit(content="<:loaded:480337167080488960> Members recorded.")
		
		open(scriptdir+"backup/"+str(ctx.guild.id)+'.txt', 'w').close()
		
		with open(scriptdir+"backup/"+str(ctx.guild.id)+".txt", 'r+') as f:
			f.writelines(mems)
		
		await msg.edit(content=f"<:loaded:480337167080488960> Members recorded. ({i})\n<:loaded:480337167080488960> List saved to file.")
		
		emotes = []
		c = 0
		for emote in ctx.guild.emojis:
			c += 1
			emotes.append(emote.name)
			url = emote.url
			r = requests.get(url, allow_redirects=True)
			if emote.animated:
				open(scriptdir+"backup/"+emote.name+".gif", 'wb').write(r.content)
			else:
				open(scriptdir+"backup/"+emote.name+".png", 'wb').write(r.content)
			await msg.edit(content=f"<:loaded:480337167080488960> Members recorded. ({i})\n<:loaded:480337167080488960> List saved to file.\n<a:loading:480693540616273922> {len(emotes)} emotes saved.\n{prog(Percent(c, len(ctx.guild.emojis), 'int'))}")
		
		await msg.edit(content=f"<:loaded:480337167080488960> Members recorded. ({i})\n<:loaded:480337167080488960> List saved to file.\n<:loaded:480337167080488960> {len(emotes)} emotes saved.\n✅ Basic backup complete.")
		PersonaCommand()
		
	@backup.command()
	@access_check()
	async def emojis(self, ctx):
		prog = ProgressBar(width=10)
		msg = await ctx.send("<a:loading:480693540616273922> Backing up emojis...")
		await asyncio.sleep(2)
		emotes = []
		c = 0
		for emote in ctx.guild.emojis:
			c += 1
			emotes.append(emote.name)
			url = emote.url
			r = requests.get(url, allow_redirects=True)
			if emote.animated:
				open(scriptdir+"backup/"+emote.name+".gif", 'wb').write(r.content)
			else:
				open(scriptdir+"backup/"+emote.name+".png", 'wb').write(r.content)
			await msg.edit(content=f"<a:loading:480693540616273922>  {len(emotes)} emotes saved.\n{prog(Percent(c, len(ctx.guild.emojis), 'int'))}")
		
		await msg.edit(content=f"<:loaded:480337167080488960>  {len(emotes)} emotes saved.")
		PersonaCommand()
		
	@backup.command(name="members")
	@access_check()
	async def _members(self, ctx):
		prog = ProgressBar(width=10)
		mems = "NAME 	|	DISPLAY_NAME 	|	ID\n"
		i = 0
		c = 0
		msg = await ctx.send("<a:loading:480693540616273922> Starting... Saving Member List...")
		for member in ctx.guild.members:
			i += 1
			c += 1
			mems += f"{member.name} 	|	({member.display_name}) 	|	[{member.id}]\n"
			if c == 10:
				await msg.edit(content=f"<a:loading:480693540616273922>  {i} members saved.\n{prog(Percent(i, len(ctx.guild.members), 'int'))}")
				c = 0
		
		await msg.edit(content="<:loaded:480337167080488960>  Members recorded.")
		
		open(scriptdir+"backup/"+str(ctx.guild.id)+'.txt', 'w').close()
		
		with open(scriptdir+"backup/"+str(ctx.guild.id)+".txt", 'r+') as f:
			f.writelines(mems)
		
		await msg.edit(content=f"<:loaded:480337167080488960>  Members recorded. ({i})\n<:loaded:480337167080488960>  List saved to file.")
		PersonaCommand()
		
	@commands.command(hidden=True)
	async def chatcount(self, ctx, *, search_term :str=""):
		try:
			with open("chat.log", "r") as f:
				cont = f.readlines()
				if search_term == "":
					await ctx.send(f"ChatLog file contains {len(cont)} entries total.")
				else:
					i = 0
					for item in cont:
						if search_term.lower() in item.lower():
							i += 1
					
					await ctx.send(f"{i} out of {len(cont)} entries contain {search_term.lower()}.")
		except Exception as e:
			await ctx.send(f"Error in file loading.\n{e} > {type(e)}")
			
	@commands.command(hidden=True)
	@access_check()
	async def chatsearch(self, ctx, *, search:str=""):
		# print(lis[len(lis)-1])
		s_list = search.split(" ")
		end = s_list[len(s_list)-1]
		search_term = ""
	
		if end.isdigit():
			limit = int(end)
			s_list.remove(end)
			search_term = ' '.join(s_list)
		else:
			search_term = search	
			limit = 3
			
		if limit > 15:
			limit = 15
		
		if limit < 1:
			limit = 1
			
		l = []
		i = 0
		em = discord.Embed()
		search_term = f"{search_term}"
		try:
			with open("chat.log", "r") as f:
				cont = f.readlines()
				if search_term == "":
					await ctx.send("Please enter a search term.")
				else:
					for item in cont:
						#for words in item.split(" "):
						if search_term.lower()+" " in item.lower() or " "+search_term.lower() in item.lower():
							i += 1
							#if len(l) <= limit:
							l.append(item)
							
					fl = '\n'.join(l[-limit+1:-1])
					ix = 0
					for item in l[-limit-1:-1]:
						ix += 1
						em.add_field(name=f"{ix}", value=item)
						
		except Exception as e:
			await ctx.send(f"Error in file loading.\n{e} {type(e)}")
		
		if len(l) == 0:
			await ctx.send("No results.")
			return
			
		await ctx.send(f"{i} out of {len(cont)} entries contain {search_term.lower()}, showing {limit}.\n", embed=em)
		
	@commands.command(hidden=True)
	async def logcount(self, ctx, *, search_term :str=""):
		try:
			with open("sys.log", "r") as f:
				cont = f.readlines()
				if search_term == "":
					await ctx.send(f"Log file contains {len(cont)} entries total.")
				else:
					i = 0
					for item in cont:
						if search_term.lower() in item.lower():
							i += 1
					
					await ctx.send(f"{i} out of {len(cont)} entries contain {search_term.lower()}.")
		except Exception as e:
			await ctx.send(f"Error in file loading.\n{e} > {type(e)}")
			
	@commands.command(hidden=True)
	@access_check()
	async def logsearch(self, ctx, *, search:str=""):
		s_list = search.split(" ")
		end = s_list[len(s_list)-1]
		search_term = ""

		if end.isdigit():
			limit = int(end)
			s_list.remove(end)
			search_term = ' '.join(s_list)
		else:
			search_term = search	
			limit = 3
			
		if limit > 15:
			limit = 15
		
		if limit < 1:
			limit = 1
			
		l = []
		i = 0
		em = discord.Embed()
		search_term = f"{search_term}"
		try:
			with open("sys.log", "r") as f:
				cont = f.readlines()
				if search_term == "":
					await ctx.send("Please enter a search term.")
				else:
					for item in cont:
						if search_term.lower()+" " in item.lower() or " "+search_term.lower() in item.lower():
							i += 1
							#if len(l) <= limit:
							l.append(item)
					#fl = '\n'.join(l[-limit:])
					ix = 0
					for item in l[-limit:]:
						ix += 1
						em.add_field(name=f"{ix}", value=item)
						
		except Exception as e:
			await ctx.send(f"Error in file loading.\n{e} {type(e)}")
		
		if len(l) == 0:
			await ctx.send("No results.")
			return
		
		source = f"Guild = {ctx.message.guild.name}\nChannel = {ctx.message.channel.name}\nAuthor = {ctx.message.author.name} ({ctx.message.author.display_name})```{ctx.message.content.replace('```', '')}```"
		AddLog(f"Log File accessed: {source}")	
		await ctx.send(f"{i} out of {len(cont)} entries contain {search_term.lower()}, showing {limit}.", embed=em)
	
	@commands.is_owner()
	@commands.command(hidden=True)		
	async def reset_mood(self, ctx):
		s = SetPersona('both', 0)
		await ctx.send(s)

	@commands.is_owner()
	@commands.command(hidden=True)		
	async def set_mood_m(self, ctx, mood:int):
		s = SetPersona('mood', mood)
		await ctx.send(s)
		
	@commands.is_owner()
	@commands.command(hidden=True)		
	async def set_mood_a(self, ctx, anger:int):
		s = SetPersona('anger', anger)
		await ctx.send(s)
		
	@commands.is_owner()
	@commands.command(hidden=True)		
	async def set_mood_b(self, ctx, both:int):
		s = SetPersona('both', both)		
		await ctx.send(s)
		
	@commands.is_owner()
	@commands.command(hidden=True)
	async def encrypt(self, ctx, msg: str):
		await ctx.send("Creating Minerva encoder...")
		enc = minerva.Pantheon()
		await ctx.send(f"Encrypting {msg}")
		res = enc.encode(msg)
		await ctx.send("Result: "+res)

	@commands.is_owner()
	@commands.command(hidden=True)
	async def decrypt(self, ctx, msg: str):
		await ctx.send("Creating Minerva encoder...")
		enc = minerva.Pantheon()
		await ctx.send(f"Decrypting {msg}")
		res = enc.decode(msg)
		await ctx.send("Result: "+res)

async def _can_run(cmd, ctx):
    try:
        return await cmd.can_run(ctx)
    except:
        return False
        						
def FindMatchCommands(ctx):
	matches = []
	#num = 0
	plausible = [cmd for cmd in client.commands if not cmd.hidden]
	for com in plausible:
		#AddLog(f"Matching: {com.name} > {fuzz.ratio(com.name, ctx)}")
		if fuzz.ratio(com.name, ctx) > 50:
		#	num += 1
			matches.append(f"**{com.name}**")
	
	if len(matches) == 1:
		return f"Did you mean {matches[0]}?"
	elif len(matches) > 1:
		fmatches = '\n'.join(matches)
		return f"Did you mean any of these commands?\n{fmatches}"
	else:
		return ""
					
def Webhook(text="", embed=None):
	#Testzone: 461851689922854922 / EHjh3I2uAZXpt1MPf0ehzH8yzwfvYLz5YiU9zALRN2YBEj0ebafmAmR7w2YPDHzTDG_o
	#Matchmaking: 461858508237701120 / jUIj3q0tb42XSDVEJnG2_7umXfRGJZZiablLN0oHcVkzfVj-ZJ4n201P-JGrgFCLb97c
	#Announce: 461868227664805908 8VHKMw0PRZc_5Rmng3NYgOiYjvCR2eV3rVNQsv7G2mxUgy2u4Vw7AaR8-7DJf1GYGrvo

	webhook_id = "461851689922854922"
	webhook_token = "EHjh3I2uAZXpt1MPf0ehzH8yzwfvYLz5YiU9zALRN2YBEj0ebafmAmR7w2YPDHzTDG_o"
	webhook = discord.Webhook.partial(webhook_id, webhook_token, adapter=discord.RequestsWebhookAdapter())
	webhook.send(text, embed=embed)
				
def setup(bot):
	global client
	bot.add_cog(admin(bot))
	client = bot

