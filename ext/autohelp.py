import discord
from discord.ext import commands
from discord.ext.commands.cooldowns import BucketType
import datetime
import humanfriendly
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
import configparser
import os
import json
import re
from persona import *
from athena_common import *

class autohelp:
	"""A set of methods for the AutoHelp system, designed to provide FAQ's and other helpful things.'"""
	global client
	def __init__(self, bot):
		global helpsens
		self.bot = bot
		client = bot
		
		return #disabled functionality
		@bot.listen('on_message')
		async def my_message(message):			
			if message.author.bot is True:
				return
			if message.content.endswith("?"):
				if message.guild.id == 396716931962503169:
						self.resp = athhelp(message.content, ReturnNone="true")
						self.helpstr = self.resp['message']
						self.prob = self.resp['match']
						if self.helpstr is not "":
							await message.channel.trigger_typing()
							await message.channel.send(f"{self.helpstr} \n**({self.prob}% match)**")	
							
	@commands.group(invoke_without_command=True, aliases=['tags'])
	async def tag(self, ctx, tag_name):
		"""Displays stored tags.
		Use admin command tag_edit to change tags."""
		await ctx.channel.trigger_typing()
		limit=5
		cur = 0
		extras = []
		em = discord.Embed()
		urls = []
		with open('tags.json') as f:
			data = json.load(f)
			for item, defin in data.items():
				if fuzz.ratio(tag_name.lower(), item.lower()) >= 80:
					if cur <= limit:
						_new_urls = re.findall(r'(https?://\S+)', defin)
						AddLog(str(_new_urls))
						if len(_new_urls) != 1:
							cur += 1
							if fuzz.ratio(tag_name.lower(), item.lower()) == 100:
								em.add_field(name=f'{item}', value=defin)
							else:
								em.add_field(name=f'{item} **({fuzz.ratio(tag_name.lower(), item.lower())}% match)**', value=defin)
						else:
							urls += _new_urls
					else:
						extras.append(item)
						
					AddLog(f"[{cur}] {item}: {defin} ({fuzz.ratio(tag_name.lower(), item.lower())}% match)\n")
			
			if len(urls) > 0:
				for item in urls:
					await ctx.send(item)
						
			if cur == 0:	
				if len(urls) == 0:		
					await ctx.send(embed=discord.Embed(colour=0xFF0000, description="No results found."))
			else:				
				if len(extras) > 0:
					em.add_field(name="More...", value=f"\n{len(extras)} more results excluded due to message size limits.\n**{HumanizeList(extras)}**")
				await ctx.send(embed=em)
				PersonaCommand()
	
	@commands.is_owner()
	@tag.command(name="edit", aliases=['-e'])
	async def tag_edit(self, ctx, search_term:str, *, new_definition):
		"""
		Edits a current tag or creates a new one.
		"""
		if "-a" in new_definition:
			new_definition = new_definition.replace("-a", "\n")
			append = True
		else:
			append = False	
		data = {}
		new_def = new_definition
		with open('tags.json') as f:
			data = json.load(f)	
			if not append:
				data[search_term] = new_def
			else:
				old_def = data[search_term]
				new_def = old_def + new_def
				data[search_term] = new_def
				
			AddLog(f"TAG CHANGED: {search_term} > {data[search_term]}")
			
		with open('tags.json', "w") as s:
			json.dump(data, s, indent=4, sort_keys=True)
		
		em = discord.Embed()
		em.add_field(name=search_term, value=data[search_term])	
		await ctx.send(embed=em)
		PersonaCommand()
		
	@commands.is_owner()
	@tag.command(name="del", aliases=['-d'])
	async def tag_del(self, ctx, search_term:str):
		"""Deletes a tag."""
		data = {}
		with open('tags.json') as f:
			data = json.load(f)	
			del data[search_term]
			
		with open('tags.json', "w") as s:
			json.dump(data, s, sort_keys=True, indent=4)
			
		await ctx.send(f"Key `{search_term}` removed.")
		PersonaCommand()
		
	@tag.command(name="list", aliases=['-l'])
	async def tags(self, ctx):
		"""Displays stored tags.
		Use admin command tag -edit|-e to change tags."""
		await ctx.channel.trigger_typing()
		tagslist = []
		with open('tags.json') as f:
			data = json.load(f)
			for item, o in data.items():
				tagslist.append(item)
				
		await ctx.send(HumanizeList(tagslist))
		PersonaCommand()
				
	@commands.is_owner()
	@commands.command(name="write", hidden=True)
	async def _write(self, ctx, file_name, *text):
		await ctx.channel.trigger_typing()
		
		self.warg = file_name.lower()
		self.wcont = ' '.join(text)
		try:
			self.wold = ReadFile(self.warg)
			if self.wold != "":
				AddLog("File exists.")
				await ctx.send("The file exists.\n**Overwriting not currently implemented.")
		except:
			pass
					
		WriteFile(self.warg, self.wcont)		
		await ctx.send("Wrote to "+self.warg+"\n```"+self.wcont+"```")
		return
	
	@commands.command(name="read", hidden=True)
	async def _read(self, ctx, file_name):
		await ctx.channel.trigger_typing()
		self.rarg = file_name
		try:	
			await ctx.send("```"+ReadFile(self.rarg)+"```")
		except Exception as e:
			await ctx.send(f"{type(e).__name__}")
	
	@commands.is_owner()
	@commands.command(name="append", hidden=True)
	async def _append(self, ctx, file_name, *text):
		await ctx.channel.trigger_typing()
		self.aarg = file_name.lower()
		self.acont = ' '.join(text)
		try:
			self.ac = ReadFile(self.aarg)
			self.ac = self.ac+"\n"
			WriteFile(self.aarg, self.ac+self.acont)		
			await ctx.send("Wrote to "+self.aarg+"\n```"+self.acont+"```")
			return
		except Exception as e:
			await ctx.send(f"{type(e).__name__}")
	
	@commands.command(name="oldsuggestlols", hidden=True, enabled=False)
	async def _suggest(self, ctx, *text):
		"""Leave a suggestion.
		Note: They may or may not be read."""
		await ctx.channel.trigger_typing()
		self.sarg = "suggestions"
		self.scont = ' '.join(text)
		try:
			self.sc = ReadFile(self.sarg)
			self.sc = self.sc+"\n[ ] "+ctx.author.name+": "
			WriteFile(self.sarg, self.sc+self.scont)	
			await ctx.send("Wrote to "+self.sarg+"\n```"+self.scont+"```")
			return
		except Exception as e:
			await ctx.send(f"{type(e).__name__}")
			
	@commands.command(name="txtlist", hidden=True)
	async def _txtlist(self, ctx):
		await ctx.channel.trigger_typing()
		self.tc = ListFile()
		await ctx.send(self.tc)
		
def athhelp(inp, **hargs):
	global helpsens
	ReturnNone = False

	for kw in hargs:
		if kw == "ReturnNone":
			if hargs[kw] == "true":
				AddLog("ReturnNone=True")
				ReturnNone = True
			else:
				AddLog("ReturnNone=False")
		
		if kw == "direct":
			AddLog("Direct Help: "+hargs[kw])
			help_direct = hargs[kw]		
			
	winner = {'highestmatch': 0, 'highesthelp': ""}	
		
	if inp.startswith("*"):
		winner['highestmatch'] = 100
		winner['highesthelp'] = inp[1:]
		
	if winner['highesthelp'] == "":
		AddLog("Running: "+inp+" (THRESHOLD: "+str(helpsens)+") >>")
		inputstr = inp.lower()
		
		help_roles = "roles?"
		help_mp = "play multiplayer or fix masterserver?"
		help_code = "code blocks?"
		help_hx = "hx coop?"
		help_render = "render display problems?"
		help_error = "error readfile eof or actor channel?"
		help_tacks = "map tacks?"
		help_bots = "bots athena aquinas morpheus?"
		help_dxrelease = "when was deus ex dx released?"
		
		
		
		AddLog("Roles: "+str(fuzz.ratio(help_roles, inputstr)))
		if fuzz.ratio(help_roles, inputstr) >= winner['highestmatch']:
			winner['highestmatch'] = fuzz.ratio(help_roles, inputstr)
			winner['highesthelp'] = "roles"
			
		AddLog("MP: "+str(fuzz.ratio(help_mp, inputstr)))
		if fuzz.ratio(help_mp, inputstr) >= winner['highestmatch']:
			winner['highestmatch'] = fuzz.ratio(help_mp, inputstr)
			winner['highesthelp'] = "mp"
			
		AddLog("Code Blocks: "+str(fuzz.ratio(help_code, inputstr)))	
		if fuzz.ratio(help_code, inputstr) >= winner['highestmatch']:
			winner['highestmatch'] = fuzz.ratio(help_code, inputstr)
			winner['highesthelp'] = "code"
			
		AddLog("HX: "+str(fuzz.ratio(help_hx, inputstr)))	
		if fuzz.ratio(help_hx, inputstr) >= winner['highestmatch']:
			winner['highestmatch'] = fuzz.ratio(help_hx, inputstr)
			winner['highesthelp'] = "hx"
		
		AddLog("Render: "+str(fuzz.ratio(help_render, inputstr)))	
		if fuzz.ratio(help_render, inputstr) >= winner['highestmatch']:
			winner['highestmatch'] = fuzz.ratio(help_render, inputstr)
			winner['highesthelp'] = "render"
		
		AddLog("Error: "+str(fuzz.ratio(help_error, inputstr)))	
		if fuzz.ratio(help_error, inputstr) >= winner['highestmatch']:
			winner['highestmatch'] = fuzz.ratio(help_error, inputstr)
			winner['highesthelp'] = "error"

		AddLog("Tacks: "+str(fuzz.ratio(help_tacks, inputstr)))	
		if fuzz.ratio(help_tacks, inputstr) >= winner['highestmatch']:
			winner['highestmatch'] = fuzz.ratio(help_tacks, inputstr)
			winner['highesthelp'] = "tacks"
			
		AddLog("Bots: "+str(fuzz.ratio(help_bots, inputstr)))	
		if fuzz.ratio(help_bots, inputstr) >= winner['highestmatch']:
			winner['highestmatch'] = fuzz.ratio(help_bots, inputstr)
			winner['highesthelp'] = "bots"
		
		AddLog("DX Release Info: "+str(fuzz.ratio(help_dxrelease, inputstr)))	
		if fuzz.ratio(help_dxrelease, inputstr) >= winner['highestmatch']:
			winner['highestmatch'] = fuzz.ratio(help_dxrelease, inputstr)
			winner['highesthelp'] = "dxrelease"
			
	AddLog("Current winner: "+str(winner['highestmatch'])+": "+winner['highesthelp'])
	
	if winner['highestmatch'] <= helpsens:
		if ReturnNone is True:
			return {'match':0, 'message':""}
		else:
			return {'match':0, 'message':"Sorry, I can't help you there. Try something else.\nHave nice day."}
		
	if winner['highesthelp'] == "roles":
		return {'match':winner['highestmatch'], 'message':"Use !roles to show basic information, !roleinfo to see information about specific roles, !join and !leave to... join and leave roles. Roles are a way for users to ping groups, for example if you need coding help, you'd put `@coders`, or if you want to get players together, you'd use `@players`"}
	elif winner['highesthelp'] == "mp":
		return {'match':winner['highestmatch'], 'message':"If multiplayer doesn't work for you, check <#396821109498183680> channel for the multiplayer patch.\nIf servers don't show, download the MP Masterserver patch (The first link). The patch is an automatic executable which just edits Deus Ex/System/DeusEx.ini and replaces `master0.gamespy.com` with `master.deusexnetwork.com`, you can also do this manually.\nIf you do not own the game, or want a multiplayer-only version, download either of the two clients listed in the channel.\nThe dropbox link is for a modernized download using a custom menu system and fully patched up to modern standards using Kenteis and updated renderers. \nThe mediafire link is for an older portable copy of the game using more basic files, with none of the patches."}
	elif winner['highesthelp'] == "code":
		return {'match':winner['highestmatch'], 'message':"For using codeblocks in Discord messages, start the code with \```, then press return, put in the code, and end the code with another \```.\nIf you want syntax highlighting like ```python\ndef AthenaWins(iWin)\n	if iWin is True:\n		Hax()\n```\nThen you start the code with \``` like usual but add the language tag to it, for example, \```html or \```python\n\n**Side note**: if you ever need to post things like \``` without it converting in to the code blocks, put \\ before it, so they look like \\\```"}
	elif winner['highesthelp'] == "hx":
		return {'match':winner['highestmatch'], 'message':"HX is an actively developed CO-OPERATIVE mod for DX Singleplayer, allowing people to play through the story together, developed by Han.\nDiscussions about the HX CO-OP mod take place in the private hx channel, viewable at the top of the channel list (if you have access)\nIf you would like access, ask an administrator to be added to the hx group. Once in the channel, check the pinned messages for downloads and info.\nHX game server status and IP details for direct connecting can be checked by using the command `!hx`"}
	elif winner['highesthelp'] == "render":
		return {'match':winner['highestmatch'], 'message':"If you have renderer problems, try upgrading to one of the enhanced renderers found in <#396821109498183680>. If you're already running these and still having an issue, try switching to one of the alternatives in the games options menu."}
	elif winner['highesthelp'] == "error":
		return {'match':winner['highestmatch'], 'message':"ReadFile Beyond EOF: Try turning off GUI fix/other fixes in Kenteis.\nNew/None actor channel: Either change renderer, then change back to original, or ingame type `debug gpf` then restart."}
	elif winner['highesthelp'] == "tacks":
		return {'match':winner['highestmatch'], 'message':"Check out this link for in-depth mapping tutorials: http://www.offtopicproductions.com/tacks/"}
	elif winner['highesthelp'] == "bots":
		return {'match':winner['highestmatch'], 'message':"Information about bots in this server.\n**Athena**: by Kaiser\nMostly used for DX related functions, but has other uses too, can query game servers, access file databases for DX mods, can chat with AI, etc.\n**Aquinas** (UB3R-B0T) by moiph\nA helper/networking bot used to help with communication, some backup functions for if Athena is down, and shows link information.\n**Morpheus** (Tatsumaki)\nA fun bot used for chat minigames, such as fishing, slots, and virtual pets, includes an in-chat money economy."}
	elif winner['highesthelp'] == "dxrelease":
		return {'match':winner['highestmatch'], 'message':"Deus Ex was released in June 2000 for PC, July 2000 for Mac, and March/June 2002 for the Playstation2.\nThe Multiplayer match was released around May 2001 for PC and Mac. \nThe Game of the Year edition was released in May 8, 2001."}
	else:			
		if ReturnNone is True:
			return {'match':0, 'message':""}
		else:
			return {'match':0, 'message':"Sorry, I can't help you there. Try something else.\nHave nice day."}

def has_link(str, *, can_contain=False, _endswith=""):
	_t = str.split(" ")
	if len(_t) > 1:
		if can_contain is False:
			return False
		else:
			for item in _t:
				if item.startswith("http://") or item.startswith("https://"):
					if _endswith == "":
						return True
					else:
						if item.endswith(_endswith):
							return True
							
	else:
		if _t[0].startswith("http://") or _t[0].startswith("https://"):
			if _endswith == "":
				return True
			else:
				if _t[0].endswith(_endswith):
					return True
			
	return False
	
def ReadFile(filename):
	roletxt = open(scriptdir+'/txt/'+filename+'.txt', 'r') 
	tmp = roletxt.read()
	roletxt.close()
	return tmp

def WriteFile(filename, content, overwrite=False):
	open(scriptdir+'/txt/'+filename+'.txt', 'w').close()
	roletxt = open(scriptdir+'/txt/'+filename+'.txt', 'w') 
	roletxt.write(content)
	roletxt.close()			
	
def ListFile():
	ls = []
	dlpath = scriptdir+'/txt/'
	for file in os.listdir(dlpath):
		filename = os.fsdecode(file)	
		ls.append(filename[:-4])
	ls.sort()
	return HumanizeList(ls)
	 		
def setup(bot):
	global client, helpsens, scriptdir
	bot.add_cog(autohelp(bot))
	client = bot
