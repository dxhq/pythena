import discord
from discord.ext import commands
from discord.ext.commands.cooldowns import BucketType
import datetime
import humanfriendly
import requests
import os
from bs4 import BeautifulSoup
import ast
import aiohttp
import praw
import secrets
import configparser
import wikipedia
import json
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
import srcomapi
from geopy.geocoders import Nominatim
import geocoder
from darksky import forecast
from pytz import timezone 
from imgurpython import ImgurClient
from persona import *
import fnmatch
import sys
import asyncio
import minerva
from athena_common import *

kb_cache = []
		
class DuckDuckGo:
	def __init__(self, query):
		self.result =  requests.get(f'https://api.duckduckgo.com/?q={query}&format=json&pretty=1&t=athena')
		self.data = json.loads(self.result.text)
		self.Heading = self.data['Heading']
		self.AbstractURL = self.data['AbstractURL']
		self.AbstractText = self.data['AbstractText']
		self.Image = self.data['Image']
		self.Entity = self.data['Entity']
		
		if self.AbstractText == "":
			self.AbstractText = self.data['RelatedTopics'][0]['Text']
		
		if self.AbstractURL == "":
			self.AbstractURL = self.data['RelatedTopics'][0]['FirstURL']
		
		if self.Entity == "":
			self.Entity = "result"
			
		self.RelatedTopics = {}
		for item in self.data['RelatedTopics']:
			try:
				self.RelatedTopics[item['Text']] = item['FirstURL']
			except:
				pass
				
	def Related(self):
		st = ""
		for item in self.RelatedTopics:
			st += item['Text']+": "+item['FirstURL']
			
		return st
		
aecol = 0xBFBFBF
reddit_client = None
imgur_client = None
	
class api:
	"""A set of commands for interacting with third-party API's.
	**Note**: Since alot of these commands connect to external websites, any errors may not be related to the bot, and could instead be from the site instead. Also responses may be slow if the site is slow. Lastly, cooldowns are in effect on the commands to prevent overloading the addresses."""
	
	global client
	def __init__(self, bot):
		global reddit_client, imgur_client
		self.bot = bot
		client = bot
		if boolinate(getConfig()['core']['autostartreddit']):
			AddLog("Logging in to Reddit.", colour="magenta")
			reddit_client_id = "GWEr-RTUYOryOQ"
			reddit_client_secret = "ZY82CuLOODcxh9q4mmcSM0JFkSA"
			reddit_user_agent = "athena.discord:rewrite (by /u/_kaiz0r)"
			reddit_client = praw.Reddit(client_id=reddit_client_id, client_secret=reddit_client_secret, user_agent=reddit_user_agent)
		
		if boolinate(getConfig()['core']['autostartimgur']):
			AddLog("Logging in to imgur.", colour="magenta")
			imgur_client_id = "e9d1efa31d257fd"
			imgur_client_secret = "26eb9675c9a021290bea54a0fc639dfd95e1db37"
			imgur_client = ImgurClient(imgur_client_id, imgur_client_secret)
			
	@commands.command(pass_context=True)
	@commands.cooldown(1, 15)
	async def gist(self, ctx, *, content:str):
		"""Saves some text online."""
		payload = {
			'name': 'Athena - By: {0}.'.format(ctx.message.author),
			'text': content,
			'private': '1',
			'expire': '0'
		}
		async with aiohttp.ClientSession() as session:
			async with session.post('https://spit.mixtape.moe/api/create', data=payload) as r:
				url = await r.text()
		await ctx.send('Uploaded to paste, URL: https://spit.mixtape.moe'+url)
   	
	@commands.command()
	async def pwned(self, ctx, email):
		"""Checks if you've been hacked.
		Runs your email against known data breaches, and tells you if your data is breached."""
		payload = {
		'User-Agent':'Athena Discord Bot'
		}
		base_url = "https://haveibeenpwned.com/api/v2/breachedaccount"
		async with aiohttp.ClientSession() as session:
			async with session.get(f"{base_url}/{email}", headers=payload) as r:
				resp = await r.text()
				rj = json.loads(resp)
				await ctx.send(f"{len(rj)} breaches.")
				if len(rj) > 0:
					i = 0
					em = discord.Embed(colour=0xFF0000)
					for item in rj:
						em.add_field(name=rj[i]['Name']+" ("+rj[i]['Domain']+")", value=f"Breached on {rj[i]['BreachDate']}\n{rj[i]['PwnCount']} accounts breached.\n{rj[i]['Description']}\nSystems breached: {rj[i]['DataClasses']}")
						i += 1
					await ctx.send(embed=em)
					
	#https://github.com/Imgur/imgurpython
	#https://apidocs.imgur.com/#authorization-and-oauth
	@commands.cooldown(2, 20, BucketType.user)	
	@commands.group(invoke_without_command=True, aliases=['img'])
	async def imgur(self, ctx, *, search=""):
		"""Accesses the imgur API's.
		The command on its own returns a result from the default gallery at random.
		The command and a search term will search for that term.
		The command followed by 'tag' and a tag name will search for that tag.
		The command followed by 'r' and a subreddit name will return images from that subreddit.
		The command followed by 'meme' will return a random currently viral meme."""
		
		items = []
		if search != "":
			items = imgur_client.gallery_search(search, advanced=None, sort='time', window='all', page=0)#gallery_tag(tag, sort='viral', page=0, window='week')
		else:
			items = imgur_client.gallery()
		
		if len(items) == 0:
			await ctx.send("No results found.")
			return
		result = random.choice(items)
		await ctx.send(result.link)
	
	@commands.cooldown(2, 20, BucketType.user)	
	@imgur.command(name="tag")
	async def _imgtag(self, ctx, img_tag:str):
		items = imgur_client.gallery_tag(img_tag, sort='viral', page=0, window='week')
		result = random.choice(items)
		await ctx.send(result.link)
		
	@commands.cooldown(2, 20, BucketType.user)					
	@imgur.command(name="r")
	async def _imgur_reddit(self, ctx, subreddit):
		if reddit_client:
			subby = reddit_client.subreddit(subreddit)
			if subby.over18 and not ctx.channel.nsfw and strict_nsfw(ctx):
				await ctx.send("You must be in a NSFW channel to view this content or not have `strict NSFW rules` for this guild.")
				return

		items = imgur_client.subreddit_gallery(subreddit, sort='time', window='week', page=0)	
		result = random.choice(items)
		await ctx.send(result.link)
	
	@commands.cooldown(2, 20, BucketType.user)	
	@imgur.command(name="meme")
	async def _imgur_memes(self, ctx):
		items = imgur_client.memes_subgallery(sort='viral', page=0, window='week')
		result = random.choice(items)
		await ctx.send(result.link)
		
	#https://api.redtube.com/docs/
	async def porns(self, tag):
		async with aiohttp.ClientSession() as session:
			async with session.get(f"https://api.redtube.com/?data=redtube.Videos.searchVideos&output=json&search=hard&tags[]={tag}&thumbsize=medium") as r:
				resp = await r.text()
				pornz = json.loads(resp)
				plist = pornz['videos']
				
				if len(plist) > 0:
					return random.choice(plist)['video']['url']	
				else:
					return "No videos found."
	
	@is_nsfw()
	@commands.cooldown(2, 20, BucketType.user)		
	@commands.command()
	async def redtube(self, ctx, tag):
		"""Searches and returns a random video from redtube."""
		f = await self.porns(tag)
		await ctx.send(f)
	
	@commands.command()
	async def urban(self, ctx, search_term, index=0):
		async with aiohttp.ClientSession() as session:
			async with session.get(f"https://api.urbandictionary.com/v0/define?term={search_term}") as r:
				resp = await r.text()
				res = json.loads(resp)
				max_index = (len(res['list']) - 1)
				
				try:
					definition = res['list'][index]['definition']
					definition = definition.replace("[", "")
					definition = definition.replace("]", "")
					definition = trim(definition, 600)
					permalink = res['list'][index]['permalink']
					thumbs_up = res['list'][index]['thumbs_up']
					thumbs_down = res['list'][index]['thumbs_down']
					author = res['list'][index]['author']
					word = res['list'][index]['word']
					example = res['list'][index]['example']
					example = example.replace("[", "")
					example = example.replace("]", "")
				except IndexError:
					if max_index > 0:
						await ctx.send(f"Please pick a number between 0 and {max_index}.")
					else:
						await ctx.send("No results found.")
					return
					
				em = discord.Embed(colour=0x1D2439)
				em.add_field(name=f"{word} ({index}/{max_index})", value=f"```{definition}```\n**Example**: {example}\n**Permalink**: {permalink}")
				em.set_footer(text=f"Definition by {author} (👍-{thumbs_up} 👎-{thumbs_down})")
				await ctx.send(embed=em)
		
	@commands.cooldown(1, 30, BucketType.user)	
	@commands.command(aliases=['yt'])
	async def youtube(self, ctx, *, search):
		decoder = minerva.Pantheon()
		encoded_key = "wqnCrsOqw4nCtMOewrTDisOfw5LCm8KXwqbDjcOTw5HDjcOBw4fDjMOdwp3DosKaw5nDi8OSw4zDm8OHwp_CksK9w4vDmcK-w4bDjsOg"
		decoded_key = decoder.decode(encoded_key)
		try:
			base_url = "https://www.googleapis.com/youtube/v3/search?"
			query = f"q={search.replace(' ', '+')}&"
			maxresults = "maxResults=1&"
			part = "part=snippet&"
			key = f"key={decoded_key}"
			async with aiohttp.ClientSession() as session:
				async with session.get(base_url+query+maxresults+part+key) as r:
					resp = await r.text()
					data = json.loads(resp)
					AddLog(str(data))
					try:
						title = data['items'][0]['snippet']['channelTitle']
						video = data['items'][0]['id']['videoId']
						videotitle = data['items'][0]['snippet']['title']
						videodesc = data['items'][0]['snippet']['description']
						#em = discord.Embed(description=f"[{videotitle} by {title}]\n{videodesc}")
						await ctx.send(f"https://www.youtube.com/watch?v={video}")
						PersonaCommand()
					except IndexError:
						await ctx.send("No videos found.")
		except KeyError:
			await ctx.send("No valid video found, try a more specific search.")
			
	@commands.cooldown(1, 30, BucketType.user)					
	@commands.group(invoke_without_command=True, aliases=["w"])
	async def weather(self, ctx, *, location:str=""):
		"""Checks the weather.
		Data provided by DarkSky.net"""
		
		#https://darksky.net/dev/docs
		if location == "":
			try:
				location = getConfig()[str(ctx.author.id)]['weather_location']
			except:
				PersonaError()
				await ctx.send(f"No location stored and missing required argument.\nUse `{ctx.message.content.split(' ')[0]} set <location>` to save your personal location, or use `weather <location>`")
				return
		
		location_arg = location	
	#	geolocator = Nominatim()
		#location = geolocator.geocode(location)
		gl = geocoder.komoot(location)
		if gl is None:
			PersonaError()
			await ctx.send("Location not found.")
			return
		lmsg = await ctx.send("<a:loading:480693540616273922> Accessing weather API...")
		weather = forecast("4bef92b904d0743356e23f31986ca793", gl.latlng[0], gl.latlng[1])
		
		#weather = forecast("4bef92b904d0743356e23f31986ca793", location.latitude, location.longitude)
		
		if weather is None:
			PersonaError()
			await ctx.send("Data not found.")
			if lmsg is not None:
				await lmsg.delete()
			return
			
		alerts = 0
		try:
			alerts = len(weather.alerts)
		except:
			pass
		
		astr = ""
		if alerts > 1:
			astr = f"**There are {alerts} alerts for this location. See `{ctx.message.content.split(' ')[0]} a {location_arg}` command.**"
		elif alerts == 1:
			astr = f"**There is an alert for this location. See `{ctx.message.content.split(' ')[0]} a {location_arg}` command.**"	
			
		precip = ""
		
		try:
			if weather.precipProbability != 0:
				precip += f" * There is a {str(weather.precipProbability*100).split('.')[0]}% chance of"
				try:
					precip += f" {weather.precipType}, "
				except:
					precip += " some weathery thing, "
			
		except:
			pass
		
		try:
			if weather.precipIntensity != 0:
				precip += f" {weather.precipIntensity} intensity (whatever that means)..\n"
		except:
			pass
		
		
		if precip != "":
			precip = f"\nPrecipitation Data:\n{precip}"	
		converted_temp = str((weather.temperature - 32) * 5/9).split(".")[0]

		f_temp = str(weather.temperature).split('.')[0]
		
		local_time = datetime.datetime.now(timezone(weather.timezone))
		if local_time.minute < 10:
			f_min = f"0{local_time.minute}"
		else:
			f_min = local_time.minute
		
		#print(vars(gl))
		#print(vars(weather))
		PersonaCommand()
		if lmsg is not None:
			await lmsg.edit(content=f"<:loaded:480337167080488960> **{gl.location} :: {weather.timezone}**\nLocal Time is {local_time.hour}:{f_min} on {local_time.day}/{local_time.month}/{local_time.year}\nCurrently {f_temp}°F/{converted_temp}°C ({str(weather.humidity).split('.')[1]}% humidity)\nCurrent weather is {weather.summary}\n{astr}{precip}")
		else:
			await ctx.send(f"**{gl.address} :: {weather.timezone}**\nLocal Time is {local_time.hour}:{f_min} on {local_time.day}/{local_time.month}/{local_time.year}\nCurrently {f_temp}°F/{converted_temp}°C ({str(weather.humidity).split('.')[1]}% humidity)\nCurrent weather is {weather.summary}\n{astr}{precip}")
		
	@commands.cooldown(1, 30, BucketType.user)			
	@weather.command(name="a")
	async def _alerts(self, ctx, *, location:str=""):
		"""Checks the weather alerts.
		Data provided by DarkSky.net"""
		
		if location == "":
			try:
				location = getConfig()[str(ctx.author.id)]['weather_location']
			except:
				PersonaError()
				await ctx.send(f"No location stored and missing required argument.\nUse `{ctx.message.content.split(' ')[0]} set <location>` to save your personal location, or use `weather <location>`")
				return
			
		lmsg = await ctx.send("<a:loading:480693540616273922> Accessing weather API...")	
		#geolocator = Nominatim()
		#location = geolocator.geocode(location)
		gl = geocoder.komoot(location)
		if gl is None:
			PersonaError()
			await ctx.send("Location not found.")
			if lmsg is not None:
				await lmsg.delete()
			return
			
		#await ctx.send(f"SENDING COORDS TO DARKSKY: {location.latitude}, {location.longitude}\n{location.address}")
		weather = forecast("4bef92b904d0743356e23f31986ca793", gl.latlng[0], gl.latlng[1])
		alerts = ""
		try:
			i = 0
			while i < len(weather.alerts):
				
				await ctx.send("**"+weather.alerts[i].title+"**\n") #+"\n"+weather.alerts[i].description[:1900])
				await ctx.send(weather.alerts[i].description[:2000])
				i += 1
			PersonaCommand()
			if lmsg is not None:
				await lmsg.delete()
		except AttributeError:
			await ctx.send(f"No alerts for {location.address}.")
			if lmsg is not None:
				await lmsg.delete()
		except Exception as e:
			AddLog(f"{type(e).__name__} - {e}")
			await ctx.send(f"{type(e).__name__} - {e}")
			PersonaError()
			if lmsg is not None:
				await lmsg.delete()
			pass
		
	@commands.cooldown(1, 30, BucketType.user)			
	@weather.command(name="fc")
	async def _forecast(self, ctx, *, location:str=""):
		"""Shows weekly forecast.
		Data provided by DarkSky.net"""
		
		if location == "":
			try:
				location = getConfig()[str(ctx.author.id)]['weather_location']
			except:
				PersonaError()
				await ctx.send(f"No location stored and missing required argument.\nUse `{ctx.message.content.split(' ')[0]} set <location>` to save your personal location, or use `weather <location>`")
				return
					
		lmsg = await ctx.send("<a:loading:480693540616273922> Accessing weather API...")				
		location_arg = location		
		#geolocator = Nominatim()
		#location = geolocator.geocode(location)
		gl = geocoder.komoot(location)
		if gl is None:
			PersonaError()
			await ctx.send("Location not found.")
			await lmsg.delete()
			return
		#weather = forecast("4bef92b904d0743356e23f31986ca793", location.latitude, location.longitude)
		
		s = "**"+gl.address+"**\n"
		weekday = datetime.date.today()
		with forecast("4bef92b904d0743356e23f31986ca793", gl.latlng[0], gl.latlng[1]) as weather:
			alerts = 0
			try:
				alerts = len(weather.alerts)
			except:
				pass
			
			astr = ""
			if alerts > 1:
				astr = f"**There are {alerts} alerts for this location. See `{ctx.message.content.split(' ')[0]} a {location_arg}` command.**"
			elif alerts == 1:
				astr = f"**There is an alert for this location. See `{ctx.message.content.split(' ')[0]} a {location_arg}` command.**"	
			
			summ = weather.daily.summary
			sl = summ.split(" ")
			old_val = ""
			for item in sl:
				if "°F" in item:
					old_val = item
					new_convert = int(item[:-2])
					new_convert = str((new_convert - 32) * 5/9).split(".")[0]
					
			summ = summ.replace(f"{old_val}", f"{old_val} ({new_convert}°C)")
			
			s += summ +'\n---\n'+astr
			
			for day in weather.daily:
				day = dict(day = datetime.date.strftime(weekday, '%a'),
						   sum = day.summary,
						   tempMin = str(day.temperatureMin).split(".")[0],
						   tempMax = str(day.temperatureMax).split(".")[0],
						   tempMinc = str((day.temperatureMin - 32) * 5/9).split(".")[0],
						   tempMaxc = str((day.temperatureMax - 32) * 5/9).split(".")[0]
						   )
				s += '\n{day}: {sum} Temp range: {tempMin} - {tempMax}°F ({tempMinc} - {tempMaxc}°C)'.format(**day)
				weekday += datetime.timedelta(days=1)	
		
		PersonaCommand()
		await ctx.send(s)	
		if lmsg:
			await lmsg.delete()
		
	@commands.cooldown(1, 5, BucketType.user)	
	@weather.command(name="set")
	async def set_loc(self, ctx, *, location):
		setConfig(parent=str(ctx.author.id), key='weather_location', value=location)
		test_case = getConfig()[str(ctx.author.id)]['weather_location']
		await ctx.send(f"Your location is saved as {test_case}")
	
	@commands.cooldown(1, 5, BucketType.user)						
	@weather.command(name="my_location")
	async def get_loc(self, ctx):
		try:
			test_case = getConfig()[str(ctx.author.id)]['weather_location']
			await ctx.send(f"Your location is saved as {test_case}")
		except:
			await ctx.send("No location stored.")
						
	@commands.command(hidden=True)
	async def revers(self, ctx, *, coords):
		await ctx.send("TEST")
		geolocator = Nominatim()
		location = geolocator.reverse(coords)
		await ctx.send(f"{location.address}")
			
	@commands.command(hidden=True)
	async def coords(self, ctx, *, place):
		await ctx.send("TEST")
		geolocator = Nominatim()
		location = geolocator.geocode(place)
		await ctx.send(f"{location.latitude}, {location.longitude}")
		
	"""'__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattr__', '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_api', '_repr', '_retrieved', 'abbreviation', 'assets', 'categories', 'created', 'data', 'derived_games', 'developers', 'embeds', 'endpoint', 'engines', 'gametypes', 'genres', 'id', 'levels', 'links', 'moderators', 'moderators', 'name', 'names', 'platforms', 'publishers', 'records', 'records', 'regions', 'release_date', 'released', 'romhack', 'ruleset', 'variables', 'weblink"""
	@commands.cooldown(1, 30, BucketType.user)	
	@commands.command(help="Shows information from Speedrun.net", brief="I can speedrun too.")
	async def sr(self, ctx, *, game_name):
	
		api = srcomapi.SpeedrunCom()
		game = api.search(srcomapi.datatypes.Game, {"name": game_name})[0]
		
		gname = game.name
		cats = len(game.records)
		#print(f"REC: {game.records}")
		s = ""
		i = 0
		
		#print(f"CATS:: {game.categories}")
		#while i < cats:
		print(game)
		s = f'### {str(game).split("Game ")[1].split(">")[0]} ###\n'
		for item in game.records:
			#try:
			#	print(game.categories[i])
			#	s += f"\n**{item}**\n"
			#except:
				#pass
			r = 0
			try:
				for run in item.runs:
					#print(f"{r}")

					#AddLog(f"DEBUG: {run} ")
					#AddLog(f"Item: {item}")
					s += f"**{str(item).split('/')[1].split('>')[0]} rank #{run['place']}** completed in {humanfriendly.format_timespan(run['run'].times['primary_t'])}\n"
					if len(s) > 1900:
						await ctx.send(s)
						s = ""
					r += 1
			except:
				s += "**No data.**"
			#print(f"{i}")
			i += 1
		
		PersonaCommand()
		await ctx.send(s)	
		#await ctx.send(f"**{game.name}**\nThere is {game.categories}\n[{len(game.records)}]{game.records[0].runs}\n\n{game}")
					
	@commands.cooldown(1, 30, BucketType.guild)	
	@commands.command(aliases=['duckduckgo', 'duck', 'web', 'srch', 'go'], enabled=False)
	async def ddg(self, ctx, *, search_term):
		"""
		Searches DuckDuckGo.
		"""
		await ctx.channel.trigger_typing()
			
		if len(search_term) < 3:
			await ctx.send("Search term must be at least 3 characters.")
			PersonaError()
			return
			
		SetTimer('ddg')
		q = DuckDuckGo(search_term)
		
		em = discord.Embed(colour=aecol, description="Query response time: "+str(SetTimer('ddg', returnmicro=True)/10000).split(".")[0]+"ms.")
		em.set_author(name="Results from DuckDuckGo", url="https://duckduckgo.com/")
		em.add_field(name=f'**{q.Heading}** ({q.Entity})', value=f"{q.AbstractText}\n{q.AbstractURL}")
		#em.add_field(name="Related", value=q.Related())
		em.set_image(url=q.Image)
		await ctx.send(embed=em)
		#await ctx.send(f"ABSTRACT: {q.AbstractURL}\nENTITY: {q.Entity}\nRESULT: {q.AbstractText}\n{q.Image}")
		PersonaCommand()
		
	@commands.cooldown(1, 10, BucketType.user)
	@commands.group(aliases=['dictionary', 'define', 'word'], invoke_without_command=True)
	async def dict(self, ctx, search_term:str):
		"""
		Dictionary Definitions (Basic)
		Searches for search_term dictionary definitions.
		Uses an offline JSON database containing basic definitions.
		"""
		await ctx.channel.trigger_typing()
		em = discord.Embed()
		lmsg = await ctx.send(f"<a:loading:480693540616273922> Searching dictionary for {search_term}, please wait...")
		limit=5
		cur = 0
		extras = []
		
		with open('dictionary.json') as f:
			r = ""
			
			data = json.load(f)
			for item, defin in data.items():
				if fuzz.ratio(search_term.lower(), item.lower()) >= 80:
					if cur <= limit:
						cur += 1
						em.add_field(name=f"[{cur}] *{item}* **({fuzz.ratio(search_term.lower(), item.lower())}% match)**", value=defin)
					else:
						extras.append(item)
						
					AddLog(f"[{cur}] {item}: {defin} **({fuzz.ratio(search_term.lower(), item.lower())}% match)**\n")
			
			if cur == 0:			
				
				if lmsg is not None:
					await lmsg.edit(content="<:loadother:480426177748533249> No results found.")
				else:
					await ctx.send("<:loadother:480426177748533249> No results found.")
			else:
				if len(extras) > 0:
					em.add_field(name="More...", value=f"\n{len(extras)} more results excluded due to message size limits.\n**{HumanizeList(extras)}**")
				
				if lmsg is not None:
					await lmsg.edit(content="<:loaded:480337167080488960> Dictionary loaded.", embed=em)
				else:
					await ctx.send(embed=em)
				PersonaCommand()
				
	@dict.command(name="-f")
	async def dict_full(self, ctx, search_term:str):
		"""Prints only the specific word definition."""
		data = {}
		lmsg = await ctx.send("<a:loading:480693540616273922> Looking that up...")
		try:
			with open('dictionary.json') as f:
				data = json.load(f)	
				await lmsg.edit(content=f"<:loaded:480337167080488960> {data[search_term.upper()]}")
				PersonaCommand()
		except:
			await lmsg.edit(content=f"<:loadother:480426177748533249> No result found.")
					
	@commands.is_owner()
	@dict.command(name="-e", hidden=True)
	async def dict_edit(self, ctx, search_term:str, *, new_definition):
		data = {}
		with open('dictionary.json') as f:
			data = json.load(f)	
			data[search_term.upper()] = new_definition
			AddLog(f"Definition of {search_term.upper()} changed to: {data[search_term.upper()]}")
			
		with open('dictionary.json', "w") as s:
			json.dump(data, s, indent=4)
		
		PersonaCommand()	
		await ctx.send(f"Definition of {search_term.upper()} changed to: {data[search_term.upper()]}")

	@commands.is_owner()
	@dict.command(name="-d", hidden=True)
	async def dict_del(self, ctx, search_term:str):
		data = {}
		with open('dictionary.json') as f:
			data = json.load(f)	
			del data[search_term.upper()]
			
		with open('dictionary.json', "w") as s:
			json.dump(data, s, indent=4)
			
		PersonaCommand()
		await ctx.send(f"Key changed.\n{search_term.upper()} > none")
		
	#==============
	#WIKIPEDIA
	#==============
	@commands.cooldown(1, 20, BucketType.user)	
	@commands.group(invoke_without_command=True)
	async def wiki(self, ctx):
		"""
		Wikipedia interface.
		Basic wikipedia API calls. Check subcommands for information.
		"""
		await ctx.channel.trigger_typing()
		await ctx.send("No command found.\nUse either `wiki search`, `wiki random` or `wiki page`\nSee `help wiki` for more information.")
	
	@commands.cooldown(1, 20, BucketType.user)	
	@wiki.command(name="search")
	async def wiki_search(self, ctx, *, search_term):
		"""
		Wikipedia search.
		Prints out the first 3 results for the search_term, with URL.
		"""
		await ctx.channel.trigger_typing()
		searches = wikipedia.search(search_term, results=3)
		fstr = ""
		for item in searches:
			fstr += f"[{item}]: {wikipedia.page(item).url}\n"
		em = discord.Embed(colour=aecol)
		em.add_field(name=f'**{search_term}**', value=f"{fstr}")
		await ctx.send(embed=em)
	
	@commands.cooldown(1, 20, BucketType.user)	
	@wiki.command(name="page")
	async def wiki_page(self, ctx, *, page_name):
		"""
		Wikipedia pages.
		Returns a page matching page_name.
		"""
		await ctx.channel.trigger_typing()
		try:
			wik = wikipedia.page(page_name)
			em = discord.Embed(colour=aecol)
			em.add_field(name="Summary", value=f"{wikipedia.summary(wik.title)[:1024]}")
			em.set_author(name=f'{wik.title}', url=wik.url)
			try:
				em.set_thumbnail(url=wik.images[0])
			except IndexError:
				em.set_footer(text="No image.")
			await ctx.send(embed=em)
		except Exception as e:
			await ctx.send(e)
			
	@commands.cooldown(1, 20, BucketType.user)	
	@wiki.command(name="random")
	async def wiki_random(self, ctx):
		"""
		Wikipedia random page.
		Returns a random page.
		"""
		await ctx.channel.trigger_typing()
		wik = wikipedia.page(wikipedia.random())
		em = discord.Embed(colour=aecol)
		em.add_field(name="Summary", value=f"{wikipedia.summary(wik.title)[:1024]}")
		em.set_author(name=f'{wik.title}', url=wik.url)
		try:
			em.set_thumbnail(url=wik.images[0])
		except IndexError:
			em.set_footer(text="No image.")
		await ctx.send(embed=em)
			
	#==============
	#REDDIT
	#===============
	@commands.cooldown(1, 20, BucketType.user)		
	@commands.group(invoke_without_command=True, help="A simple reddit client.", brief="The front page of the strange parts of the internet.")
	async def reddit(self, ctx):
		await ctx.channel.trigger_typing()
		global reddit_client
		if reddit_client is None:
			await ctx.send("**Logged Out**\nUse `reddit login`")
		else:
			await ctx.send("**Active**\nUse `reddit sub` or `reddit random`.\nSee `help reddit` for more.")

	@commands.cooldown(2, 20, BucketType.user)	
	@reddit.command(name="redditrps", aliases=['r', 'rp', 'rand_sub', 'rps'])
	async def _reddit_rps(self, ctx, sub=""):
		"""Selects a random post from a subreddit.
		
		Having the subreddit as ## chooses a random one from the favourites list.
		Add a * wildcard to the sub name to auto-find one from the favourites list.
		
		Examples:
		reddit r aww
		reddit r *sodago
		reddit r wheredidthesodago
		reddit r ##
		"""
		faves = ['aww', 'gifs', 'wheredidthesodago', 'glitch_in_the_matrix', 'thewaywewere', 'meme', 'reactiongifs', 'mildlyinteresting', 'woahdude', 'awwnime', 'perfecttiming', 'prettygirls', 'ass', 'pareidolia', 'realgirls', 'tumblr', 'unexpected', 'photoshopbattles', 'cringepics', 'youtubehaiku', 'peopleFuckingDying', 'dankmemes']
		faves.sort()
		if sub == "":
			await ctx.send("No ideas? Here's some of our faves; "+HumanizeList(faves)+"\nStill cant decide, just use ## as the sub name and let me decide for you.")
			return
			
		if reddit_client is None:
			await ctx.send(f"We're not logged in to reddit. Use `reddit login`.'")	
			return
		prog = ProgressBar()
		msg = await ctx.send(f"Starting search...\n{prog(25)}")
		fsub = ""
		if "*" in sub:
			sub = sub.replace("*", "")
			for item in faves:
				if sub in item:
					fsub = item
		elif sub == "##":
			fsub = random.choice(faves)
			await msg.edit(content=f"Getting posts from {fsub}\n{prog(50)}")
		else:
			fsub = sub
		
		if fsub == "":
			await msg.edit(content="There was a bork in the finding of the subreddit name process thing.")
			return
			
		try:
			subby = reddit_client.subreddit(fsub)
		
			if subby.over18 and not ctx.channel.nsfw and strict_nsfw(ctx):
				await ctx.send("You must be in a NSFW channel to view this content or not have `strict NSFW rules` for this guild.")
				return
				
			await msg.edit(content=f"Getting posts from {fsub}\n{prog(75)}")	
			subz = subby.hot()
			post_to_pick = random.randint(1, 100)
			for i in range(0, post_to_pick):
				submission = next(x for x in subz if not x.stickied)
				
			await msg.edit(content=f"Random post from {fsub}\n{submission.url}")
		except Exception as e:
			await msg.edit(content=f"Subreddit query error: {e}")
	
	@commands.cooldown(2, 20, BucketType.user)	
	@reddit.command(name="5050")
	async def _reddit_5050(self, ctx):
		"""Take a chance..
		Pulls a random top post from the 50/50 challenge subreddit."""
		if reddit_client is None:
			await ctx.send(f"We're not logged in to reddit. Use `reddit login`.'")	
			return
			
		if not ctx.channel.nsfw and strict_nsfw(ctx):
			await ctx.send("You must be in a NSFW channel to view this content or not have `strict NSFW rules` for this guild.")
			return
			
		memes_submissions = reddit_client.subreddit('5050pics').hot()
		post_to_pick = random.randint(1, 100)
		for i in range(0, post_to_pick):
			submission = next(x for x in memes_submissions if not x.stickied)
		await ctx.send(submission.url)
			
	@reddit.command(name="meme")
	async def _reddit_meme(self, ctx):
		"""Le spicey meymeys.
		Pulls a random top meme."""
		if reddit_client is None:
			await ctx.send(f"We're not logged in to reddit. Use `reddit login`.'")	
			return
			
		memes_submissions = reddit_client.subreddit('memes').hot()
		post_to_pick = random.randint(1, 150)
		for i in range(0, post_to_pick):
			submission = next(x for x in memes_submissions if not x.stickied)
		
		await ctx.send(submission.url)
		
	@commands.cooldown(1, 20, BucketType.user)	
	@reddit.command(name="login", help="Logs me in to the reddit app.", brief="Creates the reddit instance.")
	async def reddit_login(self, ctx):
		global reddit_client_id
		global reddit_client_secret
		global reddit_user_agent
		global reddit_client
		await ctx.channel.trigger_typing()
		if reddit_client is None:
			reddit_client = praw.Reddit(client_id=reddit_client_id, client_secret=reddit_client_secret, user_agent=reddit_user_agent)
			await ctx.send(f"Reddit instance created.")
		else:
			await ctx.send(f"Reddit is already logged in.")
		return
	
	@commands.cooldown(2, 20, BucketType.user)	
	@reddit.command(name="sub", help="Reads a subreddit with the name you request. Optional limit argument decides how many posts to find.", brief="Read a subreddit.")
	async def reddit_sub(self, ctx, name:str, limit=5):
		await ctx.channel.trigger_typing()
		if reddit_client is None:
			await ctx.send(f"We're not logged in to reddit. Use `reddit login`.'")	
			return
			
		rs = reddit_client.subreddit(name)

		if rs.over18 and not ctx.channel.nsfw and strict_nsfw(ctx):
			await ctx.send("You must be in a NSFW channel to view this content or not have `strict NSFW rules` for this guild.")
			return
			
		titles = f"**{rs.display_name}** ({rs.subscribers} subscribers)"
		prints = ""
		for submission in reddit_client.subreddit(name).hot(limit=int(limit)):
			prints += f"**{submission.title}**\n<{submission.shortlink}>\n\n"
			
		em = discord.Embed(colour=aecol)
		em.add_field(name=titles, value=prints)
		await ctx.send(embed=em)
	
	@commands.cooldown(2, 20, BucketType.user)	
	@reddit.command(name="random", help="Reads a random subreddit. Optional limit argument decides how many posts to find. Optional nswf flag decides if if the sub should be safe for work or not.", brief="Read a random subreddit.")
	async def reddit_random(self, ctx, limit=5, nsfw="false"):
		await ctx.channel.trigger_typing()
		if reddit_client is None:
			await ctx.send(f"We're not logged in to reddit. Use `reddit login`.'")	
			return
			
		if boolinate(nsfw) and not ctx.channel.nsfw and strict_nsfw(ctx):
			await ctx.send("You must be in a NSFW channel to view this content or not have `strict NSFW rules` for this guild.")
			return
						
		rs = reddit_client.random_subreddit(boolinate(nsfw))

			
		titles = f"**{rs.display_name}** ({rs.subscribers} subscribers)"
		prints = ""
		for submission in reddit_client.subreddit(rs.display_name).hot(limit=int(limit)):
			prints += f"**{submission.title}**\n<{submission.shortlink}>\n\n"
			
		em = discord.Embed(colour=aecol)
		em.add_field(name=titles, value=prints)
		await ctx.send(embed=em)
	
	#===========
	#Discord Status... in a Discord Bot.... ya never know
	@commands.command(aliases=['discord'])
	async def discord_status(self, ctx, other=""):
		"""Shows Discord status."""
		if other == "c":
			result = requests.get(f'https://srhpyqt94yxb.statuspage.io/api/v2/summary.json')

			data = json.loads(result.content)
			s = ""
			for item in data['components']:
				date = item['created_at'].split("T")[0]
				_time = item['created_at'].split("T")[1]
				time = _time.split(":")[0]+":"+_time.split(":")[1]
				dateu = item['updated_at'].split("T")[0]
				_timeu = item['updated_at'].split("T")[1]
				timeu = _timeu.split(":")[0]+":"+_timeu.split(":")[1]

				if item['status'] == "operational":
					icon = "🔸"
				else:
					icon = "🔹"
					
				s += f"{icon} {item['name']}: {item['status']} - Created: {date} - {time} - Updated: {dateu} - {timeu}\n"
				
			await ctx.send(s)
			PersonaCommand()
			return
			
		f = requests.get("https://srhpyqt94yxb.statuspage.io/api/v2/incidents/unresolved.json")
		data = json.loads(f.content)
	#	print(data)
		s = ""
		if len(data['incidents']) > 0: # monitoring investigating Identified 👁‍🗨 🔍 ⏱
			ms = data['incidents'][0]['status']
			icon = "X"
			if ms == "monitoring":
				icon = "👁‍🗨"
			elif ms == "investigating":
				icon = "⏱"
			elif ms == "Identified":
				icon = "🔍"
				
			s = f"**{data['incidents'][0]['name']}** (Status: {icon}{ms})\n\n"

			if len(data['incidents'][0]['incident_updates']) > 0:
				for item in data['incidents'][0]['incident_updates']:
					icon = "X"
					if item['status'] == "monitoring":
						icon = "👁‍🗨"
					elif item['status'] == "investigating":
						icon = "⏱"
					elif item['status'] == "identified":
						icon = "🔍"
					s += f"\n{icon} **{item['status']}**\n```{item['body']}\n-\n"
					
					date = item['created_at'].split("T")[0]
					_time = item['created_at'].split("T")[1]
					time = _time.split(":")[0]+":"+_time.split(":")[1]
					dateu = item['updated_at'].split("T")[0]
					_timeu = item['updated_at'].split("T")[1]
					timeu = _timeu.split(":")[0]+":"+_timeu.split(":")[1]
					
					if item['created_at'] != item['updated_at']:
						s += f"Alert created at {time}PDT on {date} (Updated at {timeu}PDT on {dateu})```"
					else:
						s += f"Alert created at {time}PDT on {date}```"
		else:
			s = "No issues in Discord are currently listed."
		PersonaCommand()
		await ctx.send(s)

	#===========
	#Deus Ex related searching
	#===========	
	@is_in_guild(396716931962503169)
	@commands.command(hidden=True)
	async def readkb(self, ctx, index):
		await ctx.channel.trigger_typing()
		to_get = ""
		if index.isdigit():
			if len(kb_cache) == 0:
				await ctx.send("The cache index is empty. Run `kb <search term>` first or enter a pagename, not a number.")
				return
			to_get = kb_cache[int(index)]
			
		else:
			to_get = f"https://deusexhq.gitlab.io/kb/{index}.html"
		
		try:
			result = requests.get(to_get)
		except:
			await ctx.send("An error occured while checking that page...")
			return
		outp = BeautifulSoup(result.text, 'html5lib')

		fmsg = ""
		divlist = outp.find_all('div')
		for messages in divlist:
			if "tcalert" in messages:
				#lstr = link.get('href')
				await ctx.send(messages.text.strip())
				#print(messages)
				
		flist = outp.find_all('p')
		i = 0
		for messages in flist:
			#lstr = link.get('href')
			if messages.text.strip() != "":
				await ctx.send(messages.text.strip())
			i += 1
			if i == (len(flist) -1):
				break
			
	@is_in_guild(396716931962503169)
	@commands.command()
	async def kb(self, ctx, *page):
		"""
		Searches the Knowledge Base site for articles.
		The Knowledge Base is built on Alpha's original website which was taken down and recovered.
		Unless specified otherwise, credits to the majority of the site articles go to the Alpha members and other contributors;
		
		Kaiden, Allan, Daedalus, Alex, Nobody, DJ, Clix, and others."""
		await ctx.channel.trigger_typing()
		root_url = "deusexhq" if "*" in page[0] else "dxhq"
		page = list(page)
		print(root_url)
		page[0] = page[0].replace("*", "")
		kb_cache.clear()
		if len(page) > 0:
			if page[0] == "ms":
				await ctx.send(f"Full info about the masterserver: https://{root_url}.gitlab.io/kb/dxms.html\nDirect download to the 2018 MS Patcher by Machete: https://dxhq.gitlab.io/kb/dxmsfix.exe")
				PersonaCommand()
			elif len(page[0]) < 3:
				await ctx.send("Search term must be at least 3 characters.")
				PersonaError()
				return
			elif page[0] == "discords":
				await ctx.send(f"List of all DX related discords is found here;; https://{root_url}.gitlab.io/kb/discord.html")
				PersonaCommand()
			else:
				lmsg = await ctx.send("<a:loading:480693540616273922> **Searching DXMP Knowledge Base...**")
				results=""
				async with aiohttp.ClientSession() as session:
					async with session.get(f'https://{root_url}.gitlab.io/kb/') as r:
						resp = await r.text()
						outp = BeautifulSoup(resp, 'html5lib')
						for link in outp.find_all('a'):
							lstr = link.get('href')
							if '/kb/' in lstr:# and page.lower() in lstr.lower():
								def mc(l, s):
									g = 0
									for item in l:
										if item in s:
											g += 1
									
									if g == len(l):
										return True
									else:
										return False
										
								if mc(page, lstr):
									results += f"🔸 https://{root_url}.gitlab.io{lstr}\n"
									kb_cache.append(f"https://{root_url}.gitlab.io{lstr}")	
								#for item in page:
									#if item in lstr:
										#results += f"https://dxmp.gitlab.io{lstr}\n"

						if results != "":
							PersonaCommand()
							em = discord.Embed(description=results)
							#em.add_field(name=f"Results for {HumanizeList(page)}", value=results)
							#em.set_footer(text="Results from the Deus Ex Knowledge Base", icon_url="https://cdn.discordapp.com/emojis/420490333164666890.png?v=1")
							if lmsg is not None:
								await lmsg.edit(content="<:loaded:480337167080488960> Results from KB.", embed=em)
							else:
								await ctx.send(embed=em)
						else:
							if lmsg is not None:
								await lmsg.edit(content="<:loadother:480426177748533249> No results found.")
							else:
								await ctx.send("<:loadother:480426177748533249> No results found.")
		else:
			await ctx.send(f"https://{root_url}.gitlab.io/kb/\nTo search for specific pages, add an argument to the command: `page name`")
	
	@commands.command()
	async def rtfm(self, ctx, *page):
		"""Searches Python Docs"""
		await ctx.channel.trigger_typing()
		if len(page) > 0:
			lmsg = await ctx.send("**Searching Python3 Docs...**")
			result = requests.get(f'https://docs.python.org/3/tutorial/index.html')
			outp = BeautifulSoup(result.text, 'html5lib')
			results = ""
			flist = outp.find_all('a')
			for link in flist:
				lstr = link.get('href')
				
				def mc(l, s):
					g = 0
					for item in l:
						if item in s:
							g += 1
					
					if g == len(l):
						return True
					else:
						return False
						
				if mc(page, lstr):
					results += f"🔸 https://docs.python.org/3/tutorial/{lstr}\n"
					
			await lmsg.delete()	
			if results != "":
				em = discord.Embed()
				em.add_field(name=f"Results for {HumanizeList(page)}", value=results)
				em.set_footer(text="Results from Python3 Docs", icon_url="https://docs.python.org/3/_static/py.png")
				await ctx.send(embed=em)
			else:
				await ctx.send("No results found.")
		else:
			await ctx.send('https://docs.python.org/3/tutorial/index.html')	
	
	@is_in_guild(396716931962503169)		
	@commands.command()
	async def tacks(self, ctx, page=""):
		"""
		Searches Tacks for web pages.
		"""
		await ctx.channel.trigger_typing()
		if page != "":
			lmsg = await ctx.send("<a:loading:480693540616273922> **Searching Tacks...**")
			results = ""
			async with aiohttp.ClientSession() as session:
				async with session.get(f'http://www.offtopicproductions.com/tacks/') as r:
					resp = await r.text()
					outp = BeautifulSoup(resp, 'html5lib')

					for link in outp.find_all('a'):
						lstr = link.get('href')
						try:
							if '/' not in lstr and page.lower() in lstr.lower():
								results += f"http://www.offtopicproductions.com/tacks/{lstr}\n"
						except:
							pass
					
					if results != "":
						PersonaCommand()
						em = discord.Embed()
						em.add_field(name=f"Results for {page}", value=results)
						em.set_footer(text="Results from Tacks Lab Tutorials", icon_url="https://cdn.discordapp.com/emojis/396835146080190484.png?v=1")
						if lmsg is not None:
							await lmsg.edit(content="<:loaded:480337167080488960> Connected to `~/tacks/`.", embed=em)
						else:
							await ctx.send(embed=em)

					else:
						if lmsg is not None:
							await lmsg.edit(content="<:loadother:480426177748533249> No results found.")
						else:
							await ctx.send("<:loadother:480426177748533249> No results found.")
		else:
			await ctx.send("http://www.offtopicproductions.com/tacks/\nTo search for specific pages, add an argument to the command: `page name`")
		
	@is_in_guild(396716931962503169)				
	@commands.cooldown(1, 20, BucketType.user)	
	@commands.command(name="dxn")
	async def search_deusexnetwork(self, ctx, *, search):
		"""Search Deus Ex Network archive."""
		await ctx.channel.trigger_typing()	

		result = ""
		lin = 0
		f = 0
		lmsg = await ctx.send(f'<a:loading:480693540616273922> **Searching DeusExNetwork for {search}...**')
		async with aiohttp.ClientSession() as session:
			async with session.get(f'http://deusexnetwork.com/files?q={search}') as r:
				resp = await r.text()
				outp = BeautifulSoup(resp, 'html.parser')
				fp = ''
				for link in outp.find_all('a'):
					lstr = link.get('href')
					if '/files/' in lstr and '/upload' not in lstr:
						f = 1
						if lin == 10:
							lin = -1
							em = discord.Embed(colour=aecol)
							em.add_field(name=f'**{outp.title.string}**\nSearch result for: {search}', value=fp)
							await ctx.send(embed=em)
							fp = ""
						fp = fp+'\nhttp://deusexnetwork.com'+lstr+' ('+outp.find_all('em')[lin].string+')'
						lin += 1
				
				if f == 0:
					PersonaCommand()
					em = discord.Embed(colour=0xFF0000)
					em.add_field(name=f'<:loadother:480426177748533249> **{outp.title.string}**\nSearch result for: {search}', value=f"No results found for {search}.")
					if lmsg is not None:
						await lmsg.edit(content="", embed=em)
					else:
						await ctx.send(embed=em)
				else:
					PersonaCommand()
					em = discord.Embed(colour=aecol)
					em.add_field(name=f'<:loaded:480337167080488960> **{outp.title.string}**\nSearch result for: {search}', value=fp)
					if lmsg is not None:
						await lmsg.edit(content="", embed=em)
					else:
						await ctx.send(embed=em)
	
	@commands.cooldown(1, 30)	
	@commands.command(aliases=['xk'], help="Shows an image from the xkcd webcomic site.", brief="Even bots enjoy web comics.")
	async def xkcd(self, ctx, page:int=0):
		await ctx.channel.trigger_typing()
		if page != 0:
			try:
				result =  requests.get(f'http://xkcd.com/{page}/info.0.json')
			except:
				await ctx.send("Page not found.")
				return
				
			outp = BeautifulSoup(result.text, 'html5lib')
			d = ast.literal_eval(outp.body.text)
			
			month = d.get("month")
			year = d.get("year")
			day = d.get("day")
			alt = d.get("alt")
			title = d.get("title")
			image = d.get("img")
			num = str(d.get("num"))
			
			em = discord.Embed(colour=aecol)
			em.add_field(name=title+" (#"+num+")", value=alt+"\n**Date Posted**: "+day+"/"+month+"/"+year)
			em.set_image(url=image)
			await ctx.send(embed=em)
			PersonaCommand()
		else:
			result =  requests.get('http://xkcd.com/info.0.json')
			outp = BeautifulSoup(result.text, 'html5lib')
			d = ast.literal_eval(outp.body.text)
			
			month = d.get("month")
			year = d.get("year")
			day = d.get("day")
			alt = d.get("alt")
			title = d.get("title")
			image = d.get("img")
			num = str(d.get("num"))
			
			em = discord.Embed(colour=aecol)
			em.add_field(name=title+" (#"+num+")", value=alt+"\n**Date Posted**: "+day+"/"+month+"/"+year)
			em.set_image(url=image)
			await ctx.send(embed=em)
			PersonaCommand()

def Webhook(text="", embed=None):
	#Testzone: 461851689922854922 / EHjh3I2uAZXpt1MPf0ehzH8yzwfvYLz5YiU9zALRN2YBEj0ebafmAmR7w2YPDHzTDG_o
	#Matchmaking: 461858508237701120 / jUIj3q0tb42XSDVEJnG2_7umXfRGJZZiablLN0oHcVkzfVj-ZJ4n201P-JGrgFCLb97c
	#Announce: 461868227664805908 8VHKMw0PRZc_5Rmng3NYgOiYjvCR2eV3rVNQsv7G2mxUgy2u4Vw7AaR8-7DJf1GYGrvo
	
	#https://discordapp.com/api/webhooks/461851689922854922/EHjh3I2uAZXpt1MPf0ehzH8yzwfvYLz5YiU9zALRN2YBEj0ebafmAmR7w2YPDHzTDG_o
	
	webhook_id = "461851689922854922"
	webhook_token = "EHjh3I2uAZXpt1MPf0ehzH8yzwfvYLz5YiU9zALRN2YBEj0ebafmAmR7w2YPDHzTDG_o"
	webhook = discord.Webhook.partial(webhook_id, webhook_token, adapter=discord.RequestsWebhookAdapter())
	webhook.send(text, embed=embed)
	
def setup(bot):
	global client
	bot.add_cog(api(bot))
	client = bot
