import discord
from discord.ext import commands
from discord.ext.commands.cooldowns import BucketType
from bot_common import *
from sanic import Sanic
from sanic.response import json

app = Sanic()

@app.route("/athena")
async def test(request):
    return json({"hello": "world"})
    
@app.route("/jsontest")
def post_json(request):
    return json({ "received": True, "message": request.json })

aecol = 0xFFA500

class web:
	"""A set of commands for getting information about things."""
	def __init__(self, bot):
		self.bot = bot
		app.run(host="0.0.0.0", port=8000)
				
def setup(bot):
	global client
	bot.add_cog(web(bot))
	client = bot
    
