import discord
from discord.ext import commands
from discord.ext.commands.cooldowns import BucketType
import datetime
import humanfriendly
import sys
import random
import secrets
from persona import *
from athena_common import *

aecol = 0xFFA500

class info:
	"""A set of commands for getting information about things."""
	def __init__(self, bot):
		self.bot = bot
			
	@commands.command(aliases=['tz', 'timez', 'timezones'], help="Shows time for X timezone. Uses either offset values or country codes. \nNote: List is not complete yet.\nNote: Offsets are based from GMT time.\nNote: If no timezone is specified, shows GMT time.", brief="I can tell time, honest.")
	async def time(self, ctx, timezone=""):
		await ctx.channel.trigger_typing()
		now = datetime.datetime.now()
		m = now.strftime("%M")
		h = correct_time(int(now.strftime("%H"))) #int(now.strftime("%H")) - 2
		tz = "GMT"
	
		if timezone != "":
			arg = timezone.lower()
			if "jst" in arg or "+9" in arg or "japan" in arg:
				tz = "JST / GMT+9 (Japan)"
				h += 9
			elif "ast" in arg or "atlantic" in arg or "canada" in arg or "-4" in arg:
				tz = "AST / GMT-4 (Canada)"
				h -= 4
			elif "est" in arg or "eastern" in arg or "-5" in arg:
				tz = "EST / GMT-5 (USA & Canada)"
				h -= 5
			elif "cst" in arg or "central" in arg or "-6" in arg:
				tz = "CST / GMT-6 (USA & Canada)"
				h -= 6
			elif "mst" in arg or "mountain" in arg or "-7" in arg:
				tz = "MST / GMT-7 (USA & Canada)"
				h -= 7
			elif "pst" in arg or "pacific" in arg or "-8" in arg:
				tz = "PST / GMT-8 (USA & Canada)"
				h -= 8
			elif "hst" in arg or "hast" in arg or "ahst" in arg or "cat" in arg or "-10" in arg:
				tz = "GMT-10 (Hawaiin Standard & Alaska)"
				h -= 10
			elif "idlw" in arg or "west" in arg or "international" in arg or "date" in arg or "-12" in arg:
				tz = "GMT-12 (International Date Line West)"
				h -= 12
			elif arg.startswith("+"):
				offset = arg[1:]
				if offset.isdigit() is True:
					tz = "Custom (UNKNOWN TIMEZONE NAME)"
					h += int(offset)
				else:
					await ctx.send(f"Custom offset must be an integer.")
					return
			elif arg.startswith("-"):
				offset = arg[1:]
				if offset.isdigit() is True:
					tz = "Custom (UNKNOWN TIMEZONE NAME)"
					h -= int(offset)
				else:
					await ctx.send(f"Custom offset must be an integer.")
					return
			else:
				await ctx.send(f"Couldn't find a timezone related to {arg}, try again with some other identifier.")
				return
		
		PersonaCommand()	
		h = str(h)
		await ctx.send(f"The current time is: {h}:{m} {tz}")
	
	@commands.guild_only()
	@commands.command()
	async def emoji(self, ctx, emoji_name, show=""):
		"""Gives info about an emoji."""
		em = discord.Embed()
		
		i = 0
		#for guild in client.guilds:
		for emoji in client.emojis:
			if emoji_name in emoji.name:
				i += 1
				if show == "-s":
					await ctx.send(emoji.url)
					return
				else:
					em.add_field(name=emoji.name, value=f"**Source**: {emoji.guild.name} ({emoji.guild.id})\n**URL**: {emoji.url}\n**Created**: {emoji.created_at.day}/{emoji.created_at.month}/{emoji.created_at.year}\n**ID**: {emoji.id}")
		
		if i > 0:
			await ctx.send(f"{i} emojis found.", embed=em)
		else:
			await ctx.send("No emojis found.")
		PersonaCommand()
		
	@commands.guild_only()
	@commands.command()
	async def emojis(self, ctx):
		"""Lists the servers emojis"""
		for guild in client.guilds:
			s = ""
			for emoji in guild.emojis:
				s += f"{emoji.name} "
			await ctx.send(f"Emojis from {guild.name}:\n{s}")
		PersonaCommand()
			
	@commands.guild_only()
	@commands.command(aliases=['srv', 'si'], help="Shows information about a server.\nIf otherserver is left out, shows information about the current server, else attempts to find another server based on the name.", brief="Know things about the server.")
	async def serverinfo(self, ctx, otherserver=""):
		await ctx.channel.trigger_typing()
		#Add option for other server checks
		serv = None

		if otherserver != "":
			for server in client.guilds:
				if otherserver.lower() in server.name.lower():
					serv = server
			
			if serv is None:
				await ctx.send("Server not found.")
				return
				
		else:
			serv = ctx.guild
			
		bots = []
		membercount = 0
		for member in serv.members:
			if not member.bot:
				membercount += 1
			else:
				if member.display_name is not member.name:
					bots.append(member.name+" ("+member.display_name+")")
				else:
					bots.append(member.name)
				
		chancount = 0
		for chan in serv.channels:
			chancount += 1
		
		emojistr = []
		for emoji in serv.emojis:
			emojistr.append(emoji.name)
		if len(emojistr) > 0:
			emojistr.sort()
		else:
			emojistr = ['None']
			
		rolestr = []
		for role in serv.roles:
			if "everyone" not in role.name and not role.managed:
				rolestr.append(role.name)
		rolestr.sort()
		
		created = serv.created_at
		
		ownerstr = serv.owner.name
		if serv.owner.name != serv.owner.display_name:
			ownerstr += " ("+serv.owner.display_name+")"
		em = discord.Embed(colour=aecol)
		em.add_field(name=serv.name+" ("+str(serv.id)+")", value="**Current Owner**: "+serv.owner.name+"\n**Real Members (Bots not included)**: "+str(membercount)+"\n**Channels**: "+str(chancount)+"\n**Server Region**: "+str(serv.region)+"\n**Created**: "+str(created.day)+"/"+str(created.month)+"/"+str(created.year)+"\n**Verification Level**: "+str(serv.verification_level))
		em.add_field(name="Roles", value=HumanizeList(rolestr))
		em.add_field(name="Emotes", value=HumanizeList(emojistr))
		em.add_field(name="Bots", value=HumanizeList(bots))
		em.set_thumbnail(url=serv.icon_url)
		await ctx.send(embed=em)
		PersonaCommand()
		
	@commands.guild_only()
	@commands.cooldown(1, 10, BucketType.user)
	@commands.command(aliases=['ch', 'ci'], help="Shows information about a channel. Current channel if otherchannel is left out, else attempts to find another channel based on the name.", brief="Know things about the channels.")
	async def channelinfo(self, ctx, otherchannel=""):
		await ctx.channel.trigger_typing()
		infochan = None

		if otherchannel != "":
			for chan in ctx.guild.channels:
				if otherchannel.lower() in chan.name.lower():
					infochan = chan
			
			if infochan is None:
				await ctx.send("Channel not found.")
				return		
		else:
			infochan = ctx.channel
			
		AddLog(str(type(infochan)))	
		
		if type(infochan) is discord.channel.TextChannel:
			
			topic = ""
			if infochan.topic is not None:
				if infochan.topic != "":
					topic = "*"+infochan.topic+"*"
			
			channame = infochan.name
			created = infochan.created_at
			chanid = infochan.id
			catid = infochan.category_id
			ccat = ""
			for cat in infochan.guild.categories:
				if cat.id == catid:
					ccat = ">"+cat.name
			em = discord.Embed(colour=aecol)
			em.add_field(name=f"{channame} ({chanid})", value=f"{topic}\n{ccat}\n\n**Type**: Text\n**Channel created** at {created.day}/{created.month}/{created.year}\n{infochan.mention}")
			await ctx.send(embed=em)
			PersonaCommand()
		if type(infochan) is discord.channel.VoiceChannel:
			voicemem = []
			if len(infochan.members) > 0:
				for mem in infochan.members:
					voicemem.append(mem.display_name)
			catid = infochan.category_id
			if len(voicemem) > 0:
				voicemem = "**Users in channel**: "+HumanizeList(voicemem)
			ccat = ""
			for cat in infochan.guild.categories:
				if cat.id == catid:
					ccat = ">"+cat.name	
			created = infochan.created_at
			em = discord.Embed(colour=aecol)
			em.add_field(name=f"{infochan.name} ({infochan.id})", value=f"{ccat}\n**Type**: Voice\n**Voice Channel created** at {created.day}/{created.month}/{created.year}")
			await ctx.send(embed=em)
			PersonaCommand()
			
	@commands.guild_only()
	@commands.cooldown(1, 10, BucketType.user)		
	@commands.command(aliases=['ui', 'usr'], help="Shows information about a user. If otheruser is left out, shows YOUR information, else finds a user based on the name.", brief="Next-gen stalking.")
	async def userinfo(self, ctx, otheruser=""):
		await ctx.channel.trigger_typing()
		infouser = None
		globalmems = False
		if "++" in otheruser:
			globalmems = True
			otheruser = otheruser.replace("++", "")
		if otheruser != "":
			
			if otheruser == "*":
				if globalmems is False:
					infouser = secrets.choice(ctx.guild.members)
				else:
					infouser = secrets.choice(list(client.get_all_members()))
				AddLog("Random: "+infouser.name)
			else:	
				if globalmems is False:
					for member in ctx.guild.members:
						if otheruser.lower() in member.name.lower() or otheruser.lower() in member.display_name.lower():
							infouser = member
				else:
					for member in client.get_all_members():
						if otheruser.lower() in member.name.lower() or otheruser.lower() in member.display_name.lower():
							infouser = member
				
				if infouser is None:
					await ctx.send(msg_generator(types="notfound_user", context=otheruser))
					return
				
		else:
			infouser = ctx.author
			
		namestr = ""
		if infouser.name != infouser.display_name:
			namestr = infouser.name+" (AKA "+infouser.display_name+")"
		else:
			namestr = infouser.name
			
		if infouser.bot is True:
			namestr += " [BOT]"
		
		rolestr = []
		for role in infouser.roles:
			if "everyone" not in role.name:
				rolestr.append(role.name)
		
		if len(rolestr) > 0:
			rolestr.sort()
			rolestr = HumanizeList(rolestr)
		else:
			rolestr = "None."	
		created = infouser.created_at
		joined = infouser.joined_at
		gamestr = ""
		if infouser.activity is not None:
			gamestr = f"**Playing** {infouser.activity.name}"
			try:
				times = datetime.datetime.now() - infouser.activity.start
				gamestr += f" for {humanfriendly.format_timespan(times.seconds)}"
			except Exception as e:
				AddLog(str(e))
				pass
				
			try:
				gamestr += "\n**URL**: "+infouser.activity.url
			except:
				pass
			
			try:
				gamestr += "\n**Details**: "+infouser.activity.details
			except:
				pass
				
			try:
				gamestr += "\n**Twitch Name**: "+infouser.activity.twitchname
			except:
				pass
				
		guildstr = infouser.guild.name+" ("+str(infouser.guild.id)+")"
		em = discord.Embed(colour=infouser.colour)
		em.add_field(name=namestr, value="**Roles**: "+rolestr+"\n**Account created** at "+str(created.day)+"/"+str(created.month)+"/"+str(created.year)+"\n**Joined this server** at "+str(joined.day)+"/"+str(joined.month)+"/"+str(joined.year)+"\n**Status**: "+str(infouser.status)+"\n\n"+gamestr+"\n"+guildstr)
		em.set_thumbnail(url=infouser.avatar_url)
		await ctx.send(embed=em)
		PersonaCommand()
				
def setup(bot):
	global client
	bot.add_cog(info(bot))
	client = bot
