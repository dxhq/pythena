import discord
from discord.ext import commands
from discord.ext.commands.cooldowns import BucketType
from .utils import checks
import datetime
import humanfriendly
import requests
import os
import random
import itertools
import secrets
import json
from persona import *
from athena_common import *
#import asyncio
#from image2gif import writeGif
from PIL import Image, ImageDraw, ImageFont
#import PIL.Image
#import PIL.ImageDraw
#import PIL.ImageSequence

from art import *
from dataclasses import dataclass
import lenny
import aiohttp
import memedict
import cowsay
import io
import copy
from contextlib import redirect_stdout

@dataclass
class Card:
	value: str
	suit: str

class Deck:
	def __init__(self, bot, player):
		self.cards = list(itertools.product(range(1,14),['Spade','Heart','Diamond','Club']))
		bot.decks[player] = self
		#random.shuffle(self.cards)
	
	def shuffle(self, *, bShuffle=True):
		if bShuffle:
			random.shuffle(self.cards)
		return self.cards
	
	def pull(self):
		self.new_card = self.cards[0]
		del self.cards[0]
		return Card(value=self.new_card[0], suit=self.new_card[1])
	
	#AddQuote(f"{now.day}/{now.month}/{now.year} - {correct_time(now.hour)}:{ifill(now.minute)}", f"'{message}'", f"{person}", ctx.author)	
def AddQuote(datetime:str, message:str, author:str, creator):
	data = []
	with open('quotes.json') as f:
		data = json.load(f)
	_tmp = {}
	_tmp['author'] = author
	_tmp['message'] = message
	_tmp['date'] = datetime
	_tmp['creator'] = creator.id
	_tmp['guild'] = creator.guild.id
	data[str(len(data))] = _tmp
	
	with open('quotes.json', "w") as s:
		json.dump(data, s, indent=4, sort_keys=True)

def DelQuote(index:int):
	data = {}
	_tmp = {}
	with open('quotes.json') as f:
		data = json.load(f)
		_tmp = data[str(index)]
		del data[str(index)]
	
	with open('quotes.json', "w") as s:
		json.dump(data, s, indent=4, sort_keys=True)
	
	return _tmp
	
def GetQuotes():
	with open('quotes.json') as f:
		data = json.load(f)
		return data

def create_gif(text):
	dir_path = scriptdir
	img = PIL.Image.new('RGB', (800, 800), (255, 255, 255))
	d = PIL.ImageDraw.Draw(img)

	os.mkdir(dir_path+'/tempdir')

	ctr = 1
	images = []

	for i in range(len(text)):
	    d.text((10, 10), text[:ctr], fill=(0, 0, 0))
	    img.save(dir_path+"/tempdir/"+str(ctr)+".png", "png")
	    images.append(PIL.Image.open(dir_path+"/tempdir/"+str(ctr)+".png"))
	    ctr = ctr + 1

	writeGif(dir_path+'/animation.gif', images, duration=0.1)

	os.system('rm -R ' + dir_path+'/tempdir')
	return dir_path+'/animation.gif'	
				
#NORMAL CODE			
class fun:
	"""Silly commands that dont fit in anywhere else."""
	global client
	def __init__(self, bot):
		self.bot = bot
		client = bot
		self.decks = {}
		
	def get_deck(self, ctx):
		try:
			return self.decks[ctx.author]
		except:
			return None
		
	@commands.group(invoke_without_command=True)
	async def cards(self, ctx):
		"""I have a big deck.
		Shuffles and simulates a virtual deck of cards."""
		if self.get_deck(ctx):
			Deck(self, ctx.author)
			await ctx.send("Deck recreated.")
			self.get_deck(ctx).shuffle()
		else:
			Deck(self, ctx.author)
			await ctx.send("Deck created.")
			self.get_deck(ctx).shuffle()
	
	@cards.command()
	async def draw(self, ctx):
		if self.get_deck(ctx):
			nc = self.get_deck(ctx).pull()
			await ctx.send(f"You drew {nc.value} of {nc.suit}")
		else:
			await ctx.send("No cards for you.")
	
	@commands.command(hidden=True, enabled=False)
	async def animate(self, ctx, *, text):
		gpath = create_gif(text)
		with open(gpath, "rb") as f:
			try:
				await ctx.send(file=discord.File(f))
			except FileNotFoundError:
				await ctx.send(f"no such file: {file}")
		
	@commands.group(invoke_without_command=True)
	async def quote(self, ctx):
		"""People say things, apparently.
		Manages the bots quotes system."""
		await ctx.channel.trigger_typing()
		q = GetQuotes()
		await ctx.send(f"There are {len(q)} quotes saved.")
		help_cmd = self.bot.get_command('help')
		await ctx.invoke(help_cmd, 'quote')

	@quote.command(name="mysearch")
	async def _my_search(self, ctx, find_string:str):
		if len(find_string) < 3:
			await ctx.send("Search string must be more than 3 characters.")
			return
			
		found = False
		em = discord.Embed()	
		q = GetQuotes()
		for item in q:
			if find_string.lower() in q[item]['author'].lower() or find_string.lower() in q[item]['message'].lower():
				if q[item]['creator'] == ctx.author.id:
					found = True
					creator = get_member(client, q[item]['creator'])
					cguild = q[item]['guild']
					for g in client.guilds:
						if g.id == q[item]['guild']:
							cguild = g
							
					em.add_field(name=f"{item}/{len(q) - 1}", value=f"{q[item]['message']} ~{q[item]['author']} on {q[item]['date']}\nCreated by {creator.mention} in {cguild.name}.")
				
		if found:
			await ctx.send(embed=em)
		else:
			await ctx.send("No quotes found.")	

	@quote.command(name="guildsearch")
	async def _guild_search(self, ctx, find_string:str):
		if len(find_string) < 3:
			await ctx.send("Search string must be more than 3 characters.")
			return
			
		found = False
		em = discord.Embed()	
		q = GetQuotes()
		for item in q:
			if find_string.lower() in q[item]['author'].lower() or find_string.lower() in q[item]['message'].lower():
				if q[item]['guild'] == ctx.guild.id:
					found = True
					creator = get_member(client, q[item]['creator'])
					cguild = q[item]['guild']
					for g in client.guilds:
						if g.id == q[item]['guild']:
							cguild = g
							
					em.add_field(name=f"{item}/{len(q) - 1}", value=f"{q[item]['message']} ~{q[item]['author']} on {q[item]['date']}\nCreated by {creator.mention} in {cguild.name}.")
				
		if found:
			await ctx.send(embed=em)
		else:
			await ctx.send("No quotes found.")	

	@quote.command(name="search")
	async def _search(self, ctx, find_string:str):
		if len(find_string) < 3:
			await ctx.send("Search string must be more than 3 characters.")
			return
			
		found = False
		em = discord.Embed()	
		q = GetQuotes()
		for item in q:
			if find_string.lower() in q[item]['author'].lower() or find_string.lower() in q[item]['message'].lower():
				found = True
				creator = get_member(client, q[item]['creator'])
				cguild = q[item]['guild']
				for g in client.guilds:
					if g.id == q[item]['guild']:
						cguild = g
						
				em.add_field(name=f"{item}/{len(q) - 1}", value=f"{q[item]['message']} ~{q[item]['author']} on {q[item]['date']}\nCreated by {creator.mention} in {cguild.name}.")
			
		if found:
			await ctx.send(embed=em)
		else:
			await ctx.send("No quotes found.")	
							
	@is_tier(2)	
	@quote.command(name="add")
	async def _add(self, ctx, person, *, message):
		await ctx.channel.trigger_typing()
		now = datetime.datetime.now()
		q = len(GetQuotes())
		AddQuote(f"{now.day}/{now.month}/{now.year} - {correct_time(now.hour)}:{ifill(now.minute)}", f"'{message}'", f"{person}", ctx.author)
		await ctx.send(f"[{q}] Quote added.\n[{now.day}/{now.month}/{now.year} - {correct_time(now.hour)}:{ifill(now.minute)}] '{message}' ~{person}")
	
	@quote.command(name="read")
	async def _read(self, ctx, index):
		await ctx.channel.trigger_typing()
		if not index.isdigit():
			await ctx.send("The index should be a number, doofus.")
			return
		index = int(index)
		try:
			q = GetQuotes()
			data = q[str(index)]
			em = discord.Embed()
			creator = get_member(client, data['creator'])
			cguild = data['guild']
			for item in client.guilds:
				if item.id == data['guild']:
					cguild = item
					
			em.add_field(name=f"{index}/{len(q) - 1}", value=f"{data['message']} ~{data['author']} on {data['date']}\nCreated by {creator.mention} in {cguild.name}.")
			await ctx.send(embed=em)	
		except KeyError:
			await ctx.send("No quote on that index.")
			
	@quote.command(name="rand", aliases=['random'])
	async def _rand(self, ctx, quote_filter=""):
		await ctx.channel.trigger_typing()
		if quote_filter == "":
			q = GetQuotes()
			index = random.randint(0, len(q) - 1)
			data = q[str(index)]
			em = discord.Embed()
			for item in client.guilds:
				if item.id == data['guild']:
					cguild = item
			em.add_field(name=f"{index}/{len(q) - 1}", value=f"{data['message']} ~{data['author']} on {data['date']}\nCreated by {get_member(client, data['creator']).mention} in {cguild.name}.")
			await ctx.send(embed=em)	
		else:
			if quote_filter == "guild":
				q = GetQuotes()
				data = {}
				for item in q:
					if q[item]['guild'] == ctx.guild.id:
						_tmp = {}
						_tmp['author'] = q[item]['author']
						_tmp['message'] = q[item]['message']
						_tmp['date'] = q[item]['date']
						_tmp['creator'] = q[item]['creator']
						_tmp['guild'] = q[item]['guild']
						data[str(len(data))] = _tmp
	
				if len(data) < 1:
					await ctx.send("No quotes for this filter.")
					
				index = random.randint(0, len(data) - 1)
				
				ran = data[str(index)]
				print(ran)
				em = discord.Embed()
				for item in client.guilds:
					if item.id == ran['guild']:
						cguild = item
				em.add_field(name=f"{index}/{len(data) - 1}", value=f"{ran['message']} ~{ran['author']} on {ran['date']}\nCreated by {get_member(client, ran['creator']).mention} in {cguild.name}.")
				await ctx.send(embed=em)	
			
			if quote_filter == "mine":
				q = GetQuotes()
				data = {}
				for item in q:
					if q[item]['creator'] == ctx.author.id:
						_tmp = {}
						_tmp['author'] = q[item]['author']
						_tmp['message'] = q[item]['message']
						_tmp['date'] = q[item]['date']
						_tmp['creator'] = q[item]['creator']
						_tmp['guild'] = q[item]['guild']
						data[str(len(data))] = _tmp
	
				if len(data) < 1:
					await ctx.send("No quotes for this filter.")
					
				index = random.randint(0, len(data) - 1)
				
				ran = data[str(index)]
				print(ran)
				em = discord.Embed()
				for item in client.guilds:
					if item.id == ran['guild']:
						cguild = item
				em.add_field(name=f"{index}/{len(data) - 1}", value=f"{ran['message']} ~{ran['author']} on {ran['date']}\nCreated by {get_member(client, ran['creator']).mention} in {cguild.name}.")
				await ctx.send(embed=em)	
				
	@commands.is_owner()
	@quote.command(name="del", aliases=['delete'])
	async def _del(self, ctx, index:int):
		await ctx.channel.trigger_typing()
		try:
			deleted = DelQuote(index)	
			await ctx.send(f"Deleting quote...\n`{deleted['message']}`")
		except Exception as e:
			await ctx.send(e)	

	@commands.command(hidden=True)
	async def penis(self, ctx, *, others=""):
		if others == "":
			await ctx.send(f"{ctx.author.mention} penis length is:\n8{'='*random.randint(0,70)}D")	
		else:
			others = others.split(" ")
			for item in others:
				await ctx.send(f"{item} penis length is:\n8{'='*random.randint(0,70)}D")
					
	#['beavis', 'cheese', 'daemon', 'cow', 'dragon', 'ghostbusters', 'kitty', 'meow', 'milk', 'stegosaurus', 'stimpy', 'turkey', 'turtle', 'tux']
	@commands.group(invoke_without_command=True)
	async def cowsay(self, ctx, *, text):
		"""Moo."""
		await ctx.channel.trigger_typing()
		f = io.StringIO()
		with redirect_stdout(f):
			cowsay.cow(text)
		out = f.getvalue()
		await ctx.send(f"```{out}```")
		
	@cowsay.command(name="list")
	async def _cow_list(self, ctx):
		await ctx.channel.trigger_typing()
		f = io.StringIO()
		with redirect_stdout(f):
			HumanizeList(cowsay.char_names)
		out = f.getvalue()
		await ctx.send(f"```{out}```")

	@cowsay.command(name="beavis")
	async def _cow_beavis(self, ctx, *, text):
		await ctx.channel.trigger_typing()
		f = io.StringIO()
		with redirect_stdout(f):
			cowsay.beavis(text)
		out = f.getvalue()
		await ctx.send(f"```{out}```")
		
	@cowsay.command(name="cheese")
	async def _cow_cheese(self, ctx, *, text):
		await ctx.channel.trigger_typing()
		f = io.StringIO()
		with redirect_stdout(f):
			cowsay.cheese(text)
		out = f.getvalue()
		await ctx.send(f"```{out}```")
	
	@cowsay.command(name="daemon")
	async def _cow_daemon(self, ctx, *, text):
		await ctx.channel.trigger_typing()
		f = io.StringIO()
		with redirect_stdout(f):
			cowsay.daemon(text)
		out = f.getvalue()
		await ctx.send(f"```{out}```")
	
	@cowsay.command(name="dragon")
	async def _cow_dragon(self, ctx, *, text):
		await ctx.channel.trigger_typing()
		f = io.StringIO()
		with redirect_stdout(f):
			cowsay.dragon(text)
		out = f.getvalue()
		await ctx.send(f"```{out}```")
	
	@cowsay.command(name="ghostbusters")
	async def _cow_ghostbusters(self, ctx, *, text):
		await ctx.channel.trigger_typing()
		f = io.StringIO()
		with redirect_stdout(f):
			cowsay.ghostbusters(text)
		out = f.getvalue()
		await ctx.send(f"```{out}```")
	
	@cowsay.command(name="kitty")
	async def _cow_kitty(self, ctx, *, text):
		await ctx.channel.trigger_typing()
		f = io.StringIO()
		with redirect_stdout(f):
			cowsay.kitty(text)
		out = f.getvalue()
		await ctx.send(f"```{out}```")
	
	@cowsay.command(name="meow")
	async def _cow_meow(self, ctx, *, text):
		await ctx.channel.trigger_typing()
		f = io.StringIO()
		with redirect_stdout(f):
			cowsay.meow(text)
		out = f.getvalue()
		await ctx.send(f"```{out}```")
	
	@cowsay.command(name="milk")
	async def _cow_milk(self, ctx, *, text):
		await ctx.channel.trigger_typing()
		f = io.StringIO()
		with redirect_stdout(f):
			cowsay.milk(text)
		out = f.getvalue()
		await ctx.send(f"```{out}```")
	
	@cowsay.command(name="stegosaurus")
	async def _cow_stegosaurus(self, ctx, *, text):
		await ctx.channel.trigger_typing()
		f = io.StringIO()
		with redirect_stdout(f):
			cowsay.stegosaurus(text)
		out = f.getvalue()
		await ctx.send(f"```{out}```")
	
	@cowsay.command(name="stimpy")
	async def _cow_stimpy(self, ctx, *, text):
		await ctx.channel.trigger_typing()
		f = io.StringIO()
		with redirect_stdout(f):
			cowsay.stimpy(text)
		out = f.getvalue()
		await ctx.send(f"```{out}```")
	
	@cowsay.command(name="turkey")
	async def _cow_turkey(self, ctx, *, text):
		await ctx.channel.trigger_typing()
		f = io.StringIO()
		with redirect_stdout(f):
			cowsay.turkey(text)
		out = f.getvalue()
		await ctx.send(f"```{out}```")
	
	@cowsay.command(name="turtle")
	async def _cow_turtle(self, ctx, *, text):
		await ctx.channel.trigger_typing()
		f = io.StringIO()
		with redirect_stdout(f):
			cowsay.turtle(text)
		out = f.getvalue()
		await ctx.send(f"```{out}```")
	
	@cowsay.command(name="tux")
	async def _cow_tux(self, ctx, *, text):
		await ctx.channel.trigger_typing()
		f = io.StringIO()
		with redirect_stdout(f):
			cowsay.tux(text)
		out = f.getvalue()
		await ctx.send(f"```{out}```")
							
	@commands.command()
	async def badmeme(self, ctx):
		"""Gets bad meme."""
		await ctx.channel.trigger_typing()
		async with aiohttp.ClientSession() as session:
			async with session.get("https://api.imgflip.com/get_memes") as r:
				resp = await r.text()
				res = json.loads(resp)
				url = random.choice(res['data']['memes'])
				url = url['url']
				await ctx.send(url)
		
	@commands.command()
	async def mock(self, ctx, *, text):
		"""FuuCKK YooUUUU. etc."""
		await ctx.channel.trigger_typing()
		await ctx.send(mockf(text))	
					
	#https://pypi.org/project/art/
	@commands.group(name="art", invoke_without_command=True)
	async def ascii_art(self, ctx, input_text="rand", *, args:str=""):
		"""Searches the ascii art list for <input_text>.
		Some arts take extra arguments.
		
		Command Examples:
		!art coffee 3
		!art love_you BORKS
		!art random
		"""
		await ctx.channel.trigger_typing()
		count = 1
		text = ""
		if args.isdigit():
			count = int(args)
		else:
			text = args
			
		try:
			a = art(input_text, number=count, text=text)
			await ctx.send(f"```{a}```")
			PersonaCommand()
		except:
			await ctx.send("Invalid art name.")

	@commands.group(invoke_without_command=True)
	async def textart(self, ctx, input_text, *, args:str=""):
		"""Converts <input_text> in to ascii art.
		Some arts take extra arguments, example;
		-f <font> - applies a custom font
		-f rand - makes the font random
		
		Command Examples:
		!textart coffee
		!textart lol -f block
		!textart bork -f rand
		"""
		await ctx.channel.trigger_typing()
		chr_ignore = True
		font = DEFAULT_FONT
		random = ""
		if advArg("-ci", args):
			chr_ignore = advArg("-ci", args)
		if advArg("-f", args):
			font = advArg("-f", args)
		if font == "rand":
			random = "random"
		a = text2art(input_text, chr_ignore=chr_ignore, font=font)
		await ctx.send(f"```{a}```")
		PersonaCommand()
	
	@textart.command(name="list")
	async def _text_list(self, ctx):
		f = io.StringIO()
		with redirect_stdout(f):
			font_list()
		out = f.getvalue()
		with open('output.txt', "r+") as f:
			f.writelines(out)
		await ctx.channel.send(file=discord.File(scriptdir+"output.txt"))
			
	@ascii_art.command(name="list")
	async def _art_list(self, ctx):
		f = io.StringIO()
		with redirect_stdout(f):
			art_list()
		out = f.getvalue()
		with open('output.txt', "r+") as f:
			f.writelines(out)
		await ctx.channel.send(file=discord.File(scriptdir+"output.txt"))
						
	def filter_clist(self, ls):
		for item in ls:
			if item.lower() == "or" or item.lower() == "and" or item == "" or item == " ":
				ls.remove(item)
		return list(set(ls))
				
	@commands.command(name="choice", aliases=['choices', 'choose', 'decide'])
	async def _choice(self, ctx, *, choices):
		"""Randomly chooses between various options.
		Make your options seperated by spaces.
		
		Example: choice one two three"""
		await ctx.channel.trigger_typing()
		_choices = choices.split(" ")
		_choices = self.filter_clist(_choices)
		if len(_choices) < 2:
			await ctx.send(f"A choice between **{choices}** and **{choices}**? Oh let me see, I choose... you're an idiot.")
			return
		bc = []
		for item in _choices:
			bc.append(f"**{item}**")
		decision = random.choice(_choices)
		await ctx.send(f"From {HumanizeList(bc)}, I choose **{decision}**")
		PersonaCommand()

	@commands.command()
	async def kym(self, ctx, *, meme):
		"""Get's meme definitions."""
		await ctx.channel.trigger_typing()
		try:
			await ctx.send(memedict.search(meme))
		except:
			await ctx.send("I AM ERROR")
					
	@commands.command(aliases=['face'])
	async def lenny(self, ctx, old_index=-1):
		"""Displays a random lenny face."""
		await ctx.channel.trigger_typing()
		if old_index != -1:
			lenny_oldlist = [
				"( ͡° ͜ʖ ͡°)", "( ͠° ͟ʖ ͡°)", "ᕦ( ͡° ͜ʖ ͡°)ᕤ", "( ͡~ ͜ʖ ͡°)",
				"( ͡o ͜ʖ ͡o)", "͡(° ͜ʖ ͡ -)", "( ͡͡ ° ͜ ʖ ͡ °)﻿", "(ง ͠° ͟ل͜ ͡°)ง",
				"ヽ༼ຈل͜ຈ༽ﾉ", "¯\_( ͡° ͜ʖ ͡°)_/¯", "(╯°□°）╯︵ ┻━┻", "┬─┬ノ(ಠ_ಠノ)"]
			await ctx.send(lenny_oldlist[old_index])
			return
			
		await ctx.send(lenny.lenny())
		PersonaCommand()
					
	@commands.command()
	async def scramble(self, ctx, *, msg):
		"""Scrambles your input in to a random output."""
		await ctx.channel.trigger_typing()
		if " " in msg:
			msg = msg.split(" ")
			random.shuffle(msg)
			msg = ' '.join(msg)
			await ctx.send(msg)
			PersonaCommand()
		else:
			msg = list(msg)
			random.shuffle(msg)
			msg = ''.join(msg)
			await ctx.send(msg)
			PersonaCommand()
		
	@commands.command()
	async def lmgtfy(self, ctx, *query):
		"""Let me google that for you."""
		await ctx.channel.trigger_typing()
		await ctx.send("Sure, let me just get that for you....")
		await ctx.channel.trigger_typing()
		r_url = '+'.join(query)
		times = datetime.datetime.utcnow() - ctx.message.created_at
		await ctx.send(f"Alright, here you go, this took alot of work to find, it took a whole {times.microseconds} microseconds! I could have been doing more important things... like printing out lists of servers somewhere, you'd better appreciate it.\n<http://lmgtfy.com/?iie=1&q={r_url}>")
		PersonaCommand()
		
	@commands.command(hidden=True)
	async def bubble(self, ctx, *, message):
		apil(image_file = "bubble.png", font_file = "piru.ttf", text1 = message, text2 = ctx.message.author.display_name, x = 50, y = 90, x2 = 0, y2 = 0)
		#apil(*, image_file = "b.jpg", font_file = "piru.ttf", text1 = "", text2 = "", x = 50, y = 90, x2 = 0, y2 = 0):
		await ctx.channel.send(file=discord.File("out.jpg"))
		PersonaCommand()
		
		#Replace this command with the DX insult system
		

	@commands.command()
	async def wtf(self, ctx, *, person="", person2=""):
		"""Displays random fake messages."""
		data = {
			'actions':["slapped", "smothered", "fucked", "tickled", "smashed", "bit", "twatted", "rekt", "engulfed", "morphed", "terminated", "infused", "expanded", "punched", "shot", "spit on", "spilled", "slashed", "wasted", "prodded", "shanked", "ownt", "shook"],
			'objects':["a motherfucking truck", "a big fucking fish", "a dong", "a fucking huge dong", "a darko machine", "a turd", "some kind of giant bitch", "a booby trap", "a mouldy vagina", "a fat woman", "a pussy cat", "a pigeon", "toilet paper", "a knuckle duster", "a bowl of noodles", "a bowl of pasta", "an oriental daily news!", "a maggie chow", "a chicken leg", "an octopus dick", "a giraffe bum", "a pig", "a rasher of bacon", "some soy food", "a vial of zyme", "your mother", "nicolette duclare", "sticks and stones", "a sweaty nob", "a sweaty foot", "a dogs widgey", "an overcooked egg", "a tin of beans", "a long sausage", "a hot and spicy indian curry", "some spaghetti carbonara", "a bottle of milk", "a dildo", "a vibrator", "the illuminati"]
		}
		mode = random.random()
		if mode <= 0.5: #user is a {object}
			if person == "":
				target = random.choice(ctx.guild.members)
				person = target.display_name
			fmsg = f"{person} is {random.choice(data['objects'])}"
			await ctx.send(fmsg)
			
		else: # {user1} {actions} {user2} with a/n {object}
			if person == "":
				target = random.choice(ctx.guild.members)
				person = target.display_name
			if person2 == "":
				target = random.choice(ctx.guild.members)
				person2 = target.display_name
			if person == person2:
				person2 = "themself"
			
			fmsg = f"{person} {random.choice(data['actions'])} {person2} with {random.choice(data['objects'])}"
			await ctx.send(fmsg)
					
	@commands.command()
	async def bork(self, ctx):
		"""Bork."""
		await ctx.send("https://www.youtube.com/watch?v=E2m8_-gIOqE")
		PersonaCommand()
		
	@commands.command()
	async def insult(self, ctx, other=""):
		"""Your mother was a dairy farmer.
		Generates a random insult. If <other> is supplied as another members name, insults them instead."""
		await ctx.channel.trigger_typing()
		async with aiohttp.ClientSession() as session:
			async with session.get("https://insult.mattbas.org/api/insult") as c:
				r = await c.text()

				f = False
				if other != "":
					if other == "*":
						m =  secrets.choice(list(client.get_all_members()))
						r = r.replace("You are", m.display_name+" is")
						f = True
					else:
						for member in client.get_all_members():
							if other.lower() in member.name.lower() or other.lower() in member.display_name.lower():
								r = r.replace("You are", member.display_name+" is")
								f = True
						
						if not f:
							r = r.replace("You are", other.capitalize()+" is")
				await ctx.send(r)
				PersonaCommand()
		
	@commands.command()
	async def geek(self, ctx):
		"""Download my data, baby.
		Generates a random geeky message."""
		await ctx.channel.trigger_typing()
		async with aiohttp.ClientSession() as session:
			async with session.get("https://geek-jokes.sameerkumar.website/api") as c:
				r = await c.text()
				await ctx.send(r)	
				PersonaCommand()

	@commands.command()
	async def compliment(self, ctx, other=""):
		"""You have lovely letters in your name.
		Generates a random compliment. If <other> is supplied as another members name, insults them instead."""
		await ctx.channel.trigger_typing()
		async with aiohttp.ClientSession() as session:
			async with session.get("https://compliment-api.herokuapp.com/") as c:
				r = await c.text()
				f = False
				if other != "":
					if other == "*":
						m =  secrets.choice(list(client.get_all_members()))
						r = r.replace("Your", m.display_name)
						r = r.replace("you", m.display_name)
						r = r.replace("You", m.display_name)
						r = r.replace("You are", m.display_name+" is")
						f = True
					else:
						for member in client.get_all_members():
							if other.lower() in member.name.lower() or other.lower() in member.display_name.lower():
								r = r.replace("Your", member.display_name)
								r = r.replace("you", member.display_name)
								r = r.replace("You", member.display_name)
								r = r.replace("You are", member.display_name+" is")
								f = True
						
						if not f:
							r = r.replace("Your", other.capitalize())
							r = r.replace("you", other.capitalize())
							r = r.replace("You", other.capitalize())
							
							r = r.replace("You are", other.capitalize()+" is")
				await ctx.send(r)	
				PersonaCommand()
				
	@commands.command()
	async def corporatebs(self, ctx):
		"""Seamlessly Harness B2b Outsourcing.
		Generates a random corporate BS message."""
		await ctx.channel.trigger_typing()
		async with aiohttp.ClientSession() as session:
			async with session.get("https://corporatebs-generator.sameerkumar.website/") as c:
				r = await c.text()
				j = json.loads(r)
				await ctx.send(j['phrase'])	
				PersonaCommand()

def mockf(text, diversity_bias=0.5, random_seed=None):
	# Error handling
	if diversity_bias < 0 or diversity_bias > 1:
		raise ValueError('diversity_bias must be between the inclusive range [0,1]')
	# Seed the random number generator
	random.seed(random_seed)
	# Mock the text
	out = ''
	last_was_upper = True
	swap_chance = 0.5
	for c in text:
		if c.isalpha():
			if random.random() < swap_chance:
				last_was_upper = not last_was_upper
				swap_chance = 0.5
			c = c.upper() if last_was_upper else c.lower()
			swap_chance += (1-swap_chance)*diversity_bias
		out += c
	return out
		
def apil(*, image_file = "b.jpg", font_file = "piru.ttf", text1 = "", text2 = "", x = 50, y = 90, x2 = 0, y2 = 0):
	image = Image.open(image_file)
	draw = ImageDraw.Draw(image)
	lis = text1.split(" ")
	font = ImageFont.truetype(font_file, size=round(45 / len(lis)))

	if text1 != "":
		message = text1
		color = 'rgb(0, 0, 0)' # black color
		draw.text((x, y), message, fill=color, font=font)

	if text2 != "":
		name = text2
		color = 'rgb(0, 0, 0)' # white color
		draw.text((x2, y2), name, fill=color, font=ImageFont.truetype(font_file, size=10))

	image.save('out.jpg')
		
def setup(bot):
	global client
	bot.add_cog(fun(bot))
	client = bot
