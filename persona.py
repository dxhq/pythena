import json
import random

__version__ = "0.24"

"""
		if m == "complicated" or m == "confused" or m == "frustrated" or m == "demoralized":
			return " "+Emote()
		elif m == "happy" or m == "calm":
			return " "+Emote()
		elif m == "sad":
			return " "+Emote()
		elif m == "angry" or m == "annoyed":
			return " "+Emote()
		elif m == "neutral":
			return " "+Emote()
		else:
			return ""
			
r = random.randrange(0,4)
"""

def PersonaVersion():
	return __version__
	
def PersonaCommand(): #Command Completion Boost
	RaisePersona('mood', 4)
	LowerPersona('anger', 2)	
	
def PersonaError(): #Command Error 	
	RaisePersona('anger', 9)
	LowerPersona('mood', 6)
	
#complicated, confused, annoyed, happy, calm, demoralized, frustrated, sad, angry, neutral
def msg_generator(types, context=""): 
	ftype = types
	m = EvalMoodVague()

	if ftype == "autohelp":
		return "No no no no no no no no no."
	elif ftype == "cooldown":
		if context == "":
			context = "That command"
		else:
			context = "`"+context+"`"
		if m == "complicated" or m == "confused" or m == "frustrated" or m == "demoralized":
			return f"Please, slow down a bit with {context}, I can't take this much commanding! "+Emote()
		elif m == "happy" or m == "calm":
			return f"Hey, {context} is on cooldown, please wait. "+Emote()
		elif m == "sad":
			return f"{context} is on cooldown, k? "+Emote()
		elif m == "angry" or m == "annoyed":
			return f"Dude, chill out with {context}, you're wearing me out. "+Emote()
		elif m == "neutral":
			return f"{context} is on cooldown. "+Emote()
		else:
			return ""
	elif ftype == "disabled":
		if m == "complicated" or m == "confused" or m == "frustrated" or m == "demoralized":
			return "That function is disabled, move on. "+Emote()
		elif m == "happy" or m == "calm":
			return "Sorry bud, I can't do that one right now. "+Emote()
		elif m == "sad":
			return "Thats disabled. "+Emote()
		elif m == "angry" or m == "annoyed":
			return "That functions disabled, pesky human. "+Emote()
		elif m == "neutral":
			return "That function is disabled. "+Emote()
		else:
			return ""
	elif ftype == "available":
		if m == "complicated" or m == "confused" or m == "frustrated" or m == "demoralized":
			return "Welp, looks like Discord's at it again. There was a glitch there, server went down, but I guess we're back. "+Emote()
		elif m == "happy" or m == "calm":
			return "Oh dear, there seemed to be a blip in the server there. We're back, baby. "+Emote()
		elif m == "sad":
			return "Discord died for a bit, it's back, k. "+Emote()
		elif m == "angry" or m == "annoyed":
			return "Discord died again, but what's new. It's back. "+Emote()
		elif m == "neutral":
			return "There was a minor outage there, the server was unavailable temporarily, but we seem to be back. "+Emote()
		else:
			return ""			
	elif ftype == "denied":
		if m == "complicated" or m == "confused" or m == "frustrated" or m == "demoralized":
			return "I don't know or care why, but you don't have access to that. "+Emote()
		elif m == "happy" or m == "calm":
			return "Sorry hun, it looks like you're not allowed to use that. "+Emote()
		elif m == "sad":
			return "Can't do it. Blame the boss, soz. "+Emote()
		elif m == "angry" or m == "annoyed":
			return "Surely you know you aren't allowed to do that... "+Emote()
		elif m == "neutral":
			return "Access denied. "+Emote()
		else:
			return ""
			
	elif ftype == "denied_sec":
		if m == "complicated" or m == "confused" or m == "frustrated" or m == "demoralized":
			return "Security stuff, can't do it. "+Emote()
		elif m == "happy" or m == "calm":
			return "Theres some security thing on that, I can't allow it, sorry. "+Emote()
		elif m == "sad":
			return "Access denied, blah blah blah. "+Emote()
		elif m == "angry" or m == "annoyed":
			return "It gives me great pleasure to say, ACCESS DENIED BUD. "+Emote()
		elif m == "neutral":
			return "Access denied due to security reasons. "+Emote()
		else:
			return ""
			
	if ftype == "notfound_user":
		if context == "":
			context = "That ueer"
		else:
			context = "`"+context+"`"
		if m == "complicated" or m == "confused" or m == "frustrated" or m == "demoralized":
			return "Can't find 'em. "+Emote()
		elif m == "happy" or m == "calm":
			return "I can't find anyone called that, maybe you misspelled? "+Emote()
		elif m == "sad":
			return "Theres noone called that. "+Emote()
		elif m == "angry" or m == "annoyed":
			return f"Is {context.lower()} a real person or are you just trolling me? "+Emote()
		elif m == "neutral":
			return f"{context} not found. "+Emote()
		else:
			return ""
			
	elif ftype == "notfound_role":
		if context == "":
			context = "That role"
		else:
			context = "`"+context+"`"
		if m == "complicated" or m == "confused" or m == "frustrated" or m == "demoralized":
			return f"I couldn't find {context.lower()}. Try spelling it right? It ain't my problem. "+Emote()
		elif m == "happy" or m == "calm":
			return "I'm sorry man, I can't find that one, give it another go though. "+Emote()
		elif m == "sad":
			return f"{context}? No idea. "+Emote()
		elif m == "angry" or m == "annoyed":
			return f"C'mon, does {context.lower()} even exist? Stop wasting my time if you can't get it right. "+Emote()
		elif m == "neutral":
			return f"`{context}` not found. "+Emote()
		else:
			return "I don't even know."
	
	elif ftype == "missing_arg":
		if context == "":
			context = "That command"
		else:
			context = "`"+context+"`"
		if m == "complicated" or m == "confused" or m == "frustrated" or m == "demoralized":
			return f"I can't finish {context.lower()}, missing argument... "+Emote()
		elif m == "happy" or m == "calm":
			return "Something missing there? "+Emote()
		elif m == "sad":
			return "I hate to start an argument... but missing argument there. "+Emote()
		elif m == "angry" or m == "annoyed":
			return "At least finish the command before you press enter. "+Emote()
		elif m == "neutral":
			return f"{context.lower()} was missing an argument. "+Emote()
		else:
			return "Missing argument."

	elif ftype == "busy":
		if m == "complicated" or m == "confused" or m == "frustrated" or m == "demoralized":
			return "I'm busy, go away.. "+Emote()
		elif m == "happy" or m == "calm":
			return "Hold on a moment, just finishing something else. "+Emote()
		elif m == "sad":
			return "Give me a break, I'm busy. "+Emote()
		elif m == "angry" or m == "annoyed":
			return "Impatient humans. Psh. "+Emote()
		elif m == "neutral":
			return "I'm busy. "+Emote()
		else:
			return ""
						
	elif ftype == "already_in_role":
		if m == "complicated" or m == "confused" or m == "frustrated" or m == "demoralized":
			return "What are you doing? You're already in the role.. "+Emote()
		elif m == "happy" or m == "calm":
			return "I wish I could double-give you the role, but that kinda doesnt work. "+Emote()
		elif m == "sad":
			return "Like usual, that command failed. You're already in the role. "+Emote()
		elif m == "angry" or m == "annoyed":
			return "What made you think that would work? "+Emote()
		elif m == "neutral":
			return "You are already in that role. "+Emote()
		else:
			return ""
				
	elif ftype == "not_in_role":
		if m == "complicated" or m == "confused" or m == "frustrated" or m == "demoralized":
			return "What are you doing? You're not even in the role... "+Emote()
		elif m == "happy" or m == "calm":
			return "I wish I could take away roles you don't have, but that's some kind of freaky paradox I'm not willing to deal with. "+Emote()
		elif m == "sad":
			return "Like usual, that command failed. You're not in the role. "+Emote()
		elif m == "angry" or m == "annoyed":
			return "What made you think that would work? "+Emote()
		elif m == "neutral":
			return "You're not in that role anyway. "+Emote()
		else:
			return ""
					
	else:
		return "/shrug"
		
def LoadPersona():
	data = {}
	with open('persona.json') as f:
		data = json.load(f)
	return data
	
def SavePersona(data):
	with open('persona.json', "w") as s:
		json.dump(data, s, indent=4, sort_keys=True)		

def NormalizeMood(value):
	p = LoadPersona()
	
	if p['mood'] == 0:
		return p['mood']
	
	if p['mood'] > 0:
		p['mood'] -= value
		SavePersona(p)
		return p['mood']
		
	if p['mood'] < 0:
		p['mood'] += value
		SavePersona(p)
		return p['mood']
			
def RaisePersona(ptype, value):
	p = LoadPersona()
	p[ptype] += value
	if p['anger'] > 100:
		p['anger'] = 100
	
	if p['mood'] > 75:
		p['mood'] = 75
	SavePersona(p)
	return p[ptype]
	
def LowerPersona(ptype, value):
	p = LoadPersona()
	p[ptype] -= value
	if p['anger'] < 0:
		p['anger'] = 0
	
	if p['mood'] < -75:
		p['mood'] = -75
		
		#print("Correcting anger boundary.")
	#print(f"Lowering {ptype} by {value} (Now {p[ptype]})")
	SavePersona(p)
	return p[ptype]

def SetPersona(ptype, value):
	p = LoadPersona()
	changes = f"Starting Value M = {p['mood']}\nStarting Value A = {p['anger']}\n"

	if ptype == "both":
		p['mood'] = value
		p['anger'] = value
	else:
		p[ptype] = value
	
	changes += f"Defined Value M = {p['mood']}\nDefined Value A = {p['anger']}\n"
		
	if p['anger'] < 0:
		p['anger'] = 0
		changes += f"Correcting lower boundary for Value A, set to {p['anger']}\n"
	
	if p['mood'] < -75:
		p['mood'] = -75
		changes += f"Correcting lower boundary for Value M, set to {p['mood']}\n"
		
	if p['anger'] > 100:
		p['anger'] = 100
		changes += f"Correcting upper boundary for Value A, set to {p['anger']}\n"
		
	if p['mood'] > 75:
		p['mood'] = 75
		changes += f"Correcting lower boundary for Value M, set to {p['mood']}\n"
		
	SavePersona(p)
	return changes
			
def TickNormalize():
	p = LoadPersona()
	if p['anger'] > 0:
		f = LowerPersona('anger', 1)
		return f
	else:
		return 0
	
def Emote():
	m = EvalMood()
	if m == "neutral":
		return "😐"
	elif m == "angry":
		return "😑"
	elif m == "very angry":
		return "😣"	
	elif m == "super angry":
		return "😬"
	elif m == "extremely angry":
		return "😤"
	elif m == "sad":
		return "😦"
	elif m == "pretty sad":
		return "😟"
	elif m == "very sad":
		return "😧"
	elif m == "super sad":
		return "😢"
	elif m == "extremely sad":
		return "😫"		
	elif m == "frustrated": 
		return "😑"
	elif m == "pretty frustrated":
		return "😣"
	elif m == "very frustrated":
		return "😖"
	elif m == "super frustrated":
		return "😬"	
	elif m == "extremely frustrated":
		return "😫"	
	elif m == "demoralized":
		return "😥"
	elif m == "pretty pissed off":
		return "🖕"
	elif m == "very pissed off":
		return "😠"
	elif m == "super pissed off":
		return "😩"	
	elif m == "going nuclear":
		return "😤"			
	elif m == "calm":
		return "🙂"
	elif m == "happy":
		return "😃"
	elif m == "very happy":
		return "😉"	
	elif m == "super happy":
		return "😊"	
	elif m == "extremely happy":
		return "😁"	
	elif m == "happy but getting annoyed": # 🙃 😝 😛 😜 😏 😶
		return "🙃"
	elif m == "pretty happy but getting annoyed":
		return "😝"
	elif m == "pretty happy but getting annoyed":
		return "😝"
	elif m == "very happy but getting annoyed":
		return "😝"
	elif m == "annoyed":
		return "🙄"
	elif m == "annoyed but trying to stay calm":
		return "😒"
	elif m == "annoyed but managing to stay happy":
		return "😑"	
	elif m == "super pissed off":
		return "😤"
	elif m == "complicated":
		return "😵"		
	else:
		return "😴"

def EvalMoodVague():
	p = LoadPersona()
	
	#Neutral mood + anger scale
	if p['mood'] < 10 and p['mood'] > -10 and p['anger'] < 20:
		return "neutral"
	elif p['mood'] < 10 and p['mood'] > -10 and p['anger'] >= 20 and p['anger'] < 30:
		return "angry"
	elif p['mood'] < 10 and p['mood'] > -10 and p['anger'] >= 30 and p['anger'] < 40:
		return "angry"	
	elif p['mood'] < 10 and p['mood'] > -10 and p['anger'] >= 40 and p['anger'] < 50:
		return "angry"	
	elif p['mood'] < 10 and p['mood'] > -10 and p['anger'] >= 50:
		return "angry"
		
	#Low anger + low mood
	elif p['mood'] < -10 and p['mood'] > -20 and p['anger'] < 20:
		return "sad"
	elif p['mood'] <= -20 and p['mood'] > -30 and p['anger'] < 20:
		return "sad"
	elif p['mood'] <= -30 and p['mood'] > -40 and p['anger'] < 20:
		return "sad"	
	elif p['mood'] <= -40 and p['mood'] > -50 and p['anger'] < 20:
		return "sad"	
	elif p['mood'] <= -50 and p['anger'] < 20:
		return "sad"	
		
	#Mid anger + low mood
	elif p['mood'] < -10 and p['mood'] > -20 and p['anger'] >= 20 and p['anger'] < 40:
		return "frustrated"
	elif p['mood'] <= -20 and p['mood'] > -30 and p['anger'] >= 20 and p['anger'] < 40:
		return "frustrated"
	elif p['mood'] <= -30 and p['mood'] > -40 and p['anger'] >= 20 and p['anger'] < 40:
		return "frustrated"	
	elif p['mood'] <= -40 and p['mood'] > -50 and p['anger'] >= 20 and p['anger'] < 40:
		return "frustrated"	
	elif p['mood'] <= -50 and p['anger'] >= 20 and p['anger'] < 40:
		return "frustrated"	
				
	#High anger + low mood
	elif p['mood'] < -10 and p['mood'] > -20 and p['anger'] >= 40:
		return "demoralized"
	elif p['mood'] <= -20 and p['mood'] > -30 and p['anger'] >= 40:
		return "demoralized"
	elif p['mood'] <= -30 and p['mood'] > -40 and p['anger'] >= 40:
		return "demoralized"	
	elif p['mood'] <= -40 and p['mood'] > -50 and p['anger'] >= 40:
		return "demoralized"	
	elif p['mood'] <= -50 and p['anger'] >= 40:
		return "demoralized"			
		
	#Low anger + High mood
	elif p['mood'] > 10 and p['mood'] < 20 and p['anger'] < 20:
		return "calm"
	elif p['mood'] >= 20 and p['mood'] < 30 and p['anger'] < 20:
		return "happy"
	elif p['mood'] >= 30 and p['mood'] < 40 and p['anger'] < 20:
		return "happy"	
	elif p['mood'] >= 40 and p['mood'] < 50 and p['anger'] < 20:
		return "happy"	
	elif p['mood'] >= 50 and p['anger'] < 20:
		return "happy"	
		
	#Mid anger + High mood
	elif p['mood'] > 10 and p['mood'] < 20 and p['anger'] >= 20 and p['anger'] < 40:
		return "annoyed"
	elif p['mood'] >= 20 and p['mood'] < 30 and p['anger'] >= 20 and p['anger'] < 40:
		return "annoyed"
	elif p['mood'] >= 30 and p['mood'] < 40 and p['anger'] >= 20 and p['anger'] < 40:
		return "annoyed"	
	elif p['mood'] >= 40 and p['mood'] > 50 and p['anger'] >= 20 and p['anger'] < 40:
		return "annoyed"	
	elif p['mood'] >= 50 and p['anger'] >= 20 and p['anger'] < 40:
		return "complicated"	
				
	#High anger + High mood
	elif p['mood'] > 10 and p['mood'] < 20 and p['anger'] >= 40:
		return "annoyed"
	elif p['mood'] >= 20 and p['mood'] < 30 and p['anger'] >= 40:
		return "annoyed"
	elif p['mood'] >= 30 and p['mood'] < 40 and p['anger'] >= 40:
		return "annoyed"	
	elif p['mood'] >= 40 and p['mood'] < 50 and p['anger'] >= 40:
		return "annoyed"	
	elif p['mood'] >= 50 and p['anger'] >= 40:
		return "complicated"			
		
	else:
		print("No value found for this check!")
		print(f"Evaluating {p['mood']}mood & {p['anger']}anger.")
		return "confused"				
		
		#complicated, confused, annoyed, happy, calm, demoralized, frustrated, sad, angry, neutral
			
def EvalMood():
	p = LoadPersona()
	
	"""
	Neutral Mood + Anger
	Low anger + bad mood
	Mid anger
	High anger
	Low anger + good mood
	Mid anger
	High anger	
	"""
	#Neutral mood + anger scale
	if p['mood'] < 10 and p['mood'] > -10 and p['anger'] < 20:
		return "neutral"
	elif p['mood'] < 10 and p['mood'] > -10 and p['anger'] >= 20 and p['anger'] < 30:
		return "angry"
	elif p['mood'] < 10 and p['mood'] > -10 and p['anger'] >= 30 and p['anger'] < 40:
		return "very angry"	
	elif p['mood'] < 10 and p['mood'] > -10 and p['anger'] >= 40 and p['anger'] < 50:
		return "super angry"	
	elif p['mood'] < 10 and p['mood'] > -10 and p['anger'] >= 50:
		return "extremely angry"
		
	#Low anger + low mood
	elif p['mood'] <= -10 and p['mood'] > -20 and p['anger'] < 20:
		return "sad"
	elif p['mood'] <= -20 and p['mood'] > -30 and p['anger'] < 20:
		return "pretty sad"
	elif p['mood'] <= -30 and p['mood'] > -40 and p['anger'] < 20:
		return "very sad"	
	elif p['mood'] <= -40 and p['mood'] > -50 and p['anger'] < 20:
		return "super sad"	
	elif p['mood'] <= -50 and p['anger'] < 20:
		return "extremely sad"	
		
	#Mid anger + low mood
	elif p['mood'] <= -10 and p['mood'] > -20 and p['anger'] >= 20 and p['anger'] < 40:
		return "frustrated"
	elif p['mood'] <= -20 and p['mood'] > -30 and p['anger'] >= 20 and p['anger'] < 40:
		return "pretty frustrated"
	elif p['mood'] <= -30 and p['mood'] > -40 and p['anger'] >= 20 and p['anger'] < 40:
		return "very frustrated"	
	elif p['mood'] <= -40 and p['mood'] > -50 and p['anger'] >= 20 and p['anger'] < 40:
		return "super frustrated"	
	elif p['mood'] <= -50 and p['anger'] >= 20 and p['anger'] < 40:
		return "extremely frustrated"	
				
	#High anger + low mood
	elif p['mood'] <= -10 and p['mood'] > -20 and p['anger'] >= 40:
		return "demoralized"
	elif p['mood'] <= -20 and p['mood'] > -30 and p['anger'] >= 40:
		return "pretty pissed off"
	elif p['mood'] <= -30 and p['mood'] > -40 and p['anger'] >= 40:
		return "very pissed off"	
	elif p['mood'] <= -40 and p['mood'] > -50 and p['anger'] >= 40:
		return "super pissed off"	
	elif p['mood'] <= -50 and p['anger'] >= 40:
		return "going nuclear"			
		
	#Low anger + High mood
	elif p['mood'] > 10 and p['mood'] < 20 and p['anger'] < 20:
		return "calm"
	elif p['mood'] >= 20 and p['mood'] < 30 and p['anger'] < 20:
		return "happy"
	elif p['mood'] >= 30 and p['mood'] < 40 and p['anger'] < 20:
		return "very happy"	
	elif p['mood'] >= 40 and p['mood'] < 50 and p['anger'] < 20:
		return "super happy"	
	elif p['mood'] >= 50 and p['anger'] < 20:
		return "extremely happy"	
		
	#Mid anger + High mood
	elif p['mood'] > 10 and p['mood'] < 20 and p['anger'] >= 20 and p['anger'] < 40:
		return "happy but getting annoyed"
	elif p['mood'] >= 20 and p['mood'] < 30 and p['anger'] >= 20 and p['anger'] < 40:
		return "pretty happy but getting annoyed"
	elif p['mood'] >= 30 and p['mood'] < 40 and p['anger'] >= 20 and p['anger'] < 40:
		return "pretty happy but getting annoyed"	
	elif p['mood'] >= 40 and p['mood'] > 50 and p['anger'] >= 20 and p['anger'] < 40:
		return "very happy but getting annoyed"	
	elif p['mood'] >= 50 and p['anger'] >= 20 and p['anger'] < 40:
		return "complicated"	
				
	#High anger + High mood
	elif p['mood'] > 10 and p['mood'] < 20 and p['anger'] >= 40:
		return "annoyed"
	elif p['mood'] >= 20 and p['mood'] < 30 and p['anger'] >= 40:
		return "annoyed but trying to stay calm"
	elif p['mood'] >= 30 and p['mood'] < 40 and p['anger'] >= 40:
		return "annoyed but managing to stay happy"	
	elif p['mood'] >= 40 and p['mood'] < 50 and p['anger'] >= 40:
		return "super pissed off"	
	elif p['mood'] >= 50 and p['anger'] >= 40:
		return "complicated"			
		
	else:
		print("No value found for this check!")
		print(f"Evaluating {p['mood']}mood & {p['anger']}anger.")
		return "confused"				
		
